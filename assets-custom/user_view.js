"use strict";

// Class definition
function getDataUser(){
    $('#data_user').DataTable( {
        "destroy": true,
        "processing" : true,
        "type": "POST",
        "ajax": 'user/dataUser',
        "columns"       : [
            {"data" : "USERID"},
            {"data" : "NIPP"},
            {"data" : "EMAIL"},
            {"data" : "SIP"},
            {"data" : "nmDok"},
            {"data" : "REKENING"},
            {"data" : "NM_RUANG"},
            {"data" : "btn"},
        ],
        "columnDefs": [
            {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            },
        ],
        "scrollX": true,
        "createdRow"    : function ( row, data, index ) {
            // $('td', row).eq(0).addClass('text-center');
           // $('td', row).eq(2).addClass('text-right');
        }, 
    });
}
var loading = new KTDialog({'type': 'loader', 'placement': 'top center', 'message': 'Loading ...'});
var UserView = function() {
    function getDataDokter(){
        $.ajax({
            type: "GET",
            url: "user/dataDokter", 
            dataType: 'json',
            async : false,

            success: function (data) {
                // console.log(data.data);
                var a = "<option value='0'>Silahkan Pilih</option>";
                jQuery.each(data.data, function(i, val) {
                    a+= "<option value='"+val.kdDok+"'>"+val.nmDok+"</option>";
                })
                $('#list_dokter').append(a);
            },
            error: function (xhr,status,error) {
            }
      });
    }
    function getDataKlinik(){
        $.ajax({
            type: "GET",
            url: "user/dataKlinik", 
            dataType: 'json',
            async : false,

            success: function (data) {
                var a = "<option value='0'>Silahkan Pilih</option>";
                jQuery.each(data.data, function(i, val) {
                    a+= "<option value='"+val.KDKLINIK+"'>"+val.REKENING+"</option>";
                })
                $('#list_klinik').append(a);
            },
            error: function (xhr,status,error) {
            }
      });
    }
    function getDataPerawatan(){
        $.ajax({
            type: "GET",
            url: "user/dataPerawatan", 
            dataType: 'json',
            async : false,

            success: function (data) {
                var a = "<option value='0'>Silahkan Pilih</option>";
                jQuery.each(data.data, function(i, val) {
                    a+= "<option value='"+val.KDMT+"'>"+val.NM_RUANG+"</option>";
                })
                $('#list_perawatan').append(a);
            },
            error: function (xhr,status,error) {
            }
      });
    }
    function setSelect2(){
        $('#list_dokter, #list_perawatan, #list_klinik').select2({ 
            placeholder: "Silahkan Pilih",
            width: '100%',
            dropdownParent: $('#modal-review')
        });
    }
    return {
        // Init demos
        init: function() {
            getDataUser();
            getDataDokter();
            getDataKlinik();
            getDataPerawatan();
            setSelect2()
            
            
            loading.show();

            setTimeout(function() {
                loading.hide();
            }, 3000);
        },
    };
}();

function updasdasdaate(id, type) {
    reset();
    $('.tag_type').text('Edit');
    $('#id_user').val(id);
    $('#modal-review').modal('show');
    // swal.fire('success', "id="+ id + " type=" + type, 'success');
}
$('#data_user').on( 'click', 'tr #btnproses', function () {

    $('#passrow').addClass('passrow');

    var baris = $(this).parents('tr')[0];
    var table = $('#data_user').DataTable();
    var datas = table.row(baris).data();
    $('.tag_type').text('Edit');
    $('#id_user').val(datas['USERID']);
    $('#nipp').val(datas['NIPP']);
    $('#email').val(datas['EMAIL']);
    $('#sip').val(datas['SIP']);
    $('#list_klinik').val(datas['REKENING']);
    $('#list_dokter').val(datas['nmDok']);
    $('#list_perawatan').val(datas['NM_RUANG']);

    $('#modal-review').modal('show');
    
} );
$('#data_user').on( 'click', 'tr #btnbatal', function () {
    var baris = $(this).parents('tr')[0];
    var table = $('#data_user').DataTable();
    var datas = table.row(baris).data();
    // swal.fire('Success', datas['USERID'], 'success')

    swal.fire({
        title: '<span style="font-size: 19px;font-weight: 700;">Menghapus User</span>' ,
        text:'Apakah anda yakin menghapus user ini?',
        type:'question',
        showCancelButton: true,
        confirmButtonText: 'Ya',
        cancelButtonText: 'Tidak',  
    }).then(function(result) {
        if (result.value) {
            deleteUser(datas['USERID']);
        }
    })
})
function deleteUser(id_user){
    $.ajax({
        type: "POST",
        url: "user/deleteUser", 
        data : {
            id_user:id_user
        },
        dataType: 'json',
        success: function (data) {
            console.log(data);
            getDataUser();
        },
        error: function (xhr,status,error) {
        }
    });
}
$('#add_user').click(function(){
    reset();
    $('.tag_type').text('Tambah');
    $('#passrow').removeClass('passrow');
    $('#modal-review').modal('show');
});
function reset(){
    $('input').val('');
    $('#list_dokter').select2("val", " ");
    $('#list_perawatan').select2("val", " ");
    $('#list_klinik').select2("val", " ");
}
$('#action').click(function(){
    loading.show();
    var id_user = $('#id_user').val();
    var nipp = $('#nipp').val();
    var email = $('#email').val();
    var pass = $('#pass').val();
    var pass2 = $('#pass2').val();
    var sip = $('#sip').val();
    var list_dokter = $('#list_dokter').val();
    var list_klinik = $('#list_klinik').val();
    var list_perawatan = $('#list_perawatan').val();
    // console.log(nipp,email,pass,pass2,sip,list_dokter,list_klinik,list_perawatan);
    $.ajax({
        type: "POST",
        url: "user/actionUser", 
        data : {
            id_user:id_user,
            nipp:nipp,
            email:email,
            pass:pass,
            sip:sip,
            list_dokter:list_dokter,
            list_klinik:list_klinik,
            list_perawatan:list_perawatan
        },
        dataType: 'json',
        async : false,

        success: function (data) {
            loading.hide();
            if(id_user !=""){
                swal.fire('Success', 'Berhasil Update', 'success')
            }else{
                swal.fire('Success', 'Berhasil Input', 'success')
            }
            $('#modal-review').modal('hide');
            getDataUser();
        },
        error: function (xhr,status,error) {
        }
    });
});
// Class initialization on page load
jQuery(document).ready(function() {
    UserView.init();
});