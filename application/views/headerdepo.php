<?php 
date_default_timezone_set('Asia/Jakarta'); 
$_SESSION["hrd_startMeasure"] = microtime(TRUE);
ini_set("memory_limit", "512M"); // or you could use 1G
//echo $this->benchmark->memory_usage();
$this->benchmark->mark('code_start');


?>

<!DOCTYPE html>
<html lang="en">

	<!-- begin::Head -->
	<head>

		<!--begin::Base Path (base relative path for assets of this page) -->
		<base href="../">

		<!--end::Base Path -->
		<meta charset="utf-8" />
        <link rel="icon" href="<?php echo base_url();?>assets/img/favicon.ico" type="image/x-icon" />
		<title>PHC FARMASI</title>
		<meta name="description" content="Latest updates and statistic charts">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!--begin::Fonts -->
		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
		<script>
			WebFont.load({
				google: {
					"families": ["Poppins:300,400,500,600,700"]
				},
				active: function() {
					sessionStorage.fonts = true;
				}
			});
		</script>

		<!--end::Fonts -->
<!-- <link href="<?php echo base_url()?>assets/assets_metronic/vendors/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" /> -->

		<!--end::Page Vendors Styles -->

		<!--begin:: Global Mandatory Vendors -->
		<link href="<?php echo base_url()?>assets/assets_metronic/vendors/general/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet" type="text/css" />

		<!--end:: Global Mandatory Vendors -->

		<!--begin:: Global Optional Vendors -->
		<link href="<?php echo base_url()?>assets/assets_metronic/vendors/general/tether/dist/css/tether.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url()?>assets/assets_metronic/vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url()?>assets/assets_metronic/vendors/general/bootstrap-datetime-picker/css/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url()?>assets/assets_metronic/vendors/general/bootstrap-timepicker/css/bootstrap-timepicker.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url()?>assets/assets_metronic/vendors/general/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url()?>assets/assets_metronic/vendors/general/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url()?>assets/assets_metronic/vendors/general/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url()?>assets/assets_metronic/vendors/general/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url()?>assets/assets_metronic/vendors/general/select2/dist/css/select2.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url()?>assets/assets_metronic/vendors/general/ion-rangeslider/css/ion.rangeSlider.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url()?>assets/assets_metronic/vendors/general/nouislider/distribute/nouislider.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url()?>assets/assets_metronic/vendors/general/owl.carousel/dist/assets/owl.carousel.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url()?>assets/assets_metronic/vendors/general/owl.carousel/dist/assets/owl.theme.default.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url()?>assets/assets_metronic/vendors/general/dropzone/dist/dropzone.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url()?>assets/assets_metronic/vendors/general/summernote/dist/summernote.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url()?>assets/assets_metronic/vendors/general/bootstrap-markdown/css/bootstrap-markdown.min.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url()?>assets/assets_metronic/vendors/general/animate.css/animate.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url()?>assets/assets_metronic/vendors/general/toastr/build/toastr.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url()?>assets/assets_metronic/vendors/general/morris.js/morris.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url()?>assets/assets_metronic/vendors/general/sweetalert2/dist/sweetalert2.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url()?>assets/assets_metronic/vendors/general/socicon/css/socicon.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url()?>assets/assets_metronic/vendors/custom/vendors/line-awesome/css/line-awesome.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url()?>assets/assets_metronic/vendors/custom/vendors/flaticon/flaticon.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url()?>assets/assets_metronic/vendors/custom/vendors/flaticon2/flaticon.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url()?>assets/assets_metronic/vendors/general/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css" />

		<!--end:: Global Optional Vendors -->

		<!--begin::Global Theme Styles(used by all pages) -->
		<link href="<?php echo base_url()?>assets/assets_metronic/css/demo9/style.bundle.css" rel="stylesheet" type="text/css" />
        <script src="<?php echo base_url()?>assets/assets_metronic/vendors/general/jquery/dist/jquery.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>assets/assets_metronic/js/demo9/scripts.bundle.js" type="text/javascript"></script>
		<!--end::Global Theme Styles -->

		<!--begin::Layout Skins(used by all pages) -->

		<!--end::Layout Skins -->
		<!-- <link rel="shortcut icon" href="<?php echo base_url()?>assets/assets_metronic/media/logos/favicon.ico" /> -->
	</head>
<!-- begin::Body -->
<body class="kt-page--loading-enabled kt-page--loading kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header--minimize-menu kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-aside--enabled kt-aside--left kt-aside--fixed kt-page--loading">

<!-- begin::Page loader -->

<!-- end::Page Loader -->

<!-- begin:: Page -->
<style>
    .kt-menu__link:hover{
        background:#2860a8;
    }
    .kt-header-menu .kt-menu__nav > .kt-menu__item:hover:not(.kt-menu__item--here):not(.kt-menu__item--active) > .kt-menu__link .kt-menu__link-text, .kt-header-menu .kt-menu__nav > .kt-menu__item.kt-menu__item--hover:not(.kt-menu__item--here):not(.kt-menu__item--active) > .kt-menu__link .kt-menu__link-text {
      color: #ffffff; }
    .kt-header-menu-wrapper .kt-header-menu .kt-menu__nav > .kt-menu__item.kt-menu__item--here > .kt-menu__link, .kt-header-menu-wrapper .kt-header-menu .kt-menu__nav > .kt-menu__item.kt-menu__item--active > .kt-menu__link{
        background:#2860a8;
    }
    .kt-header-menu-wrapper .kt-header-menu .kt-menu__nav > .kt-menu__item.kt-menu__item--here > .kt-menu__link .kt-menu__link-text, .kt-header-menu-wrapper .kt-header-menu .kt-menu__nav > .kt-menu__item.kt-menu__item--active > .kt-menu__link .kt-menu__link-text{
        color: #ffffff;
    }
    .dataTables_filter{
        float:right
    }
    .dataTables_paginate {
        float:right
    }
</style>
<!-- begin:: Header Mobile -->
<div id="kt_header_mobile" class="kt-header-mobile  kt-header-mobile--fixed ">
    <div class="kt-header-mobile__logo">
        <a href="<?php echo base_url()?>index.php/beranda/data">
            <img alt="Logo" src="<?php echo base_url()?>/assets/img/favicon.ico" />
        </a>
    </div>
    <div class="kt-header-mobile__toolbar">
        <!-- <button class="kt-header-mobile__toolbar-toggler kt-header-mobile__toolbar-toggler--left" id="kt_aside_mobile_toggler"><span></span></button> -->
        <button class="kt-header-mobile__toolbar-toggler" id="kt_header_mobile_toggler"><span></span></button>
        <button class="kt-header-mobile__toolbar-topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more-1"></i></button>
    </div>
</div>

<!-- end:: Header Mobile -->
<div class="kt-grid kt-grid--hor kt-grid--root">
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

            <!-- begin:: Header -->
            <div id="kt_header" class="kt-header  kt-header--fixed " data-ktheader-minimize="on">
                <div class="kt-container  kt-container--fluid ">
                    <!-- begin: Header Menu -->
                    <button class="kt-header-menu-wrapper-close" id="kt_header_menu_mobile_close_btn"><i class="la la-close"></i></button>
                    <div class="kt-header-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_header_menu_wrapper">
                        <!-- <button class="kt-aside-toggler kt-aside-toggler--left" id="kt_aside_toggler"><span></span></button> -->
                        <div id="kt_header_menu" class="kt-header-menu kt-header-menu-mobile ">
                            
                        </div>
                    </div>

                    <!-- end: Header Menu -->

                    <!-- begin:: Brand -->
                    <div class="kt-header__brand   kt-grid__item" id="kt_header_brand">
                        <a class="kt-header__brand-logo" href="<?php echo base_url()?>index.php/beranda/data">
                            <img alt="Logo" src="<?php echo base_url()?>/assets/img/favicon.ico" />
                        </a>
                    </div>

                    <!-- end:: Brand -->

                    <!-- begin:: Header Topbar -->
                    <div class="kt-header__topbar kt-grid__item">
                        <div class="kt-header__topbar-item kt-header__topbar-item--user">
                            <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,0px">
                                <span class="kt-header__topbar-welcome kt-visible-desktop">Hi,</span>
                                <span class="kt-header__topbar-username kt-visible-desktop">
                                <?php 
                                // var_dump($_SESSION);die();
                                if (isset($_SESSION["if_ses_username"]))
                                {
                                    echo str_replace('\' ', '\'', 
                                        ucwords(str_replace('\'', '\' ', 
                                            strtolower($_SESSION["if_ses_username"])
                                            )
                                        )
                                    );
                                } 
                                else 
                                {
                                    redirect(base_url().'index.php/login',$data);

                                }
                                ?>
                                </span>
                                <img alt="Pic" src="<?php echo base_url()?>/image/100_7.jpg" />

                                <!--use below badge element instead the user avatar to display username's first letter(remove kt-hidden class to display it) -->
                                <span class="kt-badge kt-badge--username kt-badge--unified-success kt-badge--lg kt-badge--rounded kt-badge--bold kt-hidden">S</span>
                            </div>
                            <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-xl">


                                <!--begin: Navigation -->
                                <div class="kt-notification">
                                    
                                    <div class="kt-notification__custom kt-space-between">
                                    <a  class="btn btn-label btn-label-brand btn-sm btn-bold" href="<?php echo base_url()?>index.php/login/logout">
                                       logout
                                    </a>
                                        <!-- <form action="<?php echo base_url()?>index.php/login/logout" method="get">
                                            <input type="submit" value="LOGOUT" class="btn btn-label btn-label-brand btn-sm btn-bold" name="LOGOUT" id="LOGOUT">
                                        </form> -->
                                        <!-- <a href="#" target="_blank" class="btn btn-label btn-label-brand btn-sm btn-bold">Sign Out</a> -->
                                        <a href="#" target="_blank" class="btn btn-label btn-label-brand btn-sm btn-bold">Change Pasword</a>
                                    </div>
                                </div>

                                <!--end: Navigation -->
                            </div>
                        </div>

                    
                        <!--end: Quick panel toggler -->
                    </div>

                    <!-- end:: Header Topbar -->
                </div>
            </div>
	<!-- end::Head -->