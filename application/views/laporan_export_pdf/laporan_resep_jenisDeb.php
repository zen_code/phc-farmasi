<!DOCTYPE html>
<html>

<head>
  <title>Report Table</title>
  <style type="text/css">
    #outtable {
      /* padding: 0px;
      width: 400px; */
      /* border: 1px solid #e3e3e3; */

      /* padding-top: 30px; */
      font-family: arial;
      border-radius: 3px;
      font-size: 14px;
    }

    @page {
      margin: 10px;
    }

    .short {
      width: 50px;
      background-color: red;
    }

    .normal {
      width: 150px;
    }

    table {
      /* border-collapse: collapse;
      font-family: arial;
      color: #5E5B5C; */

      border-left: 0.01em solid #ccc;
      border-right: 0;
      border-top: 0.01em solid #ccc;
      border-bottom: 0;
      border-collapse: collapse;
    }
    table td, table th {
        border-left: 0;
        border-right: 0.01em solid #ccc;
        border-top: 0;
        border-bottom: 0.01em solid #ccc;
    }

    /* thead th{
      text-align: left;
      padding: 10px;
    }
 
    tbody td{
      border-top: 1px solid #e3e3e3;
      padding: 10px;
    }
 
    tbody tr:nth-child(even){
      background: #F6F5FA;
    }
 
    tbody tr:hover{
      background: #EAE9F5
    } */
  </style>
</head>

<body>
  <!-- <img src="application/modules/Cetak_etiket/views/apel_biru.jpg"> -->

  <head>
    <!-- <style>
      @page { margin: 100px 25px; }
        header { position: fixed; top: -60px; left: 0px; right: 0px; background-color: lightblue; height: 50px; }
        footer { position: fixed; bottom: -60px; left: 0px; right: 0px; background-color: lightblue; height: 50px; }
        p { page-break-after: always; }
        p:last-child { page-break-after: never; }
    </style> -->
    <table style="border:0;border-left:0;border-right:0;">
        <tr>
          <td style="border:0;border-left:0;border-right:0; width:450px;">
            <img style="margin-left:20px" src="assets/img/PHC_Logo_Farmasi.png" width="110px">
          </td>
          <td style="border:0;border-left:0;border-right:0;"></td>
          <td style="border:0;border-left:0;border-right:0;"></td>
          <td style="border:0;border-left:0;border-right:0;"></td>
          <td style="border:0;border-left:0;border-right:0;"></td>
          <td style="border:0;border-left:0;border-right:0;"></td>
          <td style="border:0;border-left:0;border-right:0;"></td>
          <td style="border:0;border-left:0;border-right:0;">
            <h3 style="text-align:center;">
                
                Laporan Resep Tunai/Kredit
            </h3>
          </td>
        </tr>
    </table>
        
      
  </head>
    <div>
      

      <!-- <label style="font-size: 10px">INSTALASI FARMASI-RS.PHC SURABAYA</label>
    <label style="font-size: 10px">INSTALASI FARMASI-RS.PHC SURABAYA</label> -->
    
    
      <div class="form-group" style="margin-bottom:20px;">
      
        <div class="row">
          
          <!-- <label style="font-size: 12px; ">Surabaya, <!?php echo $q->INPUTDATE; ?></label> -->
          <!-- <div style="width: 100%; height: 8px; border-bottom: 1px solid black; text-align: center; padding-left: 90px;">
            <span style="font-size: 12px; background-color: #ffffff; padding: 0px; margin-left: 2px;">
              [No. <!?php echo $q->nomor; ?>][R/<!?php echo $q->NO; ?>]
              
            </span>
          </div> -->
          
          <div id="outtable">
            <?php
              // var_dump($query);
              // die;
              $no=1;
              ?>
            
            <table>

                <thead>
                  <!-- <img style="margin-left:20px" src="assets/img/PHC_Logo_Farmasi.png" width="110px"> -->
                    <tr style="background: #2860a8; color:white;text-align:center">
                        <th width="30" style="width:30px;">NO</th>
                        <th width="80" style="width:80px;">TANGGAL</th>
                        <th width="60" style="width:60px;">RESEP</th>
                        <th width="60" style="width:60;">KITIR</th>
                        <th width="90" style="width:90px;">NO.MEDIK</th>
                        <th width="50" style="width:50px;">IDX</th>
                        <th width="50" style="width:50px;">DEB</th>
                        <th width="60" style="width:60px;">KLINIK</th>
                        <th width="250" style="width:250px;">NAMA</th>
                        <th width="250" style="width:250px;">KELUARGA</th>
                        <th width="75" style="width:75px;">TOTAL</th>
                    </tr>
                </thead>
                
                <tbody>
                <?php foreach ($query as $q) { ?>
                   <!-- <img style="margin-left:20px" src="assets/img/PHC_Logo_Farmasi.png" width="110px"> -->
                    <tr>
                        <td width="30" style="width:30px; text-align:center;"><?php echo $no;?></td>
                        <td width="80" style="width:80px; text-align:center;"><?php echo $q->TANGGAL; ?></td>
                        <td width="60" style="width:60px; text-align:center;"><?php echo $q->NOMOR ?></td>
                        <td width="60" style="width:60px; text-align:center;"><?php echo $q->NOTA; ?></td>
                        <td width="90" style="width:90px; text-align:center;"><?php echo $q->NORM; ?></td>
                        <td width="50" style="width:50px; text-align:center;"><?php echo $q->IDX; ?></td>
                        <td width="50" style="width:50px; text-align:center;"><?php echo $q->KDDEB; ?></td>
                        <td width="60" style="width:60px; text-align:center;"><?php echo $q->KDKLIN; ?></td>
                        <td width="250" style="width:250px; text-align:left;"><?php echo $q->NMPX; ?></td>
                        <td width="250" style="width:250px; text-align:left;"><?php echo $q->NMKK; ?></td>
                        <td width="75" style="width:75px; text-align:right;"><?php echo number_format($q->JUMLAH); ?></td>
                        
                    </tr>
                    
                <?php $no++; } ?>
                
                </tbody>
                
            </table>
          </div>
        </div>
      </div>
    </div>
  
</body>

</html>