<!DOCTYPE html>
<html>

<head>
  <title>Report Table</title>
  <style type="text/css">
   @font-face {
      font-family: "dot_matrix";
      src: url("<?php echo base_url() ?>assets/fonts/1979_dot_matrix.ttf");
    }

    #outtable {
      /* padding: 0px;
      width: 400px; */
      /* border: 1px solid #e3e3e3; */

      /* padding-top: 30px; */
      margin-left: 5px;
      margin-top: 5px;
      font-family: "dot_matrix";
      border-radius: 3px;
      font-size: 9px
    }

    @page {
      margin: 47px;
    }

    .short {
      width: 50px;
      background-color: red;
    }

    .normal {
      width: 150px;
    }

    .table {
      border-collapse: collapse;
      font-family: arial;
      color: #5E5B5C;
    }

    /* thead th{
      text-align: left;
      padding: 10px;
    }
 
    tbody td{
      border-top: 1px solid #e3e3e3;
      padding: 10px;
    }
 
    tbody tr:nth-child(even){
      background: #F6F5FA;
    }
 
    tbody tr:hover{
      background: #EAE9F5
    } */
  </style>
</head>

<body>
  <!-- <img src="application/modules/Cetak_etiket/views/apel_biru.jpg"> -->

  <?php foreach ($query as $q) { ?>
    <div>


      <!-- <label style="font-size: 10px">INSTALASI FARMASI-RS.PHC SURABAYA</label>
    <label style="font-size: 10px">INSTALASI FARMASI-RS.PHC SURABAYA</label> -->

      <div class="form-group" style="border:1px solid; width: 9.5cm; height:4.6cm; margin-bottom:20px;">

        <div class="row">
          <img style="margin-left:20px; margin-top:10px" src="assets/img/PHC_Logo_Farmasi.png" width="110px"><br>
          <label style="font-size: 8.5px; font-family: 'dot_matrix' ; margin-left: 5px; ">Surabaya, <?php echo $q->INPUTDATE; ?></label>

          <div style="width: 100%; height: 6px; border-bottom: 1px solid black; text-align: center; padding-left: 90px;">
            <span style="font-size: 10px; background-color: #ffffff; padding: 0px; margin-left: 2px;">
              [No. <?php echo $q->nomor; ?>][R/<?php echo $q->NO; ?>]
              <!--Padding is optional-->
            </span>
          </div>
          <div id="outtable">
            <?php
              // var_dump($query);
              // die;
              ?>
            <table>
              <tbody>
                <tr>
                  <td>Nama</td>
                  <td>: <?php echo $q->nama; ?> - [RM : <?php echo $q->rm; ?>] [<?php echo $q->IDXPX; ?>]</td>
                </tr>
                <tr>
                  <td>Tgl Lahir</td>
                  <td>: <?php echo $q->TGL_LAHIR; ?></td>
                </tr>
                <tr>
                  <td>Obat</td>
                  <td>: <?php echo $q->nmbrg; ?></td>
                </tr>
                <tr>
                  <td>Aturan</td>
                  <td>: <?php echo $q->URAIAN; ?></td>
                </tr>
                <tr>
                  <td>*</td>
                  <td> sesudah/sebelum/selama [Makan]/[Minum] *</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  <?php } ?>
</body>

</html>