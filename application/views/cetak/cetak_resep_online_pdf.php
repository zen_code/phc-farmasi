<!DOCTYPE html>
<html>

<head>
  <title>Report Table</title>
  <style type="text/css">
    @font-face {
      font-family: "dot_matrix";
      src: url("<?php echo base_url() ?>assets/fonts/1979_dot_matrix.ttf");
    }

    #outtable {
      /* padding: 0px;
      width: 400px; */
      /* border: 1px solid #e3e3e3; */

      /* padding-top: 30px; */
      margin-left: 5px;
      font-family: "dot_matrix";
      border-radius: 3px;
      font-size: 8px
    }

    @page {
      margin: 25px;
      margin-left: 25%;
    }

    .short {
      width: 50px;
      background-color: red;
    }

    .normal {
      width: 150px;
    }

    .table {
      border-collapse: collapse;
      font-family: arial;
      color: #5E5B5C;

    }

    /* thead th{
      text-align: left;
      padding: 10px;
    }
 
    tbody td{
      border-top: 1px solid #e3e3e3;
      padding: 10px;
    }
 
    tbody tr:nth-child(even){
      background: #F6F5FA;
    }
 
    tbody tr:hover{
      background: #EAE9F5
    } */
  </style>
</head>

<body>
  <!-- <img src="application/modules/Cetak_etiket/views/apel_biru.jpg"> -->


  <div>


    <!-- <label style="font-size: 10px">INSTALASI FARMASI-RS.PHC SURABAYA</label>
    <label style="font-size: 10px">INSTALASI FARMASI-RS.PHC SURABAYA</label> -->

    <div class="form-group" style="border:1px solid; width: 9.5cm; height:27.8cm;">

      <div class="row">
        <label style="font-size: 8.5px; font-family: 'dot_matrix' ; margin-left: 5px; ">Surabaya, <?php echo $query[0]->nama; ?></label>

        <div id="outtable">
          <div style="margin-bottom:5px; width: 100%; height: 9px; border-bottom: 1px dashed black; text-align: left; padding-left: 8px;">
          </div>
          <div style="text-align:center">
            <label> Resep ini hanya berlaku di lingkungan RS PHC </label>
          </div>
          <div style="margin-bottom:5px; width: 100%; height: 9px; border-bottom: 1px dashed black; text-align: left; padding-left: 8px;">
          </div>
          <table>
            <tbody>
              <tr>
                <td>Nomor Antrian</td>
                <td>: </td>
                <td style="padding-left:30px">No. Resep</td>
                <td>:</td>
              </tr>
              <tr>
                <td>Nota</td>
                <td>: </td>
              </tr>
              <tr>
                <td>Tgl Lahir</td>
                <td>:</td>
              </tr>
            </tbody>
          </table>
          <div style="margin-bottom:5px; width: 100%; height: 5px; border-bottom: 1px dashed black; text-align: left; padding-left: 10px;">
            <span style="font-size: 8px; background-color: #ffffff; padding: 0px; margin-left: 2px;">
              [ TELAH TERIMA DARI ]
            </span>
          </div>


          <table>
            <tbody>
              <tr>
                <td>Nama</td>
                <td colspan='2'>: </td>
              </tr>
              <tr>
                <td>Keluarga</td>
                <td>: </td>
                <td style="padding-left:50px; font-weight:bold; font-size: 11px;"></td>
              </tr>
              <tr>
                <td>Alamat</td>
                <td colspan='2'>:</td>
              </tr>
              <tr>
                <td>Debitur</td>
                <td colspan='2'>:</td>
              </tr>
              <tr>
                <td>Dinas</td>
                <td colspan='2'>:</td>
              </tr>
            </tbody>
          </table>
          <div style="margin-bottom:5px; width: 100%; height: 8px; border-bottom: 1px dashed black; text-align: center; padding-left: 90px;">
            <span style="font-size: 12px; background-color: #ffffff; padding: 0px; margin-left: 2px;">
            </span>
          </div>
          <table style="width:100%">
            <tbody>
              <tr>
                <td></td>
                <td></td>
                <td> </td>
                <td> </td>
                <td align="right" style="padding-right:10px"></td>
                <!-- <td><?php echo  $q->jumlah; ?></td>
                  <td><?php echo  $q->sub_total; ?></td> -->

              </tr>
              <tr>
                <td></td>
                <td></td>
              </tr>

            </tbody>
          </table>
          <div style="margin-bottom:5px; width: 100%; height: 8px; border-bottom: 1px dashed black; text-align: center; padding-left: 90px;">
            <span style="font-size: 12px; background-color: #ffffff; padding: 0px; margin-left: 2px;">
            </span>
          </div>
          <table style="width:100%">
            <tbody>
              <tr>
                <td>Harga sudah termasuk PPN 10%</td>

                <td>Jumlah </td>
                <td>:</td>
                <td align="right" style="padding-right:10px"></td>
              </tr>
              <tr>
                <td></td>
                <td>Jasa R/ </td>
                <td>:</td>
                <td align="right" style="padding-right:10px"></td>
              </tr>
              <tr>
                <td>Op. Serliana</td>
                <td colspan="3">
                  <div style="margin-bottom:5px; width: 100%; height: 8px; border-bottom: 1px dashed black; text-align: center; padding-left: 90px;">
                    <span style="font-size: 12px; background-color: #ffffff; padding: 0px; margin-left: 2px;">
                    </span>
                  </div>
                </td>
              </tr>
              <tr>
                <td></td>
                <td>Total </td>
                <td>:</td>
                <td align="right" style="padding-right:10px"></td>
              </tr>

            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</body>

</html>