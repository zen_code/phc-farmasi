<!DOCTYPE html>
<html>

<head>
  <title>Report Table</title>
  <style type="text/css">
    @font-face {
      font-family: "dot_matrix";
      src: url("<?php echo base_url() ?>assets/fonts/1979_dot_matrix.ttf");
    }

    #outtable {
      /* padding: 0px;
      width: 400px; */
      /* border: 1px solid #e3e3e3; */

      /* padding-top: 30px; */
      margin-left: 5px;
      font-family: "dot_matrix";
      border-radius: 3px;
      font-size: 8px
    }

    @page {
      margin: 100px;
    }

    .short {
      width: 50px;
      background-color: red;
    }

    .normal {
      width: 150px;
    }

    .table {
      border-collapse: collapse;
      font-family: arial;
      color: #5E5B5C;

    }

    /* thead th{
      text-align: left;
      padding: 10px;
    }
 
    tbody td{
      border-top: 1px solid #e3e3e3;
      padding: 10px;
    }
 
    tbody tr:nth-child(even){
      background: #F6F5FA;
    }
 
    tbody tr:hover{
      background: #EAE9F5
    } */
  </style>
</head>

<body>
  <!-- <img src="application/modules/Cetak_etiket/views/apel_biru.jpg"> -->


  <div>


    <!-- <label style="font-size: 10px">INSTALASI FARMASI-RS.PHC SURABAYA</label>
    <label style="font-size: 10px">INSTALASI FARMASI-RS.PHC SURABAYA</label> -->

    <div class="form-group" style="border:1px solid; width: 9.5cm; height:13.8cm; margin-bottom:20px;">

      <div class="row">

        <div id="outtable">
          <?php
          // var_dump($query);
          // die;
          ?>
          <table style="margin-top:35px">
            <tbody>
              <tr>
                <td colspan='2'><?php echo  $query[0]->valstr1; ?>, <?php echo  $query[0]->valstr2; ?></td>
              </tr>
              <tr>
                <td>NPWP/NPPK : <?php echo  $query[0]->valstr3; ?></td>
                <td align="right"><?php echo  $query[0]->tglshift; ?></td>
              </tr>
            </tbody>
          </table>
          <div style="margin-bottom:5px; width: 100%; height: 9px; border-bottom: 1px dashed black; text-align: left; padding-left: 8px;">
            <table style="width:100%">
              <tbody>
                <tr>
                  <td>
                    <span style="font-size: 8px; background-color: #ffffff; padding: 0px; margin-left: 2px;">
                      [ NOTA INI BERLAKU SEBAGAI KUITANSI ]
                    </span></td>

                  <td align="right" style="padding-right:10px">
                    <span style="font-size: 8px;  background-color: #ffffff; padding: 0px; margin-left: 2px; ">
                      [ <?php if ($query[0]->KDDEB == 99) {
                        echo ('TUNAI');
                      } else if ($query[0]->KDDEB != 99) {
                        echo ('KREDIT');
                      }  ?> ] - <?php echo  $query[0]->KDOT; ?>
                    </span>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <table>
            <tbody>
              <tr>
                <td>Dokter</td>
                <td>: <?php echo substr($query[0]->NMDOK, 0, 25); ?> </td>
                <td style="padding-left:30px">No. Resep</td>
                <td>: <?php echo  $query[0]->nomor; ?></td>
              </tr>
              <tr>
                <td>Klinik</td>
                <td>: <?php echo  $query[0]->NMKLIN; ?></td>
                <td style="padding-left:30px">No. Nota</td>
                <td>: <?php echo  $query[0]->nota; ?></td>
              </tr>
              <tr>
                <td>Tgl Shift</td>
                <td>: <?php echo  $query[0]->tglshift; ?></td>
              </tr>
            </tbody>
          </table>
          <div style="margin-bottom:5px; width: 100%; height: 5px; border-bottom: 1px dashed black; text-align: left; padding-left: 10px;">
            <span style="font-size: 8px; background-color: #ffffff; padding: 0px; margin-left: 2px;">
              [ TELAH TERIMA DARI ]
            </span>
          </div>
          <?php $born = new DateTime($query[0]->TGL_LAHIR);
          $today = new DateTime();
          $age   = $today->diff($born);
          ?>

          <table>
            <tbody>
              <tr>
                <td>Nama</td>
                <td colspan='2'>: <?php echo $query[0]->nama; ?> (<?php echo $query[0]->SEX; ?>/ <?php echo $age->y ?> tahun) - <?php echo  $query[0]->rm; ?> [<?php echo  $query[0]->IDXPX; ?>]</td>
              </tr>
              <tr>
                <td>Keluarga</td>
                <td>: <?php echo  $query[0]->NMKK; ?> </td>
                <td style="padding-left:50px; font-weight:bold; font-size: 11px;">[<?php echo  $query[0]->ANTRIAN; ?>]</td>
              </tr>
              <tr>
                <td>Alamat</td>
                <td colspan='2'>: <?php echo  $query[0]->ALMTPX; ?></td>
              </tr>
              <tr>
                <td>Debitur</td>
                <td colspan='2'>: <?php echo  $query[0]->nmdeb; ?> [<?php if ($query[0]->STPAS == 1) {
                                                                      echo ('TUNAI');
                                                                    } else if ($query[0]->STPAS == 2) {
                                                                      echo ('KREDIT');
                                                                    }  ?> : <?php echo  $query[0]->KDOT; ?>]</td>
              </tr>
              <tr>
                <td>Dinas</td>
                <td colspan='2'>: <?php echo  $query[0]->nmdin; ?></td>
              </tr>
            </tbody>
          </table>
          <div style="margin-bottom:5px; width: 100%; height: 8px; border-bottom: 1px dashed black; text-align: center; padding-left: 90px;">
            <span style="font-size: 12px; background-color: #ffffff; padding: 0px; margin-left: 2px;">
            </span>
          </div>
          <table style="width:100%">
            <tbody>
              <?php $subtotal1 = 0 ?>
              <?php $jasa_r = 0 ?>
              <?php foreach ($query as $q) { ?>
                <tr>
                  <td><?php echo  $q->NO; ?>.<?php echo  $q->ID; ?></td>
                  <td><?php echo  $q->nmbrg; ?></td>
                  <td><?php echo  $q->HJUAL; ?> </td>
                  <td><?php echo  $q->jumlah; ?> </td>
                  <td align="right" style="padding-right:10px"><?php echo  $q->sub_total; ?> </td>
                  <!-- <td><?php echo  $q->jumlah; ?></td>
                  <td><?php echo  $q->sub_total; ?></td> -->

                </tr>
                <tr>
                  <td></td>
                  <td><?php echo  $q->SIGNA; ?></td>
                </tr>
                <?php $subtotal1 = $q->sub_total + $subtotal1 ?>
                <?php $jasa_r = $q->JASA + $jasa_r ?>
              <?php } ?>
            </tbody>
          </table>
          <div style="margin-bottom:5px; width: 100%; height: 8px; border-bottom: 1px dashed black; text-align: center; padding-left: 90px;">
            <span style="font-size: 12px; background-color: #ffffff; padding: 0px; margin-left: 2px;">
            </span>
          </div>
          <table style="width:100%">
            <tbody>
              <tr>
                <td>Harga sudah termasuk PPN 10%</td>

                <td>Jumlah </td>
                <td>:</td>
                <td align="right" style="padding-right:10px"><?php echo $subtotal1; ?></td>
              </tr>
              <tr>
                <td></td>
                <td>Jasa R/ </td>
                <td>:</td>
                <td align="right" style="padding-right:10px"><?php echo  $jasa_r; ?></td>
              </tr>
              <tr>
                <td>Op. Serliana [ <?php echo  $query[0]->LAYANAN; ?>]</td>
                <td colspan="3">
                  <div style="margin-bottom:5px; width: 100%; height: 8px; border-bottom: 1px dashed black; text-align: center; padding-left: 90px;">
                    <span style="font-size: 12px; background-color: #ffffff; padding: 0px; margin-left: 2px;">
                    </span>
                  </div>
                </td>
              </tr>
              <tr>
                <td><?php echo  $query[0]->TGL; ?> <?php echo  $query[0]->JAM; ?></td>
                <td>Total </td>
                <td>:</td>
                <td align="right" style="padding-right:10px"><?php echo ((int) $jasa_r + (int) $subtotal1) ?> </td>
              </tr>

            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</body>

</html>