<?php
defined('BASEPATH') or exit('No direct script access allowed');

?>
<style>
    .is_head {
        background: #2860a8;
        color: white;
    }
</style>

<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
        <input type="hidden" value="<?php echo base_url(); ?>" id="url">
        <!-- begin:: Subheader -->
        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        Cetak Ulang / Pembatalan Excess </h3>
                </div>
                <!-- kt-subheader__toolbar -->

            </div>
        </div>

        <!-- end:: Subheader -->

        <!-- begin:: Content -->
        <div class="kt-container  kt-grid__item kt-grid__item--fluid">

            <!--Begin::Dashboard 1-->

            <!--Begin::Row-->
            <div class="row">

                <div class="col-xl-12">
                    <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile ">
                        <!-- <div class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                Farmasi Klinis
                                </h3>
                            </div>
                        </div> -->
                        <div class="kt-portlet__body kt-portlet__body--fit">
                            <!--begin: Datatable -->
                            <!-- <div class="kt-portlet__body">
                                <button class="btn btn-info col-md-2" type="button" id="search_pasien" data-toggle="kt-tooltip" data-placement="top"><i class="fa fa-search"></i> Search Px</button>
                            </div> -->
                            <!--begin: Datatable -->
                            <div class="kt-portlet__body">
                                <button class="btn btn-info col-md-2" type="button" id="search_excess" data-toggle="kt-tooltip" data-placement="top"><i class="fa fa-search"></i> Search Excess</button>
                            </div>
                            <!-- <div class="row">
                                <div class="col-md-12">
                                    <div class="kt-portlet">
                                        <div class="kt-portlet__head">
                                            <div class="kt-portlet__head-label">
                                                <h3 class="kt-portlet__head-title">
                                                    Pencarian
                                                </h3>
                                            </div>
                                        </div>
                                        <div class="kt-portlet__body">
                                            <div class="row">
                                                <div class="form-group col-md-5">
                                                    <label>No. Kwitansi</label>
                                                    <input autofocus placeholder="tekan ''ENTER'' untuk mencari data" type="text" class="form-control searchPasien " value="" id="noKwitansiSearch" labelx="No. Kwitansi">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> -->

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="kt-portlet">
                                        <div class="kt-portlet__head">
                                            <div class="kt-portlet__head-label">
                                                <h3 class="kt-portlet__head-title">
                                                    Biodata Pasien
                                                </h3>
                                            </div>
                                        </div>
                                        <div class="kt-portlet__body">
                                            <div class="row">
                                                <div class="form-group col-md-6">
                                                    <label>Nota</label>
                                                    <input type="text" class="form-control " value="" disabled id="notaBio">
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label>Kwitansi</label>
                                                    <input type="text" class="form-control" value="" disabled id="kwitansiBio">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-md-12">
                                                    <label>Nama</label>
                                                    <input type="text" class="form-control " value="" disabled id="namaBio">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-md-6">
                                                    <label>Alamat</label>
                                                    <!-- <input type="text" class="form-control " value="" disabled id="alamatBio"> -->
                                                    <textarea class="form-control col-md-11" value="" disabled id="alamatbio" cols="35" rows="5" style="left: 10px; height :68px;"></textarea>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label>Kota</label>
                                                    <input type="text" class="form-control" value="" disabled id="kotaBio">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="kt-portlet">
                                        <div class="kt-portlet__head">
                                            <div class="kt-portlet__head-label">
                                                <h3 class="kt-portlet__head-title">
                                                    Debitur Pasien
                                                </h3>
                                            </div>
                                        </div>
                                        <div class="kt-portlet__body">
                                            <div class="row">
                                                <div class="form-group col-md-4">
                                                    <label>Jenis Pasien</label>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <input type="text" class="form-control" value="" disabled id="jenisPasienDeb">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-8">
                                                    <label>RM / Index</label>
                                                    <div class="row">
                                                        <div class="col-md-8">
                                                            <input type="text" class="form-control" value="" disabled id="rmDeb">
                                                        </div>
                                                        <div class="col-md-4">
                                                            <input type="text" class="form-control" value="" disabled id="indexDeb">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <!-- <div class="form-group col-md-6">
                                                    <label>RM / Index</label>
                                                    <div class="row">
                                                        <div class="col-md-8">
                                                            <input type="text" class="form-control" value="" disabled id="rmDeb">
                                                        </div>
                                                        <div class="col-md-4">
                                                            <input type="text" class="form-control" value="" disabled id="indexDeb">
                                                        </div>
                                                    </div>
                                                </div> -->
                                                <div class="form-group col-md-12">
                                                    <label>No. Peserta</label>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <input type="text" class="form-control" value="" disabled id="noPesertaDeb">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-md-6">
                                                    <label>Debitur</label>
                                                    <input type="text" class="form-control" value="" disabled id="noDebiturDeb">
                                                    <!-- <div class="row">
                                                        <div class="col-md-6">
                                                            <input type="text" class="form-control" value="" disabled id="noDebiturDeb">
                                                        </div> -->
                                                    <!-- <div class="col-md-8">
                                                            <input type="text" class="form-control" value="" disabled id="namaDebiturDeb">
                                                        </div> -->
                                                    <!-- </div> -->
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label>Dinas</label>
                                                    <input type="text" class="form-control" value="" disabled id="noDinasDeb">
                                                </div>
                                            </div>
                                            <!-- <div class="row">

                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control" value="" disabled id="noDinasDeb">
                                                    </div> -->
                                                    <!-- <div class="col-md-8">
                                                            <input type="text" class="form-control" value="" disabled id="namaDinasDeb">
                                                        </div> -->
                                                <!-- </div>
                                            </div> -->
                                            <!-- <div class="form-group col-md-6">
                                                    <label>Kamar</label>
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <input type="text" class="form-control" value="" disabled id="kdkmr"> 
                                                        </div>
                                                        <div class="col-md-8">
                                                            <input type="text" class="form-control" value="" disabled id="nmkmr"> 
                                                         </div>
                                                    </div>
                                                </div> -->
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="kt-portlet">
                                    <div class="kt-portlet__head">
                                        <div class="kt-portlet__head-label">
                                            <h3 class="kt-portlet__head-title">
                                                Info Klinik / RP
                                            </h3>
                                        </div>
                                    </div>
                                    <div class="kt-portlet__body">
                                        <div class="row">
                                            <div class="form-group col-md-3">
                                                <label>Klinik</label>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control inputan" value="" disabled id="kdklin">
                                                    </div>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control inputan" value="" disabled id="nmklin">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label>User</label>
                                                <div class="row">
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control inputan" value="" disabled id="user">
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class='kt-checkbox' style='top:-4px'>
                                                            <input class='checkbox_drug' type='checkbox'>
                                                            <span></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label>Dokter</label>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control inputan" value="" disabled id="kddok">
                                                    </div>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control inputan" value="" disabled id="nmdok">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group col-md-2">
                                                <label>Jenis Resep</label>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <input type="text" class="form-control inputan" value="" disabled id="kdresep">
                                                    </div>
                                                    <div class="col-md-6">
                                                        <input type="text" class="form-control inputan" value="" disabled id="nmresep">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group col-md-1">
                                                <label>Iter</label>
                                                <input type="text" class="form-control inputan" value="" disabled id="iter">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Detail Data
                                </h3>
                            </div>
                        </div>

                        <div class="kt-portlet__body">
                            <!-- <ul class="nav nav-tabs  nav-tabs-line nav-tabs-line-2x nav-tabs-line-info" role="tablist"> -->
                            <!-- <li class="nav-item">
                                        <a class="nav-link active" data-toggle="tab" href="#tab1" role="tab">Daftar Pemberian Obat</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#tab2" role="tab">Jenis Permasalahan terkait obat (DRP)</a>
                                    </li> -->
                            <!-- <li class="nav-item">
                                        <a class="nav-link active" data-toggle="tab" href="#tab3" role="tab">Detail Data</a>
                                    </li> -->
                            <!-- <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#tab4" role="tab">Monitoring Efek Samping</a>
                                    </li> -->
                            <!-- </ul> -->
                            <div class="tab-content">
                                <div class="tab-pane" id="tab1" role="tabpanel">
                                    <div style="overflow-x:auto;">
                                        <table id="table-list_obat" class="table table-striped- table-hover table-checkable" style="height:500px;overflow-y: scroll; display: block;">
                                            <thead>
                                                <tr style="background: #2860a8; color:white; text-align:center">
                                                    <th>NO.</th>
                                                    <th>KODE</th>
                                                    <th>BARANG</th>
                                                    <th>KANDUNGAN OBAT</th>
                                                    <th>SIGNA</th>
                                                    <th>DINAMYC DATE</th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td colspan="99" style="text-align:center">NO DATA</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab2" role="tabpanel">
                                    <div class="row" style="margin-bottom:20px">

                                        <div class="col-md-4">
                                            <label>Tanggal</label>
                                            <div class="input-group date">
                                                <input type="text" class="form-control form-control-sm kt-input datepicker" placeholder="From" id="tgl_cek_drp" />
                                                <div class="input-group-append">
                                                    <span class="input-group-text"><i class="la la-calendar-o glyphicon-th"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2" style="margin-top: 30px;">
                                            <label class='kt-checkbox' style='top:-4px'>
                                                <input type='checkbox' id='is_check' value="0">
                                                <span></span>
                                            </label>
                                            <label>Sudah di Cek</label>
                                        </div>
                                        <input type="hidden" id="id_trans">
                                    </div>
                                    <div class="row">
                                        <div class="col-md-8">

                                            <table class="table table-striped- table-checkable" id="table-drp" style="height:500px;overflow-y: scroll;    display: block;">
                                                <tbody>

                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label> Subjective </label>
                                                <textarea class="form-control soap" rows="3" id="soap_s"></textarea>
                                            </div>
                                            <div class="form-group">
                                                <label> Objective </label>
                                                <textarea class="form-control soap" rows="3" id="soap_o"></textarea>
                                            </div>
                                            <div class="form-group">
                                                <label> Assesment </label>
                                                <textarea class="form-control soap" rows="3" id="soap_a"></textarea>
                                            </div>
                                            <div class="form-group">
                                                <label> Plan </label>
                                                <textarea class="form-control soap" rows="3" id="soap_p"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div style="text-align: center;">
                                        <button class="btn btn-success2" id="simpan" type="button"><i class="fa fa-check"></i> SIMPAN</button>
                                    </div>
                                </div>
                                <div class="tab-pane active" id="tab3" role="tabpanel">
                                    <!-- <button class="btn btn-info col-md-2" style="margin-bottom:10px" id="tambah-rekonsiliasi"> add </button> -->
                                    <div style="overflow-x:auto;">
                                        <table id="table-rekonsiliasi" class="table table-striped- table-hover table-checkable table-bordered">
                                            <thead>
                                                <tr style="background: #2860a8; color:white;width:100%; text-align:center">
                                                    <th style="vertical-align:middle">Nota</th>
                                                    <th width="10%">Nomor</th>
                                                    <th width="20%">Debitur</th>
                                                    <th width="20%">Klinik</th>
                                                    <th width="20%">Biaya</th>
                                                    <th width="20%">Ditanggung Asuransi</th>
                                                    <th width="20%">Excess</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr class="no-data">
                                                    <td colspan="99" style="text-align:center;padding:10px">NO DATA</td>
                                                </tr>
                                            </tbody>
                                        </table>

                                        <div class="form-group m-form__group row kt-space-between" style="margin-bottom: 0px;border-right-width: 10px;padding-right: 9px;padding-left: 10px;">
                                            &nbsp;
                                            <div style="float:right" class="col-md-3">
                                                <div class="form-group m-form__group row">
                                                    <label class="col-md-4 col-form-label">
                                                        Total Excess :
                                                    </label>
                                                    <div class="col-md-8">
                                                        <input name="" disabled style="text-align:right" type="text" class="form-control m-input numbers" id="subtotal" value="0">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row" style="xfloat:right">
                                            <div class="form-group col-md-12">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <!-- kosong -->
                                                    </div>
                                                    <div class="col-md-6 text-right">
                                                        <button class="btn btn-success2" id="cetak_ulang" type="button"><i class="fa fa-check"></i> Cetak Ulang</button>
                                                        <button class="btn btn-danger2" id="batal_excess" type="button"><i class="fa fa-trash"></i> Pembatalan Excess</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div class="tab-pane " id="tab4" role="tabpanel">
                                    <div class="row">
                                        <div class="form-group col-md-4">
                                            <div class="col-md-12">
                                                <label>Tanggal Cek</label>
                                                <input type="text" class="form-control datepicker" value="" id="tgl_cek_eso" style="width: 100%;">
                                            </div>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label>Penyakit Utama</label>
                                            <textarea class="form-control" rows="3" id="penyakit_utama"></textarea>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label>Penyakit/Kondisi yang menyertainya</label>
                                            <select id="penyakit_kondisi">
                                                <option value="0">Pilih</option>
                                                <option value="Gangguan Ginjal">Gangguan Ginjal</option>
                                                <option value="Gangguan Hati">Gangguan Hati</option>
                                                <option value="Alergi">Alergi</option>
                                                <option value="Faktor Industri, Pertanian, Kimia<">Faktor Industri, Pertanian, Kimia</option>
                                                <option value="Merokok">Merokok</option>
                                                <option value="Minum Alkohol">Minum Alkohol</option>
                                                <option value="Lain-lain    ">Lain-lain</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="kt-portlet">
                                            <div class="kt-portlet__head">
                                                <div class="kt-portlet__head-label">
                                                    <h3 class="kt-portlet__head-title">
                                                        Efek Samping Obat atau Alergi
                                                    </h3>
                                                </div>
                                            </div>
                                            <div class="kt-portlet__body">
                                                <div class="row">
                                                    <div class="form-group col-md-4">
                                                        <label>Bentuk Manifestasi ESO yang terjadi</label>
                                                        <textarea class="form-control" rows="3" id="manifestasi_eso"></textarea>
                                                    </div>
                                                    <div class="form-group col-md-4">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <label>Tanggal/Saat Mulai Terjadi</label>
                                                                <input type="text" class="form-control datepicker" value="" id="tgl_mulai" style="width: 100%;">
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <label>Tanggal/Saat Kesudahan ESO</label>
                                                                <input type="text" class="form-control datepicker" value="" id="tgl_selesai" style="width: 100%;">
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <label>Kesudahan ESO</label>
                                                                <input type="text" class="form-control" value="" id="kesudahan_eso">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-4">
                                                        <label>Riwayat ESO yang dialami</label>
                                                        <textarea class="form-control" rows="3" id="riwayat_eso"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="kt-portlet">
                                            <div class="kt-portlet__head">
                                                <div class="kt-portlet__head-label">
                                                    <h3 class="kt-portlet__head-title">
                                                        Obat
                                                    </h3>
                                                </div>
                                            </div>
                                            <div class="kt-portlet__body">
                                                <div class="row">
                                                    <div class="form-group col-md-12">
                                                        <button class="btn btn-info col-md-2" style="margin-bottom:10px" id="tambah-eso"> add </button>
                                                    </div>
                                                    <div class="form-group col-md-12">
                                                        <div style="overflow-x:auto;">
                                                            <table id="table-eso" class="table table-striped- table-hover table-checkable">
                                                                <thead>
                                                                    <tr style="background: #2860a8; color:white;width:100%; text-align:center">
                                                                        <th rowspan="2" style="vertical-align:middle">NO</th>
                                                                        <th rowspan="2" style="vertical-align:middle;witdh:25%">Nama Dagang</th>
                                                                        <th rowspan="2" style="vertical-align:middle">Bentuk Sediaan</th>
                                                                        <th rowspan="2" style="vertical-align:middle">Obat yang dicurigai </th>
                                                                        <th colspan="4">Pemberian</th>
                                                                        <th rowspan="2" style="vertical-align:middle">Indikasi Penggunaan</th>
                                                                        <th rowspan="2" style="vertical-align:middle"></th>

                                                                    </tr>
                                                                    <tr style="background: #2860a8; color:white;width:100%; text-align:center">
                                                                        <th>Cara</th>
                                                                        <th>Dosis</th>
                                                                        <th>Tanggal Mulai</th>
                                                                        <th>Tanggal Akhir</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr class="no-data">
                                                                        <td colspan="99" style="text-align:center;padding:10px">NO DATA</td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="kt-portlet">
                                            <div class="kt-portlet__head">
                                                <div class="kt-portlet__head-label">
                                                    <h3 class="kt-portlet__head-title">
                                                        Keterangan
                                                    </h3>
                                                </div>
                                            </div>
                                            <div class="kt-portlet__body">
                                                <div class="row">
                                                    <div class="form-group col-md-4">
                                                        <label>Keterangan Tambahan</label>
                                                        <textarea class="form-control" rows="3" id="keterangan"></textarea>
                                                    </div>
                                                    <div class="form-group col-md-4">
                                                        <label>Tanggal Pemeriksaan Laboratorium</label>
                                                        <input type="text" class="form-control datepicker" value="" id="tanggal_lab" style="width: 100%;">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div style="text-align: center;">
                                        <button class="btn btn-success2" id="simpan_eso" type="button"><i class="fa fa-check"></i> SIMPAN</button>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <!--End::Row-->



        <!--End::Dashboard 1-->
    </div>

    <!-- end:: Content -->
</div>
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="modal-review">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content ">
            <div class="modal-header">
                <h5 class="modal-title"><span class="tag_type"></span> Excess</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table id="table-pasien" class="table table-striped- table-hover table-checkable">
                    <thead>
                        <tr style="background: #2860a8; color:white;text-align:center">
                            <th style=" color:white;">NO PESERTA</th>
                            <th style=" color:white;">KWITANSI</th>
                            <th style=" color:white;">DEBITUR</th>
                            <th style=" color:white;">SHIFT</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th style=" color:white;">NO PESERTA</th>
                            <th style=" color:white;">KWITANSI</th>
                            <th style=" color:white;">DEBITUR</th>
                            <th style=" color:white;">SHIFT</th>
                            <th></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- javascript this page -->
<script src="<?php echo base_url() ?>assets/assets_metronic/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/assets_metronic/vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>application/modules/Cetak_excess/views/cetak_excess_view.js" type="text/javascript"></script>
<!-- <script src="<?php echo base_url() ?>assets/assets_metronic/js/demo9/pages/dashboard.js" type="text/javascript"></script> -->

<script>

</script>