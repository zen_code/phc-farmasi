"use strict";

// Class definition

var CetakExcess = function () {

    return {
        // Init demos
        init: function () {
            setNamaBarang();
            // dataDrp();
            // dataPasien();
            $('.datepicker ').datepicker({
                autoclose: true,
                format: 'dd/mm/yyyy',
                changeMonth: true,
                changeYear: true,
            });
            $.fn.dataTable.Api.register('column().title()', function () {
                return $(this.header()).text().trim();
            });
            $('#penyakit_kondisi').select2({
                placeholder: "Silahkan Pilih",
                width: '100%',
            });
            $('#kt_datepicker_1').datepicker({
                rtl: KTUtil.isRTL(),
                todayHighlight: true,
                orientation: "bottom right",
            });


            $('#table-pasien tfoot th').each(function () {
                var title = $(this).text();
                switch (title) {
                    case 'NOREG':
                    case 'RM':
                    case 'NAMA':
                    case 'KAMAR':
                    case 'RUANGAN':
                        $(this).html('<input  placeholder="Search ' + title + '" type="text" class="form-control form-control-sm form-filter kt-input"/>');
                        break;
                    case 'TGL_MRS':
                        $(this).html(`<div class="input-group date">
                                        <input type="text" class="form-control form-control-sm kt-input" placeholder="From" id="kt_datepicker_1" />
                                        <div class="input-group-append">
                                            <span class="input-group-text"><i class="la la-calendar-o glyphicon-th"></i></span>
                                        </div>
                                    </div>` );
                        break;
                }
                $('#kt_datepicker_1').datepicker({ autoclose: true, format: 'dd/mm/yyyy' });
                // input = $(`<input type="text" class="form-control form-control-sm form-filter kt-input" data-col-index="` + column.index() + `"/>`);
                //                 
                // $(this).html( '<input type="text" class="form-control form-control-sm form-filter kt-input"/>' );
            });

            // var $tfoot = $('#table-pasien tfoot tr').detach();
            // // $('#table-pasien thead ').append('<tr></tr>');
            // $('#table-pasien thead').append($tfoot);

            // // DataTable
            // var table = $('#table-pasien').DataTable({
            //     responsive: true,
            //     dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
            //     lengthMenu: [5, 10, 25, 50],
            //     pageLength: 5,
            //     language: {
            //         'lengthMenu': 'Display _MENU_',
            //     },
            //     searchDelay: 500,
            //     ordering: true,
            //     processing: true,
            //     serverSide: false,
            //     ajax: {
            //         url: 'farmasi_klinis/dataPasien',
            //         type: 'POST',
            //         data: {
            //             columnsDef: ['NOREG', 'RM', 'TGL_MRS', 'NAMA', 'KAMAR','RUANGAN','btn'],
            //         },
            //     },
            //     columns       : [
            //         {data : "NOREG"},
            //         {data : "RM"},
            //         {data : "TGL_MRS"},
            //         {data : "NAMA"},
            //         {data : "KAMAR"},
            //         {data : "RUANGAN"},
            //         {data : "btn"}
            //     ],
            //     "drawCallback": function( settings ) {
            //         $('[data-toggle="kt-tooltip"]').tooltip();
            //     },

            // });

            // // Apply the search
            // table.columns().every( function () {
            //     var that = this;
            //     $( 'input', this.footer() ).on( 'keyup change clear', function () {
            //         if ( that.search() !== this.value ) {
            //             that
            //                 .search( this.value )
            //                 .draw();
            //         }
            //     });
            // });


            getDataExcess("");
        },
    };
}();

// var tablepasien = $('#table-pasien');

var setNamaBarang = function () {
    $('.namaBarangS').select2({

        // dataType: 'json',
        // beforeSend: function () {
        //     KTApp.blockPage({
        //         overlayColor: '#000000',
        //         type: 'v2',
        //         state: 'primary',
        //         message: 'Processing...'
        //     });
        // }, error: function (request, status, error) {
        //     swal.fire('warning', request.responseText, 'warning');
        // }

        width: "100%",
        ajax: {
            type: "GET",
            // url: 'telaah_resep/tabelVerifikasi?tgl=' + date,
            url: 'telaah_resep/tabelVerifikasi',
            delay: 250,
            data: function (params) {
                KTApp.blockPage({
                    overlayColor: '#000000',
                    type: 'v2',
                    state: 'primary',
                    message: 'Processing...'
                });
                // formKapal.loading({ message: "Memuat data..." });
                // return {
                //     search: params.term,
                //     //kodeCabang: '',
                // };
            },
            // beforeSend: function () {

            // },
            processResults: function (data) {
                // formKapal.loading('stop');
                KTApp.unblockPage();
                return {
                    results: $.map(data, function (obj) {
                        return {
                            id: 'idwkwk',
                            text: 'namawkwkw',

                            // id: obj.kdKapal,
                            // text: obj.nmKapal,

                            // valGt: obj.grt,
                            // valDwt: obj.dwt,
                            // valLoa: obj.loa,
                            // valBendera: obj.bendera,
                        };
                    })
                };
            },
            cache: true
        },
        // templateSelection: formatRepoSelection,

        minimumInputLength: 1,
        placeholder: "Masukkan nama atau kode obat",
        language: {
            inputTooShort: function () {
                return 'Ketikkan 1 huruf atau lebih untuk memulai pencarian';
            }
        }
    });
}

function getDataExcess(text) {
    $('#table-pasien').dataTable().fnClearTable();
    $('#table-pasien').dataTable().fnDestroy();
    var table = $('#table-pasien').DataTable({
        responsive: true,
        dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
        lengthMenu: [5, 10, 25, 50],
        pageLength: 5,
        language: {
            'lengthMenu': 'Display _MENU_',
        },
        searchDelay: 500,
        ordering: true,
        processing: true,
        serverSide: false,
        ajax: {
            url: 'cetak_excess/dataExcess',
            type: 'POST',
            data: {
                // columnsDef: ['ANTRIAN','KARCIS', 'RM', 'TGL', 'NAMA', 'DEBITUR','KLINIK','DOKTER','btn'],
                text: text
            },
        },

        columns: [
            { data: "NO_PESERTA" },
            { data: "KWITANSI" },
            { data: "DEBITUR" },
            { data: "SHIFT" },
            { data: "btn" }
        ],
        "drawCallback": function (settings) {
            $('[data-toggle="kt-tooltip"]').tooltip();
        },

    });
}

var clickedLine = 0;
var dataobat = 0;
function detailExcess(id_kwitansi_excess) {
    $('#modal-review').modal('hide');
    loading();
    $.ajax({
        type: "GET",
        url: "cetak_excess/detailExcess",
        data: { id_kwitansi_excess: id_kwitansi_excess },
        dataType: 'json',
        success: function (data) {
            $('#table-rekonsiliasi > tbody').empty();
            var total = 0;
            data.message.detail.forEach(function(val, i){
                addRow();
                var $tr = $('#table-rekonsiliasi > tbody').find('tr').last();
                $tr.find('#id_kwitansi_excess').val(val.ID_KWITANSI_EXCESS);
                $tr.find('#nota').text(val.NOTA);
                $tr.find('#nomor').text(val.NOMOR);
                $tr.find('#debitur').text(val.NMDEBT);
                $tr.find('#klinik').text('');
                $tr.find('#biaya').text(val.BIAYA_NOTA);
                $tr.find('#ditanggungAsuransi').text('');
                $tr.find('#excess').text(val.EXCESS);
                total += parseFloat(val.BIAYA_NOTA);
            });
            $('#subtotal').val(total);
            setInputMask();
            KTApp.unblockPage();
        },
        error: function (xhr, status, error) {
            
            KTApp.unblockPage();
        },
        done: function(){
            
            KTApp.unblockPage();
        }
    });
}
// function dataDrp(){
//     $.ajax({
//         type: "GET",
//         url: "farmasi_klinis/dataDrp", 
//         dataType: 'json',
//         success: function (data) {
//             $('#table-drp tbody').append(data);
//         },
//         error: function (xhr,status,error) {

//         }
//     });
// }

// function searchPasien(){
//     console.log('cari pasien ....')
// }


$(".searchPasien").on("keypress", function (event) {
    // console.log('cari pasien bro ... = '+$(this).val())
    if ((event.which == 13)) {
        // let data = {
        //     param1: "",
        //     param2: ""
        // }
        if ($(this).val() == '') {
            swal.fire('warning', '<b>"' + $(this).attr('labelx') + '"</b> tidak boleh kosong', 'warning');
        } else {
            var today = new Date();
            var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
            $.ajax({
                type: "GET",
                url: 'telaah_resep/tabelVerifikasi?tgl=' + date,
                dataType: 'json',
                beforeSend: function () {
                    KTApp.blockPage({
                        overlayColor: '#000000',
                        type: 'v2',
                        state: 'primary',
                        message: 'Processing...'
                    });
                }, error: function (request, status, error) {
                    swal.fire('warning', request.responseText, 'warning');
                }
            })
                .done(function (data) {
                    KTApp.unblockPage()
                    console.log(data)

                    // biodata section
                    $('#nomorBio').val('nomorBio');
                    $('#notaBio').val('notaBio');
                    $('#namaBio').val('namaBio');
                    $('#alamatBio').val('alamatBio');
                    $('#kotaBio').val('kotaBio');

                    // debitur section
                    $('#jenisPasienDeb').val('jenisPasienDeb');
                    $('#rmDeb').val('rmDeb');
                    $('#indexDeb').val('indexDeb');
                    $('#noPesertaDeb').val('noPesertaDeb');
                    $('#noDebiturDeb').val('noDebiturDeb');
                    $('#namaDebiturDeb').val('namaDebiturDeb');
                    $('#noDinasDeb').val('noDinasDeb');
                    $('#namaDinasDeb').val('namaDinasDeb');

                })
                .fail(function (jqXHR, textStatus, errorThrown) {
                    KTApp.unblockPage();
                    swal.fire('warning', 'Pasien dengan <b>"' + $(this).attr('labelx') + '"</b>' + $(this).val() + ' tidak ditemukan', 'warning');
                    console.log('iki error e : ' + errorThrown);
                });
        }
    }
})


$('body').on('change', '.checkbox_drug', function () {
    clickedLine = $(this).closest('tr').data('index');
    var $tr = $('#table-drp > tbody').find('tr[data-index="' + clickedLine + '"]');
    var catatan = $tr.find("#catatan");
    if ($(this).is(':checked')) {
        $(this).attr('value', 1);
        catatan.prop('disabled', false);
        $('#is_check').prop('checked', true);
    } else {
        $(this).attr('value', 0);
        catatan.prop('disabled', true);
        catatan.val('');
    }
});
$('#is_check').on('change', function () {
    if ($(this).is(':checked')) {
        $(this).attr('value', 1);
    } else {
        $(this).attr('value', 0);
    }
});


$('#search_excess').click(function () {
    $('#modal-review').modal('show')
});
$('#tambah-rekonsiliasi').click(function () {
    addRow();
    // alert('nyamm')
})

function setInputMask(){
    $(".numbers").inputmask(
        {   
            'alias': 'decimal', 
            'groupSeparator': ',', 
            'autoGroup': true,
            'removeMaskOnSubmit': true,
            'rightAlign': true,
            // autoUnmask: true,
            // groupSeparator: '.',
        }
    );
}

var i = 1;
function addRow() {
    var tr_nodata = $('#table-rekonsiliasi tbody tr.no-data');

    if (tr_nodata.length > 0) {
        tr_nodata.remove()
    }
    var a = "";
    a = '<tr data-index = "' + i + '">' +
        '<td><input type="hidden" class="form-control" id="id_kwitansi_excess"><span id="nota"></span></td>' +
        '<td><span id="nomor"></span></td>' +
        // '<td><select class="form-control namaBarangS" id="debitur"></select></td>' +
        '<td><span id="debitur"/></span></td>' +
        '<td><span id="klinik"></span></td>' +
        '<td class="text-right"><span id="biaya" class="numbers"></span></td>' +
        '<td><span id="ditanggungAsuransi"></span></td>' +
        '<td class="text-right"><span id="excess" class="numbers"></span></td>' +
        '</tr>';
    $('#table-rekonsiliasi tbody').append(a);

    i++;
    // renumber();
    input_date();
    input_number();
}
function renumber() {
    var index = 1;
    $('#table-rekonsiliasi tbody tr').each(function () {
        $(this).find('td').eq(0).html(index);//.find('.num_text')
        index++;
    });
}
$('#table-rekonsiliasi').on('click', 'tr #btn-del', function () {
    var baris = $(this).parents('tr')[0];
    baris.remove();
    renumber();

    var tr = $('#table-rekonsiliasi tbody tr');
    if (tr.length < 1) {
        var a = "";
        a += '<tr class="no-data">' +
            '<td colspan="99" style="text-align:center;padding:10px">No Data</td>' +
            '</tr>';
        $('#table-rekonsiliasi tbody').append(a);
    }
});
$('#tambah-eso').click(function () {
    if ($('#noreg').val() != "") {
        addRowEso();
    } else {
        swal.fire('warning', 'Pilih Pasien terlebih dahulu', 'warning');
    }

})
var i_eso = 1;
var listobat = ""
function addRowEso() {
    console.log('nyamm');

    var tr_nodata = $('#table-eso tbody tr.no-data');

    if (tr_nodata.length > 0) {
        tr_nodata.remove()
    }
    listobat = buildListObat(dataobat, i);
    var a = "";
    a = '<tr data-index = "' + i_eso + '">' +
        '<td style="text-align: center;vertical-align: middle;"></td>' +
        '<td>' + listobat + '</td>' +
        '<td><input type="text" class="form-control" id="satuan" disabled></td>' +
        '<td><input type="text" class="form-control" id="cara"></td>' +
        '<td><input type="text" class="form-control" id="dosis"></td>' +
        '<td><input type="text" class="form-control" id="txt_penggunaan"></td>' +
        '<td><button class="btn btn-danger" id="btn-del"><i class="fa fa-times"   data-toggle="kt-tooltip" data-placement="top" title="Delete"></i> </button></td>' +
        '</tr>';
    $('#table-eso tbody').append(a);
    i_eso++;
    renumberEso();
    input_date();
    $('.list_obat').select2();
}
function buildListObat(data, index) {
    var list = "<select class='form-control list_obat' id='list-obat-" + index + "'><option> Silahkan Pilih</option>" + data + "</select>";

    return list;
}
var x = "";
$('#table-eso').on('change', 'tr .list_obat', function () {
    x = $(this).closest('tr').data('index');
    var $tr = $('#table-eso > tbody').find('tr[data-index="' + x + '"]');
    var satuan = $tr.find(".list_obat option:selected").attr('satuan');
    $tr.find("#satuan").val(satuan);
})
function input_type(type, param) {
    var res = "";
    if (type == "date") {
        res = '<div class="input-group date">' +
            '<input type="text" class="form-control datepickers ' + param + '" readonly placeholder="Pilih Tanggal" />' +
            '</div>';
    } else if (type == "checkbox") {
        res = "<label class='kt-checkbox' style='top:-4px' >" +
            "<input type='checkbox' id='" + param + "'> " +
            "<span></span>" +
            "</label>"
    }

    return res;
}
function renumberEso() {
    var index = 1;
    $('#table-eso tbody tr').each(function () {
        $(this).find('td').eq(0).html(index);//.find('.num_text')
        index++;
    });
}
$('#table-eso').on('click', 'tr #btn-del', function () {
    var baris = $(this).parents('tr')[0];
    baris.remove();
    renumberEso();

    var tr = $('#table-eso tbody tr');
    if (tr.length < 1) {
        var a = "";
        a += '<tr class="no-data">' +
            '<td colspan="99" style="text-align:center;padding:10px">No Data</td>' +
            '</tr>';
        $('#table-eso tbody').append(a);
    }
});

function dataDrp(noreg, tgl) {
    $('#table-drp tbody').html("");
    var data = {
        noreg: noreg,
        tgl: tgl,
    }
    $.ajax({
        type: "POST",
        url: "farmasi_klinis/dataDrp",
        dataType: 'json',
        data: data,
        success: function (data) {
            $('#id_trans').val(data.id_farklin);
            $('#table-drp tbody').append(data.body);
            if (data.id_farklin != "") {
                $('#is_check').prop('checked', true);
            } else {
                $('#is_check').prop('checked', false);
            }
            KTApp.unblockPage();
        },
        error: function (xhr, status, error) {

        }
    });
}
$('#tgl_cek_drp').change(function () {
    var noreg = $('#noreg').val();
    if (noreg != "") {
        loading()
        var date = $('#tgl_cek_drp').val();
        dataDrp(noreg, date)
    } else {
        swal.fire('warning', 'Pilih Pasien terlebih dahulu', 'warning');
        $('#tgl_cek_drp').val("");
    }
})
function dataSoap(noreg, norm) {
    $('.soap ').val("");
    var data = {
        noreg: noreg,
        norm: norm,
    }
    $.ajax({
        type: "POST",
        url: "farmasi_klinis/dataSoap",
        dataType: 'json',
        data: data,
        success: function (data) {
            if (data != null) {
                data = data.data;
                var arrSoap = data.split("<br>")
                var Text = arrSoap[1].split(" : ");
                $('#soap_s').val(Text[1])

                Text = arrSoap[2].split(" : ");
                $('#soap_o').val(Text[1])

                Text = arrSoap[3].split(" : ");
                $('#soap_a').val(Text[1])

                Text = arrSoap[4].split(" : ");
                $('#soap_p').val(Text[1])
            }
        },
        error: function (xhr, status, error) {

        }
    });
}
function input_date() {
    $('.datepickers').datepicker({
        rtl: KTUtil.isRTL(),
        todayHighlight: true,
        orientation: "bottom left",
        format: 'dd/mm/yyyy',
        autoclose: true
    });
}
function input_number() {
    $(".numbers").on("keypress keyup blur", function (event) {
        $(this).val($(this).val().replace(/[^\d].+/, ""));
        if ((event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });
}
function loading() {
    KTApp.blockPage({
        overlayColor: '#000000',
        type: 'v2',
        state: 'primary',
        message: 'Processing...'
    });
}
$('#simpan').click(function () {

    // if ($('#noreg').val() == "" || $('#noreg').val() == null) {
    //     swal.fire('Peringatan', 'Belum memilih pasien', 'warning');
    // } else {
    //     loading()
    //     var drp = [];
    //     $('#table-drp .is_child').each(function () {
    //         drp.push({
    //             id: $(this).find('.checkbox_drug').attr('id'),
    //             value: $(this).find('.checkbox_drug').attr('value'),
    //             catatan: $(this).find('#catatan').val(),
    //         })
    //     });

    //     //soap
    //     var textsoap = "<p><br>S : " + $('#soap_s').val() + "<br>O : " + $('#soap_o').val() + "<br>A : " + $('#soap_a').val() + "<br>P : " + $('#soap_p').val() + "<br></p>";

    //     var data = {
    //         noreg: $('#noreg').val(),
    //         drp: drp,
    //         id_trans: $('#id_trans').val(),
    //         tgl: $('#tgl_cek_drp').val(),

    //         //erm
    //         norm: $('#rm').val(),
    //         hasil_asesmen: textsoap,
    //     }
    //     // console.log(data);
    //     $.ajax({
    //         type: "POST",
    //         url: "farmasi_klinis/action",
    //         dataType: 'json',
    //         data: data,
    //         success: function (data) {
    //             // $('#table-drp tbody').html("");
    //             // $('input').val("");
    //             KTApp.unblockPage();
    //             swal.fire(data.data.status, data.data.message, data.data.status);
    //         },
    //         error: function (xhr, status, error) {

    //         }
    //     });
    // }

})


$('#cetak_ulang').click(function () {
    if ($('#noreg').val() == "" || $('#noreg').val() == null) {
        swal.fire('Peringatan', 'Belum memilih pasien', 'warning');
    } else {
        var allow = true;
        var msg = "";
        loading();
        // console.log("asdadas");
        var data_rekon = [];
        $('#table-rekonsiliasi tbody tr').each(function (i) {
            var jumlah = $(this).find('#jumlah').val();
            var jml_obat_kembali = $(this).find('#jml_obat_kembali').val();
            // allow = false;
            if (parseInt(jml_obat_kembali) > parseInt(jumlah)) {
                allow = false;
                msg += "<br>Baris " + (i + 1) + " , Jumlah yang dikembalikan lebih besar dari jumlah asal";
            }
            data_rekon.push({
                no: (i + 1),
                nm_obat: $(this).find('#nm_obat').val(),
                jumlah: $(this).find('#jumlah').val(),
                tgl_kadaluwarsa: $(this).find('.tgl_kadaluwarsa').val(),
                rute: $(this).find('#rute').val(),
                dosis: $(this).find('#dosis').val(),
                frekuensi: $(this).find('#frekuensi').val(),
                lanjut: $(this).find('#lanjut_y').is(":checked") == true ? 1 : 0,
                nm_obat_kembali: $(this).find('#nm_obat_kembali').val(),
                jml_obat_kembali: $(this).find('#jml_obat_kembali').val(),
            })
        });
        var data = {
            noreg: $('#noreg').val(),
            norm: $('#rm').val(),
            data_rekon: data_rekon,
        }

        if (allow) {
            $.ajax({
                type: "POST",
                url: "farmasi_klinis/saveRekonsiliasi",
                dataType: 'json',
                data: data,
                success: function (data) {
                    KTApp.unblockPage();
                    swal.fire(data.data.status, data.data.massage, data.data.status);
                },
                error: function (xhr, status, error) {

                }
            });
        } else {
            KTApp.unblockPage();
            swal.fire('Error', msg, 'error');
        }
        // console.log(data_rekon);

    }
})

$('#batal_excess').click(function () {
    var id_kwitansi_excess = $('#table-rekonsiliasi tbody tr:eq(0) #id_kwitansi_excess').val();
    if(!id_kwitansi_excess){
        return false;
    }
    swal.fire({
        title: "Pembatalan Excess",
        text: "Apakah anda yakin?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Batal",
        cancelButtonText: "Cancel",
        input: 'textarea',
        inputPlaceholder: 'apa alasan dibatalkan?'
    },).then(function(result){
        if (result.dismiss) {
            return false;
        }
        loading();
        $.ajax({
            type: "POST",
            url: "cetak_excess/action",
            dataType: 'json',
            data: {
                data: {
                    ID_KWITANSI_EXCESS: $('#table-rekonsiliasi tbody tr:eq(0) #id_kwitansi_excess').val(),
                    notes: result.value
                }
            },
            success: function (data) {
                console.log(data);
                swal.fire(data.status, data.message, data.status);
            },
            error: function (response) {
                KTApp.unblockPage();
                swal('', response.responseJSON.message, 'error');
            },
            complete: function () {
                KTApp.unblockPage();
            }
        });
    })

    // if ($('#noreg').val() == "" || $('#noreg').val() == null) {
    //     swal.fire('Peringatan', 'Belum memilih pasien', 'warning');
    // } else {
    //     var allow = true;
    //     var msg = "";
    //     loading();
    //     // console.log("asdadas");
    //     var data_rekon = [];
    //     $('#table-rekonsiliasi tbody tr').each(function (i) {
    //         var jumlah = $(this).find('#jumlah').val();
    //         var jml_obat_kembali = $(this).find('#jml_obat_kembali').val();
    //         // allow = false;
    //         if (parseInt(jml_obat_kembali) > parseInt(jumlah)) {
    //             allow = false;
    //             msg += "<br>Baris " + (i + 1) + " , Jumlah yang dikembalikan lebih besar dari jumlah asal";
    //         }
    //         data_rekon.push({
    //             no: (i + 1),
    //             nm_obat: $(this).find('#nm_obat').val(),
    //             jumlah: $(this).find('#jumlah').val(),
    //             tgl_kadaluwarsa: $(this).find('.tgl_kadaluwarsa').val(),
    //             rute: $(this).find('#rute').val(),
    //             dosis: $(this).find('#dosis').val(),
    //             frekuensi: $(this).find('#frekuensi').val(),
    //             lanjut: $(this).find('#lanjut_y').is(":checked") == true ? 1 : 0,
    //             nm_obat_kembali: $(this).find('#nm_obat_kembali').val(),
    //             jml_obat_kembali: $(this).find('#jml_obat_kembali').val(),
    //         })
    //     });
    //     var data = {
    //         noreg: $('#noreg').val(),
    //         norm: $('#rm').val(),
    //         data_rekon: data_rekon,
    //     }

    //     if (allow) {
    //         $.ajax({
    //             type: "POST",
    //             url: "farmasi_klinis/saveRekonsiliasi",
    //             dataType: 'json',
    //             data: data,
    //             success: function (data) {
    //                 KTApp.unblockPage();
    //                 swal.fire(data.data.status, data.data.massage, data.data.status);
    //             },
    //             error: function (xhr, status, error) {

    //             }
    //         });
    //     } else {
    //         KTApp.unblockPage();
    //         swal.fire('Error', msg, 'error');
    //     }
    //     // console.log(data_rekon);

    // }
})

$('#table-rekonsiliasi').on('change', 'tr #lanjut_y ', function () {
    clickedLine = $(this).closest('tr').data('index');
    var $tr = $('#table-rekonsiliasi > tbody').find('tr[data-index="' + clickedLine + '"]');
    var lanjut_y = $tr.find('#lanjut_y').is(":checked")
    var lanjut_t = $tr.find('#lanjut_t').is(":checked")
    if (lanjut_y && lanjut_t) {
        $tr.find('#lanjut_t').prop('checked', false)
    }
});
$('#table-rekonsiliasi').on('change', 'tr #lanjut_t ', function () {
    clickedLine = $(this).closest('tr').data('index');
    var $tr = $('#table-rekonsiliasi > tbody').find('tr[data-index="' + clickedLine + '"]');
    var lanjut_y = $tr.find('#lanjut_y').is(":checked")
    var lanjut_t = $tr.find('#lanjut_t').is(":checked")
    if (lanjut_y && lanjut_t) {
        $tr.find('#lanjut_y').prop('checked', false)
    }
});
function dataRekon(noreg, norm) {
    // $('.soap ').val("");
    $('#table-rekonsiliasi > tbody').html("");
    var data = {
        noreg: noreg,
        norm: norm,
    }
    $.ajax({
        type: "POST",
        url: "farmasi_klinis/dataRekon",
        dataType: 'json',
        data: data,
        success: function (data) {
            if (data != null) {
                var detail = data.data.detail;
                if (detail != null) {
                    $.each(detail, function (i, val) {
                        addRow();
                        var $tr = $('#table-rekonsiliasi > tbody').find('tr').last();
                        $tr.find('#nm_obat').val(val.nm_obat);
                        $tr.find('#jumlah').val(val.jumlah);
                        $tr.find('.tgl_kadaluwarsa').val(val.tgl_kadaluwarsa);
                        $tr.find('#rute').val(val.rute);
                        $tr.find('#dosis').val(val.dosis);
                        $tr.find('#frekuensi').val(val.frekuensi);
                        if (val.lanjut == 1) {
                            $tr.find('#lanjut_y').prop("checked", true);
                        } else {
                            $tr.find('#lanjut_t').prop("checked", true);
                        }
                        $tr.find('#nm_obat_kembali').val(val.nm_obat_kembali);
                        $tr.find('#jml_obat_kembali').val(val.jml_obat_kembali);
                    });
                } else {
                    var a = "";
                    a += '<tr class="no-data"><td colspan="99" style="text-align:center;padding:10px">NO DATA</td></tr>';
                    $('#table-rekonsiliasi  tbody').append(a);
                }
            }

        },
        error: function (xhr, status, error) {

        }
    });
}
$('#simpan_eso').click(function () {
    if ($('#noreg').val() == "" || $('#noreg').val() == null) {
        swal.fire('Peringatan', 'Belum memilih pasien', 'warning');
    } else {
        var data_eso = [];
        $('#table-eso tbody tr').each(function (i) {

            data_eso.push({
                no_urut: (i + 1),
                kdbrg: $(this).find('.list_obat option:selected').val(),
                obat_dicurigai: $(this).find('#obat_dicurigai').val(),
                cara: $(this).find('#cara').val(),
                dosis: $(this).find('#dosis').val(),
                tgl_mulai: $(this).find('.tgl_mulai').val(),
                tgl_selesai: $(this).find('.tgl_selesai').val(),
                txt_penggunaan: $(this).find('#txt_penggunaan').val(),
            })
        });
        var data = {
            noreg: $('#noreg').val(),
            norm: $('#rm').val(),
            tgl_cek: $('#tgl_cek_eso').val(),
            penyakit_utama: $('#penyakit_utama').val(),
            penyakit_kondisi: $('#penyakit_kondisi').val(),
            manifestasi_eso: $('#manifestasi_eso').val(),
            tgl_mulai: $('#tgl_mulai').val(),
            tgl_selesai: $('#tgl_selesai').val(),
            kesudahan_eso: $('#kesudahan_eso').val(),
            riwayat_eso: $('#riwayat_eso').val(),
            keterangan: $('#keterangan').val(),
            tanggal_lab: $('#tanggal_lab').val(),
            data_eso: data_eso,
        }
        $.ajax({
            type: "POST",
            url: "farmasi_klinis/saveEso",
            dataType: 'json',
            data: data,
            success: function (data) {
                KTApp.unblockPage();
                swal.fire(data.data.status, data.data.massage, data.data.status);
            },
            error: function (xhr, status, error) {

            }
        });
    }
});
function dataEso(noreg, norm, tgl) {
    // $('.soap ').val("");
    $('#table-rekonsiliasi > tbody').html("");
    var data = {
        noreg: noreg,
        norm: norm,
        tgl: tgl,
    }
    $.ajax({
        type: "POST",
        url: "farmasi_klinis/dataEso",
        dataType: 'json',
        data: data,
        success: function (data) {
            if (data != null) {
                console.log(data);
                var header = data.data.header;
                var detail = data.data.detail;
                if (header !== null) {
                    if (detail != null) {

                    }
                }
            }


            // if(detail != null){
            //     $.each( detail, function( i, val ){
            //         addRow();
            //         var $tr = $('#table-rekonsiliasi > tbody').find('tr').last();
            //         $tr.find('#nm_obat').val(val.nm_obat);
            //         $tr.find('#jumlah').val(val.jumlah);
            //         $tr.find('.tgl_kadaluwarsa').val(val.tgl_kadaluwarsa);
            //         $tr.find('#rute').val(val.rute);
            //         $tr.find('#dosis').val(val.dosis);
            //         $tr.find('#frekuensi').val(val.frekuensi);
            //         if(val.lanjut == 1){
            //             $tr.find('#lanjut_y').prop("checked", true);
            //         }else{
            //             $tr.find('#lanjut_t').prop("checked", true);
            //         }
            //         $tr.find('#nm_obat_kembali').val(val.nm_obat_kembali);
            //         $tr.find('#jml_obat_kembali').val(val.jml_obat_kembali);
            //     });
            // }else{
            //     var a = "";
            //     a += '<tr class="no-data"><td colspan="99" style="text-align:center;padding:10px">NO DATA</td></tr>';
            //     $('#table-rekonsiliasi  tbody').append(a);
            // }
        },
        error: function (xhr, status, error) {

        }
    });
}
// Class initialization on page load
jQuery(document).ready(function () {
    CetakExcess.init();
});