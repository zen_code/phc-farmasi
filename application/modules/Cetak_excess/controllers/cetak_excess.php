<?php
defined('BASEPATH') or exit('No direct script access allowed');

class cetak_excess extends CI_Controller
{
	public $urlws = null;
	public function __construct()
	{
		parent::__construct();
		$this->load->model('cetak_excess_model');
		// print_r($xx); exit();
		$this->load->library('session');
		$this->urlws = $this->globals->ws_phc();
	}
	private function _render($view, $data = array())
	{
		$this->load->view('header', $data);
		// $this->load->view('body_header');
		// $this->load->view('sidebar');
		$this->load->view($view, $data);
		$this->load->view('footer');
	}

	public function index()
	{
		//load library
		// var_dump($this->session->all_userdata());die();
		$uri = &load_class('URI', 'core');
		redirect(base_url() . 'index.php/cetak_excess/data', $data);
	}

	public function data()
	{
		if($_SESSION["if_ses_depo"] == null){
			redirect(base_url().'depo');
		}else{
			if (in_array(6, $_SESSION['if_ses_menu'])) {
				$data['parent_active'] = 6;
				$data['child_active'] = 10;
				$this->_render('cetak_excess_view', $data);
			} else {
				redirect(base_url() . 'beranda/data');
			}
		}
	}

	public function dataExcess()
	{
		$text = $this->input->post('text');
		$sql = $this->cetak_excess_model->dataExcess($text);
		$data 			= array();
		foreach ($sql as $item) {
			$btn 		= '<button name="btnproses" id="btnproses" onclick="detailExcess(' . "'" . $item->ID_KWITANSI_EXCESS . "'" . ')" type="button" class="btn btn-success btn-sm" data-toggle="kt-tooltip" data-placement="top" title="Choose"> <i class="fa fa-check"></i> </button>';
			$data[] 	= array(
				"NO_PESERTA" 	=> $item->NO_PESERTA,
				"KWITANSI" => $item->ID_KWITANSI_EXCESS,
				"DEBITUR"		=> $item->NMDEBT,
				"SHIFT"		=> $item->SHIFT,
				"btn"		=> $btn
			);
		}
		$obj = array("data" => $data);
		echo json_encode($obj);
	}

	public function detailExcess()
	{
		$id_kwitansi_excess = $this->input->get('id_kwitansi_excess');
		$sql = $this->cetak_excess_model->detailExcess($id_kwitansi_excess);

		echo json_encode($sql);
	}

	public function action(){
		$data = $this->input->post('data');
		$res = $this->cetak_excess_model->saveBatal($data);

		echo json_encode($res);
	}
}
