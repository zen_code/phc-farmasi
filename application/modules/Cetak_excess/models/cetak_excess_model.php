<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

// class farmasi_klinis_model extends CI_Model{
class cetak_excess_model extends CI_Model
{
  public function __construct()
  {
    // Call the CI_Model constructor
    parent::__construct();
    $this->load->database();
  }

  public function dataExcess($text)
  {

    $where = "";
    if ($text != "") {
      $where = " and (a.NO_PESERTA like '%" . $text . "%' or c.NMDEBT like '%" . $text . "%') ";
    }

    $sql = $this->db->query("select distinct a.*, c.NMDEBT from RJ_KWITANSI_EXCESS a
                join RJ_EXCESS_BIAYA b on a.ID_KWITANSI_EXCESS = b.ID_KWITANSI_EXCESS
                join RIRJ_MDEBITUR c on c.KDDEBT = a.KDDEBT
                where b.JURNAL is not null " . $where);
    //a.STSCETAK = 0 and a.AMBIL = 0 and a.TGLAMBIL is null//where convert(varchar, a.TGL, 103) = '".$dates."'
    // var_dump($sql->result());die();
    return $sql->result();
  }

  public function detailExcess($id_kwitansi_excess)
  {
    $res = [];
    $data = [];
    try {
      $sql = $this->db->query("select a.NOTA, a.NOMOR, c.NMDEBT, a.BIAYA_NOTA, a.EXCESS, a.ID_KWITANSI_EXCESS from RJ_EXCESS_BIAYA a
      left join RJ_KWITANSI_EXCESS b on b.id_kwitansi_excess = a.ID_KWITANSI_EXCESS
      left join RIRJ_MDEBITUR c on c.KDDEBT = b.KDDEBT
            where a.ID_KWITANSI_EXCESS = '" . $id_kwitansi_excess . "'");

      $data['detail'] = $sql->result();

      // $sql = $this->db->query("select * from  RJ_EXCESS_BIAYA b where b.ID_KWITANSI_EXCESS ='" . $id_kwitansi_excess . "'");
      // $data['detail'] = $sql->result();

      $res['status'] = 'success';
      $res['message'] = $data;
    } catch (Exception $e) {
      $res['status'] = 'error';
      $res['message'] = $e;
    }

    return $res;
  }

  public function saveBatal($data)
  {
        $res = [];
        $set = '';
        if(!$data['ID_KWITANSI_EXCESS']){
          return false;
        }
        
        $set .=  ' USRBTL = \''. $_SESSION["if_ses_username"] .'\'';
        $set .=  ', KETBTL = \''. $data['notes'] .'\'';
        $set .=  ', STBTL = 1';
        $set .=  ', TGLBTL = getDate()';

        $this->db->trans_start();
        $this->db->query("update RJ_KWITANSI_EXCESS set ".$set." where ID_KWITANSI_EXCESS = '".$data['ID_KWITANSI_EXCESS']."'");
        $this->db->query("update RJ_EXCESS_BIAYA set ID_KWITANSI_EXCESS = null where ID_KWITANSI_EXCESS = '".$data['ID_KWITANSI_EXCESS']."'");
        $this->db->trans_complete();
        $res['status'] = 'Berhasil';
        $res['message'] = $data['ID_KWITANSI_EXCESS'] . ' telah dibatalkan.';

        return $res;
   }
}
