<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Info_harga extends CI_Controller {
	public $urlws = null;
	public function __construct() {
        parent::__construct();
		$this->load->model('info_harga_model');
		$this->load->library('session');
		$this->urlws = $this->globals->ws_phc();

    }
	private function _render($view,$data = array())
	{
		$this->load->view('header', $data);
	    $this->load->view($view,$data);
	    $this->load->view('footer');
	}

	public function index()
	{
		//load library
		// var_dump($this->session->all_userdata());die();
		$uri =& load_class('URI', 'core');
	  	redirect(base_url().'index.php/cetak_resep/data',$data);
	}

	public function data()
	{
		// var_dump($_SESSION["ses_userid"]);die();
		if($_SESSION["if_ses_depo"] == null){
			redirect(base_url().'depo');
		}else{
			if(in_array(6,$_SESSION['if_ses_menu'])){
				$data['parent_active'] = 6 ;
				$data['child_active'] = 42 ;
				$this->_render('info_harga_view', $data);
			}else{
				redirect(base_url().'beranda/data');}
		}
	}
	
	public function dataObat()
	{
		// $authorization = 'Authorization:'.$_SESSION["ses_token_phc"];
		// $url = $this->urlws."Farmasi/FarmasiKlinis/dataPasien";
		// $ch = curl_init();
		// curl_setopt($ch, CURLOPT_HTTPHEADER, array($authorization ));
		// curl_setopt($ch, CURLOPT_URL, $url); 
		// curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		// $output = curl_exec($ch); 
		// curl_close($ch);    
		// $cek_output = json_decode($output);
		$kdObat = $this->input->get('kdObat');
		$nmObat = $this->input->get('nmObat');
		$query = $this->info_harga_model->list_obat($kdObat,$nmObat);
		// var_dump($query->result());die();

		// if($cek_output->status == true){
		$data 			= array();
		foreach($query->result() as $item){
			$btn 		='<button name="btnproses" id="btnproses"type="button" class="btn btn-success btn-sm" data-toggle="kt-tooltip" data-placement="top" title="Choose"> <i class="fa fa-check"></i> </button>';
            
			$data[] 	= array(
				"KDBRG" 	=> $item->KDBRG,
				"NMBRG" 	=> $item->NMBRG,
				"SATUAN" 	=> $item->SATUAN ,
				"JENIS" 	=> $item->NMJENIS ,
				"KEMASAN" 	=> $item->KEMASAN ,
				"ISI" 	=> $item->ISI ,
				"btn"		=> $btn
			);
		}
		$obj = array("data"=> $data);
		echo json_encode($obj);
		// }else{
		// 	$error = array(
		// 		'data' => [],
		// 		'error' => $cek_output->message
		// 	);
			
		// 	echo json_encode($error);
		// }
		
	}
	public function getAllInfo(){
		$res = [];
		$kdBrg = $this->input->post('kdBrg');
		$res['info_stok'] = $this->info_harga_model->info_stok($kdBrg);
		$res['info_stok_mitu'] = $this->info_harga_model->info_stok_mitu($kdBrg);
		$res['info_hjual'] = $this->info_harga_model->info_hjual($kdBrg);
		// var_dump($res);die();
		echo json_encode($res);
	}
}
