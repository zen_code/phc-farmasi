"use strict";

// Class definition

var InfoHarga = function() {
    return {
        // Init demos
        init: function() {
            // loading()
        },
    };
}();


function loading(){
    KTApp.blockPage({
        overlayColor: '#000000',
        type: 'v2',
        state: 'primary',
        message: 'Processing...'
    });
}
$('#kdbrg').on('keydown', function(evt){
    
    if (evt.key === 'Enter') {
        var kdbrg = $('#kdbrg').val();
        listObat(kdbrg,"");
    }
});
$('#nmbrg').on('keydown', function(evt){
    
    if (evt.key === 'Enter') {
        var nmbrg = $('#nmbrg').val();
        evt.preventDefault(); 
        if(nmbrg.length >2 ){
            listObat("",nmbrg);
        }else{
            Swal.fire('', 'MInimal input 3 Huruf', 'error');
        }
        
    }
});
function listObat(kdbrg, nmbrg){
    loading();
    $('#table-obat_anmaag').DataTable( {
        "destroy": true,
        "processing" : true,
        "type": "POST",
        "ajax": 'info_harga/dataObat?kdObat='+kdbrg+'&nmObat='+nmbrg,
        "columns"       : [
            {"data" : "KDBRG"},
            {"data" : "NMBRG"},
            {"data" : "SATUAN"},
            {"data" : "JENIS"},
            {"data" : "KEMASAN"},
            {"data" : "ISI"},
            {"data" : "btn"},
        ],
        
        "createdRow"    : function ( row, data, index ) {
            // $('td', row).eq(0).addClass('text-center');
           // $('td', row).eq(2).addClass('text-right');
        }, 
        "drawCallback": function( settings ) {
            $('[data-toggle="kt-tooltip"]').tooltip();
        },
        "initComplete" : function(){
            KTApp.unblockPage();
            $('#modal-review').modal('show');
        }
    });
}

// var table = $('#table-obat_anmaag').DataTable();
var table ="";
 
$('#table-obat_anmaag').on( 'click', 'tr #btnproses', function () {
    var baris = $(this).parents('tr')[0];
    table = $('#table-obat_anmaag').DataTable();
    var datas = table.row(baris).data();

    $('#kdbrg').val(datas['KDBRG']);
    $('#nmbrg').val(datas['NMBRG']);
    $('#satuan').val(datas['SATUAN']);
    $('#jenis').val(datas['JENIS']);
    $('#kemasan').val(datas['KEMASAN']);
    $('#isi').val(datas['ISI']);

    $('#modal-review').modal('hide');
    getAllInfo(datas['KDBRG'])
} );
function getAllInfo(kdbrg){
    
    $.ajax({
        type: "POST",
        url: "info_harga/getAllInfo", 
        dataType: 'json',
        data: {kdBrg : kdbrg},
        success: function (data) {
            console.log(data);
            var a = "";
            $.each( data.info_stok, function( i, val ){
                a += "<tr>"+
                "<td>"+val.LAYANAN+"</td>"+
                "<td>"+val.KODE_MUTASI+"</td>"+
                "<td>"+val.BRGAW+"</td>"+
                "<td>-</td>"+
                "<td>-</td>"+
                "<td>-</td>"+
                "</tr>"
            });
            $('#tbl_stok tbody').append(a);
            a = "";
            $.each( data.info_stok_mitu, function( i, val ){
                if(i == 0){
                    a += "<tr style='background: cornflowerblue;color:white'>"+
                    "<td colspan='99'>"+val.KDBRG + " - " + val.NMBRG +"</td>"+
                    "</tr>"
                }else{
                    var sblm = data.info_stok_mitu[i-1];
                    if(val.KDBRG != sblm.KDBRG){
                        a += "<tr style='background: cornflowerblue;color:white'>"+
                        "<td colspan='99'>"+val.KDBRG + " - " + val.NMBRG +"</td>"+
                        "</tr>"
                    }
                }
                a += "<tr>"+
                "<td>"+val.LAYANAN  +"</td>"+
                "<td>"+val.KODE_MUTASI+"</td>"+
                "<td>"+val.BRGAW+"</td>"+
                "<td>-</td>"+
                "<td>-</td>"+
                "<td>-</td>"+
                "</tr>"
            });
            $('#tbl_stok_mitu tbody').append(a);
            
        },
        error: function (xhr,status,error) {

        }
    });
}
// Class initialization on page load
jQuery(document).ready(function() {
    InfoHarga.init();
});