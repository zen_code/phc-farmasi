<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<style>
    .is_head {
        background: #2860a8;
        color: white;
    }
</style>

<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
        <input type="hidden" value="<?php echo base_url(); ?>" id="url">
        <!-- begin:: Subheader -->
        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title"> Info Harga </h3>
                </div>
                <!-- kt-subheader__toolbar -->

            </div>
        </div>

        <!-- end:: Subheader -->

        <!-- begin:: Content -->
        <div class="kt-container  kt-grid__item kt-grid__item--fluid">

            <!--Begin::Dashboard 1-->

            <!--Begin::Row-->
            <div class="row">

                <div class="col-xl-12">
                    <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile ">
                        <!-- <div class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                Farmasi Klinis
                                </h3>
                            </div>
                        </div> -->
                        <div class="kt-portlet__body kt-portlet__body--fit">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="kt-portlet">
                                        <div class="kt-portlet__head">
                                            <div class="kt-portlet__head-label">
                                                <h3 class="kt-portlet__head-title">
                                                    Informasi Material
                                                </h3>
                                            </div>
                                        </div>
                                        <div class="kt-portlet__body">
                                            <div class="row">
                                                <div class="form-group col-md-3">
                                                    <label>Kode Material</label>
                                                    <input type="text" class="form-control" value="" id="kdbrg">
                                                </div>
                                                <div class="form-group col-md-9">
                                                    <label>Nama Material</label>
                                                    <input type="text" class="form-control" value="" id="nmbrg" placeholder="minimal 3 huruf untuk mencari material">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-md-3">
                                                    <label>Jenis</label>
                                                    <input type="text" class="form-control" value="" id="jenis" disabled>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <label>Kemasan</label>
                                                    <input type="text" class="form-control" value="" id="kemasan" disabled>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <label>Isi</label>
                                                    <input type="text" class="form-control" value="" id="isi" disabled>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <label>Satuan</label>
                                                    <input type="text" class="form-control" value="" id="satuan" disabled>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="kt-portlet">
                                        <div class="kt-portlet__head">
                                            <div class="kt-portlet__head-label">
                                                <h3 class="kt-portlet__head-title">
                                                   Informasi Standart
                                                </h3>
                                            </div>
                                        </div>
                                        <div class="kt-portlet__body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <table class="table table-striped- table-hover table-checkable">
                                                        <thead>
                                                            <tr style="background: #2860a8; color:white; text-align:center" id="tbl_standart">
                                                                <th>KODE</th>
                                                                <th>NAMA</th>
                                                            </tr>
                                                        </thead>
                                                    </table>

                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="kt-portlet">
                                        <div class="kt-portlet__head">
                                            <div class="kt-portlet__head-label">
                                                <h3 class="kt-portlet__head-title">
                                                   Informasi Pabrik
                                                </h3>
                                            </div>
                                        </div>
                                        <div class="kt-portlet__body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <table class="table table-striped- table-hover table-checkable">
                                                        <thead>
                                                            <tr style="background: #2860a8; color:white; text-align:center" id="tbl_pabrik">
                                                                <th>KODE</th>
                                                                <th>NAMA</th>
                                                            </tr>
                                                        </thead>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="kt-portlet">
                                        <div class="kt-portlet__head">
                                            <div class="kt-portlet__head-label">
                                                <h3 class="kt-portlet__head-title">
                                                    Informasi Bahan
                                                </h3>
                                            </div>
                                        </div>
                                        <div class="kt-portlet__body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <table class="table table-striped- table-hover table-checkable">
                                                        <thead>
                                                            <tr style="background: #2860a8; color:white; text-align:center" id="tbl_bahan">
                                                                <th>KODE</th>
                                                                <th>NAMA</th>
                                                                <th>FUNGSI</th>
                                                            </tr>
                                                        </thead>
                                                    </table>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="kt-portlet">
                                        <div class="kt-portlet__head">
                                            <div class="kt-portlet__head-label">
                                                <h3 class="kt-portlet__head-title">
                                                    Informasi Terapi
                                                </h3>
                                            </div>
                                        </div>
                                        <div class="kt-portlet__body">
                                            <div class="row">
                                        <div class="col-md-12">
                                                <table class="table table-striped- table-hover table-checkable">
                                                    <thead>
                                                        <tr style="background: #2860a8; color:white; text-align:center" id="tbl_terapi">
                                                            <th>KODE</th>
                                                            <th>NAMA</th>
                                                        </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="kt-portlet">
                                        <div class="kt-portlet__head">
                                            <div class="kt-portlet__head-label">
                                                <h3 class="kt-portlet__head-title">
                                                   Informasi Harga jual
                                                </h3>
                                            </div>
                                        </div>
                                        <div class="kt-portlet__body">
                                            <div class="row">
                                            <div class="col-md-12">
                                                <table class="table table-striped- table-hover table-checkable">
                                                    <thead>
                                                        <tr style="background: #2860a8; color:white; text-align:center" id="tbl_harga_jual">
                                                            <th>DEBITUR</th>
                                                            <th>HARGA</th>
                                                        </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="kt-portlet">
                                        <div class="kt-portlet__head">
                                            <div class="kt-portlet__head-label">
                                                <h3 class="kt-portlet__head-title">
                                                Informasi Harga Beli
                                                </h3>
                                            </div>
                                        </div>
                                        <div class="kt-portlet__body">
                                            <div class="row">
                                            <div class="col-md-12">
                                                <table class="table table-striped- table-hover table-checkable">
                                                    <thead>
                                                        <tr style="background: #2860a8; color:white; text-align:center" id="tbl_harga_beli">
                                                            <th>SUPPLIER</th>
                                                            <th>HNA</th>
                                                        </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="kt-portlet">
                                        <div class="kt-portlet__head">
                                            <div class="kt-portlet__head-label">
                                                <h3 class="kt-portlet__head-title">
                                                    Informasi Pergerakan Material
                                                </h3>
                                            </div>
                                        </div>
                                        <div class="kt-portlet__body">
                                            <div class="row">
                                                <div class="form-group col-md-12">
                                                    <label>Periode</label>
                                                    <div class="row">
                                                        <div class="form-group col-md-3">
                                                            <div class="input-group date">
                                                                <input type="text" class="form-control form-control-sm kt-input datepicker" placeholder="dari" id="date_from" />
                                                                <div class="input-group-append">
                                                                    <span class="input-group-text"><i class="la la-calendar-o glyphicon-th"></i></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-3">
                                                            <div class="input-group date">
                                                                <input type="text" class="form-control form-control-sm kt-input datepicker" placeholder="Sampai" id="date_to" />
                                                                <div class="input-group-append">
                                                                    <span class="input-group-text"><i class="la la-calendar-o glyphicon-th"></i></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-3">
                                                            <button type="button" class="btn btn-info btn-sm"><i class="fa fa-search"></i></button>
                                                        </div>

                                                    </div>
                                                    
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-md-6">
                                                    <label>Penerimaan Material</label>
                                                    <input type="text" class="form-control" value="" id="mm_in" disabled>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label>Pengeluaran Material Material</label>
                                                    <input type="text" class="form-control" value="" id="mm_out" disabled>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="kt-portlet">
                                        <div class="kt-portlet__head">
                                            <div class="kt-portlet__head-label">
                                                <h3 class="kt-portlet__head-title">
                                                    Informasi Stok Material
                                                </h3>
                                            </div>
                                        </div>
                                        <div class="kt-portlet__body">
                                            <div class="row">
                                            <div class="col-md-12" style="height:400px;overflow-y: scroll; display: block;width:100%">
                                                <table class="table table-striped- table-hover table-checkable" id="tbl_stok">
                                                    <thead>
                                                        <tr style="background: #2860a8; color:white; text-align:center">
                                                            <th>WAREHOUSE</th>
                                                            <th>CODE</th>
                                                            <th>AVAILABLE</th>
                                                            <th>RESERVED</th>
                                                            <th>TRANSFER SLOCK</th>
                                                            <th>ON-ORDER STOCK</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody></tbody>
                                                </table>
                                            </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="kt-portlet">
                                        <div class="kt-portlet__head">
                                            <div class="kt-portlet__head-label">
                                                <h3 class="kt-portlet__head-title">
                                                    Informasi Stok Mitu
                                                </h3>
                                            </div>
                                        </div>
                                        <div class="kt-portlet__body">
                                            <div class="row">
                                            <div class="col-md-12" style="height:400px;overflow-y: scroll; display: block;width:100%">
                                                <table class="table table-striped- table-hover table-checkable" id="tbl_stok_mitu"> 
                                                    <!-- style="height:500px;overflow-y: scroll; display: block;width:100%" -->
                                                    <thead>
                                                        <tr style="background: #2860a8; color:white; text-align:center">
                                                            <th>WAREHOUSE</th>
                                                            <th>CODE</th>
                                                            <th>AVAILABLE</th>
                                                            <th>RESERVED</th>
                                                            <th>TRANSFER SLOCK</th>
                                                            <th>ON-ORDER STOCK</th> 
                                                        </tr>
                                                    </thead>
                                                    <tbody></tbody>
                                                </table>
                                            </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>

            <!--End::Row-->



            <!--End::Dashboard 1-->
        </div>

        <!-- end:: Content -->
    </div>
</div>
<div class="modal fade in" tabindex="-1" role="dialog" id="modal-review">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content ">
        <div class="modal-header">
            <h5 class="modal-title">List Obat</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="row">
                <table id="table-obat_anmaag"  class="table table-striped- table-hover table-checkable">
                    <thead>
                        <tr style="background: #2860a8; color:white;text-align:center">
                            
                            <th style=" color:white;">KODE</th>  
                            <th style=" color:white;">NAMA BARANG</th>  
                            <th style=" color:white;">SATUAN</th>   
                            <th style=" color:white;">JENIS</th>   
                            <th style=" color:white;">KEMASAN</th>   
                            <th style=" color:white;">ISI</th>   
                            <th width="5%"></th>  
                        </tr>
                    </thead>
                </table>
            </div>
            
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
</div>
<!-- javascript this page -->
<script src="<?php echo base_url()?>assets/assets_metronic/vendors/custom/datatables/datatables.bundle.js"
    type="text/javascript"></script>
<script
    src="<?php echo base_url()?>assets/assets_metronic/vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"
    type="text/javascript"></script>
<script src="<?php echo base_url()?>application/modules/Info_harga/views/info_harga_view.js"
    type="text/javascript"></script>
<!-- <script src="<?php echo base_url()?>assets/assets_metronic/js/demo9/pages/dashboard.js" type="text/javascript"></script> -->

<script>

</script>