<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class info_harga_model extends CI_Model{
	public function __construct()
  {
      // Call the CI_Model constructor
      parent::__construct();
     $this->load->database();
  }
 
  public function list_obat($kdObat,$nmObat)
	{
		$where = "";
		
		if($kdObat != ""){
			$where .= " and a.KDBRG like '%".$kdObat."%' ";
		}
		if($nmObat != ""){
			$where .= " and a.NMBRG like '%".$nmObat."%' ";
		}

		$sql = "select KDBRG, NMBRG, SATUAN, b.NMJENIS,KEMASAN, ISI 
          from IF_MBRG_GD a
          join IF_MJENIS b on a.JENIS = b.KDJENIS
          where a.ACTIVE = 1 
          ".$where."
          ORDER BY a.KDBRG
          ";
		// var_dump($sql);die();
    	return $this->db->query($sql);    
	}
	public function info_stok($kdBrg){
		$sql = $this->db->query("select b.LAYANAN, b.KODE_MUTASI, a.BRGAW from IF_MBRG a
		join IF_MLAYANAN b on a.TIPEIF = b.IDLAYANAN
		where a.KDBRG= '".$kdBrg."'");
		return $sql->result();
	}
	public function info_stok_mitu($kdBrg){
		$sql = $this->db->query("select a.KDBRG, d.NMBRG, b.LAYANAN, b.KODE_MUTASI, a.BRGAW  from IF_MBRG_BHN c
		join IF_MBRG a on a.KDBRG = c.KDBRG
		join IF_MLAYANAN b on a.TIPEIF = b.IDLAYANAN
		join IF_MBRG_GD d on a.KDBRG = d.KDBRG
		where c.KDBHN = (select x.KDBHN from IF_MBRG_BHN x where x.KDBRG =  '".$kdBrg."') and b.ACTIVE = 1
		and  c.KDBRG <>  '".$kdBrg."'
		order by a.KDBRG");
		return $sql->result();
	}
	public function info_hjual($kdBrg){
		$sql = $this->db->query("select nmsupp,hbiji,hrata
		from if_htrans_gd h inner join if_trans_gd t on h.id_Trans=t.ID_TRANS
		inner join 
			(
			select b.kdsupp,s.NMSUPP,max(tgl_faktur) tgl_faktur  from if_trans_gd a
			join if_htrans_gd b on a.ID_TRANS = b.ID_TRANS
			inner join IF_MSUPP s on s.kdsupp=b.kdsuppa
			where a.KDBRG = 'S01655' and mutasi = 'i' 
			group by b.kdsupp,s.NMSUPP
			) x on x.kdsupp=h.kdsupp and h.tgl_faktur=x.tgl_faktur and t.kdbrg='".$kdBrg."'
		group by nmsupp,hbiji,hrata");
		return $sql->result();
	}
}
