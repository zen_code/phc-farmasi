<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class input_excess_model extends CI_Model
{
  public function __construct()
  {
    // Call the CI_Model constructor
    parent::__construct();
    $this->load->database();
  }

  /*public function getdatalogin($user,$pass)
  {
    $this->load->database();
    $sql = "exec inap_sp_login_bacapriv '$user','$pass'";
    return $this->db->query($sql);  
  }*/
  public function get_bio_model($kode)
  {
    $query  = $this->db->query("select a.NOTA, a.NMPX, a.ALMTPX, a.KOTAPX, a.STPAS, a.NORM, a.IDXPX, a.NOPESERTA, a.KDDEB, a.nmdeb, a.kddin, a.nmdin, b.NOMOR, a.NMKLIN, b.BIAYA_NOTA
    from IF_HTRANS a join RJ_EXCESS_BIAYA b on a.NOTA= b.NOTA 
    where b.LAYANAN = 3 and a.NOTA = '$kode'");
    return $query;
  }
  public function print_pdf_model()
  {
    $query  = $this->db->query("select t.NO,t.ID,h.nota,h.nomor,h.norm rm,h.IDXPX,h.NMPX nama,h.ALMTPX,h.KOTAPX,h.TGL_RESEP,h.KDDEB,h.nmdeb,h.kddin,h.nmdin, h.KDKLIN, h.NMKLIN, h.INPUTBY, convert(varchar, h.TGL, 103) TGL,convert(varchar, h.TGL, 8) jam2, h.JAM, h.KDDOK, h.NMDOK, t.ITER, h.STPAS, h.KDOT, r.NMJNSRESEP,
    t.KDBRG,b.nmbrg, b.SATUAN, t.JASA, t.jumlah, t.HJUAL, t.DISC, t.jumlah*t.hjual as 'sub_total',s.SIGNA from if_htrans h inner join if_trans t on h.id_Trans=t.ID_TRANS
    inner join IF_MBRG_GD b on b.kdbrg=t.kdbrg
    left join IF_MSIGNA s on s.kdsigna=t.SIGNA
    join MJENISRESEP r on h.KDOT=r.KDJNSRESEP 
    where NOTA = ''");
    return $query;
  }
  public function card_type_model()
  {
    $query  = $this->db->query("select * from IF_MBANK");
    return $query;
  }
}
