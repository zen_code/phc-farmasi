<?php
defined('BASEPATH') or exit('No direct script access allowed');

class input_excess extends CI_Controller
{
	public $urlws = null;
	var $globaldata;
	public function __construct()
	{
		parent::__construct();
		$this->load->model('input_excess_model');
		$this->load->library('session');
		$this->urlws = $this->globals->ws_phc();
		$this->data = array(
            'title' => 'Page Title',
            'robots' => 'noindex,nofollow',
            'css' => $this->config->item('css')
        );
	}
	private function _render($view, $data = array())
	{
		$this->load->view('header', $data);
		$this->load->view($view, $data);
		$this->load->view('footer');
	}

	public function index()
	{
		//load library
		// var_dump($this->session->all_userdata());die();
		$uri = &load_class('URI', 'core');
		redirect(base_url() . 'index.php/input_excess/data', $data);
	}

	public function data()
	{
		// var_dump($_SESSION["ses_userid"]);die();
		$prevurl = $this->input->get('url');
		$prevnota = $this->input->get('nota');
		if($_SESSION["if_ses_depo"] == null){
			redirect(base_url().'depo');
		}else{
			if (in_array(6, $_SESSION['if_ses_menu'])) {
				$data['parent_active'] = 6;
				$data['child_active'] = 57;
				$data['prevurl'] = $prevurl;
				$data['prevnota'] = $prevnota;
				// var_dump($data);die();
				$this->_render('input_excess_view', $data);
			} else {
				redirect(base_url() . 'beranda/data');
			}
		}
	}

	public function card_type()
	{
		$query = $this->input_excess_model->card_type_model();
		echo json_encode($query->result());
	}
	public function get_bio_ctrl()
	{
		$kode = $this->input->get('kode');
		$query = $this->input_excess_model->get_bio_model($kode);
		echo json_encode($query->result());
	}
	function print_pdf_ctrl()
	{
		// $this->load->model("cetak_resep_model");
		ob_start();
		// $data_all = $this->input->post('data_all');
		$data = $this->globaldata;
		echo json_encode($this->globaldata);
		// echo $data_all[0]['TGL'];
		// print($data_all[0]['NMDOK']);
		// var_dump($data_all);die();
		// $data = json_decode($data_all, true);
		$this->load->library('pdf');

		//        $employee_data = $this->report_timely_model->get_datamasterrmseluruh()->result();
		// $employee_data = $this->cetak_resep_model->print_pdf_model()->result();
		
		$pdf = new FPDF('P', 'mm', array(105, 155));
		$pdf->SetTitle('cetak_resep', true);
		// membuat halaman baru
		$pdf->AddPage();
		// setting margin yang akan digunakan
		$pdf->SetMargins(2.5, 2.5);
		// setting jenis font yang akan digunakan
		$pdf->SetFont('Courier', 'B', 8);
		// mencetak string
		$pdf->Cell(100, 0, '', 0, 1);
		$pdf->Cell(100, 2.82, 'RS PHC SURABAYA, Jl. Prapat Kurung Selatan No.1 Surabaya', 0, 1);
		// $pdf->SetFont('Courier', 'B', 8);
		// Memberikan space kebawah agar tidak terlalu rapat
		// echo $data_all[0]['NMDOK'];
		foreach($data as $row){
			$pdf->Cell(75, 2.82, 'NPWP/NPPKP : 01.061.180.4.051.000', 0, 0, 'L');
			$pdf->Cell(25, 2.82, $row->TGL, 0, 1);
			$pdf->Cell(100, 2.82, '--[NOTA INI BERLAKU SEBAGAI KUITANSI]--------[TUNAI]-1--', 0, 1);
			$pdf->Cell(60, 2.82, 'Dokter   : ' . $row->NMDOK, 0, 0);
			$pdf->Cell(40, 2.82, 'No.Resep : ' . $row->nomor, 0, 1);
			$pdf->Cell(60, 2.82, 'Klinik   : ' . $row->NMKLIN, 0, 0);
			$pdf->Cell(40, 2.82, 'No.Nota  : ' . $row->nota, 0, 1);
			$pdf->Cell(100, 2.82, 'Tgl Shift: ' . $row->TGL . ' #', 0, 1);	
		}
		$pdf->Cell(75, 2.82, 'NPWP/NPPKP : 01.061.180.4.051.000', 0, 0, 'L');
		// $pdf->Cell(25, 2.82, $data_all[0]['TGL'], 0, 1);
		// $pdf->Cell(100, 2.82, '--[NOTA INI BERLAKU SEBAGAI KUITANSI]--------[TUNAI]-1--', 0, 1);
		// $pdf->Cell(60, 2.82, 'Dokter   : ' . $data_all[0]['NMDOK'], 0, 0);
		// $pdf->Cell(40, 2.82, 'No.Resep : ' . $data_all[0]['nomor'], 0, 1);
		// $pdf->Cell(60, 2.82, 'Klinik   : ' . $data_all[0]['NMKLIN'], 0, 0);
		// $pdf->Cell(40, 2.82, 'No.Nota  : ' . $data_all[0]['nota'], 0, 1);
		// $pdf->Cell(100, 2.82, 'Tgl Shift: ' . $data_all[0]['TGL'] . ' #', 0, 1);
		// $pdf->Cell(10, 7, 'Periode: ' . $date_f . ' - ' . $date_t, 0, 1);
		// $pdf->Cell(10, 7, 'Debitur: ' . (count($employee_data) > 0 && $kddebt != "" ? $employee_data[0]->NMDEBT : ''), 0, 1);
		// $pdf->Cell(10, 4, '', 0, 1);
		// $pdf->Cell(25, 6, 'NO', 1, 0);
		// $pdf->Cell(75, 6, 'NAMA', 1, 0);
		// $pdf->Cell(65, 6, 'DEBITUR', 1, 0);
		// $pdf->Cell(55, 6, 'TANGGAL PEMERIKSAAN', 1, 0);
		// $pdf->Cell(25, 6, 'STATUS FIT', 1, 0);
		// $pdf->Cell(30, 6, 'STATUS UNFIT', 1, 1);
		// $pdf->SetFont('Arial', '', 10);
		$summaryFit = 0;
		$summaryUnfit = 0;
		// for($i=0; $i<6; $i++) {
		// foreach ($employee_data as $row) {
		// 	$pdf->Cell(25, 6, $row->no_peserta, 1, 0);
		// 	$pdf->Cell(75, 6, $row->NAMA, 1, 0);
		// 	$pdf->Cell(65, 6, $row->NMDEBT, 1, 0);
		// 	$pdf->Cell(55, 6, $row->tgl, 1, 0);
		// 	$pdf->Cell(25, 6, $row->fit, 1, 0, 'R');
		// 	$pdf->Cell(30, 6, $row->unfit, 1, 1, 'R');
		// 	$summaryFit += $row->fit;
		// 	$summaryUnfit += $row->unfit;
		// }
		// }
		// $pdf->SetFont('Arial', 'B', 10);
		// $pdf->Cell(220, 6, 'Summary', 1, 0);
		// $pdf->Cell(25, 6, $summaryFit, 1, 0, 'R');
		// $pdf->Cell(30, 6, $summaryUnfit, 1, 0, 'R');
		$pdf->Output();
		ob_end_flush();
	}
}
