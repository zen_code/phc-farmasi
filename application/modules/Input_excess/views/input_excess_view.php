<?php
defined('BASEPATH') or exit('No direct script access allowed');

?>
<style>
    .is_head {
        background: #2860a8;
        color: white;
    }
</style>
<form class="m-form" style="padding:20px;" method="POST" action="" id="form">
    <div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
        <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
            <input type="hidden" value="<?php echo base_url(); ?>" id="url">
            <input type="hidden" value="<?php echo ($prevurl) ?>" id="prevurl">
            <input type="hidden" value="<?php echo ($prevnota) ?>" id="prevnota">
            <!-- begin:: Subheader -->
            <div class="kt-subheader   kt-grid__item" id="kt_subheader">
                <div class="kt-container ">
                    <div class="kt-subheader__main">
                        <h3 class="kt-subheader__title"> Input Excess </h3>
                    </div>
                    <!-- kt-subheader__toolbar -->

                </div>
            </div>

            <!-- end:: Subheader -->

            <!-- begin:: Content -->
            <div class="kt-container  kt-grid__item kt-grid__item--fluid">

                <!--Begin::Dashboard 1-->

                <!--Begin::Row-->
                <div class="row">

                    <div class="col-xl-12">
                        <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile ">
                            <!-- <div class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                Farmasi Klinis
                                </h3>
                            </div>
                        </div> -->
                            <div class="kt-portlet__body kt-portlet__body--fit">
                                <!--begin: Datatable -->
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="kt-portlet">
                                            <div class="kt-portlet__head">
                                                <div class="kt-portlet__head-label">
                                                    <h3 class="kt-portlet__head-title">
                                                        Biodata Pasien
                                                    </h3>
                                                </div>
                                            </div>
                                            <div class="kt-portlet__body">
                                                <div class="m-form__group row">
                                                    <div class="form-group col-md-3">
                                                        <label>Nota</label>
                                                        <input type="number" class="form-control" value="" id="registrasi">
                                                    </div>
                                                    <div class="form-group col-md-9">
                                                        <label>Nama</label>
                                                        <input type="text" class="form-control " value="" id="name">
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-md-9">
                                                        <label>Alamat</label>
                                                        <input type="text" class="form-control " value="" id="alamat">
                                                    </div>
                                                    <div class="form-group col-md-3">
                                                        <label>Kota</label>
                                                        <input type="text" class="form-control " value="" id="kota">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="kt-portlet">
                                            <div class="kt-portlet__head">
                                                <div class="kt-portlet__head-label">
                                                    <h3 class="kt-portlet__head-title">
                                                        Debitur dan Kamar Pasien
                                                    </h3>
                                                </div>
                                            </div>
                                            <div class="kt-portlet__body">
                                                <div class="row">
                                                    <div class="form-group col-md-3">
                                                        <label>Jenis Px.</label>
                                                        <input type="text" class="form-control" value="" id="jenispx">
                                                    </div>
                                                    <div class="form-group col-md-3">
                                                        <label>RM</label>
                                                        <input type="text" class="form-control" value="" id="rm">
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                        <label>Index</label>
                                                        <input type="text" class="form-control" value="" id="indexs">
                                                    </div>
                                                    <div class="form-group col-md-4">
                                                        <label>No. Peserta</label>
                                                        <input type="text" class="form-control" value="" id="nopeserta">
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="form-group col-md-6">
                                                        <label>Debitur</label>
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <input type="text" class="form-control" value="" id="kddeb">
                                                            </div>
                                                            <div class="col-md-9">
                                                                <input type="text" class="form-control" value="" id="nmdeb">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label>Dinas</label>
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <input type="text" class="form-control" value="" id="kddin">
                                                            </div>
                                                            <div class="col-md-8">
                                                                <input type="text" class="form-control" value="" id="nmdin">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="kt-portlet__body">
                                    <div>
                                        <table id="table-list_obat" class="table table-striped- table-bordered table-hover table-checkable" style="display: block;">
                                            <thead>
                                                <tr style="background: #2860a8; color:white; text-align:center">
                                                    <th width='3%'>CEK</th>
                                                    <th width='8%'>NOTA</th>
                                                    <th width='10%'>NOMOR</th>
                                                    <th width='25%'>DEBITUR</th>
                                                    <th width='25%'>KLINIK</th>
                                                    <th width='10%'>BIAYA</th>
                                                    <th width='10%'>DITANGGUNG ASURANSI</th>
                                                    <th width='10%'>EXCESS</th>
                                                </tr>
                                            </thead>
                                            <tbody style="text-align:center">
                                                <tr>
                                                    <td colspan="99" style="text-align:center">NO DATA</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <script>
                                            $('#table-list_obat thead th').css('min-width', '160px');
                                            $('#table-list_obat thead th').eq(0).css('min-width', '50px');
                                        </script>
                                    </div>
                                    <div class="form-group m-form__group row kt-space-between">
                                        <label for="example-text-input" class="col-9 col-form-label" style="color:brown">Nominal Biaya yang Ditanggung Asuransi Mohon Disesuaikan</label>
                                        <div style="float:right" class="col-md-3">
                                            <div class="form-group m-form__group row">
                                                <label class="col-md-4 col-form-label">
                                                    Total Excess
                                                </label>
                                                <div class="col-md-8">
                                                    <input name="" disabled style="text-align:right" type="number" class="form-control m-input" id="subtotal" value="0">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-12">
                                            <form method="post" target="_blank" action="<?php echo base_url(); ?>cetak_resep/print_pdf">

                                                <!-- <input type="hidden" name="date-from" />
                                            <input type="hidden" name="date-to" />
                                            <input type="hidden" name="kode" />
                                            <input type="submit" name="export" class="btn btn-warning" value="PDF" /> -->
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="kt-portlet">
                                            <div class="kt-portlet__head">
                                                <div class="kt-portlet__head-label">
                                                    <h3 class="kt-portlet__head-title">
                                                        Pembayaran
                                                    </h3>
                                                </div>
                                            </div>
                                            <div class="kt-portlet__body">
                                                <div class="form-group m-form__group row">
                                                    <div class="col-md-6">
                                                        <!--  -->
                                                        <div class="col-md-2">
                                                            <label class="kt-checkbox" style="top:-4px">
                                                                <input type="checkbox" id="is_check" value="0">
                                                                <span></span>
                                                            </label>
                                                            <label>Tunai</label>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-md-3 col-form-label" for="pwd">Tipe Kartu : </label>
                                                            <div class="form-group col-md-8 col-sm-6" style="padding:0px; margin:0px;">
                                                                <select class="form-control" id="tipe_kartu">
                                                                    <option></option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-md-3 col-form-label" for="pwd">No. Kartu :</label>
                                                            <input class="col-md-8 col-sm-6 form-control m-input" type="number" id="no_kartu">
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-md-3 col-form-label" for="pwd">No. Trace : </label>
                                                            <input class="col-md-8 col-sm-6 form-control m-input" type="number" id="no_trace">
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-md-3 col-form-label" for="pwd">Nominal :</label>
                                                            <input class="col-md-8 col-sm-6 form-control m-input" type="number" id="nominal">
                                                        </div>
                                                        <div class="form-group row" style="float:right;margin-right: 45px;">
                                                            <button type="button" class="btn btn-success2" id="tambah_pembayaran"><span class="tag_type"></span>Tambah</button>
                                                        </div>

                                                        <!--  -->
                                                    </div>
                                                    <div class="col-md-6">
                                                        <br>
                                                        <table class="table table-striped- table-bordered table-hover table-checkable" id="tabel_pembayaran">
                                                            <thead>
                                                                <tr style="background:#2860a8;text-align: center;">
                                                                    <th style="color:white;" width="23%">CARA BAYAR</th>
                                                                    <th style="color:white;" width="23%">NO. KARTU</th>
                                                                    <th style="color:white;" width="18%">NO. TRACE</th>
                                                                    <th style="color:white;" width="23%">NOMINAL</th>
                                                                    <th style="color:white;" width="8%">ACTION</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody style="text-align:center">
                                                                <tr>
                                                                    <td colspan="99" style="text-align:center">NO DATA</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="col-md-5" style="float:right">
                                                    <div class="form-group row">
                                                        <button type="button" id="save_all" class="col-md-5 form-control m-input btn btn-success2" style="margin-left:65px"><i class="fas fa-check">&nbsp;</i>Simpan dan cetak resep</button>&emsp;
                                                        <button type="reset" id="cancel" class="col-md-5 form-control m-input btn btn-warning2" style=""><i class="fas fa-times">&nbsp;</i>Cancel</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!--End::Row-->



                <!--End::Dashboard 1-->
            </div>

            <!-- end:: Content -->
        </div>
    </div>
    <div class="modal fade" tabindex="-1" role="dialog" id="modal-review">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content ">
                <div class="modal-header">
                    <h5 class="modal-title"><span class="tag_type"></span> Pasien</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <table id="table-pasien" class="table table-striped- table-hover table-checkable">
                        <thead>
                            <tr style="background: #2860a8; color:white;text-align:center">
                                <th style=" color:white;">NOREG</th>
                                <th style=" color:white;">RM</th>
                                <th style=" color:white;">TGL_MRS</th>
                                <th style=" color:white;">NAMA</th>
                                <th style=" color:white;">KAMAR</th>
                                <th style=" color:white;">RUANGAN</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th style=" color:white;">NOREG</th>
                                <th style=" color:white;">RM</th>
                                <th style=" color:white;">TGL_MRS</th>
                                <th style=" color:white;">NAMA</th>
                                <th style=" color:white;">KAMAR</th>
                                <th style=" color:white;">RUANGAN</th>
                                <th></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</form>
<!-- javascript this page -->
<script src="<?php echo base_url() ?>assets/assets_metronic/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/assets_metronic/vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>application/modules/Input_excess/views/input_excess_view.js" type="text/javascript"></script>
<!-- <script src="<?php echo base_url() ?>assets/assets_metronic/js/demo9/pages/dashboard.js" type="text/javascript"></script> -->

<script>

</script>