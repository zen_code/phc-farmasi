"use strict";

// Class definition

var InputExcess = (function() {
  function daftar_creditcard() {
    $.ajax({
      type: "GET",
      url: "input_excess/card_type",
      data: {},
      dataType: "json",

      success: function(data) {
        console.log(data);
        jQuery.each(data, function(index, val) {
          var a = "";
          a =
            "<option value='" +
            val.KDBANK +
            "' data-kartu='" +
            val.KARTU +
            "'><b>" +
            val.KARTU +
            "</b>" +
            "</option>";
          $("#tipe_kartu").append(a);
        });
        KTApp.unblockPage();
      },
      error: function(xhr, status, error) {
        console.log(xhr, status, error);
      }
    });
    $("#tipe_kartu").select2({
      placeholder: "Pilih Cara Pembayaran",
      width: "100%"
    });
  }
  return {
    // Init demos
    init: function() {
      daftar_creditcard();
      loading();
      fill();
    }
  };
})();
function fill(){ 
  var kode = $("#prevnota").val();
  var subtotal = 0;
  var disc = 0;
  var jasa_r = 0;
  var total = 0;
  KTApp.blockPage({
    overlayColor: "#000000",
    type: "v2",
    state: "primary",
    message: "Processing..."
  });
  $.ajax({
    type: "GET",
    url: "input_excess/get_bio_ctrl",
    data: {
      kode: kode
    },
    dataType: "json",
    success: function(data) {
      data_all = data;
      // data_all = "[";
      $("#table-list_obat tbody").html("");
      jQuery.each(data, function(index, val) {
        // data_all += JSON.stringify(data[index]) + ",";
        var a = "";
        $("#registrasi").val(val.NOTA);
        $("#name").val(val.NMPX);
        $("#alamat").val(val.ALMTPX);
        $("#kota").val(val.KOTAPX);
        if (val.STPAS == 1) {
          $("#jenispx").val("TUNAI");
        } else if (val.STPAS == 2) {
          $("#jenispx").val("KREDIT");
        }
        $("#rm").val(val.NORM);
        $("#indexs").val(val.IDXPX);
        $("#nopeserta").val(val.NOPESERTA);
        $("#kddeb").val(val.KDDEB);
        $("#nmdeb").val(val.nmdeb);
        $("#kddin").val(val.kddin);
        $("#nmdin").val(val.nmdin);

        $("#kdklin").val(val.KDKLIN);
        $("#nmklin").val(val.NMKLIN);
        $("#user").val(val.INPUTBY);
        $("#tgl_entri").val(val.TGL);
        $("#jam_entri").val(val.JAM);
        $("#kddok").val(val.KDDOK);
        $("#nmdok").val(val.NMDOK);
        $("#kdjenis").val(val.KDOT);
        $("#nmjenis").val(val.NMJNSRESEP);
        $("#iter").val(val.ITER);
        a =
          "<tr>" +
          "<td>" +
          '<label class="kt-checkbox"><input type="checkbox" class="form-check-input" value="" id="cek"><span></span></label>' +
          "</td>" +
          "<td>" +
          val.NOTA +
          "</td>" +
          "<td>" +
          val.NOMOR +
          "</td>" +
          "<td>" +
          val.nmdeb +
          "</td>" +
          "<td>" +
          val.NMKLIN +
          "</td>" +
          "<td>" +
          val.BIAYA_NOTA +
          "</td>" +
          "<td>" +
          '<div id="ditanggung_cell"><input type="number" disabled id="ditanggung" class="form-control m-input"></div>'+
          "</td>" +
          "<td>" +
          '<div id="excess_cell"><input type="number" disabled id="excess" class="form-control m-input"></div>'+
          "</td>" +
          "</tr>";
        $("#table-list_obat tbody").append(a);
        subtotal = subtotal + val.sub_total;
        disc = disc + val.DISC;
        jasa_r = jasa_r + val.JASA;
      });
      $("#subtotal").val(subtotal);
      $("#disc").val(disc);
      $("#jasa_r").val(jasa_r);
      total = subtotal - disc + jasa_r;
      $("#total").val(total);
      KTApp.unblockPage();
      // data_all += "]";
    },
    error: function(xhr, status, error) {
      console.log(xhr, status, error);
    }
  });
}
//Bismillah
var data_all = "";
$("#registrasi").on("keypress", function(e) {
  if (e.keyCode == 13) {
    var kode = $("#registrasi").val();
    var subtotal = 0;
    var disc = 0;
    var jasa_r = 0;
    var total = 0;
    KTApp.blockPage({
      overlayColor: "#000000",
      type: "v2",
      state: "primary",
      message: "Processing..."
    });
    $.ajax({
      type: "GET",
      url: "input_excess/get_bio_ctrl",
      data: {
        kode: kode
      },
      dataType: "json",
      success: function(data) {
        data_all = data;
        // data_all = "[";
        $("#table-list_obat tbody").html("");
        jQuery.each(data, function(index, val) {
          // data_all += JSON.stringify(data[index]) + ",";
          var a = "";
          $("#registrasi").val(val.NOTA);
          $("#name").val(val.NMPX);
          $("#alamat").val(val.ALMTPX);
          $("#kota").val(val.KOTAPX);
          if (val.STPAS == 1) {
            $("#jenispx").val("TUNAI");
          } else if (val.STPAS == 2) {
            $("#jenispx").val("KREDIT");
          }
          $("#rm").val(val.NORM);
          $("#indexs").val(val.IDXPX);
          $("#nopeserta").val(val.NOPESERTA);
          $("#kddeb").val(val.KDDEB);
          $("#nmdeb").val(val.nmdeb);
          $("#kddin").val(val.kddin);
          $("#nmdin").val(val.nmdin);

          $("#kdklin").val(val.KDKLIN);
          $("#nmklin").val(val.NMKLIN);
          $("#user").val(val.INPUTBY);
          $("#tgl_entri").val(val.TGL);
          $("#jam_entri").val(val.JAM);
          $("#kddok").val(val.KDDOK);
          $("#nmdok").val(val.NMDOK);
          $("#kdjenis").val(val.KDOT);
          $("#nmjenis").val(val.NMJNSRESEP);
          $("#iter").val(val.ITER);
          a =
            "<tr>" +
            "<td>" +
            '<label class="kt-checkbox"><input type="checkbox" class="form-check-input" value="" id="cek"><span></span></label>' +
            "</td>" +
            "<td>" +
            val.NOTA +
            "</td>" +
            "<td>" +
            val.NOMOR +
            "</td>" +
            "<td>" +
            val.nmdeb +
            "</td>" +
            "<td>" +
            val.NMKLIN +
            "</td>" +
            "<td>" +
            val.BIAYA_NOTA +
            "</td>" +
            "<td>" +
            '<div id="ditanggung_cell"><input type="number" disabled id="ditanggung" class="form-control m-input"></div>'+
            "</td>" +
            "<td>" +
            '<div id="excess_cell"><input type="number" disabled id="excess" class="form-control m-input"></div>'+
            "</td>" +
            "</tr>";
          $("#table-list_obat tbody").append(a);
          subtotal = subtotal + val.sub_total;
          disc = disc + val.DISC;
          jasa_r = jasa_r + val.JASA;
        });
        $("#subtotal").val(subtotal);
        $("#disc").val(disc);
        $("#jasa_r").val(jasa_r);
        total = subtotal - disc + jasa_r;
        $("#total").val(total);
        KTApp.unblockPage();
        // data_all += "]";
      },
      error: function(xhr, status, error) {
        console.log(xhr, status, error);
      }
    });
  }
});
var cek_cek;
$("#table-list_obat").on("click", "tr #cek", function() {
  cek_cek = $(this)
    .parents("tr")
    .find("#cek");
  if (cek_cek.prop("checked") == true) {
    $(this)
      .parents("tr")
      .find("#ditanggung")
      .attr("disabled", false);
    // $(this).parents("tr").find("#qty2").attr({"max" : max, "min" : 0});
  } else if (cek_cek.prop("checked") == false) {
    $(this)
      .parents("tr")
      .find("#ditanggung")
      .attr("disabled", true);
    $(this)
      .parents("tr")
      .find("#ditanggung")
      .val("");
  }
});
$("#print").on("click", function() {
  // var url = "cetak_resep/print_pdf_ctrl";
  // alert("asdasd");
  // window.open(url, "_blank");
  //
  $.ajax({
    type: "POST",
    url: "cetak_resep/print_pdf_ctrl",
    data: {
      data_all: data_all
    },
    dataType: "json",
    success: function(data) {
      swal.fire("printing", "check");
    },
    error: function(xhr, status, error) {
      // console.log(xhr, status, error);
    }
  });
  //
});
$("#tambah_pembayaran").on("click", function() {
  if ($("#tabel_pembayaran tbody tr").text() == "NO DATA") {
    $("#tabel_pembayaran tbody tr").remove();
  }
  var a = "";
  var a1 = $("#tipe_kartu")
    .find(":selected")
    .data("kartu");
  var a2 = $("#no_kartu").val();
  var a3 = $("#no_trace").val();
  var a4 = $("#nominal").val();
  a =
    "<tr>" +
    "<td>" +
    a1 +
    "</td>" +
    "<td>" +
    a2 +
    "</td>" +
    "<td>" +
    a3 +
    "</td>" +
    "<td style='text-align: right;'>" +
    a4 +
    "</td>" +
    '<td><button class="btn btn-danger" type="button"  id="del-byr"><i class="fa fa-times"></i></button></td>' +
    "</tr>";
  $("#tabel_pembayaran tbody").append(a);
});
$("#tabel_pembayaran").on("click", "tr #del-byr", function() {
  $(this)
    .parents("tr")[0]
    .remove();
  if ($("#tabel_pembayaran tbody tr").length < 1) {
    $("#tabel_pembayaran tbody").append(
      "<tr><td colspan='99' style='text-align:center'>NO DATA</td></tr>"
    );
  }
});
//

function loading() {
  KTApp.blockPage({
    overlayColor: "#000000",
    type: "v2",
    state: "primary",
    message: "Processing..."
  });
}
$("#no_reseph").on("keyup, keydown", function(evt) {
  if (evt.key === "Enter") {
    console.log($("#no_reseph").val());
  }
});
$("#save_all").on('click', function(){
  // do some stuff
  $(location).attr('href', $("#prevurl").val());
});
// Class initialization on page load
jQuery(document).ready(function() {
  InputExcess.init();
});
