<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class depo extends CI_Controller {
	public $urlws = null;
	public function __construct() {
        parent::__construct();
		$this->load->model('depo_model');
		$this->load->library('session');
		$this->urlws = $this->globals->ws_phc();

    }
	private function _render($view,$data = array())
	{
		$this->load->view('headerdepo',$data);
	    // $this->load->view('body_header');
	    // $this->load->view('sidebar');
	    $this->load->view($view,$data);
	    $this->load->view('footer');
	}

	public function index()
	{
		//load library
		// var_dump($this->session->all_userdata());die();
		$uri =& load_class('URI', 'core');
	  	redirect(base_url().'index.php/depo/data',$data);
	}

	public function data()
	{
		// var_dump($_SESSION["ses_userid"]);die();
		$this->_render('depo_view');
	}
	public function getdepo(){
		$url = $this->urlws."Farmasi/Role/getDepoFarmasi";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		$output = curl_exec($ch);
		curl_close($ch);
		$data = json_decode($output);

		echo json_encode($data);
	}
	public function setdepo(){
		$val = $this->input->post('depo_val');
		$text = $this->input->post('depo_text');
		$id = $this->input->post('depo_id');
		// var_dump($val, $text);die();
		$this->session->set_userdata('if_ses_depo', $text);
		$this->session->set_userdata('if_ses_depo_code', $val);
		$this->session->set_userdata('if_ses_depo_id', $id);

		echo json_encode('S');
	}
}
