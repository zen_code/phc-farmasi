"use strict";

// Class definition

var DepoList = function() {
    function getDepo(){
        $.ajax({
            type: "GET",
            url: "depo/getdepo", 
            dataType: 'json',
            success: function (data) {
                var a = '';
                jQuery.each(data, function(i, val) {
                    a += "<option value='"+val.KODE_MUTASI  +"' data-id='"+val.IDLAYANAN+"'>"+ val.LAYANAN +"</option>";
                });
                $('#select_depo').append(a);
                $('#select_depo').select2({
                    width: '100%',
                    placeholder: 'Silihkan Pilih Depo terlebih dahulu',
                });
                KTApp.unblockPage();
                // console.log(a);
            },
            error: function (xhr,status,error) {

            }
        });
    }
    
    
    return {
        // Init demos
        init: function() {
            getDepo();
            KTApp.blockPage({
                overlayColor: '#000000',
                type: 'v2',
                state: 'primary',
                message: 'Processing...'
            });
        },
    };
}();
$('#btn_pilih').click(function(){
    KTApp.blockPage({
        overlayColor: '#000000',
        type: 'v2',
        state: 'primary',
        message: 'Processing...'
    });
    var depo_val = $('#select_depo').val();
    var depo_text = $('#select_depo option:selected').text();
    var depo_id = $('#select_depo option:selected').attr('data-id');
    $.ajax({
        type: "POST",
        url: "depo/setDepo", 
        dataType: 'json',
        data:  {
            depo_val    : depo_val,
            depo_text    : depo_text,
            depo_id    : depo_id,
            },
        success: function (data) {
            location.href = "beranda"
        },
        error: function (xhr,status,error) {

        }
    });
})

// Class initialization on page load
jQuery(document).ready(function() {
    DepoList.init();
});