<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<style>
    .passrow{
        display:none
    }
</style>
<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
    <input type="hidden" value="<?php echo base_url(); ?>" id="url">
        <!-- begin:: Subheader -->
        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        Depo </h3>
                </div>
                <!-- kt-subheader__toolbar -->
                
            </div>
        </div>

        <!-- end:: Subheader -->

        <!-- begin:: Content -->
        <div class="kt-container  kt-grid__item kt-grid__item--fluid">

            <!--Begin::Dashboard 1-->

            <!--Begin::Row-->
            <div class="row">
                
                <div class="col-xl-12">
                    <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile ">
                        <div class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Pilih Depo
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body kt-portlet__body--fit">
                            <!-- <?php echo $_SESSION["ses_depo"] . " - ". $_SESSION["ses_depo_code"]; ?> -->
                            <!--begin: Datatable -->
                            <div class="kt-portlet__body">
                                <div class="row">
                                    <div class="col-md-3 "></div>
                                    <div class="col-md-6 ">
                                        <div class="form-group">
                                            <label>Pilih Depo</label>
                                            <select id="select_depo">
                                            </select>
                                        </div>
                                        <button class="btn btn-info" id="btn_pilih"> Pilih </button>
                                    </div>
                                </div>
                                
                            </div>
                            
                            <!--end: Datatable -->
                        </div>
                    </div>
                </div>
            </div>

            <!--End::Row-->

            

            <!--End::Dashboard 1-->
        </div>

        <!-- end:: Content -->
    </div>
</div>

<!-- javascript this page -->
<!-- <script src="<?php echo base_url()?>assets/assets_metronic/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script> -->
<script src="<?php echo base_url()?>application/modules/Depo/views/depo_view.js" type="text/javascript"></script>
<!-- <script src="<?php echo base_url()?>assets-custom/user_view.js" type="text/javascript"></script> -->

<script>

</script>