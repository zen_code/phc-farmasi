"use strict";

var Laporan_mutasiBarangMasuk = (function() {
    function getkode() {
        $.ajax({
          type: "GET",
          url: "laporan_mutasiBarangMasuk/get_kode",
          data: {},
          dataType: "json",
    
          success: function(data) {
            // console.log(data);
            var datakode = data;
            jQuery.each(datakode, function(index, val) {
              var a = "";
              a =
                "<option value='" +
                val.MKODE +
                "' data-kode='" +
                val.MNAMA +
                "'><b>" +
                val.MKODE +
                " - " +
                val.MNAMA +
                "</b>" +
                "</option>";
              $("#MNAMA").append(a);
            });
          },
          error: function(xhr, status, error) {
            console.log(xhr, status, error);
          }
        });
        $("#MNAMA").select2({
          placeholder: "Pilih Kode",
          width: "100%"
        });
        
    }
    function getlayanan() {
        $.ajax({
          type: "GET",
          url: "laporan_mutasiBarangMasuk/get_layanan",
          data: {},
          dataType: "json",
    
          success: function(data) {
            // console.log(data);
            var datalayanan = data;
            jQuery.each(datalayanan, function(index, val) {
              var a = "";
              a =
                "<option value='" +
                val.IDLAYANAN +
                "' data-layanan='" +
                val.LAYANAN +
                "'><b>" +
                val.IDLAYANAN +
                " - " +
                val.LAYANAN +
                "</b>" +
                "</option>";
              $("#LAYANAN").append(a);
            });
          },
          error: function(xhr, status, error) {
            console.log(xhr, status, error);
          }
        });
        $("#LAYANAN").select2({
          placeholder: "Pilih Layanan",
          width: "100%"
        });
        
    }
    return {
        // Init demos
        init: function() {
          
            getkode();
            getlayanan();

            
            $('.datepicker').datepicker({
              autoclose: true,
              todayHighlight: true,
              format: 'dd/mm/yyyy',
              changeMonth: true,
              changeYear: true,
          });
        }
    };
})();

$(document).ready(function () {
  $("#date_from").change(function () {
    $('input[name=date_from]').val($(this).val());
    
  });
  $("#date_to").change(function () {
      $('input[name=date_to]').val($(this).val());
  });
  $("#MNAMA").change(function () {
      $('input[name=MNAMA]').val($(this).val());
  });
  $("#LAYANAN").change(function () {
    $('input[name=LAYANAN]').val($(this).val());
  });
});

function filterLaporan(){
  // alert('yey');
  tabelLaporan();
}

function tabelLaporan(){
  loading();
  var tglmulai = $('#date_from').val();
  var tglselesai = $('#date_to').val();

  var valMNAMA = $('#MNAMA').val();
  var valLAYANAN = $('#LAYANAN').val();
  
  $('#tabelReport').DataTable({
    destroy: true,
    processing: true,
    type: "GET",
    ajax:
      "laporan_mutasiBarangMasuk/laporanMutasi/?tglmulai="
      +tglmulai+"&tglselesai="+tglselesai+"&valMNAMA="+valMNAMA+"&valLAYANAN="+valLAYANAN,
    columns:[
      {
        data: "no"
      },
      {
        data: "tanggal"
      },
      // {
      //   data: "mutasi"
      // },
      {
        data: "kode"
      },
      {
        data: "nama"
      },
      {
        data: "jumlah"
      }
    ],
    scrollX: true,
    createdRow: function(row, data, index){
      $("td", row)
        .eq(0)
        .addClass("text-center");
    },
    drawCallback: function(){
      KTApp.unblockPage();
    }
  });
}

function loading(){
  KTApp.blockPage({
    overlayColor: "#000000",
    type: "v2",
    state: "primary",
    message: "Processing..."
  });
}

jQuery(document).ready(function() {
    Laporan_mutasiBarangMasuk.init();

    // $("#idlaporan").select2({
    //     placeholder: "Jenis Laporan",
    //     width: "100%"
    // });
  });