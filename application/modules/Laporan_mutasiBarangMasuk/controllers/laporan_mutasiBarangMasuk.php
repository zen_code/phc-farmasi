<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class laporan_mutasiBarangMasuk extends CI_Controller {
    public $urlws = null;

	public function __construct() {
        parent::__construct();
		$this->load->model('laporan_mutasiBarangMasuk_model');
		$this->load->library('session');
		$this->urlws = $this->globals->ws_phc();

    }

    private function _render($view,$data = array())
	{
		$this->load->view('header',$data);
	    // $this->load->view('body_header');
	    // $this->load->view('sidebar');
	    $this->load->view($view,$data);
	    $this->load->view('footer');
    }
    
    public function index()
	{
		//load library
		// var_dump($this->session->all_userdata());die();
		$uri =& load_class('URI', 'core');
	  	redirect(base_url().'index.php/laporan_mutasiBarangMasuk/data',$data);
	}

	public function data()
	{
		// var_dump($_SESSION["ses_userid"]);die();
		if($_SESSION["if_ses_depo"] == null){
			redirect(base_url().'depo');
		}else{
			if(in_array(7,$_SESSION['if_ses_menu'])){
				$data['parent_active'] = 7 ;
				$data['child_active'] = 48 ;
				$this->_render('laporan_mutasiBarangMasuk_view', $data);
			}else{
				redirect(base_url().'beranda/data');}
		}
	}
	
	public function get_kode()
	{
		$query = $this->laporan_mutasiBarangMasuk_model->get_kode_model();
		echo json_encode($query->result());
	}

	public function get_layanan()
	{
		$query = $this->laporan_mutasiBarangMasuk_model->get_layanan_model();
		echo json_encode($query->result());
	}

	public function laporanMutasi(){
		$tglmulai = $this->input->get('tglmulai'); 
		$tglselesai = $this->input->get('tglselesai'); 
		$valMNAMA = $this->input->get('valMNAMA');
		$valLAYANAN = $this->input->get('valLAYANAN');
		// var_dump($tglmulai);
		$query = $this->laporan_mutasiBarangMasuk_model->laporanMutasi($tglmulai,$tglselesai,$valMNAMA,$valLAYANAN);
		$data 			= array();
		$no = 1;
		foreach ($query->result() as $index => $item){
			$data[] 	= array(
				"no"			=> $no,
				"tanggal"		=> $item->TANGGAL,
				"kode"			=> $item->KODE,
				"nama"			=> $item->NAMA,
				"jumlah"		=> $item->JUMLAH
				
				// "btn"		=> $btn

			);
			$no++;
		}

		$obj = array("data" => $data);

		echo json_encode($obj);
	}

	public function export_excel(){
		// print_r("yey"); exit();
		// $this->load->model("laporan_mutasiBarangMasuk_model");
		$this->load->library('excel');
		$object = new PHPExcel();

		$date_f = $this->input->post('date_from');
		$date_t = $this->input->post('date_to');
		$kddebt = $this->input->post('MNAMA');
		$kdlayanan = $this->input->post('LAYANAN');

		// var_dump($date_f); die();

		$object->setActiveSheetIndex(0);
		$table_columns = array("No","Tanggal", "Kode", "Nama", "Jumlah");

		$column = 0;

		foreach ($table_columns as $field) {
			$object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
			$column++;
		}

		$excel_row = 2;

		
		$laporan_data = $this->laporan_mutasiBarangMasuk_model->laporanMutasi($date_f, $date_t, $kddebt, $kdlayanan);
		$no = 1;
		foreach ($laporan_data->result() as $row) {
			$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $no);
			$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->TANGGAL);
			$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->KODE);
			$object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row->NAMA);
			$object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row->JUMLAH);
			
			$excel_row++;
			$no++;
		}
			
		

		$object_writer = new PHPExcel_Writer_Excel2007($object);
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="Laporan Mutasi Barang.xls"');
		$object_writer->save('php://output');

	}

	function export_pdf()
	{
		// $this->load->model("report_timely_model");
		$this->load->library('pdf');

		// var_dump($this->input->post());
		$date_f = $this->input->post('date_from');
		$date_t = $this->input->post('date_to');
		$kddebt = $this->input->post('MNAMA');
		$kdlayanan = $this->input->post('LAYANAN');
		
		$laporan_data = $this->laporan_mutasiBarangMasuk_model->laporanMutasi($date_f, $date_t, $kddebt, $kdlayanan)->result();
		// var_dump($employee_data);
		$pdf = new FPDF('l', 'mm', 'A4');
		// membuat halaman baru
		$pdf->AddPage();
		// setting jenis font yang akan digunakan
		$pdf->SetFont('Arial', 'B', 16);
		// mencetak string
		$pdf->Cell(260, 12, 'Laporan Mutasi Barang Masuk', 0, 1, 'C');
		$pdf->SetFont('Arial', 'B', 10);
		// Memberikan space kebawah agar tidak terlalu rapat
		$pdf->Cell(10, 7, '', 0, 1);
		$pdf->Cell(10, 7, 'Periode: ' . $date_f . ' - ' . $date_t, 0, 1);
		$pdf->Cell(10, 7, 'Debitur: ' . (count($laporan_data) > 0 && $kddebt != "" ? $laporan_data[0]->KDIN : ''), 0, 1);
		$pdf->Cell(10, 7, 'Layanan: ' . (count($laporan_data) > 0 && $kdlayanan != "" ? $laporan_data[0]->MNAMA : ''), 0, 1);
		$pdf->Cell(10, 4, '', 0, 1);
		$pdf->Cell(10, 6, 'NO', 1, 0);
		$pdf->Cell(45, 6, 'TANGGAL', 1, 0);
		$pdf->Cell(35, 6, 'KODE', 1, 0);
		$pdf->Cell(135, 6, 'NAMA', 1, 0);
		$pdf->Cell(55, 6, 'JUMLAH', 1, 0);
		// $pdf->Cell(25, 6, 'STATUS FIT', 1, 0);
		// $pdf->Cell(30, 6, 'STATUS UNFIT', 1, 1);
		$pdf->SetFont('Arial', '', 10);
		// $summaryFit = 0;
		// $summaryUnfit = 0;
		// for($i=0; $i<6; $i++) {
		$pdf->Cell(10, 6, '', 0, 1);
		$no=1;
		foreach ($laporan_data as $row) {
			$pdf->Cell(10, 6, $no, 1, 0);
			$pdf->Cell(45, 6, $row->TANGGAL, 1, 0);
			$pdf->Cell(35, 6, $row->KODE, 1, 0);
			$pdf->Cell(135, 6, $row->NAMA, 1, 0);
			$pdf->Cell(55, 6, $row->JUMLAH, 1, 0);
			$pdf->Cell(10, 6, '', 0, 1);
			$no++;
			// $pdf->Cell(25, 6, $row->fit, 1, 0, 'R');
			// $pdf->Cell(30, 6, $row->unfit, 1, 1, 'R');
			// $summaryFit += $row->fit;
			// $summaryUnfit += $row->unfit;
		}
		
		$pdf->Output();
	}
}