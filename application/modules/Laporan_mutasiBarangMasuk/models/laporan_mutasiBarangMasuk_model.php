<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class laporan_mutasiBarangMasuk_model extends CI_Model{
    public function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
        $this->load->database();
    }
    
    public function get_kode_model()
    {
        $query  = $this->db->query("SELECT * FROM IF_MKETIN");
        return $query;
    }

    public function get_layanan_model()
    {
        $query  = $this->db->query("SELECT * FROM IF_MLAYANAN");
        return $query;
    }

    public function laporanMutasi($tglmulai, $tglselesai, $valMNAMA, $valLAYANAN)
    {
        // var_dump($valMNAMA);die();
        // $sql = ("
        // SELECT TOP 100 a.TGL, a.MUTASI, c.KDBRG, c.NMBRG, b.HJUAL, b.JUMLAH, (b.HJUAL * b.JUMLAH) as TOTAL
        // FROM IF_HTRANS a 
        // JOIN IF_TRANS b ON b.ID_TRANS = a.ID_TRANS 
        // JOIN IF_MBRG_GD c ON b.KDBRG = c.KDBRG 
        // where convert(varchar,a.TGL,103) BETWEEN '$tglmulai' AND '$tglselesai'
        // ");
        // $query = $this->db->query($sql);
       
        // return $query;
        // var $tglmulaiNew = convert(varchar,a.TGL,103);
        return $this->db->query("exec if_sp_baru_laporan_dko '$valMNAMA','$tglmulai','$tglselesai','$valLAYANAN'");
    }
    
}