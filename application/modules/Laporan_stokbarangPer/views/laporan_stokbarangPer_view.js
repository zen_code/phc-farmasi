"use strict";

// Class definition

var LaporanResep = function () {
    return {
        // Init demos
        init: function () {
            input_number();
            $('.datepicker').datepicker({
                autoclose: true,
                format: 'dd/mm/yyyy',
                changeMonth: true,
                changeYear: true,
            });
            
        },
    };
}();

function loading() {
    KTApp.blockPage({
        overlayColor: '#000000',
        type: 'v2',
        state: 'primary',
        message: 'Processing...'
    });
}

function input_number() {
    $(".numbers").on("keypress keyup blur", function (event) {
        $(this).val($(this).val().replace(/[^\d].+/, ""));
        if ((event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });
}

// Class initialization on page load
jQuery(document).ready(function () {
    LaporanResep.init();
});