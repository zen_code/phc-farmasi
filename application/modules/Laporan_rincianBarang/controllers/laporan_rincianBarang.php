<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class laporan_rincianBarang extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('laporan_rincianBarang_model');
		$this->load->library('session');
		$this->urlws = "http://localhost/phc-ws/api/";
	}
	private function _render($view,$data = array())
	{
		$this->load->view('header',$data);
	    // $this->load->view('body_header');
	    // $this->load->view('sidebar');
	    $this->load->view($view,$data);
	    $this->load->view('footer');
	}

	public function index()
	{
		//load library
		$uri =& load_class('URI', 'core');
	  	redirect(base_url().'index.php/laporan_rincianBarang/data',$data);
	}

	public function data()
	{
		// var_dump($_SESSION["ses_depo"]);die();
		if($_SESSION["if_ses_depo"] == null){
			redirect(base_url().'depo');
		}else{
			if(in_array(7,$_SESSION['if_ses_menu'])){
				$data['parent_active'] = 7 ;
				$data['child_active'] = null ;
				$this->_render('laporan_rincianBarang_view', $data);
			}else{
				redirect(base_url().'beranda/data');}
		}

		// if($_SESSION["if_ses_depo"] == null){
		// 	redirect(base_url().'depo');
		// }else{
		// 	$data['parent_active'] = 7 ;
		// 	$data['child_active'] = null ;
		// 	$this->_render('laporan_rincianBarang_view', $data);
		// }
		
	}
	public function dataPasien()
	{
		$authorization = 'Authorization:'.$_SESSION["ses_token_phc"];
		$url = $this->urlws."Farmasi/FarmasiKlinis/dataPasien";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, array($authorization ));
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		$output = curl_exec($ch); 
		curl_close($ch);    
		$cek_output = json_decode($output);
		// var_dump($cek_output);die();
		if($cek_output->status == true){
			$data 			= array();
			foreach($cek_output->data as $item){
				$btn 		='<button name="btnproses" id="btnproses" onclick="detailPasien('.$item->NOREG.')" type="button" class="btn btn-success btn-sm" data-toggle="kt-tooltip" data-placement="top" title="Choose"> <i class="fa fa-check"></i> </button>';
                
				$data[] 	= array(
					"NOREG" 	=> $item->NOREG,
					"RM" => $item->RM,
					"TGL_MRS" 	=> $item->TGL_MRS ,
					"NAMA"		=> $item->NAMA,
					"KAMAR"		=> $item->KAMAR,
					"RUANGAN"		=> $item->RUANGAN,
					"btn"		=> $btn
				);
			}
			$obj = array("data"=> $data);
			echo json_encode($obj);
		}else{
			$error = array(
				'data' => [],
				'error' => $cek_output->message
			);
			
			echo json_encode($error);
		}
		
	}
}
