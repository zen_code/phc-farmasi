"use strict";

// Class definition

var MasterBahan = function() {
    return {
        // Init demos
        init: function() {
            loading();
            getDataBahan();
        },
    };
}();

function getDataBahan() {
    // console.log('assadad');
    $('#data_bahan').DataTable({
        "destroy": true,
        "processing": true,
        "type": "GET",
        "ajax": 'master_bahan/dataBahan',
        "columns": [
            { "data": "KDBHN" },
            { "data": "NMBHN" },
            { "data": "btn" },
        ],

        "createdRow": function(row, data, index) {
            // $('td', row).eq(0).addClass('text-center');
            // $('td', row).eq(2).addClass('text-right');
        },
        "drawCallback": function(settings) {
            $('[data-toggle="kt-tooltip"]').tooltip();
        },
        "initComplete": function() {
            KTApp.unblockPage();
        }
    });
}

function update(id, type) {
    swal.fire('success', "id=" + id + " type=" + type, 'success');
}

$('#add_bahan').click(function() {
    $('input').val('');
    $('.tag_type').text('Tambah');
    $('#modal-tambah').modal({ backdrop: 'static', keyboard: false });
    $('#modal-tambah').modal('show');
});

$('#action').click(function() {
    $('#modal-tambah').modal('hide');
    loading();
    $.ajax({
        type: "POST",
        url: "master_bahan/actionBahan",
        data: {
            kode: $('#kode').val(),
            bahan: $('#bahan').val()
            
        },
        dataType: 'json',
        async: false,
        success: function(data) {
            console.log(data);
            KTApp.unblockPage();
            swal.fire(data.data.status, data.data.message, data.data.status);
            location.reload();
            getDataBahan();
        },
        error: function(xhr, status, error) {
            console.log(status, error);
        }
    });

});

$('#data_bahan').on('click', 'tr #btnproses', function() {
    var baris = $(this).parents('tr')[0];
    var table = $('#data_bahan').DataTable();
    var datas = table.row(baris).data();

    $('.tag_type').text('Edit');
    $('#kode').val(datas['KDBHN']);
    $('#bahan').val(datas['NMBHN']);
    $('#modal-tambah').modal('show');
});

$('#data_bahan').on('click', 'tr #btnbatal', function() {
    var baris = $(this).parents('tr')[0];
    var table = $('#data_bahan').DataTable();
    var datas = table.row(baris).data();
    swal.fire({
        title: '<span style="font-size: 19px;font-weight: 700;">Menghapus Bahan</span>',
        text: 'Apakah anda yakin menghapus bahan ini?',
        type: 'question',
        showCancelButton: true,
        confirmButtonText: 'Ya',
        cancelButtonText: 'Tidak',
    }).then(function(result) {
        if (result.value) {
            deleteBahan(datas['KDBHN']);
            // console.log(datas['KDBHN']);
        }
    })
})

function deleteBahan(kode) {

    $.ajax({
        type: "POST",
        url: "master_bahan/deleteBahan",
        data: {
            kode: kode
        },
        dataType: 'json',
        success: function(data) {
            KTApp.unblockPage();
            location.reload();
            getDataBahan();
        },
        error: function(xhr, status, error) {}
    });
}
function loading() {
    KTApp.blockPage({
        overlayColor: '#000000',
        type: 'v2',
        state: 'primary',
        message: 'Processing...'
    });
}
// Class initialization on page load
jQuery(document).ready(function() {
    MasterBahan.init();
});