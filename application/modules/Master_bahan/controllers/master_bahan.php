<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class master_bahan extends CI_Controller {
    public $urlws = null;
    public $app_id = 1;
    
	public function __construct() {
        parent::__construct();
		$this->load->model('master_bahan_model');
		$this->load->library('session');
		$this->urlws = $this->globals->ws_phc();
		// $this->urlwscentra = "http://centra-dev.pelindo.co.id/public/api/";
		$this->app_id = 1;
    }

    private function _render($view,$data = array())
	{
		$this->load->view('header',$data);
	    // $this->load->view('body_header');
	    // $this->load->view('sidebar');
	    $this->load->view($view,$data);
	    $this->load->view('footer');
    }
    
    public function index()
	{
		//load library
		$uri =& load_class('URI', 'core');
	  	redirect(base_url().'index.php/master_bahan/data',$data);
    }
    
    public function data()
	{
		if($_SESSION["if_ses_depo"] == null){
			redirect(base_url().'depo');
		}else{
			if(in_array(3,$_SESSION['if_ses_menu'])){
				$data['parent_active'] = 3 ;
				$data['child_active'] = 78 ;
				$this->_render('master_bahan_view',$data);
			}else{
				redirect(base_url().'beranda/data');
			}
		}
		
		
    }
    
    public function dataBahan()
	{
		// var_dump('asd');die();
		$authorization = 'Authorization:'.$_SESSION["if_ses_token_phc"];
		$url = $this->urlws."Farmasi/Bahan/dataBahan";
		// $data = array(
		// 	"app_id" => $this->app_id
		// );
		$ch = curl_init();
		// curl_setopt($ch, CURLOPT_POST, 1);
		// curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array($authorization ));
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		$output = curl_exec($ch); 
		curl_close($ch);    
		$datas = json_decode($output);
		
		if($datas->status == true){
			$data 			= array();
			foreach($datas->data as $item){
				$btn 		='<button name="btnproses" id="btnproses" type="button" class="btn btn-success btn-sm" data-toggle="kt-tooltip" data-placement="top" title="Update"> <i class="fa fa-edit"></i> </button>
                            <button name="btnbatal" id="btnbatal" type="button" class="btn btn-danger btn-sm" data-toggle="kt-tooltip" data-placement="top" title="Delete"> <i class="fa fa-times"></i> </button>';
                
				$data[] 	= array(
					"KDBHN" 		=> $item->KDBHN,
					"NMBHN" 		=> $item->NMBHN,
					"btn"			=> $btn
				);
			}
			$obj = array("data"=> $data);

			echo json_encode($obj);
		}else{
			$error = array(
				'data' => [],
				'error' => $cek_output->message
			);
			
			echo json_encode($error);
		}
		
	}

	public function actionBahan(){

		$data = array(
            "kode"     			=> $this->input->post('kode'),
            "bahan"     		=> $this->input->post('bahan'),
            "user"     			=> $_SESSION["if_ses_username"],
		);
		// var_dump( $data);die();
		$authorization = 'Authorization: '.$_SESSION["if_ses_token_phc"];
		$url = $this->urlws."Farmasi/Bahan/action";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array($authorization ));
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		$output = curl_exec($ch); 
		curl_close($ch);    

		$cek_output = json_decode($output);

		// var_dump($cek_output);die();
		if($cek_output->status == true){
			echo json_encode($cek_output);
		}else{
			$error = array(
				'data' => [],
				'error' => $cek_output->message
			);
			
		}
	}

	public function deleteBahan(){
		$data = array(
            "kode"     => $this->input->post('kode')
		);

		$authorization = 'Authorization: '.$_SESSION["if_ses_token_phc"];
		$url = $this->urlws."Farmasi/Bahan/delete";
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array($authorization ));
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		$output = curl_exec($ch); 
		curl_close($ch);    
		// var_dump($output);die();
		$cek_output = json_decode($output);
		if($cek_output->status == true){
			echo json_encode($cek_output);
		}else{
			$error = array(
				'data' => [],
				'error' => $cek_output->message
			);
			
		}
	}
}