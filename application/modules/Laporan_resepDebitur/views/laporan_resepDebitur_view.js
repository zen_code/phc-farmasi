"use strict";

// Class definition

var LaporanResep = function () {
    return {
        // Init demos
        init: function () {
            input_number();
            $('.datepicker').datepicker({
                autoclose: true,
                format: 'dd/mm/yyyy',
                changeMonth: true,
                changeYear: true,
            });
            $('#table-layanan tfoot th').each(function () {
                var title = $(this).text();
                switch (title) {
                    case 'NOREG':
                    case 'RM':
                    case 'NAMA':
                    case 'KAMAR':
                    case 'RUANGAN':
                    $(this).html('<input  placeholder="Search ' + title + '" type="text" class="form-control form-control-sm form-filter kt-input"/>');
                    break;
                    case 'TGL_MRS':
                    $(this).html(`<div class="input-group date">
                        <input type="text" class="form-control form-control-sm kt-input" placeholder="From" id="kt_datepicker_1" />
                        <div class="input-group-append">
                        <span class="input-group-text"><i class="la la-calendar-o glyphicon-th"></i></span>
                        </div>
                        </div>`);
                    break;
                }
                $('#kt_datepicker_1').datepicker({
                    autoclose: true,
                    format: 'dd/mm/yyyy'
                });
            });

            // DataTable
            var table = $('#table-layanan').DataTable({
                responsive: true,
                dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
                lengthMenu: [5, 10, 25, 50],
                pageLength: 5,
                language: {
                    'lengthMenu': 'Display _MENU_',
                },
                searchDelay: 500,
                ordering: true,
                processing: true,
                serverSide: false,
                ajax: {
                    url: 'farmasi_klinis/dataPasien',
                    type: 'POST',
                    data: {
                        columnsDef: ['NOREG', 'RM', 'TGL_MRS', 'NAMA', 'KAMAR', 'RUANGAN', 'btn'],
                    },
                },
                columns: [{
                    data: "NOREG"
                },
                {
                    data: "RM"
                },
                {
                    data: "TGL_MRS"
                },
                {
                    data: "NAMA"
                },
                {
                    data: "KAMAR"
                },
                {
                    data: "RUANGAN"
                },
                {
                    data: "btn"
                }
                ],
                "drawCallback": function (settings) {
                    $('[data-toggle="kt-tooltip"]').tooltip();
                },

            });

            // Apply the search
            table.columns().every(function () {
                var that = this;
                $('input', this.footer()).on('keyup change clear', function () {
                    if (that.search() !== this.value) {
                        that
                        .search(this.value)
                        .draw();
                    }
                });
            });
        },
    };
}();

$('#search_layanan').click(function(){
    $('#modal-layanan').modal('show')
});
$('#search_debitur').click(function(){
    $('#modal-layanan').modal('show')
});

function loading() {
    KTApp.blockPage({
        overlayColor: '#000000',
        type: 'v2',
        state: 'primary',
        message: 'Processing...'
    });
}

function input_number() {
    $(".numbers").on("keypress keyup blur", function (event) {
        $(this).val($(this).val().replace(/[^\d].+/, ""));
        if ((event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });
}

// Class initialization on page load
jQuery(document).ready(function () {
    LaporanResep.init();
});