<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class resep_online extends CI_Controller {
	public $urlws = null;
	public function __construct() {
        parent::__construct();
		$this->load->model('resep_online_model');
		$this->load->library('session');
		$this->urlws = $this->globals->ws_phc();

    }
	private function _render($view,$data = array())
	{
		$this->load->view('header',$data);
	    // $this->load->view('body_header');
	    // $this->load->view('sidebar');
	    $this->load->view($view,$data);
	    $this->load->view('footer');
	}

	public function index()
	{
		//load library
		// var_dump($this->session->all_userdata());die();
		$uri =& load_class('URI', 'core');
	  	redirect(base_url().'index.php/resep_online/data',$data);
	}

	public function data()
	{
		// var_dump($_SESSION);die();
		if($_SESSION["if_ses_depo"] == null){
			redirect(base_url().'depo');
		}else{
			if(in_array(6,$_SESSION['if_ses_menu'])){
				$data['parent_active'] = 6 ;
				$data['child_active'] = 45 ;
				$this->_render('resep_online_view', $data);
			}else{
				redirect(base_url().'beranda/data');}
		}
	}
	public function dataResepOnline()
	{
		
		$text = $this->input->post('text');
		$sql = $this->resep_online_model->dataResepOnline($text);
		// a.NOANTRIAN, a.KARCIS, a.TGL, a.NORM,a.NMPX, b.NMDEBT,a.NMKLIN, a.NMDOK, a.ID_TRANS
		
		$data 			= array();
		foreach($sql as $item){
			$dsb = $item->STSCETAK == 1 ? "" : "disabled";
			$btn 		='<button name="btnproses" id="btnproses" onclick="detailResep('."'". $item->ID_TRANS ."'".')" type="button" class="btn btn-success btn-sm" data-toggle="kt-tooltip" data-placement="top" title="Choose"> <i class="fa fa-check"></i> </button>
			<button name="btlResep" id="btlResep" onclick="prosesResep('."'". $item->ID_TRANS ."'".', 0)" type="button" '.$dsb.' class="btn btn-danger btn-sm" data-toggle="kt-tooltip" data-placement="top" title="Batal Proses Antrian"> <i class="fa fa-undo"></i> </button>';
			// var_dump($item->ID_TRANS);
			$data[] 	= array(
				"ANTRIAN" 	=> $item->NOANTRIAN,
				"KARCIS" => $item->KARCIS,
				"TGL" 	=> $item->TGL ,
				"RM" 	=> $item->NORM ,
				"NAMA"		=> $item->NMPX,
				"DEBITUR"		=> $item->NMDEBT,
				"KLINIK"		=> $item->NMKLIN,
				"DOKTER"		=> $item->NMDOK,
				"ID_TRANS"		=> $item->ID_TRANS,
				"btn"		=> $btn
			);
		}
		$obj = array("data"=> $data);
		// var_dump($dates);die();
		echo json_encode($obj);
		// die();

		// $authorization = 'Authorization:'.$_SESSION["ses_token_phc"];
		// $url = $this->urlws."Farmasi/FarmasiKlinis/dataPasien";
		// $ch = curl_init();
		// curl_setopt($ch, CURLOPT_HTTPHEADER, array($authorization ));
		// curl_setopt($ch, CURLOPT_URL, $url); 
		// curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		// $output = curl_exec($ch); 
		// curl_close($ch);    
		// $cek_output = json_decode($output);
		// // var_dump($cek_output);die();
		// if($cek_output->status == true){
		// 	$data 			= array();
		// 	foreach($cek_output->data as $item){
		// 		$btn 		='<button name="btnproses" id="btnproses" onclick="detailPasien('.$item->NOREG.')" type="button" class="btn btn-success btn-sm" data-toggle="kt-tooltip" data-placement="top" title="Choose"> <i class="fa fa-check"></i> </button>';
                
		// 		$data[] 	= array(
		// 			"NOREG" 	=> $item->NOREG,
		// 			"RM" => $item->RM,
		// 			"TGL_MRS" 	=> $item->TGL_MRS ,
		// 			"NAMA"		=> $item->NAMA,
		// 			"KAMAR"		=> $item->KAMAR,
		// 			"RUANGAN"		=> $item->RUANGAN,
		// 			"btn"		=> $btn
		// 		);
		// 	}
		// 	$obj = array("data"=> $data);
		// 	echo json_encode($obj);
		// }else{
		// 	$error = array(
		// 		'data' => [],
		// 		'error' => $cek_output->message
		// 	);
			
		// 	echo json_encode($error);
		// }
		
	}
	
	
	public function detailResep()
	{
		$id_trans_ol = $this->input->get('id_trans_ol');
		$sql = $this->resep_online_model->detailResep($id_trans_ol);

		echo json_encode($sql);
	}
	public function getDebitur()
	{
		
		$sql = $this->resep_online_model->getDebitur();
		$res = "";
		foreach($sql as $item){
			$res .= "<option value='".$item->KDDEBT."'>".$item->NMDEBT."</option>";
		}


		echo json_encode($res);
	}
	public function getSigna()
	{
		
		$sql = $this->resep_online_model->getSigna();
		$res = "";
		foreach($sql as $item){
			$res .= "<option value='".$item->kdsigna."' data-signa='".$item->signa."' data-signcepat='".$item->signcepat."'>".$item->uraian."</option>";
		}


		echo json_encode($res);
	}
	public function getObat()
	{
		$kdObat = $this->input->get('kdObat');
		$nmObat = $this->input->get('nmObat');
		$tipeif = $this->input->get('tipeif');
		$sql = $this->resep_online_model->getObat($kdObat,$nmObat,$tipeif);
		$res = "";
		$data 			= array();
		foreach($sql->result() as $item){
			$btn 		='<button name="btnproses" id="btnproses"type="button" onClick="pilih('."'".$item->KDBRG."'".','."'".$item->NMBRG."'".','."'".$item->SATUAN."'".')" class="btn btn-success btn-sm" data-toggle="kt-tooltip" data-placement="top" title="Choose"> <i class="fa fa-check"></i> </button>';
            
			$data[] 	= array(
				"KDBRG" 	=> $item->KDBRG,
				"NMBRG" 	=> $item->NMBRG,
				"SATUAN" 	=> $item->SATUAN ,
				"btn"		=> $btn
			);
		}
		$obj = array("data"=> $data);
		echo json_encode($obj);
	}

	public function prosesResep(){
		$id_trans_ol = $this->input->post('id_trans_ol');
		$type = $this->input->post('type');
		$sql = $this->resep_online_model->prosesResep($id_trans_ol, $type);

		echo json_encode($sql);
	} 
	public function getStandartObat(){
		$kdbrg = $this->input->post('kdbrg');
		$kddeb = $this->input->post('kddeb');
		$sql = $this->resep_online_model->getStandartObat($kdbrg, $kddeb);

		echo json_encode($sql);
	}

	public function cekSisaObat(){
		$kdbrg = $this->input->post('kdbrg');
		$kddeb = $this->input->post('kddeb');
		$rm = $this->input->post('rm');
		$sql = $this->resep_online_model->cekSisaObat($kdbrg, $kddeb, $rm);

		echo json_encode($sql);
	}
	
	public function action()
	{	
		$data =  array(
            "noreg" => $this->input->post('noreg'),
            "id_trans" => $this->input->post('id_trans'),
            "drp" => $this->input->post('drp'),
            "tgl" => $this->input->post('tgl'),
            "user" => $_SESSION["if_ses_username"],
            "norm" => $this->input->post('norm'),
            "hasil_asesmen" => $this->input->post('hasil_asesmen')
		);
		$data = http_build_query($data);
		// var_dump($data);die();
		$authorization = 'Authorization:'.$_SESSION["if_ses_token_phc"];
		$url = $this->urlws."Farmasi/farmasiKlinis/action";

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array($authorization ));
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		$output = curl_exec($ch); 
		curl_close($ch);    
		$datas = json_decode($output);

		// var_dump($output);die(); 
		echo json_encode($datas);
	}
	
	public function save_ctrl()
	{
		$data = array(
			"kirim" => $this->input->post('save_pengiriman_arr'),
			"bayar" => $this->input->post('save_pembayaran_arr'),
			"obat_asli" => $this->input->post('save_obat_asli_arr'),
			"obat_split" => $this->input->post('save_obat_split_arr'),
			"obat_tunai" => $this->input->post('save_obat_tunai_arr'),
			"header" => $this->input->post('save_header_arr'),
		);
		$query = $this->resep_online_model->save_model($data);
		echo json_encode($query);
	}
	
}
