<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class resep_online_model extends CI_Model{
    public function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
        $this->load->database();
    }
  
    public function dataResepOnline ($text){
      
        $where = "";
        if($text != ""){
            $where = " and (a.NOANTRIAN like '%".$text."%' or a.KARCIS like '%".$text."%' or a.ID_TRANS like '%".$text."%')" ;
        }

        $sql = $this->db->query("select top 100 a.NOANTRIAN, a.KARCIS, convert(varchar, a.TGL, 103) TGL, a.NORM,a.NMPX, b.NMDEBT,a.NMKLIN, a.NMDOK, a.ID_TRANS,a.STSCETAK from if_htrans_ol a
            join rirj_mdebitur b on a.KDDEB = b.KDDEBT 
            where nota is null  ".$where."
            order by a.NOANTRIAN");
            //a.STSCETAK = 0 and a.AMBIL = 0 and a.TGLAMBIL is null//where convert(varchar, a.TGL, 103) = '".$dates."'
            // var_dump($sql->result());die();
        return $sql->result();
    }
    public function detailResep ($id_trans_ol){
        $res = [];
        $data = [];
        try{
           $sql = $this->db->query("select a.NOTA, a.NOANTRIAN, b.NAMA, b.ALAMAT, c.NM_KABKOTA, a.IDXPX, a.NORM, d.KDDEBT, d.NMDEBT, e.KDDINAS, e.NMDINAS, f.kdKlin ,f.nmKlin,g.kdDok, g.nmDok, a.INPUTBY, a.ID_TRANS, h.MNAMA, a.kdot, i.DRP, i.TINDAK_LANJUT,a.STSCETAK, a.ID_TRANS
                  from if_htrans_ol a 
                  left join RIRJ_MASTERPX b on a.NOPESERTA = b.NO_PESERTA 
                  left join RIRJ_MKABKOTA c on b.KOTA = c.KD_KABKOTA
                  left join rirj_mdebitur d on a.KDDEB = d.KDDEBT
                  left join MDINAS e on a.KDDIN = e.KDDINAS
                  left join DR_KLINIK f on a.KDKLIN = f.kdKlin
                  left join DR_MDOKTER g on a.KDDOK = g.kdDok
                  left join IF_MKETOT h on a.kdot = h.MKODE
                  left join IF_FARKLIN_HTRANS i on a.ID_TRANS = i.ID_TRANS_RJ_ONLINE
                  where a.ID_TRANS = '".$id_trans_ol."'");
            // var_dump($sql->row()->STSCETAK);die();
            // var_dump($id_trans_ol);die();
            if($sql->row()->STSCETAK == 0){
                $data['header'] = $sql->result();

                $sql = $this->db->query("select * from  if_trans_ol b where b.ID_TRANS ='".$id_trans_ol."' order by NO");
                $data['detail'] = $sql->result();
    
                $res['status'] = 'success';
                $res['message'] = $data;
            }else{
                $res['status'] = 'error';
                $res['message'] = 'Data Masih dalam proses';
            }
            
        }catch(Exception $e){
          $res['status'] = 'error';
          $res['message'] = $e;
        }
      
        return $res;
    }
    public function getDebitur(){
        $sql = $this->db->query("select KDDEBT, NMDEBT from rirj_mdebitur");
        return $sql->result();
    }
    public function getSigna(){
        $sql = $this->db->query("select * from signa");
        return $sql->result();
    }
    public function getObat($kdObat,$nmObat,$tipeif){
        // $sql = $this->db->query("select a.KDBRG, b.NMBRG, b.SATUAN from if_mbrg a
        // join IF_MBRG_GD b on a.KDBRG = b.KDBRG
        // where a.TIPEIF =".$tipeif);
        // return $sql->result();
        $where = "";
		if($tipeif){
			$where .= " and a.TIPEIF =".$tipeif;
		}
		if($kdObat != ""){
			$where .= " and a.KDBRG like '%".$kdObat."%' ";
		}
		if($nmObat != ""){
			$where .= " and b.NMBRG like '%".$nmObat."%' ";
		}

		$sql = "select a.KDBRG, b.NMBRG, b.SATUAN from if_mbrg a
            join IF_MBRG_GD b on a.KDBRG = b.KDBRG
            where a.ACTIVE = 1 
			 ".$where."
			ORDER BY b.NMBRG
			";
		// var_dump($sql);die();
    	return $this->db->query($sql);    
    }

    public function prosesResep($id_trans_ol, $type){
        $res = [];
        
        $set = $type == 1 ? " STSCETAK = 1, AMBIL = 1, TGLAMBIL = getdate() " : "STSCETAK = 0, AMBIL = 0, TGLAMBIL = null" ;
        $this->db->query("update if_htrans_ol set ".$set." where ID_TRANS = '".$id_trans_ol."'");
        $res['status'] = 'success';
        $res['message'] = $type == 1 ? 'Sedang Memproses Resep '.$id_trans_ol : 'Sukses Batalkan Proses Resep '.$id_trans_ol;

        return $res;
    }
    public function getStandartObat($kdbrg, $kddeb){
        $res = [];
        // var_dump($kdbrg, $kddeb);die();

        $sql = $this->db->query("select * from  if_std_obat a
                join if_std_detobat b ON b.KODE = a.KODE
                join rirj_mdebitur c on a.KODE = c.STD_OBAT
                where KDBRG = '".$kdbrg."' and KDDEBT = '".$kddeb."'");
        if($sql->num_rows() > 0){
            $res['status'] = 'S';
            $res['message'] = 'OK';
        }else{
            $res['status'] = 'E';
            $res['message'] = 'Obat ini di luar Standart, apakah mau melanjutkan memakai obat ini ?';
        }
        return $res;
    }
    public function cekSisaObat($kdbrg, $kddeb, $rm){
        $res = [];
        // var_dump($kdbrg, $kddeb);die();

        $sql = $this->db->query("EXEC if_sp_baru_tampilkan_notifikasi_obat_sisa '$kdbrg', '$kddeb', '$rm'");
        if($sql->num_rows() > 0){
            $res['status'] = 'E';
            $res['message'] = 'Obat ini Masih ada sisa, apakah mau melanjutkan memakai obat ini ?';
        }else{
            $res['status'] = 'S';
            $res['message'] = 'OK';
            
        }
        return $res;
    }
    public function get_nomor_resep_model($date_now)
    {
        $query  = $this->db->query("exec if_sp_baru_generate_nomor_resep_manual '$date_now'");
        return $query->result();
    }
    public function get_kode_trans_model()
    {
        $query  = $this->db->query("exec if_sp_generate_kode_transaksi_resep_manual");
        return $query->result();
    }
    public function save_model($data)
    {
        // var_dump($data['header'][0]['KDMUT']);die();
        $idtrans = $this->get_kode_trans_model();
        $nomor = $this->get_nomor_resep_model(date("Y/m/d"));
        // print($nomor[0]->NO_RESEP." - ".$idtrans[0]->KDTRANS);
        // var_dump($data['header'][0]['KDMUT']);die();
        if($data['obat_asli'] == ''){
            print('no one');
        }else{
            print('one');
        }
        
        var_dump('--');die();
        $sql = $this->db->query("INSERT INTO IF_HTRANS(ID_TRANS,KDMUT,NOTA,TGL,TGLSHIFT,NOMOR,MUTASI,BAYAR,KDOT,STPAS,KDDEB,NMDEB,KDDIN,NMDIN,KDKLIN,NMKLIN,NORM,NMPX,NMKK,KDDOK,NMDOK,STS,JAM,JAGA,TIPEIF,LOKASI,IDXPX,ANTRIAN,RMINDUK,TAGIHINAP,ALMTPX,KOTAPX,NOPESERTA,NOKARTU,SURAT_JAMINAN,TELP_PX,TAGIHIFJALAN,IDUNIT) 
                VALUES ('".$idtrans."','" . $data['header'][0]['KDMUT'] . "','00000','" . $data['header'][0]['TGL'] . "','".$data['header'][0]['TGL_SHIFT']."','".$nomor."','".$data['header'][0]['MUTASI']."','".$data['header'][0]['BAYAR']."','".$data['header'][0]['KDOT']."','".$data['header'][0]['STPAS']."','".$data['header'][0]['KDDEB']."','".$data['header'][0]['NMDEB']."','".$data['header'][0]['KDDIN']."','".$data['header'][0]['NMDIN']."','".$data['header'][0]['KDKLIN']."','".$data['header'][0]['NMKLIN']."','".$data['header'][0]['NORM']."','".$data['header'][0]['NMPX']."','".$data['header'][0]['NMKK']."','".$data['header'][0]['KDDOK']."','".$data['header'][0]['NMDOK']."','".$data['header'][0]['STS']."','".$data['header'][0]['JAM']."','".$data['header'][0]['JAGA']."','".$data['header'][0]['TIPEIF']."','".$data['header'][0]['LOKASI']."','".$data['header'][0]['IDXPX']."','".$data['header'][0]['ANTRIAN']."','".$data['header'][0]['RMINDUK']."','".$data['header'][0]['TAGIHINAP']."','".$data['header'][0]['ALMTPX']."','".$data['header'][0]['KOTAPX']."','".$data['header'][0]['NOPESERTA']."','".$data['header'][0]['NOKARTU']."','".$data['header'][0]['SURAT_JAMINAN']."','".$data['header'][0]['TELP_PX']."','".$data['header'][0]['TAGIHIFJALAN']."','".$data['header'][0]['IDUNIT']."')");

        foreach ($data['obat_asli'] as $obat_asli) {
            $sql = $this->db->query("INSERT INTO IF_TRANS(ID_TRANS,NO,ID,KDBRG,HARGA,HBIJI,HJUAL,JUMLAH,SIGNA,SIGNA2,HARI,JASA,QTYDR,KETQTY,KDDEBI,SIGNACPT,ACTIVE,INPUTBY,INPUTDATE) 
                VALUES ('A20110101/000001','" . $obat_asli['NOURUT'] . "','" . $obat_asli['ID'] . "','" . $obat_asli['KDBRG'] . "','".$obat_asli['HARGA']."','".$obat_asli['HBIJI']."','".$obat_asli['HJUAL']."','".$obat_asli['JUMLAH']."','".$obat_asli['SIGNA']."','".$obat_asli['SIGNA2']."','".$obat_asli['HARI']."','".$obat_asli['JASA']."','".$obat_asli['QTYDR']."','".$obat_asli['KETQTY']."','".$obat_asli['KDDEBI']."','".$obat_asli['SIGNACPT']."','1','aku','2019-10-31')");
        }
        if($sql == true){
            print('gud job');
        }
    }
}
