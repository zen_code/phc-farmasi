<?php
defined('BASEPATH') or exit('No direct script access allowed');

?>
<style>
    .is_head {
        background: #2860a8;
        color: white;
    }
</style>

<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
        <input type="hidden" value="<?php echo base_url(); ?>" id="url">
        <!-- begin:: Subheader -->
        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        Proses Resep Online </h3>
                </div>
                <div class="">
                <label class="col-form-label" style="color:royalblue">
                    Date Sistem :&ensp;
                </label>
                <label id='date' class="col-form-label" style="color:#781400">
                </label>&ensp;
                <label id='jam' class="col-form-label" style="color:#781400">
                </label>&ensp;
                <label class="col-form-label" style="color:royalblue">
                    Shift :&ensp;
                </label>
                <label id='shift' class="col-form-label" style="color:#781400">
                </label>
                </div>
                <!-- kt-subheader__toolbar -->

            </div>
        </div>

        <!-- end:: Subheader -->

        <!-- begin:: Content -->
        <div class="kt-container  kt-grid__item kt-grid__item--fluid">

            <!--Begin::Dashboard 1-->

            <!--Begin::Row-->
            <div class="row">

                <div class="col-xl-12">
                    <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile ">
                        <!-- <div class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                Farmasi Klinis
                                </h3>
                            </div>
                        </div> -->
                        <div class="kt-portlet__body kt-portlet__body--fit">
                            <!--begin: Datatable -->
                            <div class="kt-portlet__body">
                                <button class="btn btn-info col-md-2" type="button" id="search_resep" data-toggle="kt-tooltip" data-placement="top"><i class="fa fa-search"></i> Search Px</button>

                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="kt-portlet">
                                        <div class="kt-portlet__head">
                                            <div class="kt-portlet__head-label">
                                                <h3 class="kt-portlet__head-title">
                                                    Biodata Pasien
                                                </h3>
                                            </div>
                                        </div>
                                        <div class="kt-portlet__body">
                                            <div class="row">
                                                <div class="form-group col-md-4">
                                                    <input type="hidden" id="kode_trans" value="0">
                                                    <input type="hidden" id="no_peserta" value="0">
                                                    <!-- hidden data -->
                                                    <label>Nomor</label>
                                                    <input type="text" class="form-control inputan" value="" disabled id="nomor">
                                                    <input type="hidden" class="form-control inputan" value="" id="id_trans_ol">

                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label>Nota</label>
                                                    <input type="text" class="form-control inputan" value="" disabled id="nota">
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label>Antrian</label>
                                                    <input type="text" class="form-control inputan" value="" disabled id="atrian">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-md-12">
                                                    <label>Nama</label>
                                                    <input type="text" class="form-control  inputan" value="" disabled id="name">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-md-6">
                                                    <label>Alamat</label>
                                                    <textarea class="form-control col-md-12 dis TabOnEnter" tabindex="3" disabled id="address" cols="35" rows="5"></textarea>
                                                </div>
                                                <div class="form-group col-md-6">

                                                    <label class="col-md-12">Kota</label>
                                                    <input type="text" class="form-control inputan" value="" disabled id="city">
                                                    <label class="col-md-12 col-form-label">Alergi</label>
                                                    <input type="text" class="form-control inputan " value="" disabled id="alergi">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-md-12">

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="col-md-12" style="padding:0">
                                        <div class="kt-portlet">
                                            <div class="kt-portlet__head">
                                                <div class="kt-portlet__head-label">
                                                    <h3 class="kt-portlet__head-title">
                                                        Debitur dan Kamar Pasien
                                                    </h3>
                                                </div>
                                            </div>
                                            <div class="kt-portlet__body">
                                                <div class="row">
                                                    <div class="form-group col-md-4">
                                                        <label>Jenis Px</label>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <input type="text" class="form-control inputan" value="" disabled id="jenis_px">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-8">
                                                        <label>RM / Index</label>
                                                        <div class="row">
                                                            <div class="col-md-8">
                                                                <input type="text" class="form-control inputan" value="" disabled id="rm">
                                                            </div>
                                                            <div class="col-md-4">
                                                                <input type="text" class="form-control inputan" value="" disabled id="indexs">
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-md-6">
                                                        <label>Debitur</label>
                                                        <input type="text" class="form-control inputan" value="" disabled id="kddeb">
                                                        <input type="hidden" class="form-control inputan" value="" disabled id="kddeb_hide">
                                                        <input type="hidden" class="form-control inputan" value="" disabled id="nmdeb_hide">

                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label>Dinas</label>
                                                        <input type="text" class="form-control inputan" value="" disabled id="kddin">
                                                        <input type="hidden" class="form-control inputan" value="" disabled id="kddin_hide">
                                                        <input type="hidden" class="form-control inputan" value="" disabled id="nmdin_hide">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6" style="">
                                            <div class="kt-portlet">
                                                <div class="kt-portlet__head">
                                                    <div class="kt-portlet__head-label">
                                                        <h3 class="kt-portlet__head-title">
                                                            Telaah Resep
                                                        </h3>
                                                    </div>
                                                </div>
                                                <div class="kt-portlet__body">
                                                    <div class="row">
                                                        <div class="form-group col-md-12">
                                                            <label>DRP</label>
                                                            <input type="text" class="form-control inputan" value="" disabled id="drp">
                                                        </div>

                                                    </div>

                                                    <div class="row">
                                                        <div class="form-group col-md-12">
                                                            <label>Tindak Lanjut</label>
                                                            <input type="text" class="form-control inputan" value="" disabled id="tindak_lanjut">

                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6" style="">
                                            <div class="kt-portlet">
                                                <div class="kt-portlet__head">
                                                    <div class="kt-portlet__head-label">
                                                        <h3 class="kt-portlet__head-title">
                                                            Plafon
                                                        </h3>
                                                    </div>
                                                </div>
                                                <div class="kt-portlet__body">
                                                    <div class="row">
                                                        <div class="form-group col-md-12">
                                                            <label>Plafon</label>
                                                            <input type="text" class="form-control inputan" value="" disabled id="plafon">
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group col-md-12">
                                                            <label>Biaya Bulan ini</label>
                                                            <input type="text" class="form-control inputan" value="" disabled id="biaya_bulan">
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="kt-portlet">
                                        <div class="kt-portlet__head">
                                            <div class="kt-portlet__head-label">
                                                <h3 class="kt-portlet__head-title">
                                                    Info Klinik / RP
                                                </h3>
                                            </div>
                                        </div>
                                        <div class="kt-portlet__body">
                                            <div class="row">
                                                <div class="form-group col-md-3">
                                                    <label>Klinik</label>
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <input type="text" class="form-control inputan" value="" disabled id="kdklin">
                                                        </div>
                                                        <div class="col-md-8">
                                                            <input type="text" class="form-control inputan" value="" disabled id="nmklin">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-3">


                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <label>User</label>
                                                            <input type="text" class="form-control inputan" value="" disabled id="user">
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label>Pengiriman</label>
                                                            <select class="form-control" id="pengiriman">
                                                                <option>Diambil Pasien</option>
                                                                <option>Halodoc</option>
                                                                <option>Farmasi</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <label>Dokter</label>
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <input type="text" class="form-control inputan" value="" disabled id="kddok">
                                                        </div>
                                                        <div class="col-md-8">
                                                            <input type="text" class="form-control inputan" value="" disabled id="nmdok">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-2">
                                                    <label>Jenis Resep</label>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <input type="text" class="form-control inputan" value="" disabled id="kdresep">
                                                        </div>
                                                        <div class="col-md-6">
                                                            <input type="text" class="form-control inputan" value="" disabled id="nmresep">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-1">
                                                    <label>Iter</label>
                                                    <input type="text" class="form-control inputan" value="" id="iter">

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="kt-portlet">
                                        <div class="kt-portlet__head">
                                            <div class="kt-portlet__head-label">
                                                <h3 class="kt-portlet__head-title">
                                                    Detail
                                                </h3>
                                            </div>
                                        </div>
                                        <div class="kt-portlet__body">
                                            <div class='table-responsive'>
                                                <table id="table-list_obat" class="table table-striped- table-hover table-checkable">
                                                    <thead>
                                                        <tr style="background: #2860a8; color:white; text-align:center">
                                                            <th style="padding-left:10px;padding-right:10px;">NO.</th>
                                                            <th style="padding-left:20px;padding-right:20px;">ID</th>
                                                            <th style="padding-left:100px;padding-right:100px;">NAMA BARANG </th>
                                                            <th style="padding-left:50px;padding-right:50px;">INFO SIGNA</th>
                                                            <th style="padding-left:20px;padding-right:20px;">QTY</th>
                                                            <th style="padding-left:20px;padding-right:20px;">SAT. DOSIS</th>
                                                            <th style="padding-left:20px;padding-right:20px;">QTY AK</th>
                                                            <th style="padding-left:20px;padding-right:20px;">UPD STOK</th>
                                                            <th style="padding-left:50px;padding-right:50px;">KETERANGAN</th>
                                                            <th>CEK</th>
                                                            <th style="padding-left:30px;padding-right:30px;">KODE</th>
                                                            <th style="padding-left:100px;padding-right:100px;">NAMA BARANG</th>
                                                            <th style="padding-left:20px;padding-right:20px;">SATUAN</th>
                                                            <th style="padding-left:50px;padding-right:50px;">SIGNA</th>
                                                            <th style="padding-left:50px;padding-right:50px;">HARGA</th>
                                                            <th style="padding-left:20px;padding-right:20px;">QTY</th>
                                                            <th>SPLIT</th>
                                                            <th style="padding-left:20px;padding-right:20px;">QTY</th>
                                                            <th style="padding-left:50px;padding-right:50px;">DEB</th>
                                                            <th>TUNAI</th>
                                                            <th style="padding-left:20px;padding-right:20px;">QTY</th>
                                                            <th style="padding-left:20px;padding-right:20px;">AK RACIKAN</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr class="no-data">
                                                            <td colspan="99" style="text-align:center">NO DATA</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div style="padding-top: 20px;">
                                                <!-- <div style="float:right" class="col-md-12 row">
                                                    <div class=" col-md-2"></div>
                                                    <div class=" col-md-3">
                                                        <div class="form-group m-form__group row">
                                                            <label class="col-md-4 col-form-label">
                                                                Sub Total :
                                                            </label>
                                                            <div class="col-md-8">
                                                                <input name="" disabled style="text-align:right" type="number" class="form-control m-input" id="subtotal" value="0">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group m-form__group row">
                                                            <label class="col-md-4 col-form-label">
                                                                Jasa Resep :
                                                            </label>
                                                            <div class="col-md-8">
                                                                <input name="" disabled type="number" style="text-align:right" class="form-control m-input" id="total_jasa_r" value="0">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group m-form__group row">
                                                            <label class="col-md-4 col-form-label">
                                                                TOTAL :
                                                            </label>
                                                            <div class="col-md-8">
                                                                <input name="" disabled type="number" style="text-align:right" class="form-control m-input" id="total_harga" value="0">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div> -->
                                                <div>
                                                    <div style="float:right" class="col-md-3">
                                                        <div class="form-group m-form__group row">
                                                            <label class="col-md-4 col-form-label">
                                                                Sub Total :
                                                            </label>
                                                            <div class="col-md-8">
                                                                <input name="" disabled style="text-align:right" type="number" class="form-control m-input" id="subtotal" value="0">
                                                            </div>
                                                        </div>
                                                        <div class="form-group m-form__group row">
                                                            <label class="col-md-4 col-form-label">
                                                                Jasa Resep :
                                                            </label>
                                                            <div class="col-md-8">
                                                                <input name="" disabled type="number" style="text-align:right" class="form-control m-input" id="total_jasa_r" value="0">
                                                            </div>
                                                        </div>
                                                        <div class="form-group m-form__group row">
                                                            <label class="col-md-4 col-form-label">
                                                                TOTAL :
                                                            </label>
                                                            <div class="col-md-8">
                                                                <input name="" disabled type="number" style="text-align:right" class="form-control m-input" id="total_harga" value="0">
                                                                <input type="hidden" id="jasa_r_spc1" value="0">
                                                                <input type="hidden" id="subtotal_spc1" value="0">
                                                                <input type="hidden" id="jasa_r_spc2" value="0">
                                                                <input type="hidden" id="subtotal_spc2" value="0">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div style="padding-right: 10px;">
                                                <div style="text-align: center; float:right;">
                                                    <button class="btn btn-success2" id="resep_dokter" type="button"><i class="fa fa-file"></i> Cetak Resep Dokter</button>
                                                    <button class="btn btn-success2" id="simpan_all" type="button"><i class="fa fa-check"></i> Simpan dan Cetak Nota</button>
                                                    <button class="btn btn-danger2" id="btl-resep" type="button"><i class="fa fa-undo"></i> Batal Antrain Resep</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>


                            </div>
                        </div>
                    </div>
                </div>

                <!--End::Row-->



                <!--End::Dashboard 1-->
            </div>

            <!-- end:: Content -->
        </div>
    </div>
</div>
<!-- Modal 1 -->
<div class="modal fade" tabindex="-1" role="dialog" id="modal-review">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content ">
            <div class="modal-header">
                <h5 class="modal-title"> Resep Online</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="m-form__group row">
                    <label class="col-md-2 col-form-label" style="text-align:center">
                        Search :
                    </label>
                    <div class="col-md-4">
                        <input name="" type="text" class="form-control m-input dis " id="search" placeholder="">
                        <!-- numbers -->
                    </div>
                </div>
                <div style="overflow-x:auto;">
                    <table id="table-pasien" class="table table-striped- table-hover table-checkable">
                        <thead>
                            <tr style="background: #2860a8; color:white;text-align:center">
                                <th style=" color:white;">ANTRIAN</th>
                                <th style=" color:white;">KARCIS</th>
                                <th style=" color:white;">TGL</th>
                                <th style=" color:white;">RM</th>
                                <th style=" color:white;">NAMA</th>
                                <th style=" color:white;">DEBITUR</th>
                                <th style=" color:white;">KLINIK</th>
                                <th style=" color:white;">DOKTER</th>
                                <th></th>
                            </tr>
                        </thead>
                        <!-- <tfoot>
                            <tr>
                                <th style=" color:white;">ANTRIAN</th>  
                                <th style=" color:white;">KARCIS</th>  
                                <th style=" color:white;">TGL</th>  
                                <th style=" color:white;">RM</th>  
                                <th style=" color:white;">NAMA</th>  
                                <th style=" color:white;">DEBITUR</th>  
                                <th style=" color:white;">KLINIK</th>  
                                <th style=" color:white;">DOKTER</th>  
                                <th></th>  
                            </tr>
                        </tfoot> -->
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
<!-- Modal 2 -->
<div class="modal fade" tabindex="-1" role="dialog" id="modal-obat">
    <div class="modal-dialog" role="document">
        <div class="modal-content ">
            <div class="modal-header">
                <h5 class="modal-title">List Obat</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <table id="table-obat" class="table table-striped- table-hover table-checkable">
                        <thead>
                            <tr style="background: #2860a8; color:white;text-align:center">

                                <th style=" color:white;">KODE</th>
                                <th style=" color:white;">NAMA BARANG</th>
                                <th style=" color:white;">SATUAN</th>
                                <th width="5%"></th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- Modal 3 -->
<div class="modal fade" id="data4send" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">

      <div class="modal-header">
        <h5 class="modal-title"><span class="tag_type"></span>Data Pengiriman</h5>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <label class="control-label col-md-4">Penerima</label>
          <div class="col-md-12">
            <input id="penerima_kirim" class="form-control" type="text">
          </div>
          <label class="control-label col-md-4">Alamat</label>
          <div class="col-md-12">
            <input id="alamat_kirim" class="form-control" type="text">
          </div>
          <label class="control-label col-md-4">Kota</label>
          <div class="col-md-12">
            <input id="kota_kirim" class="form-control" type="text">
          </div>
          <label class="control-label col-md-4">Telp/Hp</label>
          <div class="col-md-12">
            <input id="telp_kirim" class="form-control" type="text">
          </div>
          <label class="control-label col-md-4">Telp/Hp</label>
          <div class="col-md-12">
            <input id="telp2_kirim" class="form-control" type="text">
          </div>
          <label class="control-label col-md-4">Biaya</label>
          <div class="col-md-12">
            <input id="biaya_kirim" class="numbers form-control" value="0" type="number">
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" id="save_pengirim" class="btn btn-primary">Simpan</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal 4 -->
<div class="modal fade" tabindex="-1" role="dialog" id="cara_bayar">
  <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><span class="tag_type"></span>Cara Bayar</h5>
      </div>
      <div class="modal-body">
        <div class="">
          <div class="form-group row">
            <label class="col-md-3 col-form-label" for="pwd">Cara Bayar : </label>
            <div class="form-group col-md-8 col-sm-6" style="padding:0px; margin:0px;">
              <select class="form-control" id="pilih_cara_bayar">
                <option></option>
              </select>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 col-form-label" for="pwd">Total : </label>
            <input class="numbers col-md-8 col-sm-6 form-control m-input" type="number" id="totalbyr">
          </div>
          <div class="form-group row">
            <label class="col-md-3 col-form-label" for="pwd">Bayar (Cash + CC) :</label>
            <input class="numbers col-md-8 col-sm-6 form-control m-input" type="number" id="bayarbyr">
          </div>
          <div class="form-group row">
            <label class="col-md-3 col-form-label" for="pwd">Kembali :</label>
            <input class="numbers col-md-8 col-sm-6 form-control m-input" type="number" id="kembalibyr">
          </div>
          <div style="display:none" id="credit_detail">
            <div class="form-group row">
              <label class="col-md-12 col-form-label" style="text-align:center">
                ---------------------------------------------------------------------------------------------</label>
            </div>
            <div class="form-group row">
              <label class="col-md-3 col-form-label" for="pwd">*Tipe Kartu : </label>
              <div class="form-group col-md-8 col-sm-6" style="padding:0px; margin:0px;">
                <select class="form-control" id="tipe_kartu">
                  <option></option>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-3 col-form-label" for="pwd">No. Kartu :</label>
              <input class="spcnumbers col-md-8 col-sm-6 form-control m-input" type="number" id="no_kartu">
            </div>
            <div class="form-group row">
              <label class="col-md-3 col-form-label" for="pwd">**No. Trace : </label>
              <input class="spcnumbers col-md-8 col-sm-6 form-control m-input" type="number" id="no_trace">
            </div>
            <div class="form-group row">
              <label class="col-md-3 col-form-label" for="pwd">No. Batch :</label>
              <input class="spcnumbers col-md-8 col-sm-6 form-control m-input" type="number" id="no_batch">
            </div>
            <div class="form-group row">
              <label class="col-md-3 col-form-label" for="pwd">Nominal :</label>
              <input class="numbers col-md-8 col-sm-6 form-control m-input" type="number" id="nominal">
            </div>
            <div class="form-group row">
              <label class="col-md-6 col-form-label" style="color:royalblue; text-align:right">*Tipe Kartu = Cardtype pada struk EDC</label>
              <label class="col-md-6 col-form-label" style="color:brown">**No. Trace = Trace number pada struk EDC</label>
            </div>
            <div class="form-group row" style="float:right;margin-right: 0px;">
              <button type="button" class="btn btn-success2" id="tambah_pembayaran"><span class="tag_type"></span>Tambah</button>
            </div>
            <table class="table table-striped- table-bordered table-hover table-checkable" id="tabel_pembayaran">
              <thead>
                <tr style="background:#2860a8;text-align: center;">
                  <th style="color:white;" width="25%">CARA BAYAR</th>
                  <th style="color:white;" width="20%">NO. KARTU</th>
                  <th style="color:white;" width="15%">NO. TRACE</th>
                  <th style="color:white;" width="15%">NO. BATCH</th>
                  <th style="color:white;" width="20%">NOMINAL</th>
                  <th style="color:white;" width="5%">ACTION</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="proses_bayar"><span class="tag_type"></span>Proses</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal 5 -->
<div class="modal fade" id="password-modal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">

      <div class="modal-header">
        <h5 class="modal-title"><span class="tag_type"></span>Masukkan Password</h5>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <label class="control-label col-md-4">Password<span style="color: red" class="required">*</span>
          </label>
          <div class="col-md-8">
            <input id="password" class="form-control" required="required" type="password">
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
        <button type="button" id="simpanPass" class="btn btn-primary">Simpan</button>
      </div>
    </div>
  </div>
</div>
<!-- javascript this page -->
<script src="<?php echo base_url() ?>assets/assets_metronic/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/assets_metronic/vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>application/modules/resep_online/views/resep_online_view.js" type="text/javascript"></script>
<!-- <script src="<?php echo base_url() ?>assets/assets_metronic/js/demo9/pages/dashboard.js" type="text/javascript"></script> -->

<script>

</script>