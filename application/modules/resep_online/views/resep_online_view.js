"use strict";

// Class definition
var ndeb1 = 0;
var ndeb2 = 0;
var ntunai = 0;
var carabayar;
var save_pembayaran_arr = [];
var save_pengiriman_arr = [];
var save_obat_asli_arr = [];
var save_obat_split_arr = [];
var save_obat_tunai_arr = [];
var save_header_arr = [];
var ResepOnline = function() {
    return {
        // Init demos
        init: function() {
            // dataDrp();
            // dataPasien();
            $('.datepicker ').datepicker({
                autoclose:true, 
                format: 'dd/mm/yyyy', 
                changeMonth: true,
                changeYear: true,
                todayHighlight: true,
            });
            // var d = new Date();
            // var day = d.getDate();
            // var strDate =  (day < 10 ? "0"+day : day)  + "/" +(d.getMonth()+1)  + "/" + d.getFullYear()  ;
            // $('#tgl_resep_online').val(strDate);

            getDataResep("");
            getDebitur();
            getSigna();
            setInputMask();
            // getObat();
            // addRow();
        },
    };
}();

// what I've done
window.setTimeout("waktu()", 1000);
var jam = "";
var menit = "";
var detik = "";
var tahun = "";
var bulan = "";
var tgl = "";
var tahun_shift = "";
var bulan_shift = "";
var tgl_shift = "";
var tahun_history = "";
var bulan_history = "";
var tgl_history = "";
// $("#user").val($("#user_all").val());
function waktu() {
  var tanggal = new Date();
  var namahari = [
    "Minggu",
    "Senin",
    "Selasa",
    "Rabu",
    "Kamis",
    "Jumat",
    "Sabtu"
  ];
  var namabulan = [
    "Januari",
    "Februari",
    "Maret",
    "April",
    "Mei",
    "Juni",
    "Juli",
    "Agustus",
    "September",
    "Oktober",
    "November",
    "Desember"
  ];
  jam = tanggal.getHours();
  menit = tanggal.getMinutes();
  detik = tanggal.getSeconds();
  tahun = tanggal.getFullYear();
  bulan = tanggal.getMonth() + 1;
  tgl = tanggal.getDate();
  var shift;
  if (jam >= 7 && jam < 14) {
    shift = 1;
    gfg_Run("normal", "shift");
    if (jam == 7 && menit == 0 && detik == 0) {
      shift = 3;
      gfg_Run("minus", "shift");
    }
  } else if (jam >= 14 && jam < 21) {
    shift = 2;
    gfg_Run("normal", "shift");
    if (jam == 14 && menit == 0 && detik == 0) {
      shift = 1;
      gfg_Run("normal", "shift");
    }
  } else {
    shift = 3;
    gfg_Run("normal", "shift");
    if (jam >= 0 && jam < 7) {
      gfg_Run("minus", "shift");
    }
    if (jam == 21 && menit == 0 && detik == 0) {
      shift = 2;
      gfg_Run("normal", "shift");
    }
  }
  setTimeout("waktu()", 1000);
  document.getElementById("date").innerHTML =
    namahari[tanggal.getDay()] +
    ", " +
    tanggal.getDate() +
    "-" +
    namabulan[tanggal.getMonth()] +
    "-" +
    tanggal.getFullYear();
  document.getElementById("jam").innerHTML = jam + ":" + menit + ":" + detik;
  document.getElementById("shift").innerHTML = shift;
}

Date.prototype.subtractDays = function(d) {
  this.setTime(this.getTime() - d * 24 * 60 * 60 * 1000);
  return this;
};
function gfg_Run(condition, condition2) {
  var a = new Date();
  if (condition2 == "shift") {
    if (condition == "minus") {
      a.subtractDays(1);
      tahun_shift = a.getFullYear();
      bulan_shift = a.getMonth() + 1;
      tgl_shift = a.getDate();
    } else if (condition == "normal") {
      tahun_shift = a.getFullYear();
      bulan_shift = a.getMonth() + 1;
      tgl_shift = a.getDate();
    }
  } else if (condition2 == "history") {
    if (condition == "minus") {
      a.subtractDays(90);
      tahun_history = a.getFullYear();
      bulan_history = a.getMonth() + 1;
      tgl_history = a.getDate();
    }
  }
}
// what I've done
var listdeb="";

var listsigna = "";
// var tablepasien = $('#table-pasien');
function getDataResep(text){
    $('#table-pasien').dataTable().fnClearTable();
    $('#table-pasien').dataTable().fnDestroy();
    // $.fn.dataTable.Api.register('column().title()', function() {
    //     return $(this.header()).text().trim();
    // });
    // $('#table-pasien tfoot th').each( function () {
    //     var title = $(this).text();
    //     switch (title) {
    //         case 'ANTRIAN':
    //         case 'KARCIS':
    //         case 'RM':
    //         case 'NAMA':
    //         case 'DEBITUR':
    //         case 'KLINIK':
    //         case 'DOKTER':
    //             $(this).html( '<input  placeholder="Search '+title+'" type="text" class="form-control form-control-sm form-filter kt-input"/>' );
    //             break;
    //         case 'TGL':
    //             $(this).html( `<div class="input-group date">
    //                             <input type="text" class="form-control form-control-sm kt-input" placeholder="From" id="kt_datepicker_1" />
                               
    //                         </div>` );
    //             break;
    //     }
    //     $('#kt_datepicker_1').datepicker({autoclose:true, format: 'dd/mm/yyyy'});
    //     // input = $(`<input type="text" class="form-control form-control-sm form-filter kt-input" data-col-index="` + column.index() + `"/>`);
    //     //                 
    //     // $(this).html( '<input type="text" class="form-control form-control-sm form-filter kt-input"/>' );
    // } );
    var table = $('#table-pasien').DataTable({
        responsive: true,
        dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
        lengthMenu: [5, 10, 25, 50],
        pageLength: 5,
        language: {
            'lengthMenu': 'Display _MENU_',
        },
        searchDelay: 500,
        ordering: true,
        processing: true,
        serverSide: false,
        ajax: {
            url: 'resep_online/dataResepOnline',
            type: 'POST',
            data: {
                // columnsDef: ['ANTRIAN','KARCIS', 'RM', 'TGL', 'NAMA', 'DEBITUR','KLINIK','DOKTER','btn'],
                text : text
            },
        },
         
        columns       : [
            {data : "ANTRIAN"},
            {data : "KARCIS"},
            {data : "TGL"},
            {data : "RM"},
            {data : "NAMA"},
            {data : "DEBITUR"},
            {data : "KLINIK"},
            {data : "DOKTER"},
            {data : "btn"}
        ],
        "drawCallback": function( settings ) {
            $('[data-toggle="kt-tooltip"]').tooltip();
        },
       
    });
 
    // Apply the search
    // table.columns().every( function () {
    //     var that = this;
    //     $( 'input', this.footer() ).on( 'keyup change clear', function () {
    //         if ( that.search() !== this.value ) {
    //             that
    //                 .search( this.value )
    //                 .draw();
    //         }
    //     });
    // });
}

var clickedLine = 0;
var dataobat = 0;

// $('#tgl_resep_online').change(function(){
//     var dates = $('#tgl_resep_online').val();
//     getDataResep(dates);
// });
$('#search').on('keyup', function(evt){
    // console.log( $(this).val()); 
    var text = $(this).val();
    if (evt.key === 'Enter') {
        getDataResep(text);
        console.log(); 
    }
})
$('#search_resep').click(function(){
    $('#modal-review').modal('show')
 });
function detailResep(id_trans){
    loading();
    $('#modal-review').modal('hide')
    $.ajax({
        type: "GET",
        url: "resep_online/detailResep", 
        data:{id_trans_ol : id_trans},
        dataType: 'json',
        success: function (data) {
            // console.log(data);return false;
            if(data.status == 'success'){
                var header = data.message.header[0];
                $('#nota').val(header.NOTA);
                $('#id_trans_ol').val(header.ID_TRANS);
                $('#atrian').val(header.NOANTRIAN);
                $('#name').val(header.NAMA);
                $('#address').val(header.ALAMAT);
                $('#city').val(header.NM_KABKOTA);
                $('#alergi').val();
                $('#jenis_px').val(header.KDDEBT == '999' ? 'Tunai' : 'Kredit');
                $('#rm').val(header.NORM);
                $('#indexs').val(header.IDXPX);
                $('#kddeb').val(header.KDDEBT + " - "+ header.NMDEBT);
                $('#kddeb_hide').val(header.KDDEBT);
                $('#nmdeb_hide').val(header.NMDEBT);
                $('#kddin').val(header.KDDINAS+ " - "+ header.NMDINAS);
                $('#kddin_hide').val(header.KDDINAS);
                $('#nmdin_hide').val(header.NMDINAS);
                // $('#nmdin').val(header.NMDINAS);
                $('#kdklin').val(header.kdKlin);
                $('#nmklin').val(header.nmKlin);
                $('#user').val(header.INPUTBY);
                $('#kddok').val(header.kdDok);
                $('#nmdok').val(header.nmDok);
                $('#kdresep').val(header.kdot);
                $('#nmresep').val(header.MNAMA);
                $('#iter').val();
                $('#drp').val(header.DRP);
                $('#tindak_lanjut').val(header.TINDAK_LANJUT);
                $('#plafon').val();
                $('#biaya_bulan').val();
                setDetail(data.message.detail, header.KDDEBT);
                prosesResep(header.ID_TRANS,1);
                // KTApp.unblockPage();

            }else{
                $('.inputan').val('');
                Swal.fire(data.status, data.message, data.status);
                $('#table-list_obat tbody').html('');
                KTApp.unblockPage();
            }
            
        },
        error: function (xhr,status,error) {

        }
    });
}
function loading(){
    KTApp.blockPage({
        overlayColor: '#000000',
        type: 'v2',
        state: 'primary',
        message: 'Processing...'
    });
}
var i = 1;
function addRow(val_signa){
    var tr_nodata = $('#table-list_obat tbody tr.no-data');

    if(tr_nodata.length > 0 ){
        tr_nodata.remove()
    }
    var a ="";
    listdeb = buildList('list_deb', datadeb, i, 'disabled');
    // if(val_signa != null && val_signa != ''){
        
    // }
    listsigna = buildList('list_signa', datasigna, i, 'disabled');
    a = '<tr data-index = "'+i+'">'+
            
            '<td style="text-align: center;vertical-align: middle;"></td>' +
            '<td><input type="text" class="form-control" id="id" disabled>'+
                '<input type="hidden" id="signa_racik">'+
                '<input type="hidden" id="harga_hide">'+
                '<input type="hidden" id="hbiji_hide">'+
                '<input type="hidden" id="hjual_hide">'+
                '<input type="hidden" id="hari_hide">'+
                '<input type="hidden" id="jasa_r_hide">'+
                '<input type="hidden" id="kdbrg_hide">'+
                '<input type="hidden" id="nmbrg_hide">'+
                '<input type="hidden" id="satuan_hide">'+
            '</td>' +
            '<td><input type="text" class="form-control" id="nmbrg" disabled></td>' +
            '<td><input type="text" class="form-control" id="signa2" disabled></td>' +
            '<td><input type="text" class="form-control numbers" id="qty" disabled></td>' +
            '<td><input type="text" class="form-control" id="sat_dosis" disabled></td>' +
            '<td><input type="text" class="form-control" id="qty_ak" disabled></td>' +
            '<td><input type="text" class="form-control" id="upd_stok" disabled></td>' +
            '<td><input type="text" class="form-control" id="keterangan" disabled></td>' +
            '<td style="vertical-align: middle;text-align: center;">'+input_type("checkbox","cek", 0, '')+'</td>' +
            '<td><input type="text" class="form-control hide_show"  id="kdbrg_cek" disabled></td>' +
            '<td><input type="text" class="form-control hide_show" id="nmbrg_cek" disabled></td>' +
            '<td><input type="text" class="form-control hide_show racik" id="satuan" disabled></td>' +
            '<td>'+listsigna+'</td>' +
            '<td><input type="text" class="form-control numbers hide_show racik" id="harga" disabled></td>' +
            '<td><input type="text" class="form-control hide_show racik" id="qty2" disabled></td>' +
            '<td style="vertical-align: middle;text-align: center;">'+input_type("checkbox","split", 0, 'hide_show racik')+'</td>' +
            '<td><input type="text" class="form-control numbers hide_show racik" id="qty_split" value="0" disabled>'+
                '<input type="hidden" id="harga_split_hide" value="0">'+
            '</td>' +
            '<td>'+listdeb+'</td>' +
            '<td style="vertical-align: middle;text-align: center;">'+input_type("checkbox","tunai", 0, 'hide_show racik')+'</td>' +
            '<td><input type="text" class="form-control numbers hide_show racik" id="qty_tunai" value="0" disabled>'+
                '<input type="hidden" id="harga_tunai_hide" value="0">'+
            '</td>' +
            '<td><input type="text" class="form-control hide_show racik" id="ak_racikan"></td>' +
            
        '</tr>';
    $('#table-list_obat tbody').append(a);
    
    i++;
    renumber();
    setInputMask();
    $('.list').select2();
    $('.select2-container').addClass('hide_show x_sign');
    $('.hide_show').hide();
}
function renumber() {
    var index = 1;
    $('#table-list_obat tbody tr').each(function () {
        $(this).find('td').eq(0).html(index);//.find('.num_text')
        index++;
    });
}
function input_type(type, param, val, hide){
    var res = "";
    if(type == "date"){
        res = '<div class="input-group date">'+
                '<input type="text" class="form-control datepickers '+param+'" readonly placeholder="Pilih Tanggal" />'+
            '</div>' ;  
    }else if(type == "checkbox"){
        var checked = val == 1 ? 'checked' : '';
        res = "<label class='kt-checkbox "+hide+"' style='top:-4px' >"+
                "<input type='checkbox' id='"+param+"' "+checked+"> "+
                "<span></span>"+
            "</label>"
    }
    
    return res;
}
$()
var x = "";
$('#table-list_obat').on('change', 'tr #split', function () {
    x = $(this).closest('tr').data('index');
    var $tr = $('#table-list_obat > tbody').find('tr[data-index="'+x+'"]');
    var qty_split = $tr.find("#qty_split");
    var deb_split = $tr.find(".list_deb");
    var deb = $('#kddeb_hide').val();
    if($(this).is(':checked')){
        $(this).attr('value', 1);
        qty_split.prop('disabled', false);
        deb_split.prop('disabled', false);
    } else {
        $(this).attr('value', 0);
        $(this).closest('tr').find('#harga_split_hide').val(0);
        qty_split.prop('disabled', true);
        qty_split.val('0');
        deb_split.prop('disabled', true);
        deb_split.val(deb);
        deb_split.trigger('change');
    }
    count_subtotal();
})
$('#table-list_obat').on('change', 'tr #tunai', function () {
    x = $(this).closest('tr').data('index');
    var $tr = $('#table-list_obat > tbody').find('tr[data-index="'+x+'"]');
    var qty_tunai = $tr.find("#qty_tunai");
    if($(this).is(':checked')){
        $(this).attr('value', 1);
        qty_tunai.prop('disabled', false);
    } else {
        $(this).attr('value', 0);
        $(this).closest('tr').find('#harga_tunai_hide').val(0);
        qty_tunai.prop('disabled', true);
        qty_tunai.val('0');
    }
    count_subtotal();
});
$("#table-list_obat").on('input', 'tr #qty_split', function(){
    var $tr = $(this).closest('tr');
    var qty = $tr.find('#qty').val();
    var qty_split = $tr.find('#qty_split').val();
    var qty2 = qty - qty_split;
    var harga = $tr.find('#harga').val();
    var h_biji = harga / qty;
    $tr.find('#harga_split_hide').val(qty_split*h_biji);
    $tr.find('#qty2').val(qty2);
    count_subtotal();
});
$("#table-list_obat").on('input', 'tr #qty_tunai', function(){
    var $tr = $(this).closest('tr');
    var qty = $tr.find('#qty').val();
    var qty_split = $tr.find('#qty_split').val();
    var qty_tunai = $tr.find('#qty_tunai').val();
    var qty2 = qty - qty_split - qty_tunai;
    var harga = $tr.find('#harga').val();
    var h_biji = harga / qty;
    $tr.find('#harga_tunai_hide').val(qty_tunai*h_biji);
    $tr.find('#qty2').val(qty2);
    count_subtotal();
});
$('#table-list_obat').on('change', 'tr #cek', function () {
    x = $(this).closest('tr').data('index');
    var $tr = $('#table-list_obat > tbody').find('tr[data-index="'+x+'"]');
    var $sblm = $('#table-list_obat > tbody').find('tr[data-index="'+(x-1)+'"]');
    var $next = $('#table-list_obat > tbody').find('tr[data-index="'+(x+1)+'"]');
    var signa_racik = $tr.find("#signa_racik").val();
    var ids = $tr.find("#id").val();
    if($(this).is(':checked')){
        $tr.find(".list_signa").prop('disabled', false);
        $tr.find("#kdbrg_cek").prop('disabled', false);
        $tr.find("#nmbrg_cek").prop('disabled', false);
        $tr.find("#qty2").prop('disabled', false);
        $tr.find(".hide_show").show();
        
        if(signa_racik != '' && ids != '0'){
            $tr.find(".x_sign").hide();
            $tr.find("#kdbrg_cek").prop('disabled', true);
            $tr.find("#nmbrg_cek").prop('disabled', true); 
            $tr.find(".racik").hide(); 
            $sblm.find('#cek').attr("checked", true);
            $sblm.find('#cek').trigger("change");
            $sblm.find(".x_sign").show();
            $sblm.find('#ak_racikan').val($tr.find("#qty2").val());
        }
        if(signa_racik == '' && ids != '0'){
            $tr.find(".x_sign").hide();
        }
        
    } else {
        $tr.find(".list_signa").prop('disabled', true);
        $tr.find("#kdbrg_cek").prop('disabled', true);
        $tr.find("#nmbrg_cek").prop('disabled', true);
        $tr.find("#qty2").prop('disabled', true);
        $tr.find(".hide_show").hide();
        if(signa_racik != '' && ids != '0'){
            $tr.find(".x_sign").hide();
            $tr.find("#kdbrg_cek").prop('disabled', false);
            $tr.find("#nmbrg_cek").prop('disabled', false);
            $sblm.find('#cek').attr("checked", false);
            $sblm.find('#cek').trigger("change");
            $sblm.find(".x_sign").hide();
            $sblm.find('#ak_racikan').val($tr.find("#qty2").val());
        }
        if(signa_racik == '' && ids != '0'){
            $tr.find(".x_sign").hide();
        }
    }
    count_subtotal();
});
function count_subtotal() {
    ndeb1 = 0;
    ndeb2 = 0;
    ntunai = 0;
    var total = 0;
    var total_jasa_r = 0;
    var harga_total = 0;
    var jasa_r_spc1 = 0;
    var subtotal_spc1 = 0;
    var jasa_r_spc2 = 0;
    var subtotal_spc2 = 0;
    // kuning = 0;
    for (var i = 0; i < $("#table-list_obat > tbody > tr").length; i++) {
        var cekdeb1 = $("#table-list_obat tbody tr").eq(i).find('#cek').prop('checked');
        var cekdeb2 = $("#table-list_obat tbody tr").eq(i).find('#split').prop('checked');
        var cektunai = $("#table-list_obat tbody tr").eq(i).find('#tunai').prop('checked');
        if(cekdeb1){
            ndeb1++;
            total = parseFloat(total) + parseFloat($("#table-list_obat tbody").find("tr").eq(i).find("#harga").val());
            total_jasa_r = parseFloat(total_jasa_r) + parseFloat($("#table-list_obat tbody").find("tr").eq(i).find("#jasa_r_hide").val());
        }
        if(cekdeb1 && cekdeb2){
            ndeb2++;
            subtotal_spc1 = parseFloat(subtotal_spc1) + parseFloat($("#table-list_obat tbody").find("tr").eq(i).find("#harga_split_hide").val());
        }
        if(cekdeb1 && cekdeb2 && cektunai){
            ntunai++;
            subtotal_spc2 = parseFloat(subtotal_spc2) + parseFloat($("#table-list_obat tbody").find("tr").eq(i).find("#harga_tunai_hide").val());
        }
    }
    $("#subtotal").val(total);
    $("#total_jasa_r").val(total_jasa_r);
    $("#subtotal_spc1").val(subtotal_spc1);
    $("#jasa_r_spc1").val(jasa_r_spc1);
    $("#subtotal_spc2").val(subtotal_spc2);
    $("#jasa_r_spc2").val(jasa_r_spc2);
    harga_total = total + total_jasa_r;
    // harga_total = total;
    $("#total_harga").val(harga_total);
    // console.log(total);
}
function setInputMask(){
    $(".numbers").inputmask(
        {   
            'alias': 'decimal', 
            'groupSeparator': ',', 
            'autoGroup': true,
            'removeMaskOnSubmit': true,
            'rightAlign': true,
            autoUnmask: true,
            // groupSeparator: '.',
        }
    );
}
var datadeb = [];
function getDebitur(){
    $.ajax({
        type: "GET",
        url: "resep_online/getDebitur", 
        dataType: 'json',
        success: function (data) {
            datadeb = data;
            // addRow();
        },
        error: function (xhr,status,error) {

        }
    });
}
var datasigna = [];
function getSigna(){
    $.ajax({
        type: "GET",
        url: "resep_online/getSigna", 
        dataType: 'json',
        success: function (data) {
            datasigna = data;
            // addRow();
        },
        error: function (xhr,status,error) {

        }
    });
}
$('#table-list_obat').on('keydown', 'tr #kdbrg_cek ', function (evt) {
    x = $(this).closest('tr').data('index');
    var $tr = $('#table-list_obat > tbody').find('tr[data-index="'+x+'"]');
    var kdObat = $tr.find('#kdbrg_cek').val();

    if (evt.key === 'Enter') {
        listObat(kdObat,"");
    }
});
$('#table-list_obat').on('keydown', 'tr #nmbrg_cek ', function (evt) {
    x = $(this).closest('tr').data('index');
    var $tr = $('#table-list_obat > tbody').find('tr[data-index="'+x+'"]');
    var nmObat = $tr.find('#nmbrg_cek').val();

    if (evt.key === 'Enter') {
        listObat("",nmObat);
    }
});
function listObat(kdObat,nmObat){
    
    loading();
    $('#table-obat').DataTable( {
        "destroy": true,
        "processing" : true,
        "type": "POST",
        "ajax": 'resep_online/getObat?kdObat='+kdObat+'&nmObat='+nmObat+'&tipeif='+$('#depo_id').val(),
        "columns"       : [
            {"data" : "KDBRG"},
            {"data" : "NMBRG"},
            {"data" : "SATUAN"},
            {"data" : "btn"},
        ],
        
        "createdRow"    : function ( row, data, index ) {
            // $('td', row).eq(0).addClass('text-center');
           // $('td', row).eq(2).addClass('text-right');
        }, 
        "drawCallback": function( settings ) {
            $('[data-toggle="kt-tooltip"]').tooltip();
        },
        "initComplete" : function(){
            KTApp.unblockPage();
            $('#modal-obat').modal('show');
        }
    });

}
function pilih(kode, nama,satuan){
    var $tr = $('#table-list_obat > tbody').find('tr[data-index="'+x+'"]');
    loading();
    $('#modal-obat').modal('hide');
    
    $.ajax({
        type: "POST",
        url: "resep_online/getStandartObat", 
        dataType: 'json',
        data : {
            kdbrg : kode,
            kddeb : $('#kddeb').val()
        },
        success: function (data) {
            if(data.status == 'S'){
                cekSisaObat(kode, nama,satuan);
            }else{
                Swal.fire({
                    title: 'Standart Obat',
                    text: data.message,
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Ya',
                    cancelButtonText: 'Tidak'
                }).then((result) => {
                    if (result.value) {
                        //tunai
                        Swal.fire({
                            title: 'Tunai',
                            text: 'Mau ditunaikan ?? ',
                            type: 'question',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Ya',
                            cancelButtonText: 'Tidak'
                        }).then((result) => {
                            if (result.value) {
                                //ganti warna 
                                $tr.css("background-color", "#ffff99"); //kuning "rgb(255, 255, 153)"
                                $tr.find('#tunai').attr("checked", true);
                                $tr.find('#tunai').trigger("change");
                            }else{
                                
                            }
                            cekSisaObat(kode, nama,satuan);
                            
                        })

                    }else{
                        kode = $tr.find('#kdbrg_hide').val();
                        nama = $tr.find('#nmbrg_hide').val();
                        satuan = $tr.find('#satuan_hide').val();
                        cekSisaObat(kode, nama,satuan);
                    }
                   
                    
                })
            }
            
        },
        error: function (xhr,status,error) {

        }
    });
    
}
function cekSisaObat(kode,nama,satuan){
    // console.log(kode,nama,satuan)
    var $tr = $('#table-list_obat > tbody').find('tr[data-index="'+x+'"]');
    $.ajax({
        type: "POST",
        url: "resep_online/cekSisaObat", 
        dataType: 'json',
        data : {
            kdbrg : kode,       
            kddeb : $('#kddeb').val(),
            norm  : $('#rm').val(),
        },
        success: function (data) {
           
            if(data.status == 'S'){
                $tr.find('#kdbrg_cek').val(kode);
                $tr.find('#nmbrg_cek').val(nama);
                $tr.find('#satuan').val(satuan);
            }else{
                Swal.fire({
                    title: 'Sisa Obat',
                    text: data.message,
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Ya',
                    cancelButtonText: 'Tidak'
                }).then((result) => {
                    if (result.value) {
                        $tr.find('#kdbrg_cek').val(kode);
                        $tr.find('#nmbrg_cek').val(nama);
                        $tr.find('#satuan').val(satuan);
                        //ganti warna
                        $tr.css("background-color", "#ff9999"); //pink "rgb(255, 153, 153)"

                    }else{
                        $tr.find('#kdbrg_cek').val($tr.find('#kdbrg_hide').val());
                        $tr.find('#nmbrg_cek').val($tr.find('#nmbrg_hide').val());
                        $tr.find('#satuan').val($tr.find('#satuan_hide').val());
                    }
                    
                })
            }
            KTApp.unblockPage();
        },
        error: function (xhr,status,error) {

        }
    });
}
function buildList(name, data, index, dsb){
    var list = "<select class='form-control list "+name+"' id='"+name+"-"+index+"' "+dsb+" style='width:100%'><option> Silahkan Pilih</option>"+data+"</select>";
    
    return list;
}
function setDetail(data, deb){
    $('#table-list_obat tbody').html('');
    $.each( data, function( i, val ){
        console.log(val)
        addRow(val.SIGNA);
        var $tr = $('#table-list_obat > tbody').find('tr').last();
        $tr.find('#id').val(val.ID);
        $tr.find('#nmbrg').val(val.NAMABRGFULL);
        // $tr.find('#jenis').val(val.SIGNA2);
        $tr.find('#signa2').val(val.SIGNA2);
        $tr.find('#qty').val(val.JML_IF);
        $tr.find('#sat_dosis').val(val.SATDOSIS);
        $tr.find('#qty_ak').val(val.JML_IF2);
        $tr.find('#upd_stok').val(val.STOK_IF_AK);
        $tr.find('#keterangan').val(val.KETQTY);
        $tr.find('#harga').val(val.HJUAL);
        $tr.find('#qty2').val(val.JUMLAH);
        // $tr.find('#nm_brg').val(val.NAMABRGFULL);
        // $tr.find('#jenis').val('obat');
        // $tr.find('#signa').val(val.SIGNACPT);
        // $tr.find('#ak_racikan').val(val.TIPE_QTY);
        $tr.find('.list_deb').val(deb);
        $tr.find('.list_deb').trigger('change');
        if(val.SIGNA != null && val.SIGNA != ''){
            $tr.find('.list_signa').val(val.SIGNA+"  ");
            $tr.find('.list_signa').trigger('change');
        }
        
        $tr.find('#kdbrg_cek').val(val.KDBRG);
        $tr.find('#nmbrg_cek').val(val.NAMABRGFULL);
        $tr.find('#satuan').val(val.SATUAN);
        $tr.find('#harga_hide').val(val.HARGA);
        $tr.find('#hbiji_hide').val(val.HBIJI);
        $tr.find('#hjual_hide').val(val.HJUAL);
        $tr.find('#jasa_r_hide').val(val.JASA);
        $tr.find('#hari_hide').val(val.HARI);
        $tr.find('#signa_racik').val(val.SIGNARACIK);
        $tr.find('#kdbrg_hide').val(val.KDBRG);
        $tr.find('#nmbrg_hide').val(val.NAMABRGFULL);
        $tr.find('#satuan_hide').val(val.SATUAN);

        // $tr.find('.hide_show').hide();
        // console.log( $tr.find('.hide_show'));
    });
}
$('#btl-resep').click(function(){
    var ids = $('#id_trans_ol').val();
    if(ids != null && ids != ""){
        prosesResep(ids,0)
    }else{
        Swal.fire('error', 'Belum Memilih Resep', 'error')
    }
});
function prosesResep(id_trans, type){
    // console.log(id_trans, type);return false;
    loading();
    $.ajax({
        type: "POST",
        url: "resep_online/prosesResep", 
        data:{
            id_trans_ol : id_trans,
            type : type
        },
        dataType: 'json',
        success: function (data) {
            if(type == 0){
                Swal.fire(data.status, data.message, data.status);
                $('.inputan').val('');
                $('#modal-review').modal('hide');
            }
            getDataResep("");
            KTApp.unblockPage();
        },
        error: function (xhr,status,error) {

        }
    });
}
//////////////////////////////////////
function daftar_creditcard() {
    $.ajax({
      type: "GET",
      url: "entry_resep/card_type",
      data: {},
      dataType: "json",
  
      success: function(data) {
        // console.log(data);
        jQuery.each(data, function(index, val) {
          var a = "";
          a =
            "<option value='" +
            val.KDBANK +
            "' data-kartu='" +
            val.KARTU +
            "'><b>" +
            val.KARTU +
            "</b>" +
            "</option>";
          $("#tipe_kartu").append(a);
        });
      },
      error: function(xhr, status, error) {
        console.log(xhr, status, error);
      }
    });
    $("#tipe_kartu").select2({
      placeholder: "Pilih Cara Pembayaran",
      width: "100%",
      dropdownParent: $("#cara_bayar")
    });
}
function pilih_cara_bayar() {
    $("#pilih_cara_bayar").html("");
    $.ajax({
      type: "GET",
      url: "entry_resep/cara_bayar_ctrl",
      data: {
        kdmut: $("#depo_nm").val()
      },
      dataType: "json",
      success: function(data) {
        carabayar = data;
        jQuery.each(carabayar, function(index, val) {
          var a = "";
          a =
            "<option value='" +
            val.IDCRBAYAR +
            "' data-crbayar='" +
            val.STAT +
            "' data-ket='" +
            val.KET +
            "'><b>" +
            val.IDCRBAYAR +
            " - " +
            val.KET +
            "</b>" +
            "</option>";
          $("#pilih_cara_bayar").append(a);
        });
      },
      error: function(xhr, status, error) {
        console.log(xhr, status, error);
      }
    });
    $("#pilih_cara_bayar").select2({
      placeholder: "Pilih Cara Pembayaran",
      width: "100%",
      dropdownParent: $("#cara_bayar")
    });
}
function get_nomor_peserta() {
    var rm = $("#rm").val();
    $.ajax({
      type: "POST",
      url: "entry_resep/get_nomor_peserta_ctrl",
      dataType: "json",
      data: {
        rm: rm
      },
      success: function(data) {
        // console.log(data);
        $("#no_peserta").val(data[0].NO_PESERTA);
        // $tr.find("#jasa_r").val(data[0].JASA_R);
      },
      error: function(xhr, status, error) {}
    });
}
function get_pass(password) {
    loading();
    $.ajax({
      type: "POST",
      url: "entry_resep/cekpass",
      data: {
        pass : password
      },
      dataType: "json",
  
      success: function(data) {
        if (data["responseCode"] == 'ok') {
        //   get_kode_trans();
          $("#password-modal").modal("hide");
          $("#user").val(data["username"]);
          save();
        } else {
          swal.fire("password salah", "error");
          $("#password").val("");
          $("#password").focus();
        }
        KTApp.unblockPage();
      },
      error: function(xhr, status, error) {
        console.log(xhr, status, error);
      }
    });
}
/////////////////////////////////////
$("#data4send").on("shown.bs.modal", function() {
    $("#penerima_kirim").focus();
    $("#telp_kirim, #telp2_kirim").val("");
    $("#penerima_kirim").val($("#name").val());
    $("#alamat_kirim").val($("#address").val());
    $("#kota_kirim").val($("#city").val());
});
$("#cara_bayar").on("shown.bs.modal", function() {
    $("#bayarbyr, #kembalibyr, #no_kartu, #no_trace, #no_batch, #nominal").val(
      ""
    );
    $("#credit_detail").css("display", "none");
    $("#tabel_pembayaran tbody tr").remove();
    $("#tabel_pembayaran tbody").append(
      "<tr><td colspan='99' style='text-align:center'>NO DATA</td></tr>"
    );
    $("#totalbyr").val(parseFloat($("#total_harga").val()));
    $("#pilih_cara_bayar, #tipe_kartu")
      .val("")
      .trigger("change");
});
$("#password-modal").on("shown.bs.modal", function() {
    $("#password").focus();
    $("#password").val("");
});
/////////////////////////////////////
$('#simpan_all').click(function(){
    if ($("#pengiriman").val() != "Farmasi") {
        save_pengiriman_arr = [];
        pre_save();
    } else if ($("#pengiriman").val() == "Farmasi") {
        $("#data4send").modal("show");
    }
    //// ^ what i've done
    
});
$("#save_pengirim").on("click", function() {
    var prev_total = parseFloat($("#total_harga").val());
    var kirim = parseFloat($("#biaya_kirim").val());
    $("#total_harga").val(prev_total + kirim);
    save_pengiriman_arr.push({
        PENERIMA : $("#penerima_kirim").val(),
        ALMT : $("#alamat_kirim").val(),
        KOTA : $("#kota_kirim").val(),
        TELP : $("#telp_kirim").val(),
        TELP2 : $("#telp2_kirim").val(),
        BIAYA : $("#biaya_kirim").val(),
    });
    $("#data4send").modal("hide");
    pre_save();
});
function pre_save(){
    get_nomor_peserta();
    // get_nomor_resep();
    get_all_resep();
    if((ndeb1 > 0)&&(ndeb2 > 0)&&(ntunai > 0)){
        daftar_creditcard();
        pilih_cara_bayar();
        $("#cara_bayar").modal("show");
        $("#pilih_cara_bayar").on("change", function() {
            if ($("#pilih_cara_bayar").find(":selected").data("crbayar") == "C") {
                $("#credit_detail").css("display", "block");
            } else if ($("#pilih_cara_bayar").find(":selected").data("crbayar") == "T") {
                $("#credit_detail").css("display", "none");
            }
        });
    }else{
        save_pengiriman_arr = [];
        bayar="";
        $("#password-modal").modal("show");
    }
}
function get_all_resep() {
    save_obat_asli_arr = [];
    save_obat_split_arr = [];
    save_obat_tunai_arr = [];
    // var subtotal_spc1 = 0; //untuk obat biasa
    // var jasa_r_spc1 = 0;
    // var subtotal_spc2 = 0; //untuk obat terlarang
    // var jasa_r_spc2 = 0;
    var jmlhresep = $("#table-list_obat > tbody > tr").length;
    for (var i = 0; i < jmlhresep; i++) {
        var cekdeb1 = $("#table-list_obat tbody tr").eq(i).find('#cek').prop('checked');
        var cekdeb2 = $("#table-list_obat tbody tr").eq(i).find('#split').prop('checked');
        var cektunai = $("#table-list_obat tbody tr").eq(i).find('#tunai').prop('checked');
        if(cekdeb1){
            save_obat_asli_arr.push({
                NOURUT : $("#table-list_obat > tbody > tr").eq(i).find('td').eq(0).html(),
                ID : $("#table-list_obat > tbody > tr").eq(i).find('#id').val(),
                KDBRG : $("#table-list_obat > tbody > tr").eq(i).find('#kdbrg_cek').val(),
                HARGA : $("#table-list_obat > tbody > tr").eq(i).find('#harga_hide').val(),
                HBIJI : $("#table-list_obat > tbody > tr").eq(i).find('#hbiji_hide').val(),
                HJUAL : $("#table-list_obat > tbody > tr").eq(i).find('#hjual_hide').val(),
                JUMLAH : $("#table-list_obat > tbody > tr").eq(i).find('#qty2').val(),
                SIGNA : $("#table-list_obat > tbody > tr").eq(i).find(".list_signa").find(":selected").val(),
                SIGNA2 : $("#table-list_obat > tbody > tr").eq(i).find(".list_signa").find(":selected").data('signa'),
                HARI : $("#table-list_obat > tbody > tr").eq(i).find('#hari_hide').val(),
                JASA : $("#table-list_obat > tbody > tr").eq(i).find('#jasa_r_hide').val(),
                QTYDR : $("#table-list_obat > tbody > tr").eq(i).find('#qty').val(),
                KETQTY : $("#table-list_obat > tbody > tr").eq(i).find('#keterangan').val(),
                KDDEBI : $("#kddeb_hide").val(),
                SIGNACPT : $("#table-list_obat > tbody > tr").eq(i).find(".list_signa").find(":selected").data('signcepat'),
            });
        }
        if (cekdeb1 && cekdeb2){
            save_obat_split_arr.push({
                NOURUT : $("#table-list_obat > tbody > tr").eq(i).find('td').eq(0).html(),
                ID : $("#table-list_obat > tbody > tr").eq(i).find('#id').val(),
                KDBRG : $("#table-list_obat > tbody > tr").eq(i).find('#kdbrg_cek').val(),
                HARGA : $("#table-list_obat > tbody > tr").eq(i).find('#harga_hide').val(),
                HBIJI : $("#table-list_obat > tbody > tr").eq(i).find('#hbiji_hide').val(),
                HJUAL : $("#table-list_obat > tbody > tr").eq(i).find('#hjual_hide').val(),
                JUMLAH : $("#table-list_obat > tbody > tr").eq(i).find('#qty_split').val(),
                SIGNA : $("#table-list_obat > tbody > tr").eq(i).find(".list_signa").find(":selected").val(),
                SIGNA2 : $("#table-list_obat > tbody > tr").eq(i).find(".list_signa").find(":selected").data('signa'),
                HARI : $("#table-list_obat > tbody > tr").eq(i).find('#hari_hide').val(),
                JASA : $("#table-list_obat > tbody > tr").eq(i).find('#jasa_r_hide').val(),
                QTYDR : $("#table-list_obat > tbody > tr").eq(i).find('#qty').val(),
                KETQTY : $("#table-list_obat > tbody > tr").eq(i).find('#keterangan').val(),
                KDDEBI : $("#table-list_obat > tbody > tr").eq(i).find('.list_deb').val(),
                SIGNACPT : $("#table-list_obat > tbody > tr").eq(i).find(".list_signa").find(":selected").data('signcepat'),
            });
        }
        if (cekdeb1 && cekdeb2 && cektunai){
            save_obat_tunai_arr.push({
                NOURUT : $("#table-list_obat > tbody > tr").eq(i).find('td').eq(0).html(),
                ID : $("#table-list_obat > tbody > tr").eq(i).find('#id').val(),
                KDBRG : $("#table-list_obat > tbody > tr").eq(i).find('#kdbrg_cek').val(),
                HARGA : $("#table-list_obat > tbody > tr").eq(i).find('#harga_hide').val(),
                HBIJI : $("#table-list_obat > tbody > tr").eq(i).find('#hbiji_hide').val(),
                HJUAL : $("#table-list_obat > tbody > tr").eq(i).find('#hjual_hide').val(),
                JUMLAH : $("#table-list_obat > tbody > tr").eq(i).find('#qty_tunai').val(),
                SIGNA : $("#table-list_obat > tbody > tr").eq(i).find(".list_signa").find(":selected").val(),
                SIGNA2 : $("#table-list_obat > tbody > tr").eq(i).find(".list_signa").find(":selected").data('signa'),
                HARI : $("#table-list_obat > tbody > tr").eq(i).find('#hari_hide').val(),
                JASA : $("#table-list_obat > tbody > tr").eq(i).find('#jasa_r_hide').val(),
                QTYDR : $("#table-list_obat > tbody > tr").eq(i).find('#qty').val(),
                KETQTY : $("#table-list_obat > tbody > tr").eq(i).find('#keterangan').val(),
                KDDEBI : '999',
                SIGNACPT : $("#table-list_obat > tbody > tr").eq(i).find(".list_signa").find(":selected").data('signcepat'),
            });
        }
    }
    // $("#subtotal_spc1").val(subtotal_spc1);
    // $("#jasa_r_spc1").val(jasa_r_spc1);
    // $("#subtotal_spc2").val(subtotal_spc2);
    // $("#jasa_r_spc2").val(jasa_r_spc2);
    // loop_detail = loop_detail + "</DocumentElement>";
}
$("#tambah_pembayaran").on("click", function() {
    if ($("#tabel_pembayaran tbody tr").text() == "NO DATA") {
      $("#tabel_pembayaran tbody tr").remove();
    }
    var a = "";
    var a1 = $("#tipe_kartu")
      .find(":selected")
      .data("kartu");
    var a2 = $("#no_kartu").val();
    var a3 = $("#no_trace").val();
    var a4 = $("#no_batch").val();
    var a5 = $("#nominal").val();
    a =
      "<tr>" +
      "<td>" +
      a1 +
      "</td>" +
      "<td>" +
      a2 +
      "</td>" +
      "<td>" +
      a3 +
      "</td>" +
      "<td>" +
      a4 +
      "</td>" +
      "<td style='text-align: right;'>" +
      a5 +
      "</td>" +
      '<td><button class="btn btn-danger" type="button"  id="del-byr"><i class="fa fa-times"></i></button></td>' +
      "</tr>";
    $("#tabel_pembayaran tbody").append(a);
});
var bayar;
$("#proses_bayar").on("click", function() {
    var l_table = $("#tabel_pembayaran tbody tr").length;
    bayar = $("#pilih_cara_bayar").find(":selected").data("ket");
    if (l_table == 0) {
        save_pembayaran_arr = [];
        $("#password-modal").modal("show");
    } else {
      for (var i = 0; i < l_table; i++) {
        save_pembayaran_arr.push({
            BANK : $("#tabel_pembayaran tbody tr").eq(i).find("td").eq(0).html(),
            NO_REK : $("#tabel_pembayaran tbody tr").eq(i).find("td").eq(1).html(),
            RUPIAH : $("#tabel_pembayaran tbody tr").eq(i).find("td").eq(4).html(),
            DIBAYAR : $("#tabel_pembayaran tbody tr").eq(i).find("td").eq(4).html(),
            NO_TRACE : $("#tabel_pembayaran tbody tr").eq(i).find("td").eq(2).html()
        });
      }
      $("#password-modal").modal("show");
    }
    $("#cara_bayar").modal("hide");
});
$("#tabel_pembayaran").on("click", "tr #del-byr", function() {
    $(this)
      .parents("tr")[0]
      .remove();
    if ($("#tabel_pembayaran tbody tr").length < 1) {
      $("#tabel_pembayaran tbody").append(
        "<tr><td colspan='99' style='text-align:center'>NO DATA</td></tr>"
      );
    }
});
$("#simpanPass").on("click", function() {
    get_pass($("#password").val());
});
$("#password").on("keypress", function(e){
    if(e.keyCode == 13){
        get_pass($("#password").val());
    }
});
function save(){
    console.log(save_pengiriman_arr);
    console.log(save_pembayaran_arr);
    console.log(save_obat_asli_arr);
    console.log(save_obat_split_arr);
    console.log(save_obat_tunai_arr);
    // what I've done
    loading();
    var stpas = "";
    var sts = "";
    if ($("#jenispx").val() == "TUNAI") {
        stpas = 1;
        sts = 1;
    } else if ($("#jenispx").val() == "KREDIT") {
        stpas = 2;
        sts = 2;
    }
    save_header_arr = [];
    save_header_arr.push({
        ID_TRANS : $("#kode_trans").val(),
        KDMUT : $("#depo_nm").val(),
        NOTA : '',///
        TGL : tahun +"-" +bulan +"-" +tgl,
        TGLSHIFT : tahun_shift +"-" +bulan_shift +"-" +tgl_shift,
        NOMOR : '1',///
        MUTASI : 'O',
        BAYAR : bayar,
        KDOT : $("#kdresep").val(),
        STPAS : stpas,
        KDDEB : $("#kddeb_hide").val(),
        NMDEB : $("#nmdeb_hide").val(),
        KDDIN : $("#kddin_hide").val(),
        NMDIN : $("#nmdin_hide").val(),
        KDKLIN : $("#kdklin_hide").val(),
        NMKLIN : $("#nmklin_hide").val(),
        NORM : $("#rm").val(),
        NMPX : $("#name").val(),
        NMKK : '',
        KDDOK : $("#kddok_hide").val(),
        NMDOK : $("#nmdok_hide").val(),
        STS : sts,
        JAM : $("#jam").html(),
        JAGA : $("#shift").html(),
        TIPEIF : $("#depo_id").val(),
        LOKASI : '',
        IDXPX : $("#indexs").val(),
        ANTRIAN : $("#antrian").val(),
        RMINDUK : '',
        TAGIHINAP : '',
        ALMTPX : $("#address").val(),
        KOTAPX : $("#city").val(),
        NOPESERTA : $("#no_peserta").val(),
        NOKARTU : '',
        SURAT_JAMINAN : '',
        TELP_PX : '',
        TAGIHIFJALAN : '',
        IDUNIT : '001'
    });

    var data = {
        save_pengiriman_arr : save_pengiriman_arr,
        save_pembayaran_arr : save_pembayaran_arr,
        save_obat_asli_arr : save_obat_asli_arr,
        save_obat_split_arr : save_obat_split_arr,
        save_obat_tunai_arr : save_obat_tunai_arr,
        save_header_arr : save_header_arr,
    }
    $.ajax({
        type: "POST",
        url: "resep_online/save_ctrl",
        data: data,
        dataType: 'json',
        success: function(data) {
            console.log(data);
            KTApp.unblockPage();
            // swal.fire(data.status, data.message, data.status);
            // clear();
        },
        error: function(xhr, status, error) {
            console.log(xhr);
        }
    });
    // what I've done

    // var data_asli = [];
    // var data_split = [];
    // var data_tunai = [];
    // var msg_error = ""
    // $('#table-list_obat tbody tr').each(function(i){
    //     var qty_awal = parseFloat($(this).find('#qty').val());
    //     var qty =  parseFloat($(this).find('#qty2').val());
    //     var qty_split =  parseFloat($(this).find('#qty_split').val());
    //     var qty_tunai =  parseFloat($(this).find('#qty_tunai').val());
    //     if(qty_awal > qty+qty_split+qty_tunai){
    //         msg_error += "<br> Baris "+(i+1)+" Jumlah Obat masih kurang dari pesanan dokter";
    //     }
    //     if(qty_awal < qty+qty_split+qty_tunai){
    //         msg_error += "<br> Baris "+(i+1)+" Jumlah Obat melebih dari pesanan dokter";
    //     }
    //     console.log(qty_awal < parseFloat(qty+qty_split+qty_tunai))
        
    //     if(qty > 0 ){
    //         data_asli.push({
    //             qty :   $(this).find('#qty2').val()
    //         });
    //     }

    //     var split = $(this).find('#split').is(':checked');
    //     if(split == true){
    //         data_split.push({
    //             qty :   $(this).find('#qty_split').val()
    //         });
    //     }

    //     var tunai = $(this).find('#tunai').is(':checked');
    //     if(tunai == true){
    //         data_tunai.push({
    //             qty :   $(this).find('#qty_tunai').val()
    //         });
    //     } 

    // });
    // console.log(msg_error)
    // if(msg_error!= ""){
    //     Swal.fire('Gagal', msg_error, 'error')
    // }else{
    //     //do simpan if_htrans dan if_trans 

    //     //do cetak nota
        
    //     //do cetak copy resep
        
    // }
    // console.log(data_asli, data_split, data_tunai)
};

$('#resep_dokter').click(function(){
    console.log('resep_dokter')
});
$('#hitung_ulang').click(function(){
    console.log('hitung ulang')
});
// Class initialization on page load
jQuery(document).ready(function() {
    ResepOnline.init();
});