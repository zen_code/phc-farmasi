<?php
defined('BASEPATH') or exit('No direct script access allowed');

?>
<style>
  .passrow {
    display: none
  }

  table.dataTable th.focus,
  table.dataTable td.focus {
    outline: none;
  }

  #events {
    margin-bottom: 1em;
    padding: 1em;
    background-color: #f6f6f6;
    border: 1px solid #999;
    border-radius: 3px;
    height: 100px;
    overflow: auto;
  }
</style>
<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
  <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
    <input type="hidden" value="<?php echo base_url(); ?>" id="url">
    <!-- begin:: Subheader -->
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
      <div class="kt-container ">
        <div class="kt-subheader__main">
          <h3 class="kt-subheader__title">
            Laporan Farmasi Klinis </h3>
        </div>
        <div class="">
          <label class="col-form-label" style="color:royalblue">
            Date Sistem :&ensp;
          </label>
          <label id='date' class="col-form-label" style="color:#781400">
          </label>&ensp;
          <label id='jam' class="col-form-label" style="color:#781400">
          </label>&ensp;
          <label class="col-form-label" style="color:royalblue">
            Shift :&ensp;
          </label>
          <label id='shift' class="col-form-label" style="color:#781400">
          </label>
        </div>
        <!-- kt-subheader__toolbar -->

      </div>
    </div>

    <!-- end:: Subheader -->

    <!-- begin:: Content -->
    <div class="kt-container  kt-grid__item kt-grid__item--fluid">

      <!--Begin::Dashboard 1-->

      <!--Begin::Row-->
      <div class="row">
        <div class="col-xl-12 order-lg-2 order-xl-1">
          <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile">
            <!-- <div class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm bg-success">
              <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title" style="color: white;">
                  Biodata Pasien
                </h3>
              </div>
            </div> -->
            <form class="m-form" style="padding:20px;" method="POST" action="" id="form">
              <div class="form-group row">
                <div class="col-md-4" style="padding:20px 10px 20px 10px;">
                  <!--Biodata Pasien-->
                  <div class="m-portlet">
                    <div class="card">
                      <div class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                        <div class="kt-portlet__head-label">
                          <h3 class="kt-portlet__head-title">
                            Filter Laporan
                          </h3>
                        </div>
                      </div>

                      <div class="card-body">
                        <div class="m-portlet__body">
                          <div>
                            <!--  use to store every hidden input (start) -->
                            <input type="hidden" id="status_naik_kelas" value="0">
                            <input type="hidden" id="kode_trans" value="0">
                            <input type="hidden" id="antrian" value="0">
                          </div> <!--  use to store every hidden input (stop) -->

                          <div class="m-form__group row">
                            <label class="col-md-4 col-form-label m-form__group">
                              Jenis Laporan
                            </label>
                          </div>
                          <div class="m-form__group row">
                            <div class="col-md-12">
                              <!-- <select class="form-control dis selectpicker TabOnEnter" id="idlaporan" name="laporan"> -->
                              <select class="form-control" id="idlaporan" name="idlaporan" data-live-search="true">

                                <option value="">Pilih Jenis Laporan</option>
                                <option value="jenis1">Laporan Resep Loss-Rawat Jalan Tunai</option>
                                <option value="jenis2">Laporan Profis Farmasi</option>
                                <option value="jenis3">Laporan Obat Generik</option>
                                <option value="jenis4">Laporan Psikotropika & Narkotika</option>
                                <option value="jenis5">Laporan Penjualan Obat</option>
                                <option value="jenis6">Laporan Profis & Pendapatan Farmasi</option>
                                <option value="jenis7">Absensi Pengoplos Obat Kemoterapi</option>
                                <option value="jenis8">Laporan Evaluasi BHP Kemoterapi</option>
                                <option value="jenis9">Laporan Penggunaan e-Resep</option>
                                <option value="jenis10">Laporan Pemakaian Obat per Dokter</option>
                                <option value="jenis11">Laporan Pemakaian Obat per Jenis</option>
                                <option value="jenis12">Laporan per Jenis Tindakan per Dokter</option>
                                <option value="jenis13">Laporan Praktek Dokter</option>
                                <option value="jenis14">Laporan Pendapatan per Debitur per Klinik</option>
                                <option value="jenis15">Laporan Evaluasi e-Resep</option>
                                <option value="jenis16">Laporan Absensi Pengoplos Antibiotik</option>
                                <option value="jenis17">Laporan Evaluasi Biaya BHP Antibiotik</option>
                                <option value="jenis18">Laporan Rincian Telaah Farmasi Klinis</option>
                                <option value="jenis19">Laporan Rekapan Farmasi Klinis Rawat Jalan</option>
                                <option value="jenis20">Laporan Kajian Resep Pelayanan Farmasi Jalan</option>
                              </select>
                            </div>
                          </div>

                          <div class="m-form__group row">
                            <label class="col-md-4 col-form-label m-form__group">
                              Periode
                            </label>
                          </div>
                          <div class="m-form__group row">
                            <div class="form-group col-md-5">
                              <div class="m-form__group input-group date">
                                <input type="text" class="form-control kt-input datepicker" placeholder="From" id="date_from" />
                                <div class="input-group-append">
                                  <span class="input-group-text"><i class="la la-calendar-o glyphicon-th"></i></span>
                                </div>
                              </div>
                            </div>
                            <label for="example-text-input" class="col-2 col-form-label" style="text-align:center; font-weight: bold;">-</label>
                            <div class="form-group col-md-5">
                              <div class="m-form__group input-group date">
                                <input type="text" class="form-control kt-input datepicker" placeholder="To" id="date_to" />
                                <div class="input-group-append">
                                  <span class="input-group-text"><i class="la la-calendar-o glyphicon-th"></i></span>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="m-form__group row">
                            <label class="col-md-4 col-form-label m-form__group">
                              Dokter
                            </label>
                          </div>
                          <div class="m-form__group row">
                            <div class="col-md-12">
                              <input name="" type="hidden" class="form-control m-input spcdis" id="kddok" value="" placeholder="">
                              <select class="form-control" id="nmdok" name="name">
                                <option></option>
                              </select>
                            </div>
                          </div>
                          <br>
                          <div class="m-form__group row">
                            <div class="col-md-7">

                            </div>
                            <div class="col-md-5">

                              <button type="button" id="save_all" class="form-control m-input btn btn-success2" style=""><i class="fas fa-check">&nbsp;</i>Preview</button>&emsp;

                            </div>
                          </div>

                        </div>
                      </div>
                    </div>
                  </div>

                </div>

                <div class="col-md-8" style="padding:20px 10px 10px 10px;">
                  <!--Debitur Pasien-->
                  
                  <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile menujenis1 box">
                    <div class="m-portlet">
                      <div class="card">
                        <div
                            class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Laporan Resep Loss-Rawat Jalan Tunai
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body kt-portlet__body--fit" id="tabelsatu">
                            <!--begin: Datatable -->
                            <div class="kt-portlet__body">
                              <table class="table table-striped- table-bordered table-hover table-checkable" id="obat">
                                <thead>
                                  <tr style="background:#2860a8;text-align: center;">
                                      <th style="color:white" width="11%">Tanggal</th>
                                      <th style="color:white" width="6%">Kode</th>
                                      <th style="color:white" width="20%">Klinik</th>
                                      <th style="color:white" width="8%">Kunjungan</th>
                                      <th style="color:white" width="8%">Total Resep</th>
                                      <th style="color:white" width="8%">Resep Loss</th>
                                      
                                      
                                  </tr>
                                </thead>
                                <tbody>
                                

                                </tbody>
                              </table>
                            </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile menujenis2 box">
                    <div class="m-portlet">
                      <div class="card">
                        <div
                            class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Laporan Profis Resep
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body kt-portlet__body--fit" id="tabeldua">
                            <!--begin: Datatable -->
                          <div class="kt-portlet__body">
                            <table class="table table-striped- table-bordered table-hover table-checkable" id="obat">
                              <thead>
                                <tr style="background:#2860a8;text-align: center;">
                                    <th style="color:white" width="8%">Kode</th>
                                    <th style="color:white" width="22%">Tanggal</th>
                                    <th style="color:white" width="22%">Unit</th>
                                    <th style="color:white" width="15%">Jumlah Resep</th>
                                    <th style="color:white" width="30%">Pendapatan Tanpa Embalase</th>
                                    <th style="color:white" width="18%">Pendapatan</th>
                                    
                                    
                                </tr>
                              </thead>
                              <tbody>
                              

                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>

                    </div>
                  </div>

                  <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile menujenis3 box">
                    <div class="m-portlet">
                      <div class="card">
                        <div
                            class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Laporan Obat Generik
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body kt-portlet__body--fit" id="tabeltiga">
                            <!--begin: Datatable -->
                          <div class="kt-portlet__body">
                            <table class="table table-striped- table-bordered table-hover table-checkable" id="obat">
                              <thead>
                                <tr style="background:#2860a8;text-align: center;">
                                    <th style="color:white" width="13%">Tanggal</th>
                                    <th style="color:white" width="20%">Jenis</th>
                                    <th style="color:white" width="20%">Jumlah Resep</th>
                                    <th style="color:white" width="15%">Kuantum</th>                                
                                </tr>
                              </thead>
                              <tbody>
                              

                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>

                    </div>
                  </div>

                  <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile menujenis4 box">
                    <div class="m-portlet">
                      <div class="card">
                        <div
                            class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Laporan Psikotropika & Narkotika
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body kt-portlet__body--fit" id="tabeltiga">
                            <!--begin: Datatable -->
                          <div class="kt-portlet__body">
                            <table class="table table-striped- table-bordered table-hover table-checkable" id="obat">
                              <thead>
                                <tr style="background:#2860a8;text-align: center;">
                                    <th style="color:white" width="13%">Tanggal</th>
                                    <th style="color:white" width="10%">Unit</th>
                                    <th style="color:white" width="20%">Jenis</th>
                                    <th style="color:white" width="15%">Jumlah Resep</th>                                
                                    <th style="color:white" width="15%">Kuantum</th>                                
                                </tr>
                              </thead>
                              <tbody>
                              

                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>

                    </div>
                  </div>

                  <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile menujenis5 box">
                    <div class="m-portlet">
                      <div class="card">
                        <div
                            class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Laporan Penjualan Obat
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body kt-portlet__body--fit" id="tabeltiga">
                            <!--begin: Datatable -->
                          <div class="kt-portlet__body">
                            <table class="table table-striped- table-bordered table-hover table-checkable" id="obat">
                              <thead>
                                <tr style="background:#2860a8;text-align: center;">
                                    <th style="color:white" width="20%">Layanan</th>
                                    <th style="color:white" width="10%">Kode</th>
                                    <th style="color:white" width="30%">Barang</th>
                                    <th style="color:white" width="10%">Satuan</th>                                
                                    <th style="color:white" width="8%">Isi</th>                                
                                    <th style="color:white" width="10%">Jenis</th>                                
                                    <th style="color:white" width="10%">Jumlah</th>                                
                                </tr>
                              </thead>
                              <tbody>
                              

                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>

                    </div>
                  </div>

                  <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile menujenis6 box">
                    <div class="m-portlet">
                      <div class="card">
                        <div
                            class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Laporan Profis dan Pendapatan Farmasi
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body kt-portlet__body--fit" id="tabeltiga">
                            <!--begin: Datatable -->
                          <div class="kt-portlet__body">
                            <table class="table table-striped- table-bordered table-hover table-checkable" id="obat">
                              <thead>
                                <tr style="background:#2860a8;text-align: center;">
                                    <th style="color:white" width="15%">Periode</th>
                                    <th style="color:white" width="15%">Layanan</th>
                                    <th style="color:white" width="30%">Debitur</th>
                                    <th style="color:white" width="15%">Jumlah Resep</th>
                                    <th style="color:white" width="22%">Pendapatan Tanpa Embalase</th>
                                    <th style="color:white" width="18%">Pendapatan</th>                               
                                </tr>
                              </thead>
                              <tbody>
                              

                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>

                    </div>
                  </div>

                  <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile menujenis7 box">
                    <div class="m-portlet">
                      <div class="card">
                        <div
                            class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Laporan Absensi Pengoplos Obat Kemoterapi
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body kt-portlet__body--fit" id="tabeltiga">
                            <!--begin: Datatable -->
                          <div class="kt-portlet__body">
                            <table class="table table-striped- table-bordered table-hover table-checkable" id="obat">
                              <thead>
                                <tr style="background:#2860a8;text-align: center;">
                                    <th style="color:white" width="5%">No</th>
                                    <th style="color:white" width="15%">Tanggal</th>
                                    <th style="color:white" width="22%">Debitur</th>
                                    <th style="color:white" width="15%">Jumlah Pasien</th>
                                    <th style="color:white" width="30%">Pengoplos</th>                            
                                </tr>
                              </thead>
                              <tbody>
                              

                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>

                    </div>
                  </div>

                  <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile menujenis8 box">
                    <div class="m-portlet">
                      <div class="card">
                        <div
                            class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Laporan Evaluasi Biaya BHP Kemoterapi
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body kt-portlet__body--fit" id="tabeltiga">
                            <!--begin: Datatable -->
                          <div class="kt-portlet__body">
                            <table class="table table-striped- table-bordered table-hover table-checkable" id="obat">
                              <thead>
                                <tr style="background:#2860a8;text-align: center;">
                                    <th style="color:white" width="5%">No</th>
                                    <th style="color:white" width="15%">Tanggal</th>
                                    <th style="color:white" width="18%">Jumlah Oplosan</th>
                                    <th style="color:white" width="22%">Pendapatan</th>
                                    <th style="color:white" width="22%">Biaya</th>                            
                                </tr>
                              </thead>
                              <tbody>
                              

                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>

                    </div>
                  </div>

                  <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile menujenis9 box">
                    <div class="m-portlet">
                      <div class="card">
                        <div
                            class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Laporan Penggunaan e-Resep
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body kt-portlet__body--fit" id="tabeltiga">
                            <!--begin: Datatable -->
                          <div class="kt-portlet__body">
                            <table class="table table-striped- table-bordered table-hover table-checkable" id="obat">
                              <thead>
                                <tr style="background:#2860a8;text-align: center;">
                                    <th style="color:white" width="8%">Nota</th>
                                    <th style="color:white" width="8%">Antrian</th>
                                    <th style="color:white" width="8%">Tanggal</th>
                                    <th style="color:white" width="7%">Jam</th>
                                    <th style="color:white" width="8%">RM</th>
                                    <th style="color:white" width="15%">Pasien</th>
                                    <th style="color:white" width="17%">Debitur</th>
                                    <th style="color:white" width="15%">Klinik</th>
                                    <th style="color:white" width="22%">Dokter</th>                            
                                </tr>
                              </thead>
                              <tbody>
                              

                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>

                    </div>
                  </div>

                  <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile menujenis10 box">
                    <div class="m-portlet">
                      <div class="card">
                        <div
                            class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Laporan Pemakaian Obat per Dokter
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body kt-portlet__body--fit" id="tabeltiga">
                            <!--begin: Datatable -->
                          <div class="kt-portlet__body">
                            <table class="table table-striped- table-bordered table-hover table-checkable" id="obat">
                              <thead>
                                <tr style="background:#2860a8;text-align: center;">
                                    <th style="color:white" width="5%">No</th>
                                    <th style="color:white" width="12%">Layanan</th>
                                    <th style="color:white" width="23%">Dokter</th>
                                    <th style="color:white" width="10%">Kode</th>
                                    <th style="color:white" width="30%">Obat</th>
                                    <th style="color:white" width="10%">Jumlah</th>                            
                                </tr>
                              </thead>
                              <tbody>
                              

                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>

                    </div>
                  </div>

                  <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile menujenis11 box">
                    <div class="m-portlet">
                      <div class="card">
                        <div
                            class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Laporan Pemakaian Obat per Jenis
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body kt-portlet__body--fit" id="tabeltiga">
                            <!--begin: Datatable -->
                          <div class="kt-portlet__body">
                            <table class="table table-striped- table-bordered table-hover table-checkable" id="obat">
                              <thead>
                                <tr style="background:#2860a8;text-align: center;">
                                    <th style="color:white" width="5%">No</th>
                                    <th style="color:white" width="10%">Tanggal</th>
                                    <th style="color:white" width="15%">Unit</th>
                                    <th style="color:white" width="20%">Obat</th>
                                    <th style="color:white" width="8%">Resep</th>
                                    <th style="color:white" width="10%">Satuan</th>
                                    <th style="color:white" width="10%">Jumlah</th>                            
                                </tr>
                              </thead>
                              <tbody>
                              

                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>

                    </div>
                  </div>

                  <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile menujenis12 box">
                    <div class="m-portlet">
                      <div class="card">
                        <div
                            class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Laporan per Jenis Pendidikan per Dokter
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body kt-portlet__body--fit" id="tabeltiga">
                            <!--begin: Datatable -->
                          <div class="kt-portlet__body">
                            <table class="table table-striped- table-bordered table-hover table-checkable" id="obat">
                              <thead>
                                <tr style="background:#2860a8;text-align: center;">
                                    <th style="color:white" width="5%">No</th>
                                    <th style="color:white" width="12%">Layanan</th>
                                    <th style="color:white" width="23%">Dokter</th>
                                    <th style="color:white" width="10%">Kode</th>
                                    <th style="color:white" width="30%">Obat</th>
                                    <th style="color:white" width="10%">Jumlah</th>                            
                                </tr>
                              </thead>
                              <tbody>
                              

                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>

                    </div>
                  </div>

                  <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile menujenis13 box">
                    <div class="m-portlet">
                      <div class="card">
                        <div
                            class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Laporan Prakter Dokter
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body kt-portlet__body--fit" id="tabeltiga">
                            <!--begin: Datatable -->
                          <div class="kt-portlet__body">
                            <table class="table table-striped- table-bordered table-hover table-checkable" id="obat">
                              <thead>
                                <tr style="background:#2860a8;text-align: center;">
                                    <th style="color:white" width="5%">No</th>
                                    <th style="color:white" width="12%">Layanan</th>
                                    <th style="color:white" width="23%">Dokter</th>
                                    <th style="color:white" width="10%">Kode</th>
                                    <th style="color:white" width="30%">Obat</th>
                                    <th style="color:white" width="10%">Jumlah</th>                            
                                </tr>
                              </thead>
                              <tbody>
                              

                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>

                    </div>
                  </div>

                  <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile menujenis14 box">
                    <div class="m-portlet">
                      <div class="card">
                        <div
                            class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Laporan Pendapatan per Debitur per Klinik
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body kt-portlet__body--fit" id="tabeltiga">
                            <!--begin: Datatable -->
                          <div class="kt-portlet__body">
                            <table class="table table-striped- table-bordered table-hover table-checkable" id="obat">
                              <thead>
                                <tr style="background:#2860a8;text-align: center;">
                                    <th style="color:white" width="5%">No</th>
                                    <th style="color:white" width="12%">Layanan</th>
                                    <th style="color:white" width="23%">Dokter</th>
                                    <th style="color:white" width="10%">Kode</th>
                                    <th style="color:white" width="30%">Obat</th>
                                    <th style="color:white" width="10%">Jumlah</th>                            
                                </tr>
                              </thead>
                              <tbody>
                              

                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>

                    </div>
                  </div>

                  <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile menujenis15 box">
                    <div class="m-portlet">
                      <div class="card">
                        <div
                            class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Laporan Evaluasi e-Resep
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body kt-portlet__body--fit" id="tabeltiga">
                            <!--begin: Datatable -->
                          <div class="kt-portlet__body">
                            <table class="table table-striped- table-bordered table-hover table-checkable" id="obat">
                              <thead>
                                <tr style="background:#2860a8;text-align: center;">
                                    <th style="color:white" width="10%">Kode</th>
                                    <th style="color:white" width="25%">Dokter</th>
                                    <th style="color:white" width="20%">Klinik</th>
                                    <th style="color:white" width="18%">Eresep</th>
                                    <th style="color:white" width="18%">Manual</th>                            
                                </tr>
                              </thead>
                              <tbody>
                              

                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>

                    </div>
                  </div>

                  <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile menujenis16 box">
                    <div class="m-portlet">
                      <div class="card">
                        <div
                            class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Absensi Pengoplos Antibiotik
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body kt-portlet__body--fit" id="tabeltiga">
                            <!--begin: Datatable -->
                          <div class="kt-portlet__body">
                            <table class="table table-striped- table-bordered table-hover table-checkable" id="obat">
                              <thead>
                                <tr style="background:#2860a8;text-align: center;">
                                    <th style="color:white" width="5%">No</th>
                                    <th style="color:white" width="10%">Tanggal</th>
                                    <th style="color:white" width="15%">Debitur</th>
                                    <th style="color:white" width="15%">Jumlah Pasien</th>
                                    <th style="color:white" width="25%">Pengoplos</th>                            
                                </tr>
                              </thead>
                              <tbody>
                              

                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>

                    </div>
                  </div>

                  <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile menujenis17 box">
                    <div class="m-portlet">
                      <div class="card">
                        <div
                            class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Evaluasi Biaya BHP Antibiotik
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body kt-portlet__body--fit" id="tabeltiga">
                            <!--begin: Datatable -->
                          <div class="kt-portlet__body">
                            <table class="table table-striped- table-bordered table-hover table-checkable" id="obat">
                              <thead>
                                <tr style="background:#2860a8;text-align: center;">
                                    <th style="color:white" width="5%">No</th>
                                    <th style="color:white" width="10%">Tanggal</th>
                                    <th style="color:white" width="20%">Jumlah Oplosan</th>
                                    <th style="color:white" width="20%">Pendapatan</th>
                                    <th style="color:white" width="20%">Biaya</th>                            
                                </tr>
                              </thead>
                              <tbody>
                              

                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>

                    </div>
                  </div>

                  <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile menujenis18 box">
                    <div class="m-portlet">
                      <div class="card">
                        <div
                            class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Laporan Rincian Telaah Farmasi Klinis
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body kt-portlet__body--fit" id="tabeltiga">
                            <!--begin: Datatable -->
                          <div class="kt-portlet__body">
                            <table class="table table-striped- table-bordered table-hover table-checkable" id="obat">
                              <thead>
                                <tr style="background:#2860a8;text-align: center;">
                                    <th style="color:white" width="5%">Karcis</th>
                                    <th style="color:white" width="5%">RM</th>
                                    <th style="color:white" width="5%">Tanggal</th>
                                    <th style="color:white" width="15%">Nama</th>
                                    <th style="color:white" width="15%">Dokter</th>
                                    <th style="color:white" width="20%">DRP</th>
                                    <th style="color:white" width="20%">TIndakan</th>
                                    <th style="color:white" width="20%">Telaah Interaksi</th>
                                    <th style="color:white" width="10%">Verifikator</th>                            
                                </tr>
                              </thead>
                              <tbody>
                              

                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>

                    </div>
                  </div>

                  <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile menujenis19 box">
                    <div class="m-portlet">
                      <div class="card">
                        <div
                            class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Laporan Rekapan Farmasi Klinis Rawat Jalan
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body kt-portlet__body--fit" id="tabeltiga">
                            <!--begin: Datatable -->
                          <div class="kt-portlet__body">
                            <table class="table table-striped- table-bordered table-hover table-checkable" id="obat">
                              <thead>
                                <tr style="background:#2860a8;text-align: center;">
                                    <th style="color:white" width="5%">No</th>
                                    <th style="color:white" width="12%">Layanan</th>
                                    <th style="color:white" width="23%">Dokter</th>
                                    <th style="color:white" width="10%">Kode</th>
                                    <th style="color:white" width="30%">Obat</th>
                                    <th style="color:white" width="10%">Jumlah</th>                            
                                </tr>
                              </thead>
                              <tbody>
                              

                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>

                    </div>
                  </div>

                  <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile menujenis20 box">
                    <div class="m-portlet">
                      <div class="card">
                        <div
                            class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Laporan Kajian Resep Pelayanan Farmasi Jalan
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body kt-portlet__body--fit" id="tabeltiga">
                            <!--begin: Datatable -->
                          <div class="kt-portlet__body">
                            <table class="table table-striped- table-bordered table-hover table-checkable" id="obat">
                              <thead>
                                <tr style="background:#2860a8;text-align: center;">
                                    <th style="color:white" width="5%">No</th>
                                    <th style="color:white" width="12%">Uraian</th>
                                    <th style="color:white" width="23%">Dokter</th>
                                    <th style="color:white" width="10%">Kode</th>
                                    <th style="color:white" width="30%">Obat</th>
                                    <th style="color:white" width="10%">Jumlah</th>                            
                                </tr>
                              </thead>
                              <tbody>
                              

                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>

                    </div>
                  </div>

                </div>
              </div>

            </form>
          </div>
        </div>
      </div>

      <!--End::Row-->



      <!--End::Dashboard 1-->
    </div>
    <!-- end:: Content -->
  </div>
</div>
<!-- javascript this page -->
<script src="<?php echo base_url() ?>assets/assets_metronic/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
<!-- <script src="<?php //echo base_url()
                  ?>application/modules/User/views/user_view.js" type="text/javascript"></script> -->
<script src="<?php echo base_url() ?>application/modules/Laporan_farmasiKlinis/views/laporan_farmasiKlinis_view.js" type="text/javascript"></script>
<script type="text/javascript">
  $(function() {
    $(".datepicker").datepicker({
      format: 'dd-mm-yyyy',
      autoclose: true,
      todayHighlight: true,
    });
  });
</script>
<script>
  $(function() {
    $("#idlaporan").select2({
      placeholder: "Jenis Laporan",
      allowClear: true,
      width: "100%"
    });
  });

  $("#idlaporan").change(function() {
      var jenis = $(this).val();
      if (jenis == "jenis1") {
        $(".box")
          .not(".menujenis1")
          .hide();
        $(".menujenis1").show();
      } else if (jenis == "jenis2") {
          $(".box")
            .not(".menujenis2")
            .hide();
          $(".menujenis2").show();
      } else if (jenis == "jenis3") {
          $(".box")
            .not(".menujenis3")
            .hide();
          $(".menujenis3").show();
      } else if (jenis == "jenis4") {
          $(".box")
            .not(".menujenis4")
            .hide();
          $(".menujenis4").show();
      } else if (jenis == "jenis5") {
          $(".box")
            .not(".menujenis5")
            .hide();
          $(".menujenis5").show();
      } else if (jenis == "jenis6") {
          $(".box")
            .not(".menujenis6")
            .hide();
          $(".menujenis6").show();
      } else if (jenis == "jenis7") {
          $(".box")
            .not(".menujenis7")
            .hide();
          $(".menujenis7").show();
      } else if (jenis == "jenis8") {
          $(".box")
            .not(".menujenis8")
            .hide();
          $(".menujenis8").show();
      } else if (jenis == "jenis9") {
          $(".box")
            .not(".menujenis9")
            .hide();
          $(".menujenis9").show();
      } else if (jenis == "jenis10") {
          $(".box")
            .not(".menujenis10")
            .hide();
          $(".menujenis10").show();
      } else if (jenis == "jenis11") {
          $(".box")
            .not(".menujenis11")
            .hide();
          $(".menujenis11").show();
      } else if (jenis == "jenis12") {
          $(".box")
            .not(".menujenis12")
            .hide();
          $(".menujenis12").show();
      } else if (jenis == "jenis15") {
          $(".box")
            .not(".menujenis15")
            .hide();
          $(".menujenis15").show();
      } else if (jenis == "jenis16") {
          $(".box")
            .not(".menujenis16")
            .hide();
          $(".menujenis16").show();
      } else if (jenis == "jenis17") {
          $(".box")
            .not(".menujenis17")
            .hide();
          $(".menujenis17").show();
      } else if (jenis == "jenis18") {
          $(".box")
            .not(".menujenis18")
            .hide();
          $(".menujenis18").show();
      } else if (jenis == "jenis19") {
          $(".box")
            .not(".menujenis19")
            .hide();
          $(".menujenis19").show();
      } else if (jenis == "jenis20") {
          $(".box")
            .not(".menujenis20")
            .hide();
          $(".menujenis20").show();
      } else {
          $(".box").hide();
      }
    });

    $(".box").hide();
</script>