"use strict";

var Laporan_farmasiKlinis = (function() {
    function getdokter() {
        $.ajax({
          type: "GET",
          url: "laporan_farmasiKlinis/get_dokter",
          data: {},
          dataType: "json",
    
          success: function(data) {
            // console.log(data);
            var datadokter = data;
            jQuery.each(datadokter, function(index, val) {
              var a = "";
              a =
                "<option value='" +
                val.kdDok +
                "' data-dokter='" +
                val.nmDok +
                "'><b>" +
                val.kdDok +
                " - " +
                val.nmDok +
                "</b>" +
                "</option>";
              $("#nmdok").append(a);
            });
          },
          error: function(xhr, status, error) {
            console.log(xhr, status, error);
          }
        });
        $("#nmdok").select2({
          placeholder: "Pilih Dokter",
          width: "100%"
        });
        
    }
    return {
        // Init demos
        init: function() {
          getdokter();

        }
    };
})();

jQuery(document).ready(function() {
    Laporan_farmasiKlinis.init();

    
});