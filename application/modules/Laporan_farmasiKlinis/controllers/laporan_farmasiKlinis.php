<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class laporan_farmasiKlinis extends CI_Controller {
    public $urlws = null;

	public function __construct() {
        parent::__construct();
		$this->load->model('laporan_farmasiKlinis_model');
		$this->load->library('session');
		$this->urlws = $this->globals->ws_phc();

    }

    private function _render($view,$data = array())
	{
		$this->load->view('header',$data);
	    // $this->load->view('body_header');
	    // $this->load->view('sidebar');
	    $this->load->view($view,$data);
	    $this->load->view('footer');
    }
    
    public function index()
	{
		//load library
		// var_dump($this->session->all_userdata());die();
		$uri =& load_class('URI', 'core');
	  	redirect(base_url().'index.php/laporan_farmasiKlinis/data',$data);
	}

	public function data()
	{
		// var_dump($_SESSION["ses_userid"]);die();
		if($_SESSION["if_ses_depo"] == null){
			redirect(base_url().'depo');
		}else{
			if(in_array(7,$_SESSION['if_ses_menu'])){
				$data['parent_active'] = 7 ;
				$data['child_active'] = 44 ;
				$this->_render('laporan_farmasiKlinis_view', $data);
			}else{
				redirect(base_url().'beranda/data');}
		}
    }
    
    public function get_dokter()
	{
		$query = $this->laporan_farmasiKlinis_model->get_dokter_model();
		echo json_encode($query->result());
	}
}