<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class laporan_farmasiKlinis_model extends CI_Model{
    public function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
        $this->load->database();
    }

    public function get_dokter_model()
    {
        $query  = $this->db->query("exec ok_sp_baru_list_mdokter");
        return $query;
    }
}