<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class cetak_resep_model extends CI_Model
{
  public function __construct()
  {
    // Call the CI_Model constructor
    parent::__construct();
    $this->load->database();
  }

  /*public function getdatalogin($user,$pass)
  {
    $this->load->database();
    $sql = "exec inap_sp_login_bacapriv '$user','$pass'";
    return $this->db->query($sql);  
  }*/
  public function get_bio_model($kode)
  {
    $query  = $this->db->query("select 
    G.LAYANAN,(SELECT VALSTR FROM IF_MGLOBAL WHERE KDGLOBAL=1) as valstr1,(SELECT VALSTR FROM IF_MGLOBAL WHERE KDGLOBAL=2) as valstr2,(SELECT VALSTR FROM IF_MGLOBAL WHERE KDGLOBAL=3) as valstr3,CONVERT(VARCHAR,h.TGLSHIFT,103) tglshift,CONVERT(VARCHAR,h.TGL,103) TGL,h.ANTRIAN,r.KDJNSRESEP,h.NMKK,M.SEX,CONVERT(VARCHAR,M.TGL_LHR,103) TGL_LAHIR,t.NO,t.ID,h.nota,h.nomor,h.norm rm,h.IDXPX,h.NMPX nama,h.ALMTPX,h.KOTAPX,CONVERT(VARCHAR ,h.INPUTDATE,103) INPUTDATE,h.KDDEB,h.nmdeb,h.kddin,h.nmdin, h.KDKLIN, h.NMKLIN, h.INPUTBY, h.JAM, h.KDDOK, h.NMDOK, t.ITER, h.STPAS, h.KDOT, r.NMJNSRESEP,
    t.KDBRG,b.nmbrg, b.SATUAN, t.jumlah,t.HJUAL,t.jumlah*t.hjual as 'sub_total',s.SIGNA, t.DISC, t.JASA from if_htrans h inner join if_trans t on h.id_Trans=t.ID_TRANS
    inner join IF_MBRG_GD b on b.kdbrg=t.kdbrg INNER JOIN RIRJ_MASTERPX M ON M.NO_PESERTA=h.NOPESERTA INNER JOIN IF_MLAYANAN G on G.IDLAYANAN=h.TIPEIF
    left join IF_MSIGNA s on s.kdsigna=t.SIGNA
    join MJENISRESEP r on h.KDOT=r.KDJNSRESEP
    where NOTA = '$kode'");
    return $query;
  }


  public function print_pdf_model($kode)
  {
    $query  = $this->db->query("select G.LAYANAN,
    (SELECT VALSTR FROM IF_MGLOBAL WHERE KDGLOBAL=1) as valstr1,(SELECT VALSTR FROM IF_MGLOBAL WHERE KDGLOBAL=2) as valstr2,(SELECT VALSTR FROM IF_MGLOBAL WHERE KDGLOBAL=3) as valstr3,CONVERT(VARCHAR,h.TGLSHIFT,103) tglshift,CONVERT(VARCHAR,h.TGL,103) TGL,h.ANTRIAN,r.KDJNSRESEP,h.NMKK,M.SEX,CONVERT(VARCHAR,M.TGL_LHR,103) TGL_LAHIR,t.NO,t.ID,h.nota,h.nomor,h.norm rm,h.IDXPX,h.NMPX nama,h.ALMTPX,h.KOTAPX,CONVERT(VARCHAR ,h.INPUTDATE,103) INPUTDATE,h.KDDEB,h.nmdeb,h.kddin,h.nmdin, h.KDKLIN, h.NMKLIN, h.INPUTBY, h.JAM, h.KDDOK, h.NMDOK, t.ITER, h.STPAS, h.KDOT, r.NMJNSRESEP,
    t.KDBRG,b.nmbrg, b.SATUAN, t.jumlah,t.HJUAL,t.jumlah*t.hjual as 'sub_total',s.SIGNA, t.DISC, t.JASA from if_htrans h 
    inner join if_trans t on h.id_Trans=t.ID_TRANS
    inner join IF_MBRG_GD b on b.kdbrg=t.kdbrg 
    INNER JOIN RIRJ_MASTERPX M ON M.NO_PESERTA=h.NOPESERTA 
    INNER JOIN IF_MLAYANAN G on G.IDLAYANAN=h.TIPEIF
    left join IF_MSIGNA s on s.kdsigna=t.SIGNA
    join MJENISRESEP r on h.KDOT=r.KDJNSRESEP
    where NOTA = '$kode'");
    return $query;
  }

  public function get_bio_online($kode)
  {
    $sql = " Select a.INPUTDATE,(SELECT VALSTR FROM IF_MGLOBAL WHERE KDGLOBAL=1) as valstr1,(SELECT VALSTR FROM IF_MGLOBAL WHERE KDGLOBAL=2),m.sip,a.NMPX nama,a.nota,t.KDBRG,s.nmbrg,t.NO,t.ID,r.SIGNA,t.jumlah,t.ITER,a.norm,i.MODIBY,a.NMKLIN,a.NMDOK,g.nmDok,a.KDDEB,a.nmdeb,a.nota, a.NOANTRIAN, b.NAMA, b.ALAMAT, c.NM_KABKOTA, a.IDXPX, a.NORM rm, d.KDDEBT, d.NMDEBT, e.KDDINAS, e.NMDINAS, f.kdKlin ,f.nmKlin,g.kdDok, g.nmDok, a.INPUTBY, a.ID_TRANS, h.MNAMA, a.kdot, i.DRP, i.TINDAK_LANJUT,a.STSCETAK, a.ID_TRANS
    from if_htrans_ol a
    join IF_MUSER m on a.KDDOK = m.KDDOK 
    inner join IF_TRANS_OL t on a.ID_TRANS=t.ID_TRANS
    inner join IF_MBRG_GD s on s.kdbrg=t.kdbrg 
    left join IF_MSIGNA r on r.kdsigna=t.SIGNA
    left join RIRJ_MASTERPX b on a.NOPESERTA = b.NO_PESERTA 
    left join RIRJ_MKABKOTA c on b.KOTA = c.KD_KABKOTA
    left join rirj_mdebitur d on a.KDDEB = d.KDDEBT
    left join MDINAS e on a.KDDIN = e.KDDINAS
    left join DR_KLINIK f on a.KDKLIN = f.kdKlin
    left join DR_MDOKTER g on a.KDDOK = g.kdDok
    left join IF_MKETOT h on a.kdot = h.MKODE
    left join IF_FARKLIN_HTRANS i on a.ID_TRANS = i.ID_TRANS_RJ_ONLINE
    where NOTA = '$kode'";
    $query  = $this->db->query($sql);
    // var_dump($sql);die();
    return $query;
  }


  public function print_pdf_model_online($kode)
  {
    $query  = $this->db->query("select  a.INPUTDATE,(SELECT VALSTR FROM IF_MGLOBAL WHERE KDGLOBAL=1) as valstr1,(SELECT VALSTR FROM IF_MGLOBAL WHERE KDGLOBAL=2),
    m.sip,a.NMPX nama,a.nota,t.KDBRG,s.nmbrg,t.NO,t.ID,r.SIGNA,t.jumlah,t.ITER,a.norm,i.MODIBY,a.NMKLIN,a.NMDOK,g.nmDok,a.KDDEB,a.nmdeb,a.nota, a.NOANTRIAN, b.NAMA, b.ALAMAT, c.NM_KABKOTA, a.IDXPX, a.NORM rm, d.KDDEBT, d.NMDEBT, e.KDDINAS, e.NMDINAS, f.kdKlin ,f.nmKlin,g.kdDok, g.nmDok, a.INPUTBY, a.ID_TRANS, h.MNAMA, a.kdot, i.DRP, i.TINDAK_LANJUT,a.STSCETAK, a.ID_TRANS
    from if_htrans_ol a
    join IF_MUSER m on a.KDDOK = m.KDDOK 
    inner join IF_TRANS_OL t on a.ID_TRANS=t.ID_TRANS
    inner join IF_MBRG_GD s on s.kdbrg=t.kdbrg 
    left join IF_MSIGNA r on r.kdsigna=t.SIGNA
    left join RIRJ_MASTERPX b on a.NOPESERTA = b.NO_PESERTA 
    left join RIRJ_MKABKOTA c on b.KOTA = c.KD_KABKOTA
    left join rirj_mdebitur d on a.KDDEB = d.KDDEBT
    left join MDINAS e on a.KDDIN = e.KDDINAS
    left join DR_KLINIK f on a.KDKLIN = f.kdKlin
    left join DR_MDOKTER g on a.KDDOK = g.kdDok
    left join IF_MKETOT h on a.kdot = h.MKODE
    left join IF_FARKLIN_HTRANS i on a.ID_TRANS = i.ID_TRANS_RJ_ONLINE
    where NOTA = '$kode'");
    return $query;
  }

  
}
