<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Cetak_resep extends CI_Controller
{
	public $urlws = null;
	public $globaldata = array();
	public function __construct()
	{
		parent::__construct();
		$this->load->model('cetak_resep_model');
		$this->load->library('session');
		$this->urlws = $this->globals->ws_phc();
		$this->globaldata = array(
			'title' => 'Page Title',
			'robots' => 'noindex,nofollow'
		);
	}
	private function _render($view, $data = array())
	{
		$this->load->view('header', $data);
		$this->load->view($view, $data);
		$this->load->view('footer');
	}

	public function index()
	{
		//load library
		// var_dump($this->session->all_userdata());die();
		$uri = &load_class('URI', 'core');
		redirect(base_url() . 'index.php/cetak_resep/data', $data);
	}

	public function data()
	{
		// var_dump($_SESSION["ses_userid"]);die();
		if ($_SESSION["if_ses_depo"] == null) {
			redirect(base_url() . 'depo');
		} else {
			if (in_array(6, $_SESSION['if_ses_menu'])) {
				$data['parent_active'] = 6;
				$data['child_active'] = 37;
				$this->_render('cetak_resep_view', $data);
			} else {
				redirect(base_url() . 'beranda/data');
			}
		}
	}

	// public function get_bio_ctrl()
	// {
	// 	$kode = $this->input->get('kode');
	// 	$query = $this->cetak_resep_model->get_bio_model($kode);
	// 	$this->globaldata = $query;
	// 	echo json_encode($this->globaldata->result());
	// }

	// public function get_bio_ctrl()
	// {
	// 	$kode = $this->input->get('kode');
	// 	$query = $this->cetak_resep_model->get_bio_model($kode);
	// 	echo json_encode($query->result());
	// }

	// public function cetakPdf()
	// {
	// 	$kode = $this->input->get('kode');
	// 	$query = $this->cetak_resep_model->print_pdf_model($kode);
	// 	// $query2 = $this->cetak_resep_model->get_bio_model($kode);
	// 	$data['query'] = $query->result();
	// 	// array_push($data['query'], $query2->result());
	// 	$this->load->library('pdf2');
	// 	// $customPaper = array(0,0,297.6378,150.236);

	// 	$this->pdf2->setPaper('A4', 'portrait');
	// 	$this->pdf2->load_view('../views/cetak/cetak_resep_pdf',  $data);
	// }


	public function get_bio_ctrl_online()
	{
		$kode = $this->input->get('kode');
		$query1 = $this->cetak_resep_model->get_bio_online($kode);
		// var_dump($query1->result());
		echo json_encode($query1->result());
		// var_dump($query1->result());die();
		// var_dump($kode);die();
		// var_dump("sduisu");die();
	}

	public function cetakPdf2()
	{
		$kode = $this->input->get('kode');
		$query = $this->cetak_resep_model->print_pdf_model_online($kode);
		// $query2 = $this->cetak_resep_model->get_bio_model($kode);
		$data['query'] = $query->result();
		// var_dump($kode);die();
		// array_push($data['query'], $query2->result());
		$this->load->library('pdf2');
		// $customPaper = array(0,0,297.6378,150.236);

		$this->pdf2->setPaper('A4', 'portrait');
		$this->pdf2->load_view('../views/cetak/cetak_resep_online_pdf',  $data);
	}
}
