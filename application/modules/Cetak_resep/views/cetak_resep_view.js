"use strict";

// Class definition

var CetakResep = (function() {
    return {
        // Init demos
        init: function() {

        }
    };
})();

// Bismillah
// var data_all = "";
// $("#registrasi").on("keypress", function(e) {
//     if (e.keyCode == 13) {
//         var kode = $("#registrasi").val();
//         var subtotal = 0;
//         var disc = 0;
//         var jasa_r = 0;
//         var total = 0;
//         KTApp.blockPage({
//             overlayColor: "#000000",
//             type: "v2",
//             state: "primary",
//             message: "Processing..."
//         });
//         $.ajax({
//             type: "GET",
//             url: "cetak_resep/get_bio_ctrl",
//             data: {
//                 kode: kode
//             },
//             dataType: "json",
//             success: function(data) {
//                 data_all = data;
//                 // data_all = "[";
//                 $("#table-list_obat tbody").html("");
//                 jQuery.each(data, function(index, val) {
//                     // data_all += JSON.stringify(data[index]) + ",";
//                     var a = "";
//                     $("#no_resep").val(val.nomor);
//                     $("#registrasi").val(val.nota);
//                     $(".dis").attr("disabled", true);
//                     $("#name").val(val.nama);
//                     $("#gender").val(val.SEX);
//                     $("#nmkk").val(val.NMKK);
//                     $("#alamat").val(val.ALMTPX);
//                     $("#antrian").val(val.ANTRIAN);
//                     $("#layanan").val(val.LAYANAN);
//                     $("#valstr1").val(val.valstr1);
//                     $("#valstr2").val(val.valstr2);
//                     $("#valstr3").val(val.valstr3);
//                     $("#tglshift").val(val.tglshift);
//                     $("#tgl_lhr").val(val.TGL_LAHIR);
//                     $("#kota").val(val.KOTAPX);
//                     if (val.STPAS == 1) {
//                         $("#jenispx").val("TUNAI");
//                     } else if (val.STPAS == 2) {
//                         $("#jenispx").val("KREDIT");
//                     }
//                     $("#rm").val(val.rm);
//                     $("#indexs").val(val.IDXPX);
//                     $("#tgl_resep").val(val.INPUTDATE);
//                     $("#kddeb").val(val.KDDEB);
//                     $("#nmdeb").val(val.nmdeb);
//                     $("#kddin").val(val.kddin);
//                     $("#nmdin").val(val.nmdin);
//                     $("#kdklin").val(val.KDKLIN);
//                     $("#nmklin").val(val.NMKLIN);
//                     $("#user").val(val.INPUTBY);
//                     $("#tgl_entri").val(val.TGL);
//                     $("#jam_entri").val(val.JAM);
//                     $("#kddok").val(val.KDDOK);
//                     $("#nmdok").val(val.NMDOK);
//                     $("#kdjenis").val(val.KDOT);
//                     $("#nmjenis").val(val.NMJNSRESEP);
//                     $("#iter").val(val.ITER);
//                     a =
//                         "<tr>" +
//                         "<td>" +
//                         val.NO +
//                         "</td>" +
//                         "<td>" +
//                         val.ID +
//                         "</td>" +
//                         "<td>" +
//                         val.KDBRG +
//                         "</td>" +
//                         "<td>" +
//                         val.nmbrg +
//                         "</td>" +
//                         "<td>" +
//                         val.SATUAN +
//                         "</td>" +
//                         "<td>" +
//                         val.SIGNA +
//                         "</td>" +
//                         "<td>" +
//                         val.HJUAL +
//                         "</td>" +
//                         "<td>" +
//                         val.jumlah +
//                         "</td>" +
//                         "<td>" +
//                         val.sub_total +
//                         "</td>" +
//                         "</tr>";
//                     $("#table-list_obat tbody").append(a);
//                     subtotal = subtotal + val.sub_total;
//                     disc = disc + val.DISC;
//                     jasa_r = jasa_r + val.JASA;
//                 });
//                 $("#subtotal").val(subtotal);
//                 $("#disc").val(disc);
//                 $("#jasa_r").val(jasa_r);
//                 total = subtotal - disc + jasa_r;
//                 $("#total").val(total);

//                 KTApp.unblockPage();
//                 // data_all += "]";
//             },
//             error: function(xhr, status, error) {
//                 console.log(xhr, status, error);
//             }
//         });
//     }
// });

var data_all = "";
$("#registrasi").on("keypress", function(e) {
    if (e.keyCode == 13) {
        var kode = $("#registrasi").val();
        var subtotal = 0;
        var disc = 0;
        var jasa_r = 0;
        var total = 0;
        KTApp.blockPage({
            overlayColor: "#000000",
            type: "v2",
            state: "primary",
            message: "Processing..."
        });

        $.ajax({
            type: "GET",
            url: "cetak_resep/get_bio_ctrl_online",
            data: {
                kode: kode
            },
            dataType: "json",
            success: function(data) {
                console.log(data);
                // data_all = "[";
                $("#table-list_obat tbody").html("");
                jQuery.each(data, function(index, val) {
                    // data_all += JSON.stringify(data[index]) + ",";
                    var a = "";
                    $("#no_resep").val(val.ANTRIRESEP);
                    $("#registrasi").val(val.nota);
                    console.log('sfsf');
                    $(".dis").attr("disabled", true);
                    $("#name").val(val.nama);
                    $("#gender").val(val.SEX);
                    $("#nmkk").val(val.NMKK);
                    $("#alamat").val(val.ALMTPX);
                    $("#antrian").val(val.ANTRIAN);
                    $("#layanan").val(val.LAYANAN);
                    $("#valstr1").val(val.valstr1);
                    $("#valstr2").val(val.valstr2);
                    $("#valstr3").val(val.valstr3);
                    $("#tglshift").val(val.tglshift);
                    $("#sip").val(val.sip);
                    $("#tgl_lhr").val(val.TGL_LAHIR);
                    $("#kota").val(val.KOTAPX);
                    if (val.STPAS == 1) {
                        $("#jenispx").val("TUNAI");
                    } else if (val.STPAS == 2) {
                        $("#jenispx").val("KREDIT");
                    }
                    $("#rm").val(val.rm);
                    $("#indexs").val(val.IDXPX);
                    $("#tgl_resep").val(val.INPUTDATE);
                    $("#kddeb").val(val.KDDEB);
                    $("#nmdeb").val(val.nmdeb);
                    $("#kddin").val(val.kddin);
                    $("#nmdin").val(val.nmdin);
                    $("#kdklin").val(val.KDKLIN);
                    $("#nmklin").val(val.NMKLIN);
                    $("#user").val(val.INPUTBY);
                    $("#tgl_entri").val(val.TGL);
                    $("#jam_entri").val(val.JAM);
                    $("#kddok").val(val.KDDOK);
                    $("#nmdok").val(val.NMDOK);
                    $("#kdjenis").val(val.KDOT);
                    $("#nmjenis").val(val.NMJNSRESEP);
                    $("#iter").val(val.ITER);
                    a =
                        "<tr>" +
                        "<td>" +
                        val.NO +
                        "</td>" +
                        "<td>" +
                        val.ID +
                        "</td>" +
                        "<td>" +
                        val.KDBRG +
                        "</td>" +
                        "<td>" +
                        val.nmbrg +
                        "</td>" +
                        "<td>" +
                        val.SATUAN +
                        "</td>" +
                        "<td>" +
                        val.SIGNA +
                        "</td>" +
                        "<td>" +
                        val.HJUAL +
                        "</td>" +
                        "<td>" +
                        val.jumlah +
                        "</td>" +
                        "<td>" +
                        val.sub_total +
                        "</td>" +
                        "</tr>";
                    $("#table-list_obat tbody").append(a);
                    subtotal = subtotal + val.sub_total;
                    disc = disc + val.DISC;
                    jasa_r = jasa_r + val.JASA;
                });
                $("#subtotal").val(subtotal);
                $("#disc").val(disc);
                $("#jasa_r").val(jasa_r);
                total = subtotal - disc + jasa_r;
                $("#total").val(total);
                KTApp.unblockPage();
                // data_all += "]";
            },
            error: function(xhr, status, error) {
                console.log(xhr, status, error);
            }
        });
    }
});

function loading() {
    KTApp.blockPage({
        overlayColor: "#000000",
        type: "v2",
        state: "primary",
        message: "Processing..."
    });
}
$("#no_resep").on("keyup, keydown", function(evt) {
    if (evt.key === "Enter") {
        console.log($("#no_resep").val());
    }
});

// $('#print').click(function() {
//     // var url = "cetak_resep/print_pdf_ctrl";
//     // alert("asdasd");
//     // window.open(url, "_blank");
//     //
//     // var kode = $("#registrasi").val();
//     // $.ajax({
//     //   type: "POST",
//     //   url: "cetak_resep/print_pdf_ctrl",
//     //   data: {
//     //     kode: kode,
//     //     tgl : $("#tgl_entri").val(),
//     //     nmdok : $("#nmdok").val(),
//     //     nomor : $("#no_resep").val(),
//     //     nmklin : $("#nmklin").val(),
//     //     nota : $("#registrasi").val(),
//     //     jenispx : $("#jenispx").val(),
//     //     // data_all : data_all
//     //   },
//     //   dataType: "json",
//     //   success: function(data) {
//     //     swal.fire("printing", 'check');
//     //   },
//     //   error: function(xhr, status, error) {
//     //     // console.log(xhr, status, error); 
//     //   }
//     // });
//     //
//     var kode = "";
//     if ($('#total').val() == '') {
//         swal.fire('Perhatian', 'Lengkapi', 'warning');
//     } else {
//         window.open("cetak_resep/cetakPdf?kode=" + $("#registrasi").val() + "", '_blank');
//         swal.fire('printing', 'check', 'success');
//     }
// });

$('#print2').click(function() {
    var kode = "";
    window.open("cetak_resep/cetakPdf2?kode=" + $("#registrasi").val() + "", '_blank');
    swal.fire('printing', 'check', 'success');
});



// function cetakPDF() {
//     url = "cetak_resep/export_pdf";
//     alert("asdasd");
//     window.open(url, "_blank");
// }
// Class initialization on page load
jQuery(document).ready(function() {
    CetakResep.init();
});