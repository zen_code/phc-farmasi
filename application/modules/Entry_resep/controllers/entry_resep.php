<?php
defined('BASEPATH') or exit('No direct script access allowed');

class entry_resep extends CI_Controller
{
	public $urlws = null;
	public $urlwscentra = null;
	public function __construct()
	{
		parent::__construct();
		$this->load->model('entry_resep_model');
		$this->load->library('session');
		$this->urlws = $this->globals->ws_phc();
		$this->urlwscentra = $this->globals->ws_centra();
	}
	private function _render($view, $data = array())
	{
		$this->load->view('header', $data);
		// $this->load->view('body_header');
		// $this->load->view('sidebar');
		$this->load->view($view, $data);
		$this->load->view('footer');
	}

	public function index()
	{
		//load library
		// var_dump($this->session->all_userdata());die();
		$uri = &load_class('URI', 'core');
		redirect(base_url() . 'index.php/entry_resep/data', $data);
	}

	public function data()
	{
		// var_dump($_SESSION["ses_userid"]);die();
		if ($_SESSION["if_ses_depo"] == null) {
			redirect(base_url() . 'depo');
		} else {
			if (in_array(6, $_SESSION['if_ses_menu'])) {
				$data['parent_active'] = 6;
				$data['child_active'] = 8;
				$this->_render('entry_resep_view', $data);
			} else {
				redirect(base_url() . 'beranda/data');
			}
		}
	}
	// public function autofill()
	// {
	// 	// $notareg = $_GET['notareg'];
	// 	// $bit = $_GET['bit'];
	// 	// $depo = $_GET['depo'];
	// 	$notareg = $this->input->post('notareg');
	// 	$bit = $this->input->post('bit');
	// 	$depo = $this->input->post('depo');
	// 	$search = $this->entry_resep_model->search($notareg, $bit, $depo)->result();
	// 	echo json_encode($search);
	// }

	public function get_list_data_ctrl()
	{
		$data = array(
			"notareg"     => $this->input->post('notareg'),
			"layanan"     => $this->input->post('layanan'),
		);
		$authorization = 'Authorization: ' . $_SESSION["if_ses_token_phc"];
		$url = $this->urlws . "Farmasi/entry_resep/get_list_data_ctrl_ws";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array($authorization));
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$output = curl_exec($ch);
		curl_close($ch);
		// var_dump($output);die();
		$cek_output = json_decode($output);
		if ($cek_output->status == true) {
			// echo json_encode($cek_output);
			$data 			= array();
			foreach ($cek_output->data as $item) {
				$btn 		= '<button name="btnproses" id="choose_this" type="button" onClick="fill_data('."'".$item->notareg."'".')" class="btn btn-success btn-sm" data-toggle="kt-tooltip" data-placement="top" title="Choose"> <i class="fa fa-check"></i> </button>';

				$data[] 	= array(
					"NOTAREG" 	=> $item->notareg,
					"NAMA" => $item->NMPX,
					"btn"		=> $btn
				);
			}
			$obj = array("data" => $data);
			echo json_encode($obj);
		} else {
			$error = array(
				'data' => [],
				'error' => $cek_output->message
			);
		}
	}
	public function autofill()
	{
		$data = array(
			"notareg"     => $this->input->post('kode'),
			"bit"     => $this->input->post('bit'),
			"depo"     => $this->input->post('depo'),
		);
		$authorization = 'Authorization: ' . $_SESSION["if_ses_token_phc"];
		$url = $this->urlws . "Farmasi/entry_resep/autofill_bio";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array($authorization));
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$output = curl_exec($ch);
		curl_close($ch);
		// var_dump($output);die();
		$cek_output = json_decode($output);
		if ($cek_output->status == true) {
			echo json_encode($cek_output);
		} else {
			$error = array(
				'data' => [],
				'error' => $cek_output->message
			);
		}
	}

	public function riwayat_obat_pasien()
	{
		$kodeobat = $this->input->post('kodeobat');
		$search = $this->entry_resep_model->riwayat_obat_pasien_model($kodeobat)->result();
		echo json_encode($search);
	}

	public function listSigna()
	{
		// $kddeb = $this->input->get("kddeb");

		$list_signa = $this->entry_resep_model->getListSigna();
		echo json_encode($list_signa->result());
	}
	public function get_bentukobat_ctrl()
	{
		$query = $this->entry_resep_model->get_bentukobat_model();
		echo json_encode($query->result());
	}
	public function get_dokter()
	{
		$query = $this->entry_resep_model->get_dokter_model();
		echo json_encode($query->result());
	}
	public function get_debitur(){
		$query = $this->entry_resep_model->get_debitur_model();
		echo json_encode($query->result());
	}
	public function get_dinas(){
		$kddeb = $this->input->get('kddeb');
		$query = $this->entry_resep_model->get_dinas_model($kddeb);
		echo json_encode($query->result());
	}
	public function get_dinas_all()
	{
		$query = $this->entry_resep_model->get_dinas_all_model();
		echo json_encode($query->result());
	}
	public function jenis_resep_ctrl()
	{
		$query = $this->entry_resep_model->jenis_resep_model();
		echo json_encode($query->result());
	}
	public function cara_bayar_ctrl()
	{
		$kdmut = $this->input->get('kdmut');
		$query = $this->entry_resep_model->cara_bayar_model($kdmut);
		echo json_encode($query->result());
	}
	public function card_type()
	{
		$query = $this->entry_resep_model->card_type_model();
		echo json_encode($query->result());
	}
	public function showobat()
	{
		//$keyword = $_GET['dataobat'];
		// $keyword = $this->input->get('par');
		$layanan = $this->input->post('layanan');
		$keyword = $this->input->post('keyword');
		
		// var_dump($keyword);die();
		$query = $this->entry_resep_model->show_obat($layanan, $keyword);
		$data = array();
		foreach ($query->result() as $item) {
			$act = '<button name="btnproses" id="choose" type="button" class="btn btn-success btn-sm "> <i class="fa fa-check"></i> </button>';
			$data[] = array(
				"kode" => $item->KODE,
				"barang" => $item->BARANG,
				"satuan" => $item->SATUAN,
				"jenis" => $item->JENIS,
				"depo" => $item->DEPO,
				"stok_pusat" => $item->STOK_PUSAT,
				"stok_gudang" => $item->STOK_GUDANG,
				"act" => $act
			);
		}
		$obj = array("data" => $data);
		echo json_encode($obj);
	}
	public function get_riwayat_obat()
	{
		$rm = $this->input->post('rm');
		$date1 = $this->input->post('date1');
		$date2 = $this->input->post('date2');
		$query = $this->entry_resep_model->getriwayatobat($rm, $date1, $date2);
		$data = array();
		// var_dump($rm);die();
		foreach ($query->result() as $item){
			if($item->SISA_HARI > 0){
				$data[] = array(
					"nota" => $item->NOTA,
					"tanggal" => $item->TGL,
					"obat" => $item->OBAT,
					"jumlah" => $item->JUMLAH,
					"dokter" => $item->DOKTER,
				);
			}
			
		}
		$obj = array("data"=>$data);
		echo json_encode($obj);
	}
	public function get_data_riwayat_per_obat_mitu_rm()
	{
		$rm = $this->input->post('rm');
		$bahan = $this->input->post('bahan');
		$query = $this->entry_resep_model->get_data_riwayat_per_obat_mitu_rm_model($rm, $bahan);
		echo json_encode($query->result());
	}
	public function save()
	{
		$headerresep = $this->input->post('headerresep');
		$detilresep = $this->input->post('detail');
		$bayarresep = $this->input->post('bayarresep');
		$kirimresep = $this->input->post('kirimresep');
		$resepstatus = $this->input->post('resepstatus');
		$user = $this->input->post('user');
		$query = $this->entry_resep_model->save_model($headerresep, $detilresep, $bayarresep, $kirimresep, $resepstatus, $user);
		echo json_encode($query->result());
	}
	public function get_h_biji_ctrl()
	{
		$kode = $this->input->post('kode');
		$query = $this->entry_resep_model->get_h_biji_model($kode);
		echo json_encode($query->result());
	}
	public function get_h_jual_ctrl()
	{
		$kode = $this->input->post('kode');
		$deb = $this->input->post('deb');
		$layanan = $this->input->post('layanan');
		$is_jalan = $this->input->post('is_jalan');
		$status = $this->input->post('status');
		$query = $this->entry_resep_model->get_h_jual_model($kode, $deb, $layanan, $is_jalan, $status);
		echo json_encode($query->result());
	}
	public function get_hna_ctrl()
	{
		$kode = $this->input->post('kode');
		$query = $this->entry_resep_model->get_hna_model($kode);
		echo json_encode($query->result());
	}
	public function get_h_rata_ctrl()
	{
		$kode = $this->input->post('kode');
		$query = $this->entry_resep_model->get_h_rata_model($kode);
		echo json_encode($query->result());
	}
	public function get_jasa_r_ctrl()
	{
		$jumlah = $this->input->post('jumlah');
		$deb = $this->input->post('deb');
		$jenis_r = $this->input->post('jenis_r');
		$status = $this->input->post('status');
		$kode = $this->input->post('kode');
		$query = $this->entry_resep_model->get_jasa_r_model($jumlah, $deb, $jenis_r, $status, $kode);
		echo json_encode($query->result());
	}
	public function get_status_naik_kelas_ctrl()
	{
		$no = $this->input->post('no');
		$query = $this->entry_resep_model->get_status_naik_kelas_model($no);
		echo json_encode($query->result());
	}
	public function get_nomor_resep_ctrl()
	{
		$date_now = $this->input->post('date_now');
		$query = $this->entry_resep_model->get_nomor_resep_model($date_now);
		echo json_encode($query->result());
	}
	public function get_kode_trans_ctrl()
	{
		$query = $this->entry_resep_model->get_kode_trans_model();
		echo json_encode($query->result());
	}
	public function get_nomor_peserta_ctrl()
	{
		$rm = $this->input->post('rm');
		$query = $this->entry_resep_model->get_nomor_peserta_model($rm);
		echo json_encode($query->result());
	}
	public function get_plafon_ctrl()
	{
		$notareg = $this->input->post('notareg');
		$query = $this->entry_resep_model->get_plafon_model($notareg);
		echo json_encode($query->result());
	}
	public function cekpass()
	{
		$data = array(
			"username"     => $this->input->post('pass'),
		);
		$authorization = 'Authorization: Bearer ' . $_SESSION["if_ses_token_centra"];
		$url = $this->urlwscentra . "dp/base/setting/user/verify-token";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array($authorization));
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$output = curl_exec($ch);
		curl_close($ch);
		// var_dump($_SESSION["if_ses_token_centra"]);die();
		$cek_output = json_decode($output);
		if ($cek_output->responseCode == 'ok') {
			echo json_encode($cek_output);
		} else {
			$error = array(
				'data' => [],
				'error' => $cek_output->message
			);
		}
	}
}
