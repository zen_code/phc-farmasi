<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class entry_resep_model extends CI_Model
{
  public function __construct()
  {
    // Call the CI_Model constructor
    parent::__construct();
    $this->load->database();
  }

  public function search($notareg, $bit, $depo)
  {
    $sql = "exec if_sp_baru_tampilkan_info_pasien_jalan_inap '$notareg' ,'$bit' ,'$depo'";
    return $this->db->query($sql);
    // $query= $this->db->get_where('IF_HTRANS',array('NOTA'=>$notareg));
    // return $query;
  }

  public function show_obat($layanan, $keyword)
  {
    // $this->db->like('NMOBAT', $keyword);
    // $this->db->or_like('KDOBAT', $keyword);
    // return $this->db->get('MOBAT');
    // $query  = $this->db->query("exec if_sp_baru_tampilkan_list_obat_layanan '$layanan','$keyword'");
    // return $this->db->query($query);
    $query  = $this->db->query("exec if_sp_baru_tampilkan_list_obat_layanan '$layanan','$keyword'");
    return $query;
  }

  public function getListSigna()
  {
    $query  = $this->db->query("exec if_sp_baru_tampilkan_signa");
    return $query;
  }
  public function get_bentukobat_model()
  {
    $query  = $this->db->query("select * from IF_MBENTUK_OBAT");
    return $query;
  }
  public function get_dokter_model()
  {
    $query  = $this->db->query("exec ok_sp_baru_list_mdokter");
    return $query;
  }
  public function get_debitur_model()
  {
    $query  = $this->db->query("select * from RIRJ_MDEBITUR");
    return $query;
  }
  public function get_dinas_model($kddeb)
  {
    $query  = $this->db->query("select * from RIRJ_MDEBT_DINAS where KDDEBT='$kddeb'");
    return $query;
  }
  public function get_dinas_all_model()
  {
    $query  = $this->db->query("select * from RIRJ_MDEBT_DINAS");
    return $query;
  }
  public function jenis_resep_model()
  {
    $query  = $this->db->query("select * from MJENISRESEP");
    return $query;
  }
  public function cara_bayar_model($kdmut)
  {
    $query  = $this->db->query("select * from IF_MCRBAYAR where KDMUT='$kdmut'");
    return $query;
  }
  public function card_type_model()
  {
    $query  = $this->db->query("select * from IF_MBANK");
    return $query;
  }
  public function riwayat_obat_pasien_model($kodeobat)
  {
    $query  = $this->db->query("exec if_sp_baru_cek_bahan '$kodeobat'");
    return $query;
  }
  // public function updateUser($id_user, $nipp, $email, $sip, $list_dokter, $list_klinik, $list_perawatan, $create_user)
  // {
  //   $sql = "exec app_sp_baru_update_user '$id_user','$nipp' ,'$email' ,'$sip' ,'$list_dokter' ,'$list_klinik' ,'$list_perawatan', '$create_user'";
  //   // var_dump($sql);die();
  //   return $this->db->query($sql);
  // }
  public function getriwayatobat($rm, $date1, $date2)
  {
    $query  = $this->db->query("exec if_sp_baru_tampilkan_riwayat_obat_pasien '$rm','$date1','$date2'");
    return $query;
  }
  public function get_data_riwayat_per_obat_mitu_rm_model($rm, $bahan)
  {
    $query  = $this->db->query("exec if_sp_baru_tampilkan_history_per_obat_mitu_rm '$rm','$bahan'");
    return $query;
  }
  public function save_model($headerresep, $detilresep, $bayarresep, $kirimresep, $resepstatus, $user)
  {
    $query  = $this->db->query("exec if_sp_baru_simpan_resep_manual '$headerresep', '$detilresep', '$bayarresep', '$kirimresep', '$resepstatus', '$user'");
    return $query;
  }
  public function get_h_biji_model($kode)
  {
    $query  = $this->db->query("exec if_sp_baru_tampilkan_harga_biji '$kode'");
    return $query;
  }
  public function get_h_jual_model($kode, $deb, $layanan, $is_jalan, $status)
  {
    $query  = $this->db->query("exec if_sp_baru_tampilkan_harga_jual '$kode','$deb','$layanan','$is_jalan','$status'");
    return $query;
  }
  public function get_hna_model($kode)
  {
    $query  = $this->db->query("exec if_sp_baru_tampilkan_hna '$kode'");
    return $query;
  }
  public function get_h_rata_model($kode)
  {
    $query  = $this->db->query("exec if_sp_baru_tampilkan_harga_rata2 '$kode'");
    return $query;
  }
  public function get_jasa_r_model($jumlah, $deb, $jenis_r, $status, $kode)
  {
    $query  = $this->db->query("exec if_sp_baru_tampilkan_jasa_r '$jumlah','$deb','$jenis_r','$status','$kode'");
    return $query;
  }
  public function get_status_naik_kelas_model($no)
  {
    $query  = $this->db->query("select STATUSNAIKKELAS from RI_MASTERPX_BPJS where NOREG='$no'");
    return $query;
  }
  public function get_nomor_resep_model($date_now)
  {
    $query  = $this->db->query("exec if_sp_baru_generate_nomor_resep_manual '$date_now'");
    return $query;
  }
  public function get_kode_trans_model()
  {
    $query  = $this->db->query("exec if_sp_generate_kode_transaksi_resep_manual");
    return $query;
  }
  public function get_nomor_peserta_model($rm)
  {
    $query  = $this->db->query("select * from RIRJ_MASTERPX where KBUKU='$rm'");
    return $query;
  }
  public function get_plafon_model($notareg)
  {
    $query = $this->db->query("exec if_sp_baru_baca_plafon_pasien '$notareg'");
    return $query;
  }
}
