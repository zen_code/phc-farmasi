<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<style>
  .passrow {
    display: none
  }

  table.dataTable th.focus,
  table.dataTable td.focus {
    outline: none;
  }

  #events {
    margin-bottom: 1em;
    padding: 1em;
    background-color: #f6f6f6;
    border: 1px solid #999;
    border-radius: 3px;
    height: 100px;
    overflow: auto;
  }
</style>
<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
  <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
    <input type="hidden" value="<?php echo base_url(); ?>" id="url">
    <!-- begin:: Subheader -->
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
      <div class="kt-container ">
        <div class="kt-subheader__main">
          <h3 class="kt-subheader__title">
            Entry Resep </h3>
        </div>
        <div class="">
          <label class="col-form-label" style="color:royalblue">
            Date Sistem :&ensp;
          </label>
          <label id='date' class="col-form-label" style="color:#781400">
          </label>&ensp;
          <label id='jam' class="col-form-label" style="color:#781400">
          </label>&ensp;
          <label class="col-form-label" style="color:royalblue">
            Shift :&ensp;
          </label>
          <label id='shift' class="col-form-label" style="color:#781400">
          </label>
        </div>
        <!-- kt-subheader__toolbar -->

      </div>
    </div>

    <!-- end:: Subheader -->

    <!-- begin:: Content -->
    <div class="kt-container  kt-grid__item kt-grid__item--fluid">

      <!--Begin::Dashboard 1-->

      <!--Begin::Row-->
      <div class="row">
        <div class="col-xl-12 order-lg-2 order-xl-1">
          <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile">
            <!-- <div class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm bg-success">
              <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title" style="color: white;">
                  Biodata Pasien
                </h3>
              </div>
            </div> -->
            <form class="m-form" style="padding:0px 0px 20px 0px;" method="POST" action="" id="form">
              <div class="form-group row">
                <div class="col-md-6" style="padding:0px 10px 20px 10px;">
                  <!--Biodata Pasien-->
                  <div class="m-portlet">
                    <div class="card">
                      <div class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                        <div class="kt-portlet__head-label">
                          <h3 class="kt-portlet__head-title">
                            Biodata Pasien
                          </h3>
                        </div>
                      </div>

                      <div class="card-body">
                        <div class="m-portlet__body">
                          <div>
                            <!--  use to store every hidden input (start) -->
                            <input type="hidden" id="status_naik_kelas" value="0">
                            <input type="hidden" id="kode_trans" value="0">
                            <input type="hidden" id="antrian" value="">
                            <input type="hidden" id="no_peserta" value="0">
                            <input type="hidden" id="user" value="">
                          </div> <!--  use to store every hidden input (stop) -->
                          <div class="m-form__group row">
                            <label class="col-md-4 col-form-label">
                              *Nomor :
                            </label>
                            <label class="col-md-4 col-form-label">
                              Layanan :
                            </label>
                            <label class="col-md-4 col-form-label">
                              Nota/Reg. :
                            </label>
                          </div>
                          <div class="m-form__group row">
                            <div class="col-md-4">
                              <input name="" type="number" disabled class="form-control m-input" id="nomor" placeholder="[Auto]">
                            </div>
                            <!--  -->
                            <div class="col-md-4">
                              <select class="form-control dis" id="layanan">
                                <option>Rawat Jalan</option>
                                <option>Rawat Inap</option>
                              </select>
                            </div>
                            <!--  -->
                            <div class="col-md-4">
                              <input name="" type="text" class="form-control m-input dis spcnumbers TabOnEnter" tabindex="1" id="notareg" onkeypress="return caridata(event)" placeholder="">
                            </div>
                          </div>
                          <div class="m-form__group row">
                            <label class="col-md-12 col-form-label m-form__group">
                              Nama :
                            </label>
                          </div>
                          <div class="m-form__group row">
                            <div class="col-md-12 m-form__group">
                              <input name="nama" type="text" class="form-control m-input dis TabOnEnter" tabindex="2" id="nama" placeholder="">
                            </div>
                          </div>
                          <div class="m-form__group row">
                            <div class="col-md-6">
                              <div class="m-form__group row">
                                <label class="col-md-12 col-form-label">
                                  Alamat :
                                </label>
                                <textarea class="form-control col-md-11 dis TabOnEnter" tabindex="3" id="alamat" cols="35" rows="5" style="left: 10px;"></textarea>
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="m-form__group row">
                                <label class="col-md-12 col-form-label">
                                  Kota :
                                </label>
                                <div class="col-md-12">
                                  <input name="" type="text" class="form-control m-input dis TabOnEnter" tabindex="4" id="kota" placeholder="">
                                </div>
                                <label class="col-md-12 col-form-label" style="color:red;">
                                  Alergi :
                                </label>
                                <div class="col-md-12">
                                  <input name="" type="text" class="form-control m-input dis" id="alergi" placeholder="">
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                </div>

                <div class="col-md-6" style="padding:0px 10px 10px 10px;">
                  <!--Debitur Pasien-->
                  <div class="m-portlet">
                    <div class="card">
                      <div class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                        <div class="kt-portlet__head-label">
                          <h3 class="kt-portlet__head-title">
                            Debitur Pasien
                          </h3>
                        </div>
                      </div>
                      <div class="card-body">
                        <div class="m-portlet__body">
                          <div class="form-group m-form__group row">
                            <div class="col-md-6">
                              <div class="m-form__group row">
                                <label class="col-md-12 col-form-label">
                                  Jenis PX. :
                                </label>
                              </div>
                              <div class="m-form__group row">
                                <div class="col-md-12">
                                  <select class="form-control dis" id="jenispx">
                                    <option>TUNAI</option>
                                    <option>KREDIT</option>
                                  </select>
                                </div>
                              </div>
                              <div class="m-form__group row">
                                <label class="col-md-12 col-form-label">
                                  Debitur :
                                </label>
                              </div>
                              <div class="m-form__group row">
                                <div class="col-md-12 form-group">
                                  <input name="" type="hidden" class="form-control m-input" id="hidden_deb" value="" placeholder="">
                                  <input name="" type="hidden" class="form-control m-input spcdis" id="kddeb" value="" placeholder="">
                                  <select class="form-control spcdis selectpicker TabOnEnter" id="nmdeb" name="name">
                                    <option></option>
                                  </select>
                                </div>
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="m-form__group row">
                                <label class="col-md-12 col-form-label">
                                  RM / Index:
                                </label>
                                <!-- <label class="col-md-4 col-form-label">
                                  Index :
                                </label> -->
                              </div>
                              <div class="m-form__group row">
                                <div class="col-md-12">
                                  <input name="" type="hidden" class="form-control m-input dis" id="rm" placeholder="">
                                  <input name="" type="text" class="form-control m-input dis" id="rmshow" placeholder="">
                                  <input name="" type="hidden" class="form-control m-input dis" id="idx" placeholder="">
                                </div>
                                <!-- <div class="col-md-4">
                                  
                                </div> -->
                              </div>
                              <div class="m-form__group row">
                                <label class="col-md-12 col-form-label">
                                  Dinas :
                                </label>
                              </div>
                              <div class="m-form__group row">
                                <div class="form-group col-md-12">
                                  <input name="" type="hidden" class="form-control m-input spcdis" id="kddin" value="" placeholder="">
                                  <select class="form-control spcdis selectpicker TabOnEnter" id="din" name="name">
                                    <option></option>
                                  </select>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- Saldo Excess-->
                  <div class="m-portlet">
                    <div class="card" style="margin-top:20px">
                      <div class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                        <div class="kt-portlet__head-label">
                          <h3 class="kt-portlet__head-title">
                            Saldo Excess (Khusus untuk Debitur Excess-Pelindo III/492)
                          </h3>
                        </div>
                      </div>
                      <div class="card-body">
                        <div class="m-portlet__body">
                          <div class="m-form__group row">
                            <label class="col-md-6 numbers col-form-label" for="pwd">Plafon :</label>
                            <label class="col-md-6 numbers col-form-label" for="pwd">Biaya Bulan Ini :</label>
                          </div>
                          <div class="m-form__group row">
                            <div class="col-md-6">
                              <input name="" type="number" disabled class="form-control m-input" id="plafon" placeholder="">
                            </div>
                            <div class="col-md-6">
                              <input name="" type="number" disabled class="form-control m-input" id="biaya_bulan_ini" placeholder="">
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                </div>
              </div>
              <!-- Info klinik / RP-->
              <div class="m-portlet" style="padding-bottom: 20px">
                <div class="card">
                  <div class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                    <div class="kt-portlet__head-label">
                      <h3 class="kt-portlet__head-title">
                        Info Klinik / RP
                      </h3>
                    </div>
                  </div>
                  <div class="card-body">
                    <div class="m-portlet__body">
                      <div class="m-form__group row">
                        <label class="col-md-1 col-form-label">
                          Klinik :
                        </label>
                        <label class="col-md-3 col-form-label">
                          RP :
                        </label>
                        <label class="col-md-3 col-form-label">
                          Dokter :
                        </label>
                        <label class="col-md-2 col-form-label">
                          Pengiriman :
                        </label>
                        <label class="col-md-2 col-form-label">
                          Jenis Resep :
                        </label>
                        <label class="col-md-1 col-form-label">
                          Iter :
                        </label>
                      </div>
                      <div class="m-form__group row">
                        <div class="col-md-1">
                          <input name="" type="text" class="form-control m-input dis" id="kdklin" placeholder="">
                        </div>
                        <div class="col-md-3">
                          <input name="" type="text" class="form-control m-input dis" id="nmklin" placeholder="">
                        </div>
                        <div class="form-group col-md-3">
                          <input name="" type="hidden" class="form-control m-input spcdis" id="kddok" value="" placeholder="">
                          <select class="form-control spcdis selectpicker TabOnEnter" id="nmdok" name="name">
                            <option></option>
                          </select>
                        </div>
                        <div class="col-md-2">
                          <select class="form-control" id="pengiriman">
                            <option>Diambil Pasien</option>
                            <option>Halodoc</option>
                            <option>Farmasi</option>
                          </select>
                        </div>
                        <div class="form-group col-md-2" style="">
                          <select class="form-control" id="jenis_resep">
                            <option></option>
                          </select>
                        </div>
                        <!-- <div class="col-md-1">
                          <input name="" type="number" class="form-control m-input dis" id="" placeholder="">
                        </div>
                        <div class="col-md-2">
                          <input name="" type="text" class="form-control m-input dis" id="" placeholder="">
                        </div> -->
                        <div class="col-md-1">
                          <input name="" type="number" value="0" class="form-control m-input numbers" id="iter" placeholder="">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <button type="button" class="btn btn-success2" id="riwayat-obat" style="margin-left:10px"><i class="fa fa-history">&nbsp;</i>Riwayat Sisa Obat Pasien</button>
              <div class="kt-portlet__body kt-portlet__body--fit">
                <div class="kt-portlet__body" style="padding: 10px 10px 0px 10px">
                  <table class="table table-striped- table-bordered table-hover table-checkable" id="obat">
                    <thead>
                      <tr style="background:#2860a8;text-align: center;">
                        <th style="color:white" width="3%">NO</th>
                        <th style="color:white" width="5%">RACIKAN?</th>
                        <th style="color:white" width="9%">KODE</th>
                        <th style="color:white" width="24%">NAMA BARANG</th>
                        <th style="color:white" width="16%">SIGNA</th>
                        <th style="color:white" width="10%">HARGA</th>
                        <th style="color:white" width="5%">QTY Diberi</th>
                        <th style="color:white" width="5%">QTY Dokter</th>
                        <th style="color:white" width="10%">TOTAL</th>
                        <th style="color:white" width="10%">KETERANGAN</th>
                        <th style="color:white" width="3%">ACTION</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                      $i = 1;
                      $j = 0;
                      ?>

                    </tbody>
                  </table>
                </div>
                <div class="form-group m-form__group row kt-space-between" style="margin-bottom: 0px;border-right-width: 10px;padding-right: 9px;padding-left: 10px;">
                  <button type="button" id="hapus_rincian_obat" class="col-md-2 form-control m-input btn btn-danger2" style="margin-left:10px"><i class="fas fa-trash">&nbsp;</i>Hapus rincian obat</button>&nbsp;
                  <div style="float:right" class="col-md-3">
                    <div class="form-group m-form__group row">
                      <label class="col-md-4 col-form-label">
                        Sub Total :
                      </label>
                      <div class="col-md-8">
                        <input name="" disabled style="text-align:right" type="text" class="numbers form-control m-input" id="subtotal" value="0">
                      </div>
                    </div>
                  </div>
                </div>
                <!-- Output -->
                <div>
                  <div style="float:right" class="col-md-3">
                    <!-- <div class="form-group m-form__group row">
                      <label class="col-md-4 col-form-label">
                        Sub Total :
                      </label>
                      <div class="col-md-8">
                        <input name="" disabled style="text-align:right" type="number" class="form-control m-input" id="subtotal" value="0">
                      </div>
                    </div> -->
                    <div class="form-group m-form__group row">
                      <label class="col-md-4 col-form-label">
                        Jasa Resep :
                      </label>
                      <div class="col-md-8">
                        <input name="" disabled type="text" style="text-align:right" class="numbers form-control m-input" id="total_jasa_r" value="0">
                      </div>
                    </div>
                    <div class="form-group m-form__group row">
                      <label class="col-md-4 col-form-label">
                        TOTAL :
                      </label>
                      <div class="col-md-8">
                        <input name="" disabled type="text" style="text-align:right" class="numbers form-control m-input" id="total_harga" value="0">
                        <input type="hidden" id="jasa_r_spc1" value="0">
                        <input type="hidden" id="subtotal_spc1" value="0">
                        <input type="hidden" id="jasa_r_spc2" value="0">
                        <input type="hidden" id="subtotal_spc2" value="0">
                      </div>
                    </div>
                  </div>
                </div>
                <div>
                  <div class="col-md-5" style="float:right">
                    <div class="form-group row">
                      <button type="button" id="save_all" class="col-md-5 form-control m-input btn btn-success2" style="margin-left:65px"><i class="fas fa-check">&nbsp;</i>Simpan dan cetak resep</button>&emsp;
                      <button type="reset" id="cancel" class="col-md-5 form-control m-input btn btn-warning2" style=""><i class="fas fa-times">&nbsp;</i>Cancel</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>

      <!--End::Row-->



      <!--End::Dashboard 1-->
    </div>
    <!-- end:: Content -->
  </div>
</div>
<!-- Modal -->
<!-- Modal 1 -->
<div class="modal fade" tabindex="-1" role="dialog" id="cara_bayar">
  <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><span class="tag_type"></span>Cara Bayar</h5>
      </div>
      <div class="modal-body">
        <div class="">
          <div class="form-group row">
            <label class="col-md-3 col-form-label" for="pwd">Cara Bayar : </label>
            <div class="form-group col-md-8 col-sm-6" style="padding:0px; margin:0px;">
              <select class="form-control" id="pilih_cara_bayar">
                <option></option>
              </select>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 col-form-label" for="pwd">Total : </label>
            <input class="numbers col-md-8 col-sm-6 form-control m-input" type="number" id="totalbyr">
          </div>
          <div class="form-group row">
            <label class="col-md-3 col-form-label" for="pwd">Bayar (Cash + CC) :</label>
            <input class="numbers col-md-8 col-sm-6 form-control m-input" type="number" id="bayarbyr">
          </div>
          <div class="form-group row">
            <label class="col-md-3 col-form-label" for="pwd">Kembali :</label>
            <input class="numbers col-md-8 col-sm-6 form-control m-input" type="number" id="kembalibyr">
          </div>
          <div style="display:none" id="credit_detail">
            <div class="form-group row">
              <label class="col-md-12 col-form-label" style="text-align:center">
                ---------------------------------------------------------------------------------------------</label>
            </div>
            <div class="form-group row">
              <label class="col-md-3 col-form-label" for="pwd">*Tipe Kartu : </label>
              <div class="form-group col-md-8 col-sm-6" style="padding:0px; margin:0px;">
                <select class="form-control" id="tipe_kartu">
                  <option></option>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-3 col-form-label" for="pwd">No. Kartu :</label>
              <input class="spcnumbers col-md-8 col-sm-6 form-control m-input" type="number" id="no_kartu">
            </div>
            <div class="form-group row">
              <label class="col-md-3 col-form-label" for="pwd">**No. Trace : </label>
              <input class="spcnumbers col-md-8 col-sm-6 form-control m-input" type="number" id="no_trace">
            </div>
            <div class="form-group row">
              <label class="col-md-3 col-form-label" for="pwd">No. Batch :</label>
              <input class="spcnumbers col-md-8 col-sm-6 form-control m-input" type="number" id="no_batch">
            </div>
            <div class="form-group row">
              <label class="col-md-3 col-form-label" for="pwd">Nominal :</label>
              <input class="numbers col-md-8 col-sm-6 form-control m-input" type="number" id="nominal">
            </div>
            <div class="form-group row">
              <label class="col-md-6 col-form-label" style="color:royalblue; text-align:right">*Tipe Kartu = Cardtype pada struk EDC</label>
              <label class="col-md-6 col-form-label" style="color:brown">**No. Trace = Trace number pada struk EDC</label>
            </div>
            <div class="form-group row" style="float:right;margin-right: 0px;">
              <button type="button" class="btn btn-success2" id="tambah_pembayaran"><span class="tag_type"></span>Tambah</button>
            </div>
            <table class="table table-striped- table-bordered table-hover table-checkable" id="tabel_pembayaran">
              <thead>
                <tr style="background:#2860a8;text-align: center;">
                  <th style="color:white;" width="25%">CARA BAYAR</th>
                  <th style="color:white;" width="20%">NO. KARTU</th>
                  <th style="color:white;" width="15%">NO. TRACE</th>
                  <th style="color:white;" width="15%">NO. BATCH</th>
                  <th style="color:white;" width="20%">NOMINAL</th>
                  <th style="color:white;" width="5%">ACTION</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="proses_bayar"><span class="tag_type"></span>Proses</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<!-- Daftar Pencarian Obat (DPO)-->
<div class="modal fade" tabindex="-1" role="dialog" id="dpo">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><span class="tag_type"></span>Daftar Obat</h5>
      </div>
      <div class="modal-body">
        <table class="table table-striped- table-bordered table-hover table-checkable" id="daftar_obat">
          <thead>
            <tr style="background:#2860a8;text-align: center;">
              <th style="color:white;">KODE</th>
              <th style="color:white;">BARANG</th>
              <th style="color:white">SATUAN</th>
              <th style="color:white">JENIS</th>
              <th style="color:white"><?php echo $_SESSION["if_ses_depo"];?></th>
              <th style="color:white">STOK PUSAT</th>
              <th style="color:white">STOK GUDANG</th>
              <th style="color:white">ACTION</th>
            </tr>
          </thead>
          <!-- <tbody>

          </tbody> -->
        </table>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal 3 -->
<div class="modal fade" tabindex="-1" role="dialog" id="add-qty-nonracik">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <!--<div class="modal-header">
        <h5 class="modal-title"><span class="tag_type"></span> User</h5>
      </div>-->
      <div class="modal-body">
        <div class="">
          <div class="form-group row">
            <label class="col-md-3 col-form-label" for="pwd">QTY RESEP (DIBERI) :</label>
            <input class="col-md-2 form-control m-input numbers" type="text" id="qty_resep_diberi">
            <label class="col-md-3 col-form-label" for="pwd">QTY RESEP (DOKTER) :</label>
            <input class="col-md-2 form-control m-input numbers" type="text" id="qty_resep_dokter">
          </div>
          <div class="form-group row">
            <label class="col-md-3 col-form-label" for="pwd">KETERANGAN :</label>
            <input class="col-md-8 col-sm-6 form-control m-input" type="text" id="keterangan_nonracik">
          </div>
          <div class="form-group row">
            <label class="col-md-3 col-form-label" for="pwd">SIGNA :</label>
            <!-- <input class="col-md-3 form-control m-input" type="text" id="signacpt"> -->
            <div class="form-group col-md-8 col-sm-6" style="padding:0px; margin:0px;">
              <select class="form-control" id="signa_search_nonracik">
                <option></option>
              </select>
            </div>
          </div>
          <!-- <div class="form-group row">
            <label class="col-md-3 col-form-label" for="pwd">KET. SIGNA :</label>
            <input class="col-md-7 form-control m-input" type="text" id="pwd">
          </div> -->
          <label class="row col-md-6 col-form-label" style="color:red">:&nbsp;: HISTORY PASIEN PER OBAT & MITU</label>
          <div class="form-group row">
            <label class="col-md-3 col-form-label" for="pwd">OBAT :</label>
            <input class="col-md-2 form-control m-input rwyt_obat_kd" type="text">&nbsp;
            <input class="col-md-6 col-sm-5 col-md-offset-2 form-control m-input rwyt_obat_nm" type="text">
          </div>
        </div>
        <table class="table table-striped- table-bordered table-hover table-checkable" id="riwayat_obat_mitu_nonracik">
          <thead>
            <tr style="background:#2860a8;text-align: center;">
              <th style="color:white;">TANGGAL</th>
              <th style="color:white">KITIR</th>
              <th style="color:white">NOMOR</th>
              <th style="color:white">KODE</th>
              <th style="color:white;">NAMA BARANG</th>
              <th style="color:white">QTY</th>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="proses-nonracik"><span class="tag_type"></span>Proses</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal 4 -->
<div class="modal fade" tabindex="-1" role="dialog" id="add-qty-racik">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <!--<div class="modal-header">
        <h5 class="modal-title"><span class="tag_type"></span> User</h5>
      </div>-->
      <div class="modal-body">
        <div class="">
          <div class="form-group row">
            <label class="col-md-3 col-form-label" for="pwd">OBAT :</label>
            <input class="col-md-2 form-control m-input rwyt_obat_kd" type="text">&nbsp;
            <input class="col-md-6 form-control m-input rwyt_obat_nm" type="text">
          </div>
          <div class="form-group row">
            <label class="col-md-3 col-form-label" for="pwd">QTY OBAT :</label>
            <input class="col-md-3 form-control m-input numbers" type="text" id="qty_obat">
          </div>
          <label class="row col-md-6 col-form-label" style="color:red">:&nbsp;: HISTORY PASIEN PER OBAT & MITU</label>
        </div>
        <table class="table table-striped- table-bordered table-hover table-checkable" id="riwayat_obat_mitu_racik">
          <thead>
            <tr style="background:#2860a8;text-align: center;">
              <th style="color:white;"></th>
              <th style="color:white;">TANGGAL</th>
              <th style="color:white">KITIR</th>
              <th style="color:white">NOMOR</th>
              <th style="color:white">KODE</th>
              <th style="color:white">QTY</th>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="proses-racik"><span class="tag_type"></span>Proses</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal 5 -->
<div class="modal fade" tabindex="-1" role="dialog" id="qty_ak_racik">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <!--<div class="modal-header">
        <h5 class="modal-title"><span class="tag_type"></span> User</h5>
      </div>-->
      <div class="modal-body">
        <div class="">
          <div class="form-group row">
            <label class="col-md-3 col-form-label" for="pwd">Bentuk Resep :</label>
            <select class="form-control col-md-3" id="bentuk_resep">
              <option>Puyer / Kapsul</option>
              <option>Non Puyer / Kapsul</option>
            </select>
            <label class="col-md-3 col-form-label" for="pwd">QTY AK. RACIKAN :</label>
            <input class="col-md-2 form-control col-sm-6 m-input numbers" type="text" id="qty_ak_racikan">
          </div>
          <div class="form-group row">
            <label class="col-md-3 col-form-label" for="pwd">KETERANGAN :</label>
            <input class="col-md-8 col-sm-6 form-control m-input" type="text" id="keterangan_racik">
          </div>
          <div class="form-group row">
            <label class="col-md-3 col-form-label" for="pwd">SIGNA :</label>
            <div class="form-group col-md-8 col-sm-6" style="padding:0px; margin:0px;">
              <select class="form-control" id="signa_search_racik">
                <option></option>
              </select>
            </div>
          </div>
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="proses-qty-ak"><span class="tag_type"></span>Proses</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal 6 -->
<div class="modal fade" tabindex="-1" role="dialog" id="modal6">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" style="background-color:white"><span class="tag_type"></span> NOMOR RESEP (KREDIT) </h5>
      </div>
      <div class="modal-body bg-dark">
        <h1 class="modal-title">5563</h1>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="action"><span class="tag_type"></span>OK</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal 7 -->
<div class="modal fade" tabindex="-1" role="dialog" id="modal-resep-ulang">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Riwayat Resep Online</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-group row">
          <div class="col-lg-4 col-md-4 col-sm-6">
            <div class="input-group date">
              <input type="text" class="form-control datepicker" readonly placeholder="pilih Tanggal " id="date_from" />
              <div class="input-group-append">
                <span class="input-group-text">
                  <i class="la la-calendar-check-o"></i>
                </span>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-6">
            <div class="input-group date">
              <input type="text" class="form-control datepicker" readonly placeholder="pilih Tanggal" id="date_to" />
              <div class="input-group-append">
                <span class="input-group-text">
                  <i class="la la-calendar-check-o"></i>
                </span>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-6">
            <button class="btn btn-info" style="line-height:15px" id="filter-iterasi_date"><i class="fa fa-search"></i> Filter</button>
            <button class="btn btn-danger" style="line-height:15px" id="filter-iterasi_date_reset"> Reset</button>
          </div>
        </div>
        <table class="table table-fixed table-bordered" id="table-riwayat-resep" style="font-size:10px;margin-top:10px">
          <thead>
            <tr class="tbl">
              <th>ID</th>
              <th>KARCIS</th>
              <th>TGL</th>
              <th>ANTRIAN</th>
              <th>DEBITUR</th>
              <th>KLINIK</th>
              <th>DOKTER</th>
              <th>STATUS</th>
              <th width="16%">AKSI</th>
            </tr>
          </thead>
          <tbody>

          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<!-- Modal 8 -->
<div class="modal fade" tabindex="-1" role="dialog" id="modal-riwayat-obat">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content ">
      <div class="modal-header">
        <h5 class="modal-title">Riwayat Sisa Obat Pasien</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="card-body">
        <div class="form-group row">
          <label class="col-md-2 col-form-label" for="pwd">Periode :</label>
          <input class="col-md-3 form-control m-input datepicker" placeholder="pilih tanggal" type="text" id="date_start">
          &nbsp;<i class="fas fa-minus col-form-label"></i>&nbsp;
          <input class="col-md-3 form-control m-input datepicker" type="text" placeholder="pilih tanggal" id="date_stop">&nbsp;&nbsp;
          <button type="button" class="btn btn-info" id="search_riwayat"><i class="fas fa-search">&nbsp;</i>Search</button>
        </div>
        <table class="table table-striped table-bordered table-hover table-checkable" id="tabel-riwayat-obat">
          <thead>
            <tr style="background:#2860a8;text-align: center;">
              <th style="color:white;">NOTA</th>
              <th style="color:white;">TANGGAL</th>
              <th style="color:white">OBAT</th>
              <th style="color:white">JUMLAH</th>
              <th style="color:white">DOKTER</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <!-- <td colspan=5 style="text-align:center;">NO DATA</td> -->
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<!-- Modal 9 -->
<div class="modal fade" id="data4send" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">

      <div class="modal-header">
        <h5 class="modal-title"><span class="tag_type"></span>Data Pengiriman</h5>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <label class="control-label col-md-4">Penerima</label>
          <div class="col-md-12">
            <input id="penerima_kirim" class="form-control" type="text">
          </div>
          <label class="control-label col-md-4">Alamat</label>
          <div class="col-md-12">
            <input id="alamat_kirim" class="form-control" type="text">
          </div>
          <label class="control-label col-md-4">Kota</label>
          <div class="col-md-12">
            <input id="kota_kirim" class="form-control" type="text">
          </div>
          <label class="control-label col-md-4">Telp/Hp</label>
          <div class="col-md-12">
            <input id="telp_kirim" class="form-control" type="text">
          </div>
          <label class="control-label col-md-4">Telp/Hp</label>
          <div class="col-md-12">
            <input id="telp2_kirim" class="form-control" type="text">
          </div>
          <label class="control-label col-md-4">Biaya</label>
          <div class="col-md-12">
            <input id="biaya_kirim" class="numbers form-control" value="0" type="number">
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" id="save_pengirim" class="btn btn-primary">Simpan</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal 10 -->
<div class="modal fade" id="password-modal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">

      <div class="modal-header">
        <h5 class="modal-title"><span class="tag_type"></span>Masukkan Password</h5>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <label class="control-label col-md-4">Password<span style="color: red" class="required">*</span>
          </label>
          <div class="col-md-8">
            <input id="password" class="form-control" required="required" type="password">
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
        <button type="button" id="simpanPass" class="btn btn-primary">Simpan</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal 11 -->
<div class="modal fade" id="input_antrian" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">

      <div class="modal-header">
        <h5 class="modal-title"><span class="tag_type"></span>Masukkan Antrian</h5>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <label class="control-label col-md-4">Antrian :</label>
          <div class="col-md-8">
            <input id="no_antrian" class="form-control" required="required" type="text">
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
        <button type="button" id="getantrian" class="btn btn-primary">Simpan</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal 12 -->
<div class="modal fade" tabindex="-1" role="dialog" id="no_id">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><span class="tag_type"></span>List Data</h5>
      </div>
      <div class="modal-body">
        <table class="table table-striped- table-bordered table-hover table-checkable" id="daftar_obat">
          <thead>
            <tr style="background:#2860a8;text-align: center;">
              <th style="color:white;">Nota / Noreg</th>
              <th style="color:white;">Nama</th>
              <th style="color:white">SATUAN</th>
              <th style="color:white">JENIS</th>
              <th style="color:white"><?php echo $_SESSION["if_ses_depo"];?></th>
              <th style="color:white">STOK PUSAT</th>
              <th style="color:white">STOK GUDANG</th>
              <th style="color:white">ACTION</th>
            </tr>
          </thead>
          <!-- <tbody>

          </tbody> -->
        </table>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<!--  -->
<div class="modal fade noclose" tabindex="-1" role="dialog" id="list_data">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content ">
            <div class="modal-header">
                <h5 class="modal-title"><span class="tag_type"></span> Pasien</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table id="table_list_data" class="table table-striped- table-bordered table-hover table-checkable" style="text-align:center">
                    <thead>
                        <tr style="background: #2860a8; color:white;text-align:center">
                            <th style="color:white;" width="30%"><div id="notaORnoreg"></div></th>
                            <th style="color:white;" width="50%">NAMA</th>
                            <th style="color:white;" width="20%">ACTION</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- End Modal -->
<script>
  $('#modal-try').click(function() {
    $('#modal1').modal('show');
  });
</script>
<!-- javascript this page -->
<script src="<?php echo base_url() ?>assets/assets_metronic/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
<!-- <script src="<?php //echo base_url()
                  ?>application/modules/User/views/user_view.js" type="text/javascript"></script> -->
<script src="<?php echo base_url() ?>application/modules/Entry_resep/views/entry_resep_view.js" type="text/javascript"></script>
<script type="text/javascript">
  $(function() {
    $(".datepicker").datepicker({
      format: 'yyyy-mm-dd',
      autoclose: true,
      todayHighlight: true,
    });
  });
</script>