"use strict";

// Class definition
var datasigna = "";
var dataresep = "";
var databentukobat = "";
var carabayar = "";
var datadokter = "";
var datadebitur = "";
//
var bayar = "";
var kirimresep = "";
var detail = "";
var loop_detail = "";
var a = "";
var loop_save = 0;
var kuning = 0;
var cekkuning;
var Entri_resep = (function() {
  function getListSigna() {
    $.ajax({
      type: "GET",
      url: "entry_resep/listSigna",
      data: {},
      dataType: "json",

      success: function(data) {
        datasigna = data;
        addRow("inisial");
        KTApp.unblockPage();
      },
      error: function(xhr, status, error) {
        console.log(xhr, status, error);
      }
    });
  }
  function getBentukObat() {
    $.ajax({
      type: "GET",
      url: "entry_resep/get_bentukobat_ctrl",
      data: {},
      dataType: "json",

      success: function(data) {
        databentukobat = data;
      },
      error: function(xhr, status, error) {
        console.log(xhr, status, error);
      }
    });
  }
  function getJenisResep() {
    $.ajax({
      type: "GET",
      url: "entry_resep/jenis_resep_ctrl",
      data: {},
      dataType: "json",

      success: function(data) {
        dataresep = data;
        jQuery.each(dataresep, function(index, val) {
          var a = "";
          a =
            "<option value='" +
            val.KDJNSRESEP +
            "' data-resep='" +
            val.NMJNSRESEP +
            "'><b>" +
            val.KDJNSRESEP +
            " - " +
            val.NMJNSRESEP +
            "</b>" +
            "</option>";
          $("#jenis_resep").append(a);
        });
      },
      error: function(xhr, status, error) {
        console.log(xhr, status, error);
      }
    });
    $("#jenis_resep").select2({
      placeholder: "Pilih Jenis Resep",
      width: "100%"
    });
  }
  function getdokter() {
    $.ajax({
      type: "GET",
      url: "entry_resep/get_dokter",
      data: {},
      dataType: "json",

      success: function(data) {
        datadokter = data;
        jQuery.each(datadokter, function(index, val) {
          var a = "";
          a =
            "<option value='" +
            val.kdDok +
            "' data-dokter='" +
            val.nmDok +
            "'><b>" +
            val.kdDok +
            " - " +
            val.nmDok +
            "</b>" +
            "</option>";
          $("#nmdok").append(a);
        });
      },
      error: function(xhr, status, error) {
        console.log(xhr, status, error);
      }
    });
    $("#nmdok").select2({
      placeholder: "Pilih Dokter",
      width: "100%"
    });
  }
  function getdebitur() {
    $.ajax({
      type: "GET",
      url: "entry_resep/get_debitur",
      data: {},
      dataType: "json",

      success: function(data) {
        datadebitur = data;
        jQuery.each(datadebitur, function(index, val) {
          var a = "";
          a =
            "<option value='" +
            val.KDDEBT +
            "' data-debitur='" +
            val.NMDEBT +
            "'><b>" +
            val.KDDEBT +
            " - " +
            val.NMDEBT +
            "</b>" +
            "</option>";
          $("#nmdeb").append(a);
        });
      },
      error: function(xhr, status, error) {
        console.log(xhr, status, error);
      }
    });
    $("#nmdeb").select2({
      placeholder: "Pilih Debitur",
      width: "100%"
    });
  }
  function getdinas_all() {
    $.ajax({
      type: "GET",
      url: "entry_resep/get_dinas_all",
      data: {},
      dataType: "json",

      success: function(data) {
        jQuery.each(data, function(index, val) {
          var a = "";
          a =
            "<option value='" +
            val.KDDIN +
            "' data-dinas='" +
            val.NMDIN +
            "'><b>" +
            val.KDDIN +
            " - " +
            val.NMDIN +
            "</b>" +
            "</option>";
          $("#din").append(a);
        });
      },
      error: function(xhr, status, error) {
        console.log(xhr, status, error);
      }
    });
    $("#din").select2({
      placeholder: "Pilih Dinas",
      width: "100%"
    });
  }
  function input_number() {
    $(".spcnumbers").on("keypress keyup blur", function(event) {
      $(this).val(
        $(this)
          .val()
          .replace(/[^\d].+/, "")
      );
      if (event.which < 48 || event.which > 57) {
        event.preventDefault();
      }
    });
  }
  return {
    // Init demos
    init: function() {
      // console.log($("#token").val());
      getListSigna();
      getBentukObat();
      getdokter();
      getdebitur();
      getdinas_all();
      getJenisResep();
      input_number();
      setInputMask();
      // $("#list_data").modal({backdrop: 'static', keyboard: false});
    }
  };
})();
function setInputMask() {
  $(".numbers").inputmask({
    alias: "decimal",
    groupSeparator: ",",
    autoGroup: true,
    removeMaskOnSubmit: true,
    rightAlign: true,
    autoUnmask: true
    // groupSeparator: '.',
  });
}
$("#add-qty-nonracik").on("shown.bs.modal", function() {
  $("#qty_resep_diberi").focus();
  $("#qty_resep_diberi, #qty_resep_dokter, #keterangan_nonracik").val("");
  $("#signa_search_nonracik")
    .val("")
    .trigger("change");
});
$("#add-qty-racik").on("shown.bs.modal", function() {
  $("#qty_obat").focus();
  $("#qty_obat").val("");
});
$("#qty_ak_racik").on("shown.bs.modal", function() {
  $("#qty_ak_racikan").focus();
  $("#qty_ak_racikan, #keterangan_racik").val("");
  $("#signa_search_racik")
    .val("")
    .trigger("change");
});
$("#cara_bayar").on("shown.bs.modal", function() {
  $("#bayarbyr, #kembalibyr, #no_kartu, #no_trace, #no_batch, #nominal").val(
    ""
  );
  $("#credit_detail").css("display", "none");
  $("#tabel_pembayaran tbody tr").remove();
  $("#tabel_pembayaran tbody").append(
    "<tr><td colspan='99' style='text-align:center'>NO DATA</td></tr>"
  );
  $("#totalbyr").val(parseFloat($("#total_harga").val()));
  $("#pilih_cara_bayar, #tipe_kartu")
    .val("")
    .trigger("change");
});
$("#data4send").on("shown.bs.modal", function() {
  $("#penerima_kirim").focus();
  $("#telp_kirim, #telp2_kirim").val("");
  $("#penerima_kirim").val($("#nama").val());
  $("#alamat_kirim").val($("#alamat").val());
  $("#kota_kirim").val($("#kota").val());
});
$("#password-modal").on("shown.bs.modal", function() {
  $("#password").focus();
  $("#password").val("");
});
$("#input_antrian").on("shown.bs.modal", function() {
  $("#no_antrian").focus();
  $("#no_antrian").val("");
});
$("#bayarbyr").on("input", function() {
  $("#kembalibyr").val(
    parseFloat($("#bayarbyr").val()) - parseFloat($("#totalbyr").val())
  );
});
$("#cancel").on("click", function() {
  clear();
});
function clear() {
  $("#nmdok")
    .val("")
    .trigger("change");
  $("#nmdeb")
    .val("")
    .trigger("change");
  $("#jenis_resep")
    .val("")
    .trigger("change");
  $("#din")
    .val("")
    .trigger("change");
  $(".dis, .spcdis").attr("disabled", false);
  delete_rincian_obat();
}
var enterkey = 0;
function bentukobat(data, index) {
  var bentukobat = "";
  jQuery.each(data, function(i, val) {
    bentukobat +=
      "<option value='" +
      val.ID_BENTUK +
      "' data-bentukobat='" +
      val.BENTUK_RACIKAN +
      "'>" +
      val.ID_BENTUK +
      " - " +
      val.BENTUK_RACIKAN +
      "</option>";
  });
  var list =
    "<select class='form-control list_bentuk_obat' id='bentukobat-" +
    index +
    "'><option></option>" +
    bentukobat +
    "</select>";
  return list;
}
function buildListSigna(data, index, stat) {
  var signa = "";
  jQuery.each(data, function(i, val) {
    signa +=
      "<option value='" +
      val.SIGNACEPAT +
      "' data-uraian='" +
      val.URAIAN +
      "' data-hari='" +
      val.PERHARI +
      "' data-kode='" +
      val.KODE +
      "' data-signa='" +
      val.SIGNA +
      "'><b>" +
      val.URAIAN +
      "</b> - " +
      val.SIGNACEPAT +
      " - " +
      val.KODE +
      "</option>";
  });
  if (stat == "nonracik") {
    var list =
      "<select class='form-control list_signa' id='signa-" +
      index +
      "'><option></option>" +
      signa +
      "</select>";
    return list;
  } else if (stat == "racik") {
    var list =
      "<select class='form-control list_signa' disabled id='signa-" +
      index +
      "'><option></option>" +
      signa +
      "</select>";
    return list;
  } else if (stat == "racik_plus") {
    var list =
      "<select class='form-control list_signa' id='signa-" +
      index +
      "'><option></option>" +
      signa +
      "</select>";
    return list;
  }
}
var h = 0;
var i = 0;
var j = 0;
window.setTimeout("waktu()", 1000);
var jam = "";
var menit = "";
var detik = "";
var tahun = "";
var bulan = "";
var tgl = "";
var tahun_shift = "";
var bulan_shift = "";
var tgl_shift = "";
var tahun_history = "";
var bulan_history = "";
var tgl_history = "";
var status_entry = "auto";
// $("#user").val($("#user_all").val());
function waktu() {
  var tanggal = new Date();
  var namahari = [
    "Minggu",
    "Senin",
    "Selasa",
    "Rabu",
    "Kamis",
    "Jumat",
    "Sabtu"
  ];
  var namabulan = [
    "Januari",
    "Februari",
    "Maret",
    "April",
    "Mei",
    "Juni",
    "Juli",
    "Agustus",
    "September",
    "Oktober",
    "November",
    "Desember"
  ];
  jam = tanggal.getHours();
  menit = tanggal.getMinutes();
  detik = tanggal.getSeconds();
  tahun = tanggal.getFullYear();
  bulan = tanggal.getMonth() + 1;
  tgl = tanggal.getDate();
  var shift;
  if (jam >= 7 && jam < 14) {
    shift = 1;
    gfg_Run("normal", "shift");
    if (jam == 7 && menit == 0 && detik == 0) {
      shift = 3;
      gfg_Run("minus", "shift");
    }
  } else if (jam >= 14 && jam < 21) {
    shift = 2;
    gfg_Run("normal", "shift");
    if (jam == 14 && menit == 0 && detik == 0) {
      shift = 1;
      gfg_Run("normal", "shift");
    }
  } else {
    shift = 3;
    gfg_Run("normal", "shift");
    if (jam >= 0 && jam < 7) {
      gfg_Run("minus", "shift");
    }
    if (jam == 21 && menit == 0 && detik == 0) {
      shift = 2;
      gfg_Run("normal", "shift");
    }
  }
  setTimeout("waktu()", 1000);
  document.getElementById("date").innerHTML =
    namahari[tanggal.getDay()] +
    ", " +
    tanggal.getDate() +
    "-" +
    namabulan[tanggal.getMonth()] +
    "-" +
    tanggal.getFullYear();
  document.getElementById("jam").innerHTML = jam + ":" + menit + ":" + detik;
  document.getElementById("shift").innerHTML = shift;
}

Date.prototype.subtractDays = function(d) {
  this.setTime(this.getTime() - d * 24 * 60 * 60 * 1000);
  return this;
};
function gfg_Run(condition, condition2) {
  var a = new Date();
  if (condition2 == "shift") {
    if (condition == "minus") {
      a.subtractDays(1);
      tahun_shift = a.getFullYear();
      bulan_shift = a.getMonth() + 1;
      tgl_shift = a.getDate();
    } else if (condition == "normal") {
      tahun_shift = a.getFullYear();
      bulan_shift = a.getMonth() + 1;
      tgl_shift = a.getDate();
    }
  } else if (condition2 == "history") {
    if (condition == "minus") {
      a.subtractDays(90);
      tahun_history = a.getFullYear();
      bulan_history = a.getMonth() + 1;
      tgl_history = a.getDate();
    }
  }
}
function addRow(condition) {
  var a = "";
  var b = "";
  var c = "";
  var d = "";
  var e = "";
  var total = "";
  // console.log(condition);
  h++;
  c =
    "</div></td>" +
    '<td><div id="harga_cell"><input type="text" id="harga" style="text-align:right" disabled class="form-control m-input"></div></td>' +
    '<td><div id="qty_cell"><input type="text" id="qty" disabled class="numbers form-control m-input numbers"></div></td>' +
    '<td><div id="qty2_cell"><input type="text" id="qty2" disabled class="numbers form-control m-input numbers"></div></td>' +
    '<td><div id="total_cell"><input type="text" id="total" style="text-align:right" value="0" disabled class="numbers form-control m-input"></div>' +
    '<input type="hidden" id="h_biji" value="0"></input>' +
    '<input type="hidden" id="hna" value="0"></input>' +
    '<input type="hidden" id="h_rata" value="0"></input>' +
    '<input type="hidden" id="jasa_r" value="0"></input>' +
    '<input type="hidden" id="hari" value="0"></input></div></td>';
  if (condition == "nonracik") {
    i++;
    b =
      '<tr id="baris" style="text-align: center;" data-id="' +
      i +
      '" data-index = "' +
      h +
      '" data-index2 = "' +
      j +
      '">' +
      "<td>" +
      i +
      "</td>" +
      "<td>" +
      '<label class="kt-checkbox"><input type="checkbox" class="form-check-input" value="" id="racikan"><span></span></label>' +
      "</td>" +
      '<td><div id="dataobat_cell"><input type="text" id="dataobat" class="form-control m-input"></div></td>' +
      '<td><div id="namaobat_cell"><input type="text" id="namaobat" disabled class="form-control m-input"></div></td>' +
      '<td><div id="signa_cell">' +
      buildListSigna(datasigna, h, "nonracik");
    d =
      '<td><div id="keterangan_cell"><input type="text" id="keterangan" disabled class="form-control m-input"></td>' +
      '<td><button class="btn btn-danger" type="button"  id="btn-del"><i class="fa fa-times"></i></button></td>' +
      "</tr>";
    total = b + c + d;
    $tr.after(total);
    $(".list_signa").select2({
      placeholder: "Silahkan Pilih"
    });
  } else if (condition == "inisial") {
    i++;
    b =
      '<tr id="baris" style="text-align: center;" data-id="' +
      i +
      '" data-index = "' +
      h +
      '" data-index2 = "' +
      j +
      '">' +
      "<td>" +
      i +
      "</td>" +
      "<td>" +
      '<label class="kt-checkbox"><input type="checkbox" class="form-check-input" value="" id="racikan"><span></span></label>' +
      "</td>" +
      '<td><div id="dataobat_cell"><input type="text" id="dataobat" class="form-control m-input"></div></td>' +
      '<td><div id="namaobat_cell"><input type="text" id="namaobat" disabled class="form-control m-input"></div></td>' +
      '<td><div id="signa_cell">' +
      buildListSigna(datasigna, h, "nonracik");
    // console.log("i = " + i);
    d =
      '<td><div id="keterangan_cell"><input type="text" id="keterangan" disabled class="form-control m-input"></td>' +
      '<td><button class="btn btn-danger" type="button"  id="btn-del"><i class="fa fa-times"></i></button></td>' +
      "</tr>";
    total = b + c + d;
    $("#obat tbody").append(total);
    // $tr.after(total);
    $(".list_signa").select2({
      placeholder: "Silahkan Pilih"
    });
  } else if (condition == "racik") {
    j++;
    // console.log("j = " + j);
    a =
      '<tr id="baris" style="text-align: center;" data-id="' +
      i +
      '"data-index = "' +
      h +
      '" data-index2 = "' +
      j +
      '">' +
      "<td></td>" +
      "<td>" +
      '<label class="kt-checkbox"><input type="checkbox" disabled checked=true class="form-check-input" value="" id="racikan"><span></span></label>' +
      "</td>" +
      '<td><div id="dataobat_cell"><input type="text" id="dataobat" class="form-control m-input"></div></td>' +
      '<td><div id="namaobat_cell"><input type="text" id="namaobat" disabled class="form-control m-input"></div></td>' +
      '<td><div id="signa_cell">' +
      buildListSigna(datasigna, h, "racik");
    e =
      '<td><div id="keterangan_cell"><input type="text" id="keterangan" disabled class="form-control m-input"></td>' +
      '<td><button class="btn btn-success" type="button"  id="btn-done"><i class="fa fa-check"></i></button></td>' +
      "</tr>";
    // '<td><button style="display:none" class="btn btn-danger" type="button"  id="btn-del"><i class="fa fa-times"></i></button></td>' +
    // "</tr>";
    total = a + c + e;
    // $("#obat tbody").append(total);
    $tr.after(total);
    $(".list_signa").select2({
      placeholder: "Silahkan Pilih"
    });
  } else if (condition == "racik_add") {
    j++;
    // console.log("j = " + j);
    a =
      '<tr id="baris" style="text-align: center;" data-id="' +
      i +
      '"data-index = "' +
      h +
      '" data-index2 = "' +
      j +
      '">' +
      "<td></td>" +
      "<td>" +
      '<label class="kt-checkbox"><input type="checkbox" disabled checked=true class="form-check-input" value="" id="racikan"><span></span></label>' +
      "</td>" +
      '<td><div id="dataobat_cell"><input type="text" id="dataobat" class="form-control m-input"></div></td>' +
      '<td><div id="namaobat_cell"><input type="text" id="namaobat" disabled class="form-control m-input"></div></td>' +
      '<td><div id="signa_cell">' +
      buildListSigna(datasigna, h, "racik");
    e =
      '<td><div id="keterangan_cell"><input type="text" id="keterangan" disabled class="form-control m-input"></td>' +
      '<td><button class="btn btn-danger" type="button"  id="btn-del"><i class="fa fa-times"></i></button></td>' +
      "</tr>";
    // '<td><button style="display:none" class="btn btn-danger" type="button"  id="btn-del"><i class="fa fa-times"></i></button></td>' +
    // "</tr>";
    total = a + c + e;
    // $("#obat tbody").append(total);
    $tr.after(total);
    $(".list_signa").select2({
      placeholder: "Silahkan Pilih"
    });
  } else if (condition == "racik_plus") {
    j++;
    // console.log("j = " + j);
    a =
      '<tr id="baris" style="text-align: center;" data-id="' +
      i +
      '"data-index = "' +
      h +
      '" data-index2 = "' +
      j +
      '">' +
      "<td></td>" +
      "<td>" +
      '<label class="kt-checkbox"><input type="checkbox" disabled checked=true class="form-check-input" value="" id="racikan"><span></span></label>' +
      "</td>" +
      '<td><div id="dataobat_cell"><input type="text" id="dataobat" disabled class="form-control m-input"></div></td>' +
      '<td><div id="namaobat_cell">' +
      bentukobat(databentukobat, h) +
      "</div></td>" +
      '<td><div id="signa_cell">' +
      buildListSigna(datasigna, h, "racik_plus");
    e =
      '<td><div id="keterangan_cell"><input type="text" id="keterangan" disabled class="form-control m-input"></td>' +
      '<td><button class="btn btn-danger" type="button"  id="btn-del"><i class="fa fa-times"></i></button></td>' +
      "</tr>";
    // '<td><button style="display:none" class="btn btn-danger" type="button"  id="btn-del"><i class="fa fa-times"></i></button></td>' +
    // "</tr>";
    total = a + c + e;
    // $("#obat tbody").append(total);
    $tr.after(total);
    $(".list_signa").select2({
      placeholder: "Silahkan Pilih"
    });
    $(".list_bentuk_obat").select2({
      placeholder: "Silahkan Pilih"
    });
  }
  renumber();
  setInputMask();
}
$("#obat").on("click", "tr #racikan", function() {
  clickedLine = $(this)
    .closest("tr")
    .data("index");
  // console.log(clickedLine);
  $tr = $("#obat > tbody").find('tr[data-index="' + clickedLine + '"]');
  var racikan = $tr.find("#racikan");
  if (racikan.prop("checked") == true) {
    $tr.find(".list_signa").attr("disabled", true);
    $tr.find(".list_signa").val("");
    $tr.find(".list_signa").trigger("change");
  } else if (racikan.prop("checked") == false) {
    $tr.find(".list_signa").attr("disabled", false);
  }
});
function getdinas(kddeb) {
  var datadinas = "";
  $.ajax({
    type: "GET",
    url: "entry_resep/get_dinas",
    data: {
      kddeb: kddeb
    },
    dataType: "json",

    success: function(data) {
      datadinas = data;
      jQuery.each(datadinas, function(index, val) {
        var a = "";
        a =
          "<option value='" +
          val.KDDIN +
          "' data-dinas='" +
          val.NMDIN +
          "'><b>" +
          val.KDDIN +
          " - " +
          val.NMDIN +
          "</b>" +
          "</option>";
        $("#din").append(a);
      });
    },
    error: function(xhr, status, error) {
      console.log(xhr, status, error);
    }
  });
  $("#din").select2({
    placeholder: "Pilih Dinas",
    width: "100%"
  });
}
function get_status_naik_kelas(no) {
  $.ajax({
    type: "POST",
    url: "entry_resep/get_status_naik_kelas_ctrl",
    dataType: "json",
    data: {
      no: no
    },
    success: function(data) {
      // console.log(data);
      if (data == "") {
        $("#status_naik_kelas").val(0);
      } else {
        $("#status_naik_kelas").val(data[0].STATUSNAIKKELAS);
      }
      // $tr.find("#jasa_r").val(data[0].JASA_R);
    },
    error: function(xhr, status, error) {}
  });
}
function get_plafon(notareg) {
  $.ajax({
    type: "POST",
    url: "entry_resep/get_plafon_ctrl",
    dataType: "json",
    data: {
      notareg: notareg
    },
    success: function(data) {
      $("#plafon").val(data[0].PLAFON);
      $("#biaya_bulan_ini").val(data[0].RUPIAH);
    },
    error: function(xhr, status, error) {}
  });
}
var bit = 1;
var nota_or_noreg="";
var depo = "";
var layanan = "";
function caridata(event) {
  var x = event.keyCode;
  if (x == 13) {
    var notareg = document.getElementById("notareg").value;
    layanan = $("#layanan").val();
    depo = $("#depo_id").val();
    if (notareg == "") {
      swal.fire("error", "Nomor nota / registrasi kosong", "error");
    } else {
      if (layanan == "Rawat Jalan") {
        bit = 1;
        nota_or_noreg = "NOTA";
        $("#status_naik_kelas").val(0);
      } else if (layanan == "Rawat Inap") {
        bit = 0;
        nota_or_noreg = "NOREG";
        get_status_naik_kelas(notareg);
      }
      if (notareg == "0000000000") {
        status_entry = "manual";
        $("#jenispx").val("TUNAI");
        $("#nmdeb").val("999");
        $("#nmdeb").trigger("change");
        // getdinas("999");
        // console.log(getdinas("999"));
        $("#din").val("00000");
        $("#din").trigger("change");
      } else {
        getListData(notareg, layanan);
        $("#list_data").modal({backdrop: 'static'});
      }
    }
  }
}
function getListData(notareg, layanan) {
  $("#notaORnoreg").html(nota_or_noreg);
  $("#table_list_data").DataTable({
    destroy: true,
    responsive: true,
    // dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
    lengthMenu: [5, 10, 25, 50],
    pageLength: 5,
    language: {
      lengthMenu: "Display _MENU_"
    },
    searchDelay: 500,
    ordering: true,
    processing: true,
    serverSide: false,
    ajax: {
      url: "entry_resep/get_list_data_ctrl",
      type: "POST",
      data: {
        notareg: notareg,
        layanan : layanan
      }
    },
    columns: [
      { data: "NOTAREG" },
      { data: "NAMA" },
      { data: "btn" }
    ],
    drawCallback: function(settings) {
      $('[data-toggle="kt-tooltip"]').tooltip();
    }
  });
  KTApp.unblockPage();
}
function fill_data(kode){
  $("#list_data").modal('hide');
  status_entry = "auto";
  KTApp.blockPage({
    overlayColor: "#000000",
    type: "v2",
    state: "primary",
    message: "Processing..."
  });
  get_plafon(kode);
  $.ajax({
    url: "index.php/entry_resep/autofill",
    type: "POST",
    data: {
      kode: kode,
      bit: bit,
      depo: depo
    },
    success: function(data) {
      var hasil = JSON.parse(data);
      var val = hasil.data[0];
      if (val.STATUS == 0) {
        swal.fire("error", val.NOTIF, "error");
      } else {
        //biodata pasien
        document.getElementById("notareg").value = val.NOTA;
        document.getElementById("nama").value = val.NAMA;
        document.getElementById("alamat").value = val.ALAMAT;
        document.getElementById("kota").value = val.KOTA;
        //debitur pasien
        $("#kddeb").val(val.KODE_DEBITUR);
        document.getElementById("jenispx").value = val.JENIS_PASIEN;
        $("#rmshow").val(val.RM + " / " + val.IDX);
        document.getElementById("rm").value = val.RM;
        document.getElementById("idx").value = val.IDX;
        $("#nmdeb").val(val.KODE_DEBITUR);
        $("#nmdeb").trigger("change");
        // console.log($("#nmdeb").val());
        // console.log($("#nmdeb").find(":selected").data("debitur"));
        $("#din").val(val.KODE_DINAS);
        $("#din").trigger("change");
        // document.getElementById("kddin").value = val.KODE_DINAS;
        // document.getElementById("din").value = val.DINAS;
        //info klinik
        document.getElementById("kdklin").value = val.KODE_KLINIK;
        document.getElementById("nmklin").value = val.KLINIK;
        document.getElementById("kddok").value = val.KODE_DOKTER;
        // $("#user").val($("#user_all").val());
        $("#nmdok").val(val.KODE_DOKTER);
        $("#nmdok").trigger("change");
        // console.log($("#nmdok").val());
        // console.log($("#nmdok").find(":selected").data("dokter"));
        // if
        if (layanan == "Rawat Jalan") {
          $(".dis , .spcdis").attr("disabled", true);
        } else if (layanan == "Rawat Inap") {
          $(".dis").attr("disabled", true);
          $(".spcdis").attr("disabled", false);
          getdinas(val.KODE_DINAS);
        }
      }
      KTApp.unblockPage();
    }
  });
  gfg_Run("minus", "history");
}
// if ($("#nmdeb").val() != $("#hidden_deb").val()) {
//   $("#din").val("00000");
//   $("#din").trigger("change");
// }
$("#nmdeb").on("change", function() {
  var kode_debitur = $("#nmdeb").val();
  $("#din").html("");
  $.ajax({
    type: "GET",
    url: "entry_resep/get_dinas",
    data: {
      kddeb: kode_debitur
    },
    dataType: "json",

    success: function(data) {
      jQuery.each(data, function(index, val) {
        var a = "";
        a =
          "<option value='" +
          val.KDDIN +
          "' data-dinas='" +
          val.NMDIN +
          "' data-debdinas='" +
          val.KDDEBT +
          "'><b>" +
          val.KDDIN +
          " - " +
          val.NMDIN +
          "</b>" +
          "</option>";
        $("#din").append(a);
      });
    },
    error: function(xhr, status, error) {
      console.log(xhr, status, error);
    }
  });
  if ($("#kddeb").val() != $("#nmdeb").val()) {
    $("#din").val("00000");
    $("#din").trigger("change");
  }
});
var clickedLine = "";
var table = "";
var $tr = "";
// cari obat
$("#obat").on("keydown", "tr #dataobat", function(evt) {
  if (evt.keyCode == 13) {
    evt.preventDefault();
    clickedLine = $(this)
      .closest("tr")
      .data("index");
    // console.log(clickedLine);
    $tr = $("#obat > tbody").find('tr[data-index="' + clickedLine + '"]');
    var depo = document.getElementById("depo_id").value;
    var keyword = $tr.find("#dataobat").val();
    if (keyword.length < 3) {
      Swal.fire({
        type: "info",
        text: "Input minimal 3 karakter",
      });
    } else {
      table = $("#daftar_obat").DataTable({
        destroy: true,
        processing: true,
        ajax: {
          type: "POST",
          url: "entry_resep/showobat",
          data: {
            layanan: depo,
            keyword: keyword
          }
        },
        columns: [
          { data: "kode" },
          { data: "barang" },
          { data: "satuan" },
          { data: "jenis" },
          { data: "depo" },
          { data: "stok_pusat" },
          { data: "stok_gudang" },
          { data: "act" }
        ],
        createdRow: function(row, data, index) {
          $("td", row)
            .eq(0)
            .addClass("text-center");
        },
        drawCallback: function(settings) {
          // $('#daftar_obat tbody tr').eq(0).select();
          // if( typeof table !== 'undefined')
          this.api()
            .row(":eq(0)")
            .select();
        },
        keys: {
          keys: [13 /* ENTER */, 38 /* UP */, 40 /* DOWN */]
        }
      });

      // console.log(table.row(":eq(0)").data());

      $("#daftar_obat").on("click", "tr #choose", function() {
        var data = table.row($(this).closest("tr")).data();
        var stok1 = $(this).closest('tr').find('td').eq(4).html();
        var stok2 = $(this).closest('tr').find('td').eq(5).html();
        var stok3 = $(this).closest('tr').find('td').eq(6).html();
        if ((stok1 == '0')&&(stok2 == '0')&&(stok3 == '0')){
          Swal.fire({
            type: "info",
            text: "Stok kosong",
          });
        }else{
          pilih(data);
          $("#dpo").modal("hide");
        }
      });

      $("#daftar_obat > tbody").on("dblclick", "tr", function() {
        var data = table.row(this).data();
        var stok1 = $(this).find('td').eq(4).html();
        var stok2 = $(this).find('td').eq(5).html();
        var stok3 = $(this).find('td').eq(6).html();
        if ((stok1 == '0')&&(stok2 == '0')&&(stok3 == '0')){
          Swal.fire({
            type: "info",
            text: "Stok kosong",
          });
        }else{
          pilih(data);
          $("#dpo").modal("hide");
        }
      });

      function pilih(datadata) {
        // var $tr = $('#table-list_obat > tbody').find('tr[data-index="'+x+'"]');
        loading();
        // $("#modal-obat").modal("hide");

        $.ajax({
          type: "POST",
          url: "resep_online/getStandartObat",
          dataType: "json",
          data: {
            kdbrg: datadata["kode"],
            kddeb: $("#nmdeb").val()
          },
          success: function(data) {
            if (data.status == "S") {
              cekSisaObat(datadata);
            } else {
              Swal.fire({
                title: "Standart Obat",
                text: data.message,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: "Ya",
                cancelButtonText: "Tidak"
              }).then(result => {
                if (result.value) {
                  //tunai
                  Swal.fire({
                    title: "Tunai",
                    text: "Mau ditunaikan ?? ",
                    type: "question",
                    showCancelButton: true,
                    confirmButtonColor: "#3085d6",
                    cancelButtonColor: "#d33",
                    confirmButtonText: "Ya",
                    cancelButtonText: "Tidak"
                  }).then(result => {
                    if (result.value) {
                      //ganti warna
                      $tr.css("background-color", "#ffff99"); //kuning "rgb(255, 255, 153)"
                      // $tr.find("#tunai").attr("checked", true);
                      // $tr.find("#tunai").trigger("change");
                    } else {
                    }
                    cekSisaObat(datadata);
                  });
                } else {
                  $tr.find("#dataobat").val("");
                  $tr.find("#namaobat").val("");
                  KTApp.unblockPage();
                  // kode = $tr.find("#kdbrg_hide").val();
                  // nama = $tr.find("#nmbrg_hide").val();
                  // satuan = $tr.find("#satuan_hide").val();
                  // cekSisaObat(kode, nama, satuan);
                }
              });
            }
          },
          error: function(xhr, status, error) {}
        });
      }
      function cekSisaObat(datadata) {
        // console.log(kode,nama,satuan)
        // var $tr = $("#table-list_obat > tbody").find('tr[data-index="' + x + '"]');
        $.ajax({
          type: "POST",
          url: "resep_online/cekSisaObat",
          dataType: "json",
          data: {
            kdbrg: datadata["kode"],
            kddeb: $("#nmdeb").val(),
            norm: $("#rm").val()
          },
          success: function(data) {
            if (data.status == "S") {
              // $tr.find("#kdbrg_cek").val(kode);
              // $tr.find("#nmbrg_cek").val(nama);
              // $tr.find("#satuan").val(satuan);
              fill_row(datadata);
            } else {
              Swal.fire({
                title: "Sisa Obat",
                text: data.message,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: "Ya",
                cancelButtonText: "Tidak"
              }).then(result => {
                if (result.value) {
                  fill_row(datadata);
                  $tr.css("background-color", "#ff9999"); //pink "rgb(255, 153, 153)"
                  // $tr.find("#kdbrg_cek").val(kode);
                  // $tr.find("#nmbrg_cek").val(nama);
                  // $tr.find("#satuan").val(satuan);
                  //ganti warna
                } else {
                  $tr.find("#dataobat").val("");
                  $tr.find("#namaobat").val("");
                  // $tr.find("#kdbrg_cek").val($tr.find("#kdbrg_hide").val());
                  // $tr.find("#nmbrg_cek").val($tr.find("#nmbrg_hide").val());
                  // $tr.find("#satuan").val($tr.find("#satuan_hide").val());
                }
              });
            }
            KTApp.unblockPage();
          },
          error: function(xhr, status, error) {}
        });
      }
      function fill_row(data) {
        $tr.find("#dataobat").val(data["kode"]);
        $tr.find("#namaobat").val(data["barang"]);
        get_h_jual(
          data["kode"],
          $("#nmdeb").val(),
          $("#depo_id").val(),
          bit,
          0
        );
        // console.log(data["kode"],$("#nmdeb").val(),$("#depo_id").val(),bit,0);
        get_h_biji(data["kode"]);
        get_hna(data["kode"]);
        get_h_rata(data["kode"]);
        $tr.find("#qty").attr("disabled", false);
        $tr.find("#qty2").attr("disabled", false);
        // $tr.find("#signa").attr("disabled", false);
        $tr.find("#keterangan").attr("disabled", false);
      }

      //Handle event when cell gains focus
      $("#daftar_obat").on("key-focus.dt", function(e, datatable, cell) {
        // Select highlighted row
        $(table.row(cell.index().row).node()).addClass("selected");
      });
      // Handle event when cell looses focus
      $("#daftar_obat").on("key-blur.dt", function(e, datatable, cell) {
        // Deselect highlighted row
        $(table.row(cell.index().row).node()).removeClass("selected");
      });
      // Handle key event that hasn't been handled by KeyTable
      $("#daftar_obat").on("key.dt", function(
        e,
        datatable,
        key,
        cell,
        originalEvent
      ) {
        // If ENTER key is pressed
        if (key === 13) {
          // Get highlighted row data
          var data = table.row(cell.index().row).data();
          var stok1 = Object.values(data)[4];
          var stok2 = Object.values(data)[5];
          var stok3 = Object.values(data)[6];
          if ((stok1 == '0')&&(stok2 == '0')&&(stok3 == '0')){
            Swal.fire({
              type: "info",
              text: "Stok kosong",
            });
          }else{
            pilih(data);
            $("#dpo").modal("hide");
          }
        }
      });
      $("#dpo").modal({backdrop: 'static'});
    }
    // $("#daftar_obat > tbody > tr").eq(0).attr("class", "selected");
  }
});
var data22;
function get_h_biji(kode) {
  $.ajax({
    type: "POST",
    url: "entry_resep/get_h_biji_ctrl",
    dataType: "json",
    data: {
      kode: kode
    },
    success: function(data) {
      $tr.find("#h_biji").val(data[0].HBIJI);
      // console.log(data[0].HBIJI);
    },
    error: function(xhr, status, error) {}
  });
}
function get_h_jual(kode, deb, layanan, is_jalan, status) {
  $.ajax({
    type: "POST",
    url: "entry_resep/get_h_jual_ctrl",
    dataType: "json",
    data: {
      kode: kode,
      deb: deb,
      layanan: layanan,
      is_jalan: is_jalan,
      status: status
    },
    success: function(data) {
      $tr.find("#harga").val(data[0].HJUAL);
      // console.log(data[0].HJUAL);
    },
    error: function(xhr, status, error) {}
  });
}
function get_hna(kode) {
  $.ajax({
    type: "POST",
    url: "entry_resep/get_hna_ctrl",
    dataType: "json",
    data: {
      kode: kode
    },
    success: function(data) {
      $tr.find("#hna").val(data[0].HNA);
      // console.log(data[0].HNA);
    },
    error: function(xhr, status, error) {}
  });
}
function get_h_rata(kode) {
  $.ajax({
    type: "POST",
    url: "entry_resep/get_h_rata_ctrl",
    dataType: "json",
    data: {
      kode: kode
    },
    success: function(data) {
      // $tr.find("#h_rata").val(data[0].HRATA);
      $tr.find("#h_rata").val(data[0].HRATA);
      // console.log(data[0].HRATA);
    },
    error: function(xhr, status, error) {}
  });
}
function get_jasa_r(jumlah, deb, jenis_r, status, kode) {
  $.ajax({
    type: "POST",
    url: "entry_resep/get_jasa_r_ctrl",
    dataType: "json",
    data: {
      jumlah: jumlah,
      deb: deb,
      jenis_r: jenis_r,
      status: status,
      kode: kode
    },
    success: function(data) {
      $tr.find("#jasa_r").val(data[0].JASA_R);
      // console.log(data[0].JASA_R);
      count_subtotal();
    },
    error: function(xhr, status, error) {}
  });
}
var data_id = "";
var status_racik = "";
var data_index = "";
var data_index2 = "";
var isitext = "";
var trlength1 = "";
var trlength2 = "";
var deldata = "";
$("#obat").on("click", "tr #btn-del", function() {
  var baris = $(this).parents("tr")[0];
  // trlength1 = $("#obat > tbody > tr").length;
  // // data_id = $(this).parents("tr").data("id");
  data_id = $(this)
    .parents("tr")
    .data("id");
  data_index = $(this)
    .parents("tr")
    .data("index");
  status_racik = $("#obat > tbody > tr")
    .eq(data_index)
    .find("td")
    .eq(1)
    .find("#racikan")
    .prop("checked");
  data_index2 = $(this)
    .parents("tr")
    .data("index2");
  isitext = $("#obat > tbody > tr")
    .eq(data_index)
    .find("td")
    .eq(0)
    .text();
  // console.log("isi " + isitext);
  baris.remove();
  // $('#obat tr[data-id="' + data_id + '"]').remove();
  // ////////
  // var tr = $("#obat tbody tr");
  trlength2 = $("#obat > tbody > tr").length;
  // deldata = trlength1 - trlength2;
  // console.log("length = " + trlength2);
  renumber();
  i--;
  h--;
  // h = h - deldata;

  // console.log(trlength);
  if (trlength2 < 1) {
    h = 0;
    i = 0;
    j = 0;
    addRow("inisial");
    count_subtotal();
  }
  count_subtotal();
});
$("#obat").on("click", "tr #btn-done", function() {
  // $("#qty_ak_racik").modal({backdrop: 'static'});
  addRow("racik_plus");
  j = 0;
  // addRow("nonracik");
  $tr.find("#btn-done").attr("id", "btn-del");
  $tr.find("#btn-del").removeClass("btn-success");
  $tr.find("#btn-del").addClass("btn-danger");
  $tr
    .find("#btn-del")
    .find("i")
    .removeClass("fa-check");
  $tr
    .find("#btn-del")
    .find("i")
    .addClass("fa-times");
});
var forloop = 0;
var newid = 1;
var no_data = "";
var id_before = "";
var id_after = "";

function renumber() {
  var index = 1;
  var last = "";
  $("#obat tbody tr").each(function(i) {
    if (i == 0) {
      $(this)
        .find("td")
        .eq(0)
        .html(index); //.find('.num_text')
      index++;
      last = $(this).attr("data-id");
    } else {
      if (last !== $(this).attr("data-id")) {
        $(this)
          .find("td")
          .eq(0)
          .html(index); //.find('.num_text')
        index++;
        last = $(this).attr("data-id");
      }
    }
    // console.log(i); //$(this).attr("data-id")
  });
}
// function renumber() {
//   console.log("data-idx = " + data_index);
//   for (forloop = data_index; forloop < trlength2; forloop++) {
//     // $tr.find("#baris")["prevObject"][0].setAttribute("data-index", forloop);
//     // $("#obat > tbody > tr")
//     //   .eq(forloop)[0]
//     //   .setAttribute("data-index", forloop);
//     data_index2 = $("#obat > tbody > tr")
//       .eq(forloop)
//       .data("index2");
//     newid = $("#obat > tbody > tr")
//       .eq(forloop)
//       .data("id");
//     if (status_racik == true) {
//       $("#obat > tbody > tr")
//         .eq(forloop)[0]
//         .setAttribute("data-index", forloop);
//       if (data_index2 == 0) {
//         $("#obat > tbody > tr")
//           .eq(forloop)[0]
//           .setAttribute("data-index2", data_index2);
//       } else {
//         $("#obat > tbody > tr")
//           .eq(forloop)[0]
//           .setAttribute("data-index2", data_index2 - 1);
//       }
//     } else if (status_racik == false) {
//       $("#obat > tbody > tr")
//         .eq(forloop)[0]
//         .setAttribute("data-index", forloop);
//       $("#obat > tbody > tr")
//         .eq(forloop)
//         .removeData();
//       $("#obat > tbody > tr")
//         .eq(forloop)[0]
//         .setAttribute("data-id", newid - 1);
//     }
//     if (forloop != 0) {
//       id_before = $("#obat > tbody > tr")
//         .eq(forloop - 1)
//         .data("id");
//       id_after = $("#obat > tbody > tr")
//         .eq(forloop)
//         .data("id");
//       if (id_before == after) {
//         $("#obat > tbody > tr")
//           .eq(forloop)
//           .find("td")
//           .eq(0)
//           .html("");
//       } else {
//         $("#obat > tbody > tr")
//           .eq(forloop)
//           .find("td")
//           .eq(0)
//           .html(id_after);
//       }
//     } else {
//       $("#obat > tbody > tr")
//         .eq(forloop)
//         .find("td")
//         .eq(0)
//         .html("1");
//     }

//     // no_data = $("#obat > tbody > tr")
//     //   .eq(forloop)
//     //   .find("td")
//     //   .eq(0)
//     //   .text();
//     // if (isitext == "") {
//     //   //problem with index2
//     // } else {
//     // }
//     // console.log("no_data = " + no_data);
//     // $("#obat > tbody > tr")
//     //   .eq(forloop)
//     //   .removeData();
//     // $("#obat > tbody > tr")
//     //   .eq(forloop)[0]
//     //   .setAttribute("data-id", newid - 1);
//     // if (no_data == "") {
//     //   $("#obat > tbody > tr")
//     //     .eq(forloop)
//     //     .find("td")
//     //     .eq(0)
//     //     .html("");
//     // } else {
//     //   $("#obat > tbody > tr")
//     //     .eq(forloop)
//     //     .find("td")
//     //     .eq(0)
//     //     .html(newid - 1);
//     // }

//     // console.log("data idx ="+$("#obat > tbody > tr").eq(forloop).data("index"));
//     // console.log("loop = "+ forloop);
//     // console.log($("#obat > tbody > tr").eq(forloop)[0]);
//   }
// }
// try
$("#obat").on("click", "tr", function() {
  var id = $(this).data("index");
  // console.log(id);
});
// try
// cek enter
$(document).on("keypress", ".TabOnEnter", function(e) {
  //Only do something when the user presses enter
  if (e.keyCode == 13) {
    var nextElement = $('[tabindex="' + (this.tabIndex + 1) + '"]');
    // console.log(this, nextElement);
    if (nextElement.length) nextElement.focus();
    else $('[tabindex="1"]').focus();
  }
});
$("#riwayat-obat").click(function() {
  var rm = $("#rm").val();
  //   var tahun_history = "";
  // var bulan_history = "";
  // var tgl_history = "";
  var date1 = tahun_history + "-" + bulan_history + "-" + tgl_history;
  // var date1 = "2000-10-04";
  var date2 = tahun + "-" + bulan + "-" + tgl;
  // console.log(date1 + "///" + date2);
  if (rm.length == 0) {
    swal.fire("Lengkapi Biodata Pasien");
  } else if (rm.length > 0) {
    $("#modal-riwayat-obat").modal({backdrop: 'static'});
    get_tabel_riwayat_obat(rm, date1, date2);
  }
});

$("#search_riwayat").click(function() {
  var rm = $("#rm").val();
  var day1 = $("#date_start")
    .datepicker("getDate")
    .getDate();
  var month1 =
    $("#date_start")
      .datepicker("getDate")
      .getMonth() + 1;
  var year1 = $("#date_start")
    .datepicker("getDate")
    .getFullYear();
  var date1 = year1 + "-" + month1 + "-" + day1;
  var day2 = $("#date_stop")
    .datepicker("getDate")
    .getDate();
  var month2 =
    $("#date_stop")
      .datepicker("getDate")
      .getMonth() + 1;
  var year2 = $("#date_stop")
    .datepicker("getDate")
    .getFullYear();
  var date2 = year2 + "-" + month2 + "-" + day2;
  // console.log(rm);
  // console.log(date1);
  // console.log(date2);
  get_tabel_riwayat_obat(rm, date1, date2);
});

function get_tabel_riwayat_obat(rm, date1, date2) {
  $("#tabel-riwayat-obat").DataTable({
    destroy: true,
    processing: true,
    ajax: {
      type: "POST",
      url: "entry_resep/get_riwayat_obat",
      data: {
        rm: rm,
        date1: date1,
        date2: date2
      }
    },
    columns: [
      { data: "nota" },
      { data: "tanggal" },
      { data: "obat" },
      { data: "jumlah" },
      { data: "dokter" }
    ],
    createdRow: function(row, data, index) {
      $("td", row)
        .eq(0)
        .addClass("text-center");
    }
  });
}
$("#cancel").click(function() {
  document.getElementsByClassName("dis").value = "";
  document.getElementsByClassName("spcdis").value = "";
  $(".dis, .spcdis").attr("disabled", false);
  // $("#s-riwayat-obat").modal({backdrop: 'static'});
});

// add quantity
var racikan_id = 0;
$("#proses-qty-ak").on("click", function() {
  // var getsigna = $("#signa_search_racik").val();
  // $tr.find("#signa").val(getsigna);
  //$tr.find("#qty").val($("#qty_ak_racikan").val());

  var l_table = $("#obat tbody tr").length;
  addRow("racik_plus");
  var newtr = $("#obat tbody")
    .find("tr")
    .eq(l_table - 1);
  newtr.find("#keterangan").val($("#keterangan_racik").val());
  newtr.find("#qty").val($("#qty_ak_racikan").val());
  newtr.find("#namaobat").val($("#bentuk_resep").val());
  newtr.find("#qty, #qty2, #keterangan").attr("disabled", false);
  newtr.find("#harga").val("0");
  $("#qty_ak_racik").modal("hide");
});
var hitung_hari = 0;
var perhari = 0;
$("#obat").on("input", "tr #qty", function() {
  clickedLine = $(this)
    .closest("tr")
    .data("index");
  $tr = $("#obat > tbody").find('tr[data-index="' + clickedLine + '"]');
  $tr.find("#total").val($tr.find("#harga").val() * $tr.find("#qty").val());
  if ($tr.find(".list_signa").prop("disabled") == false) {
    perhari = parseFloat(
      $tr
        .find(".list_signa")
        .find(":selected")
        .data("hari")
    );
    if (perhari == 0) {
      hitung_hari = 0;
    } else {
      hitung_hari = Math.ceil(parseFloat($tr.find("#qty").val()) / perhari);
    }
    $tr.find("#hari").val(hitung_hari);
  } else if ($tr.find(".list_signa").prop("disabled") == true) {
    $tr.find("#hari").val(0);
  }
  $tr.find("#qty2").val($tr.find("#qty").val());
  count_subtotal();
});
$("#obat").on("change", "tr .list_bentuk_obat", function() {
  clickedLine = $(this)
    .closest("tr")
    .data("index");
  $tr = $("#obat > tbody").find('tr[data-index="' + clickedLine + '"]');
  $tr.find("#qty").attr("disabled", false);
  $tr.find("#qty2").attr("disabled", false);
  $tr.find("#keterangan").attr("disabled", false);
});
$("#obat").on("focus", "tr #keterangan", function() {
  clickedLine = $(this)
    .closest("tr")
    .data("index");
  $tr = $("#obat > tbody").find('tr[data-index="' + clickedLine + '"]');
  var jum = $tr.find("#qty").val();
  var jenis_r = "";
  var kdbrg = "";
  var stat = $("#status_naik_kelas").val();
  if ($tr.find("#racikan").prop("checked") == false) {
    jenis_r = 1;
  } else if ($tr.find("#racikan").prop("checked") == true) {
    jenis_r = $tr.find(".list_bentuk_obat").val();
  }
  if ($tr.find(".list_signa").prop("disabled") == false) {
    if ($tr.find("#dataobat").prop("disabled") == false) {
      kdbrg = $tr.find("#dataobat").val();
    } else if ($tr.find("#dataobat").prop("disabled") == true) {
      kdbrg = "000000";
    }
    get_jasa_r(jum, $("#nmdeb").val(), jenis_r, stat, kdbrg);
  } else if ($tr.find(".list_signa").prop("disabled") == true) {
    $tr.find("#jasa_r").val(0);
  }
});
$("#obat").on("keypress", "tr #keterangan", function(evt) {
  if (evt.keyCode == 13) {
    clickedLine = $(this)
      .closest("tr")
      .data("index");
    tambah_baris();
  }
});
$("#obat").on("keypress", "tr #qty", function(evt) {
  if (evt.keyCode == 13) {
    clickedLine = $(this)
      .closest("tr")
      .data("index");
    var jum = $tr.find("#qty").val();
    var jenis_r = "";
    var kdbrg = "";
    var stat = $("#status_naik_kelas").val();
    if ($tr.find("#racikan").prop("checked") == false) {
      jenis_r = 1;
    } else if ($tr.find("#racikan").prop("checked") == true) {
      jenis_r = $tr.find(".list_bentuk_obat").val();
    }
    if ($tr.find(".list_signa").prop("disabled") == false) {
      if ($tr.find("#dataobat").prop("disabled") == false) {
        kdbrg = $tr.find("#dataobat").val();
      } else if ($tr.find("#dataobat").prop("disabled") == true) {
        kdbrg = "000000";
      }
      get_jasa_r(jum, $("#nmdeb").val(), jenis_r, stat, kdbrg);
    } else if ($tr.find(".list_signa").prop("disabled") == true) {
      $tr.find("#jasa_r").val(0);
    }
    tambah_baris();
  }
});

function tambah_baris() {
  $tr = $("#obat > tbody").find('tr[data-index="' + clickedLine + '"]');
  var racikan = $tr.find("#racikan");
  //
  //
  // $tr.find("#qty, #qty2, #dataobat, #keterangan").attr("disabled", "true");
  // $tr.find("#dataobat").attr("disabled", "true");
  // console.log(racikan.prop("checked"));
  // $(".rwyt_obat_kd").val($tr.find("#dataobat").val());
  // $(".rwyt_obat_nm").val($tr.find("#namaobat").val());
  if (racikan.prop("checked") == true) {
    racikan_id = 1;
    if ($tr.find("#btn-done").length == 1) {
      $tr.find("#btn-done").attr("id", "btn-del");
      $tr.find("#btn-del").removeClass("btn-success");
      $tr.find("#btn-del").addClass("btn-danger");
      $tr
        .find("#btn-del")
        .find("i")
        .removeClass("fa-check");
      $tr
        .find("#btn-del")
        .find("i")
        .addClass("fa-times");
      if (j == 0 && racikan_id == 1) {
        j++;
        $tr.find("#baris")["prevObject"][0].setAttribute("data-index2", j);
      }
      addRow("racik");
      count_subtotal();
    } else if (
      $tr.find("#btn-del").length == 1 &&
      $tr
        .find("td")
        .eq(0)
        .html().length == 1
    ) {
      addRow("racik");
      count_subtotal();
    } else if (
      $tr.find("#btn-del").length == 1 &&
      $tr
        .find("td")
        .eq(0)
        .html().length == 0 &&
      $tr.find("#dataobat").prop("disabled") == false
    ) {
      addRow("racik_add");
      count_subtotal();
    } else if ($tr.find("#dataobat").prop("disabled") == true) {
      addRow("nonracik");
      count_subtotal();
    }
    // $("#add-qty-racik").modal({backdrop: 'static'});
    // $("#qty_obat").focus();
    // search_riwayat_obat_pasien($tr.find("#dataobat").val());
  } else {
    racikan_id = 0;
    addRow("nonracik");
    count_subtotal();
    // $("#add-qty-nonracik").modal({backdrop: 'static'});
    // $("#qty_resep_diberi").focus();
    // search_riwayat_obat_pasien($tr.find("#dataobat").val());
  }
  $tr = $("#obat > tbody").find('tr[data-index="' + clickedLine + 1 + '"]');
  $tr.find("#dataobat").focus();
}

$("#hapus_rincian_obat").on("click", function() {
  delete_rincian_obat();
});
function delete_rincian_obat() {
  var table = document.getElementById("obat");
  //or use :  var table = document.all.tableid;
  for (var k = table.rows.length - 1; k > 0; k--) {
    table.deleteRow(k);
  }
  h = 0;
  i = 0;
  j = 0;
  addRow("inisial");
  $("#subtotal").val(0);
  $("#total_harga").val(0);
}
function count_subtotal() {
  var total = 0;
  var total_jasa_r = 0;
  var harga_total = 0;
  kuning = 0;
  for (var i = 0; i < $("#obat > tbody > tr").length; i++) {
    total =
      parseFloat(total) +
      parseFloat(
        $("#obat tbody")
          .find("tr")
          .eq(i)
          .find("#total")
          .val()
      );
    total_jasa_r =
      parseFloat(total_jasa_r) +
      parseFloat(
        $("#obat tbody")
          .find("tr")
          .eq(i)
          .find("#jasa_r")
          .val()
      );
    cekkuning = $("#obat tbody tr").eq(i).css("background-color");
    if(cekkuning == "rgb(255, 255, 153)"){
      kuning ++;
    }
    // console.log("row" + total);
    // console.log("row" + total_jasa_r);
  }
  $("#subtotal").val(total);
  $("#total_jasa_r").val(total_jasa_r);
  harga_total = total + total_jasa_r;
  // harga_total = total;
  $("#total_harga").val(harga_total);
  // console.log(total);
}
function pilih_cara_bayar() {
  $("#pilih_cara_bayar").html("");
  $.ajax({
    type: "GET",
    url: "entry_resep/cara_bayar_ctrl",
    data: {
      kdmut: $("#depo_nm").val()
    },
    dataType: "json",
    success: function(data) {
      carabayar = data;
      jQuery.each(carabayar, function(index, val) {
        var a = "";
        a =
          "<option value='" +
          val.IDCRBAYAR +
          "' data-crbayar='" +
          val.STAT +
          "' data-ket='" +
          val.KET +
          "'><b>" +
          val.IDCRBAYAR +
          " - " +
          val.KET +
          "</b>" +
          "</option>";
        $("#pilih_cara_bayar").append(a);
      });
    },
    error: function(xhr, status, error) {
      console.log(xhr, status, error);
    }
  });
  $("#pilih_cara_bayar").select2({
    placeholder: "Pilih Cara Pembayaran",
    width: "100%",
    dropdownParent: $("#cara_bayar")
  });
}
function daftar_creditcard() {
  $.ajax({
    type: "GET",
    url: "entry_resep/card_type",
    data: {},
    dataType: "json",

    success: function(data) {
      // console.log(data);
      jQuery.each(data, function(index, val) {
        var a = "";
        a =
          "<option value='" +
          val.KDBANK +
          "' data-kartu='" +
          val.KARTU +
          "'><b>" +
          val.KARTU +
          "</b>" +
          "</option>";
        $("#tipe_kartu").append(a);
      });
    },
    error: function(xhr, status, error) {
      console.log(xhr, status, error);
    }
  });
  $("#tipe_kartu").select2({
    placeholder: "Pilih Cara Pembayaran",
    width: "100%",
    dropdownParent: $("#cara_bayar")
  });
}
$("#tambah_pembayaran").on("click", function() {
  if ($("#tabel_pembayaran tbody tr").text() == "NO DATA") {
    $("#tabel_pembayaran tbody tr").remove();
  }
  var a = "";
  var a1 = $("#tipe_kartu")
    .find(":selected")
    .data("kartu");
  var a2 = $("#no_kartu").val();
  var a3 = $("#no_trace").val();
  var a4 = $("#no_batch").val();
  var a5 = $("#nominal").val();
  a =
    "<tr>" +
    "<td>" +
    a1 +
    "</td>" +
    "<td>" +
    a2 +
    "</td>" +
    "<td>" +
    a3 +
    "</td>" +
    "<td>" +
    a4 +
    "</td>" +
    "<td style='text-align: right;'>" +
    a5 +
    "</td>" +
    '<td><button class="btn btn-danger" type="button"  id="del-byr"><i class="fa fa-times"></i></button></td>' +
    "</tr>";
  $("#tabel_pembayaran tbody").append(a);
});
var bayarresep = "<DocumentElement>";
$("#proses_bayar").on("click", function() {
  var l_table = $("#tabel_pembayaran tbody tr").length;
  bayar = $("#pilih_cara_bayar")
    .find(":selected")
    .data("ket");
  if (l_table == 0) {
    bayarresep =
      "<BayarResep>" +
      "<BANK></BANK>" +
      "<NO_REK></NO_REK>" +
      "<RUPIAH>" +
      $("#totalbyr").val() +
      "</RUPIAH>" +
      "<DIBAYAR>" +
      $("#totalbyr").val() +
      "</DIBAYAR>" +
      "<NO_TRACE></NO_TRACE>" +
      "</BayarResep>" +
      "</DocumentElement>";
  } else {
    for (var i = 0; i < l_table; i++) {
      bayarresep +=
        "<DocumentElement>" +
        "<BayarResep>" +
        "<BANK>" +
        $("#tabel_pembayaran tbody tr")
          .eq(i)
          .find("td")
          .eq(0)
          .html() +
        "</BANK>" +
        "<NO_REK>" +
        $("#tabel_pembayaran tbody tr")
          .eq(i)
          .find("td")
          .eq(1)
          .html() +
        "</NO_REK>" +
        "<RUPIAH>" +
        $("#tabel_pembayaran tbody tr")
          .eq(i)
          .find("td")
          .eq(4)
          .html() +
        "</RUPIAH>" +
        "<DIBAYAR>" +
        $("#tabel_pembayaran tbody tr")
          .eq(i)
          .find("td")
          .eq(4)
          .html() +
        "</DIBAYAR>" +
        "<NO_TRACE>" +
        $("#tabel_pembayaran tbody tr")
          .eq(i)
          .find("td")
          .eq(2)
          .html() +
        "</NO_TRACE>" +
        "</BayarResep>";
    }
    bayarresep = bayarresep + "</DocumentElement>";
    $("#input_antrian").modal({backdrop: 'static'});
  }
  $("#cara_bayar").modal("hide");
  $("#input_antrian").modal({backdrop: 'static'});
});
$("#tabel_pembayaran").on("click", "tr #del-byr", function() {
  $(this)
    .parents("tr")[0]
    .remove();
  if ($("#tabel_pembayaran tbody tr").length < 1) {
    $("#tabel_pembayaran tbody").append(
      "<tr><td colspan='99' style='text-align:center'>NO DATA</td></tr>"
    );
  }
});

function get_nomor_resep() {
  var date_now = tahun + "-" + bulan + "-" + tgl;
  $.ajax({
    type: "POST",
    url: "entry_resep/get_nomor_resep_ctrl",
    dataType: "json",
    data: {
      date_now: date_now
    },
    success: function(data) {
      // console.log(data);
      $("#nomor").val(data[0].NO_RESEP);
    },
    error: function(xhr, status, error) {}
  });
}
function get_nomor_peserta() {
  var rm = $("#rm").val();
  $.ajax({
    type: "POST",
    url: "entry_resep/get_nomor_peserta_ctrl",
    dataType: "json",
    data: {
      rm: rm
    },
    success: function(data) {
      // console.log(data);
      $("#no_peserta").val(data[0].NO_PESERTA);
      // $tr.find("#jasa_r").val(data[0].JASA_R);
    },
    error: function(xhr, status, error) {}
  });
}
function get_kode_trans() {
  $.ajax({
    type: "POST",
    url: "entry_resep/get_kode_trans_ctrl",
    dataType: "json",
    data: {},
    success: function(data) {
      // console.log(data);
      $("#kode_trans").val(data[0].KDTRANS);
      // $tr.find("#jasa_r").val(data[0].JASA_R);
      save();
    },
    error: function(xhr, status, error) {}
  });
}
$("#save_all").on("click", function() {
  if(($("#jenispx").val() == "KREDIT")&&(kuning > 0)){
    loop_save = 2;
  }
  else{
    loop_save = 0;
  }
  start_save('');
});
function start_save(condition){
  get_all_resep();
  if ($("#pengiriman").val() != "Farmasi") {
    kirimresep = null;
    pre_save(condition);
  } else if ($("#pengiriman").val() == "Farmasi") {
    $("#data4send").modal({backdrop: 'static'});
  }
}
$("#save_pengirim").on("click", function() {
  var prev_total = parseFloat($("#total_harga").val());
  var kirim = parseFloat($("#biaya_kirim").val());
  $("#total_harga").val(prev_total + kirim);
  kirimresep =
    "<DocumentElement>" +
    "<KirimResep>" +
    "<PENERIMA>" +
    $("#penerima_kirim").val() +
    "</PENERIMA>" +
    "<ALMT>" +
    $("#alamat_kirim").val() +
    "</ALMT>" +
    "<KELURAHAN></KELURAHAN>" +
    "<KECAMATAN></KECAMATAN>" +
    "<KOTA>" +
    $("#kota_kirim").val() +
    "</KOTA>" +
    "<TELP>" +
    $("#telp_kirim").val() +
    "</TELP>" +
    "<TELP2>" +
    $("#telp2_kirim").val() +
    "</TELP2>" +
    "<BIAYA>" +
    $("#biaya_kirim").val() +
    "</BIAYA>" +
    "</KirimResep>" +
    "</DocumentElement>";
  $("#data4send").modal("hide");
  pre_save('');
});
function get_all_resep() {
  loop_detail = "<DocumentElement>";
  var subtotal_spc1 = 0; //untuk obat biasa
  var jasa_r_spc1 = 0;
  var subtotal_spc2 = 0; //untuk obat terlarang
  var jasa_r_spc2 = 0;
  var jmlhresep = $("#obat > tbody > tr").length - 1;
  for (var i = 0; i < jmlhresep; i++) {
    cekkuning = $("#obat tbody tr").eq(i).css("background-color");
    var cond;
    if(loop_save == 2){
      cond = cekkuning != "rgb(255, 255, 153)";
    }else if(loop_save == 1){
      cond = cekkuning == "rgb(255, 255, 153)";
    }else if(loop_save == 0){
      cond = true;
    }
    if(cond){
      detail =
        "<DetilResep>" +
        "<NOURUT>" +
        $("#obat > tbody > tr")
          .eq(i)
          .data("id") +
        "</NOURUT>" +
        "<ID>" +
        $("#obat > tbody > tr")
          .eq(i)
          .data("index2") +
        "</ID>" +
        "<KDBRG>" +
        $("#obat tbody")
          .find("tr")
          .eq(i)
          .find("#dataobat")
          .val() +
        "</KDBRG>" +
        "<KDBRGDR></KDBRGDR>" +
        "<HARGA>" +
        $("#obat tbody")
          .find("tr")
          .eq(i)
          .find("#total")
          .val() +
        "</HARGA>" +
        "<HBIJI>" +
        $("#obat tbody")
          .find("tr")
          .eq(i)
          .find("#h_biji")
          .val() +
        "</HBIJI>" +
        "<HJUAL>" +
        $("#obat tbody")
          .find("tr")
          .eq(i)
          .find("#harga")
          .val() +
        "</HJUAL>" +
        "<DISC></DISC>" +
        "<JUMLAH>" +
        $("#obat tbody")
          .find("tr")
          .eq(i)
          .find("#qty")
          .val() +
        "</JUMLAH>" +
        "<PUYER></PUYER>" +
        "<ITER></ITER>" +
        "<SIGNA>" +
        $("#obat tbody")
          .find("tr")
          .eq(i)
          .find(".list_signa")
          .find(":selected")
          .data("kode") +
        "</SIGNA>" +
        "<SIGNA2>" +
        $("#obat tbody")
          .find("tr")
          .eq(i)
          .find(".list_signa")
          .find(":selected")
          .data("signa") +
        "</SIGNA2>" +
        "<HARI>" +
        $("#obat tbody")
          .find("tr")
          .eq(i)
          .find("#hari")
          .val() +
        "</HARI>" +
        "<JASA>" +
        $("#obat tbody")
          .find("tr")
          .eq(i)
          .find("#jasa_r")
          .val() +
        "</JASA>" +
        "<QTYDR>" +
        $("#obat tbody")
          .find("tr")
          .eq(i)
          .find("#qty2")
          .val() +
        "</QTYDR>" + //qty2
        "<KETQTY>" +
        $("#obat tbody")
          .find("tr")
          .eq(i)
          .find("#keterangan")
          .val() +
        "</KETQTY>" + //keterangan
        "<KDDEBI>" +
        $("#nmdeb")
          .find(":selected")
          .val() +
        "</KDDEBI>" +
        "<SIGNACPT>" +
        $("#obat tbody")
          .find("tr")
          .eq(i)
          .find(".list_signa")
          .find(":selected")
          .val() +
        "</SIGNACPT>" +
        "</DetilResep>";
      loop_detail = loop_detail + detail;
      if(loop_save == 2){
        subtotal_spc1 = subtotal_spc1 + parseFloat($("#obat tbody").find("tr").eq(i).find("#total").val());
        jasa_r_spc1 = jasa_r_spc1 + parseFloat($("#obat tbody").find("tr").eq(i).find("#jasa_r").val());
      }else if(loop_save == 1){
        subtotal_spc2 = subtotal_spc2 + parseFloat($("#obat tbody").find("tr").eq(i).find("#total").val());
        jasa_r_spc2 = jasa_r_spc2 + parseFloat($("#obat tbody").find("tr").eq(i).find("#jasa_r").val());
      }
    }
  }
  $("#subtotal_spc1").val(subtotal_spc1);
  $("#jasa_r_spc1").val(jasa_r_spc1);
  $("#subtotal_spc2").val(subtotal_spc2);
  $("#jasa_r_spc2").val(jasa_r_spc2);
  loop_detail = loop_detail + "</DocumentElement>";
}
function pre_save(condition) {
  get_nomor_peserta();
  get_nomor_resep();
  // get_kode_trans();
  if (($("#jenispx").val() == "TUNAI")||(condition == 'simpan2')) {
    daftar_creditcard();
    pilih_cara_bayar();
    $("#cara_bayar").modal({backdrop: 'static'});
    $("#pilih_cara_bayar").on("change", function() {
      if (
        $("#pilih_cara_bayar")
          .find(":selected")
          .data("crbayar") == "C"
      ) {
        $("#credit_detail").css("display", "block");
      } else if (
        $("#pilih_cara_bayar")
          .find(":selected")
          .data("crbayar") == "T"
      ) {
        $("#credit_detail").css("display", "none");
      }
    });
  } else if ($("#jenispx").val() == "KREDIT") {
    bayar = "";
    bayarresep =
      "<DocumentElement>" +
      "<BayarResep>" +
      "<BANK></BANK>" +
      "<NO_REK></NO_REK>" +
      "<RUPIAH>" +
      $("#total_harga").val() +
      "</RUPIAH>" +
      "<DIBAYAR>" +
      $("#total_harga").val() +
      "</DIBAYAR>" +
      "<NO_TRACE></NO_TRACE>" +
      "</BayarResep>" +
      "</DocumentElement>";
    $("#input_antrian").modal({backdrop: 'static'});
  }
}
$("#getantrian").on("click", function() {
  $("#antrian").val($("#no_antrian").val());
  $("#input_antrian").modal("hide");
  $("#password-modal").modal({backdrop: 'static'});
});
$("#no_antrian").on("keypress", function(e){
  if(e.keyCode == 13){
    $("#antrian").val($("#no_antrian").val());
    $("#input_antrian").modal("hide");
    $("#password-modal").modal({backdrop: 'static'});
  }
});
function get_pass(password) {
  loading();
  $.ajax({
    type: "POST",
    url: "entry_resep/cekpass",
    data: {
      pass : password
    },
    dataType: "json",

    success: function(data) {
      data;
      if (data["responseCode"] == 'ok') {
        get_kode_trans();
        $("#password-modal").modal("hide");
        $("#user").val(data["username"]);
        // save();
      } else {
        swal.fire("password salah", "error");
        $("#password").val("");
        $("#password").focus();
      }
      KTApp.unblockPage();
    },
    error: function(xhr, status, error) {
      console.log(xhr, status, error);
    }
  });
}
$("#simpanPass").on("click", function() {
  get_pass($("#password").val());
});
$("#password").on("keypress", function(e){
  if(e.keyCode == 13){
    get_pass($("#password").val());
  }
});
function save() {
  loading();
  var xml_total = "";
  var headerresep = "";
  var resepstatus = "";
  var user = $("#user").val();
  var stpas = "";
  var sts = "";
  if ($("#jenispx").val() == "TUNAI") {
    stpas = 1;
  } else if ($("#jenispx").val() == "KREDIT") {
    stpas = 2;
  }
  if ($("#jenispx").val() == "TUNAI" && $("#layanan").val() == "Rawat Jalan") {
    sts = 1;
  } else if (
    $("#jenispx").val() == "KREDIT" &&
    $("#layanan").val() == "Rawat Jalan"
  ) {
    sts = 2;
  } else if (
    $("#jenispx").val() == "TUNAI" &&
    $("#layanan").val() == "Rawat Inap"
  ) {
    sts = 3;
  } else if (
    $("#jenispx").val() == "KREDIT" &&
    $("#layanan").val() == "Rawat Inap"
  ) {
    sts = 4;
  }
  // console.log(detilresep);
  headerresep =
    "<DocumentElement>" +
    "<HeaderResep>" +
    "<ID_TRANS>" +
    $("#kode_trans").val() +
    "</ID_TRANS>" + //idtrans -> SP
    "<KDMUT>" +
    $("#depo_nm").val() +
    "</KDMUT>" +
    "<NOTA>" +
    $("#notareg").val() +
    "</NOTA>" +
    "<TGL>" +
    tahun +
    "-" +
    bulan +
    "-" +
    tgl +
    "</TGL>" +
    "<TGLSHIFT>" +
    tahun_shift +
    "-" +
    bulan_shift +
    "-" +
    tgl_shift +
    "</TGLSHIFT>" + //tgl dgn aturan shift
    "<NOMOR>" +
    $("#nomor").val() +
    "</NOMOR>" + //Nomor
    "<MUTASI>O</MUTASI>" + //O
    "<BAYAR>" +
    bayar +
    "</BAYAR>" + //cara bayar (jika tunai) (jika kredit default "")
    "<KDOT>" +
    $("#jenis_resep").val() +
    "</KDOT>" + //jenis rsep
    "<STPAS>" +
    stpas +
    "</STPAS>" + //1->tunai 2->kredit (jenispx)
    "<KDDEB>" +
    $("#nmdeb").val() +
    "</KDDEB>" +
    "<NMDEB>" +
    $("#nmdeb").find(':selected').data('debitur') +
    "</NMDEB>" +
    "<KDDIN>" +
    $("#din").val() +
    "</KDDIN>" +
    "<NMDIN>" +
    $("#din").find(':selected').data('dinas') +
    "</NMDIN>" +
    "<KDKLIN>" +
    $("#kdklin").val() +
    "</KDKLIN>" +
    "<NMKLIN>" +
    $("#nmklin").val() +
    "</NMKLIN>" +
    "<NORM>" +
    $("#rm").val() +
    "</NORM>" +
    "<NMPX>" +
    $("#nama").val() +
    "</NMPX>" +
    "<NMKK></NMKK>" + //nama KK "kosong"
    "<KDDOK>" +
    $("#nmdok").val() +
    "</KDDOK>" +
    "<NMDOK>" +
    $("#nmdok").find(':selected').data('dokter') +
    "</NMDOK>" +
    "<STS>" +
    sts +
    "</STS>" + //tunai rj -> 1, kredit rj ->2, tunai ri -> 3, kredit ri-> 4
    "<JAM>" +
    $("#jam").html() +
    "</JAM>" +
    "<JAGA>" +
    $("#shift").html() +
    "</JAGA>" +
    "<TIPEIF>" +
    $("#depo_id").val() +
    "</TIPEIF>" +
    "<LOKASI></LOKASI>" + //cek IF_MLOKASI (kosong)
    "<IDXPX>" +
    $("#idx").val() +
    "</IDXPX>" +
    "<ANTRIAN>" +
    $("#antrian").val() +
    "</ANTRIAN>" + // input manual
    "<RMINDUK></RMINDUK>" + // kosong
    "<TAGIHINAP></TAGIHINAP>" + //RJ -> "0",RI -> BPJS('kemo') (KOSONG)
    "<ALMTPX>" +
    $("#alamat").val() +
    "</ALMTPX>" +
    "<KOTAPX>" +
    $("#kota").val() +
    "</KOTAPX>" +
    "<NOPESERTA>" +
    $("#rm").val() +
    "</NOPESERTA>" + //no peserta -> RIRJ_MASTERPX norm
    "<NOKARTU></NOKARTU>" + //RJ_KARCIS->RJ, RI_MASTERPX->RI  use noreg
    "<SURAT_JAMINAN></SURAT_JAMINAN>" + //-=-
    "<TELP_PX></TELP_PX>" + //
    "<TAGIHIFJALAN></TAGIHIFJALAN>" + //(KOSONG)
    "<IDUNIT>001</IDUNIT>" + //"001"
    "</HeaderResep>" +
    "</DocumentElement>";
  resepstatus = null;
  // "<DocumentElement>" +
  // "<ResepStatus>" +
  // "<STS_RESEP_STAT></STS_RESEP_STAT>" +
  // "<NOURUT_RESEP_STAT></NOURUT_RESEP_STAT>" +
  // "<PFX></PFX>" +
  // "<STSRACIK></STSRACIK>" +
  // "<WAKTURESEP></WAKTURESEP>" +
  // "<STS_HITUNG></STS_HITUNG>" +
  // "<TLP></TLP>" +
  // "</ResepStatus>" +
  // "</DocumentElement>";
  console.log(headerresep);
  console.log(loop_detail);
  console.log(bayarresep);
  console.log(kirimresep);
  console.log(resepstatus);
  console.log(tgl + "/" + bulan + "/" + tahun);
  $.ajax({
    type: "POST",
    url: "entry_resep/save",
    dataType: "json",
    data: {
      headerresep: headerresep,
      detail: loop_detail,
      bayarresep: bayarresep,
      kirimresep: kirimresep,
      resepstatus: resepstatus,
      user: user
    },
    success: function(data) {
      KTApp.unblockPage();
      swal.fire(data[0].KETERANGAN);
      // console.log(data[0].KETERANGAN);
      if (data[0].KETERANGAN == "Resep berhasil diproses !") {
        if(loop_save = 2){
          loop_save = 1;
          kuning = 0;
          start_save('simpan2');
        }else{
          var plafon = parseFloat($("#plafon").val());
          var biaya = parseFloat($("#biaya_bulan_ini").val()) + parseFloat($("#total_harga").val());
          if((biaya > plafon)&&(plafon != 0)){
            $(location).attr('href','input_excess/data?url=entry_resep/data&nota='+$("#notareg").val()+'');
          }else{
            document.getElementById("form").reset();
            clear();
          }
        }
      }
    },
    error: function(xhr, status, error) {
      KTApp.unblockPage();
      swal.fire(error, status);
    }
  });
}
function loading() {
  KTApp.blockPage({
    overlayColor: "#000000",
    type: "v2",
    state: "primary",
    message: "Processing..."
  });
}
// Class initialization on page load
jQuery(document).ready(function() {
  loading();
  Entri_resep.init();
});
