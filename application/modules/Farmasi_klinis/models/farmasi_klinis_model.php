<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class farmasi_klinis_model extends CI_Model{
	public function __construct()
  {
      // Call the CI_Model constructor
      parent::__construct();
     $this->load->database();
  }
 
  /*public function getdatalogin($user,$pass)
  {
    $this->load->database();
    $sql = "exec inap_sp_login_bacapriv '$user','$pass'";
    return $this->db->query($sql);  
  }*/
  public function detailPasien($noreg)
  {
    $sql = "SELECT
          M.noreg NOREG,MP.KBUKU RM,MP.NAMA, MP.SEX,convert(varchar(10), MP.TGL_LHR, 103) TGL_LHR, MP.ALAMAT, MP.KOTA,M.[index],
          M.kdebi,M.kdini,
          convert(varchar(10), m.tgmas, 103) TGL_MRS,convert(varchar(10), m.tgl_plng, 103) TGL_PRS,
          K.NM_KMR KAMAR,K.KD_KMR, 
          RP.NM_RUANG2 RUANGAN,RP.RP, M.kddok, M.nm_diag_msk
            FROM RI_MASTERPX M 
            INNER JOIN RIRJ_MASTERPX MP ON MP.NO_PESERTA=M.nopeserta
            INNER JOIN RI_MKAMAR K ON K.KD_KMR=M.kmr_skrg AND K.[STATUS]=1
            INNER JOIN RI_MRPERAWATAN RP ON RP.RP=K.RP AND RP.STT_ACT=1
            where M.noreg = ".$noreg."
            ORDER BY 1";
    $detail_result = $this->db->query($sql);

    $result['detail'] = $detail_result->result();
    return $result;    
  }
  public function dataObat(){
    $sql ="select  a.ID_TRANS,a.TGL, b.KDBRG,c.NMBRG, b.JUMLAH 
    from if_htrans a
    join if_trans b on a.ID_TRANS = b.ID_TRANS
    join if_mbrg_gd c on b.KDBRG = c.KDBRG
    where NOTA = 71681 order by a.TGL asc";
    return $this->db->query($sql);    
  }
  public function dataDrp(){
    $query = $this->db->query("EXEC if_sp_baru_farklin_tampilkan_parameter_drp");
    return $query->result();
  }
  public function action($noreg, $drp, $user,$id_trans){
    try{
      $res = [];
      if($id_trans){
        //edit
        $this->db->query("update IF_FARKLIN_INAP_HTRANS set modiby = ".$user.", MODIDATE = getDate() where ID_TRANS_FARKLIN_INAP = '".$id_trans."'");
        
        foreach($drp as $item){
          $this->db->query("update IF_FARKLIN_INAP_TRANS set modiby = ".$user.", MODIDATE = getDate(), ACTIVE = ".$item['value']." where SUB_DRP = '".$item['id']."' and ID_TRANS_FARKLIN_INAP ='".$id_trans."'");
        }
        
        $res['status'] = 'success';
        $res['massage'] = 'Sukses Proses data';
      }else{
        //add new
        //header
        $query = $this->db->query("EXEC IF_SP_GETAUTOIDTRANS_FARKLIN_INAP");
        $id_farklin_inap = $query->row();

        // var_dump($id_farklin_inap->ID_GENERATE);die();
        $query = $this->db->query("insert into IF_FARKLIN_INAP_HTRANS (ID_TRANS_FARKLIN_INAP, NOREG, TGL_CEK, ACTIVE, INPUTBY, INPUTDATE )
                                  values ('".$id_farklin_inap->ID_GENERATE."','".$noreg."',getDate(), 1, '".$user."', getdate())");
        $insert_header = $query;
        if($insert_header){
          //detail
          $val = "";
          $koma = "";
          foreach($drp as $index =>$item){
            
            if($index == 0){
              $koma = "";
            }else{
              $koma = ",";
            }
            $val .= $koma . "('".$id_farklin_inap->ID_GENERATE."',".$item['id'].",".$item['value'].",'".$user."',getDate() )";
            // $val .= $koma."('".$id_farklin_inap->ID_GENERATE."')" ;
          }
          $query = $this->db->query("insert into IF_FARKLIN_INAP_TRANS (ID_TRANS_FARKLIN_INAP, SUB_DRP, ACTIVE, INPUTBY, INPUTDATE) values ".$val );
          // var_dump($val);
          // die();
          $res['status'] = 'success';
          $res['massage'] = 'Sukses Proses data';
        }else{
          $res['status'] = 'error';
          $res['massage'] = 'Gagal Proses data';
        }
      }
      return $res;
    }catch(Exception $e){
      $res['status'] = 'error';
      $res['massage'] = 'Gagal Proses data';
      return $res;
    }
    
  }
  public function dataSatuan(){
      $sql = $this->db->query(" select KDSAT, SATUAN from IF_MSATUAN");
          // var_dump($sql->result());die();
      return $sql->result();
  }
}
