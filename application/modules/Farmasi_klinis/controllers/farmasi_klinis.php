<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class farmasi_klinis extends CI_Controller {
	public $urlws = null;
	public function __construct() {
        parent::__construct();
		$this->load->model('farmasi_klinis_model');
		$this->load->library('session');
		$this->urlws = $this->globals->ws_phc();

    }
	private function _render($view,$data = array())
	{
		$this->load->view('header',$data);
	    // $this->load->view('body_header');
	    // $this->load->view('sidebar');
	    $this->load->view($view,$data);
	    $this->load->view('footer');
	}

	public function index()
	{
		//load library
		// var_dump($this->session->all_userdata());die();
		$uri =& load_class('URI', 'core');
	  	redirect(base_url().'index.php/farmasi_klinis/data',$data);
	}

	public function data()
	{
		// var_dump($_SESSION["if_ses_userid"]);die();
		if($_SESSION["if_ses_depo"] == null){
			redirect(base_url().'depo');
		}else{
			if(in_array(6,$_SESSION['if_ses_menu'])){
				$data['parent_active'] = 6 ;
				$data['child_active'] = 10 ;
				$this->_render('farmasi_klinis_view', $data);
			}else{
				redirect(base_url().'beranda/data');}
		}
	}
	
	public function dataPasien()
	{
		$authorization = 'Authorization:'.$_SESSION["if_ses_token_phc"];
		$url = $this->urlws."Farmasi/FarmasiKlinis/dataPasien";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, array($authorization ));
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		$output = curl_exec($ch); 
		curl_close($ch);    
		$cek_output = json_decode($output);
		// var_dump($cek_output);die();
		if($cek_output->status == true){
			$data 			= array();
			foreach($cek_output->data as $item){
				$btn 		='<button name="btnproses" id="btnproses" onclick="detailPasien('.$item->NOREG.')" type="button" class="btn btn-success btn-sm" data-toggle="kt-tooltip" data-placement="top" title="Choose"> <i class="fa fa-check"></i> </button>';
                
				$data[] 	= array(
					"NOREG" 	=> $item->NOREG,
					"RM" => $item->RM,
					"TGL_MRS" 	=> $item->TGL_MRS ,
					"NAMA"		=> $item->NAMA,
					"KAMAR"		=> $item->KAMAR,
					"RUANGAN"		=> $item->RUANGAN,
					"btn"		=> $btn
				);
			}
			$obj = array("data"=> $data);
			echo json_encode($obj);
		}else{
			$error = array(
				'data' => [],
				'error' => $cek_output
			);
			
			echo json_encode($error);
		}
		
	}
	public function detailPasien()
	{
		$authorization = 'Authorization:'.$_SESSION["if_ses_token_phc"];
		$url = $this->urlws."Farmasi/farmasiKlinis/detailPasien";
		$data = array(
            "noreg"         => $this->input->get('noreg'),
		);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array($authorization ));
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		$output = curl_exec($ch); 
		curl_close($ch);    
		$data = json_decode($output);

		// var_dump($data);die();
		$arrDate = [];
		$arrObat = [];
		$datas = [];
		$result = [];
		$thead = "";
		$tbody = "";
		$listObatEso = [];
		$textObatEso = "";

		// var_dump($data);die();
		foreach($data->data->listobat as $item){
			array_push($datas, ["kd_obat"=> $item->KODE, "nm_obat"=>$item->OBAT, "nm_signa"=>$item->SIGNA,"tgl"=>$item->TGL, "jumlah"=>$item->JUMLAH ]);
			if(!in_array($item->TGL, $arrDate)){
				array_push($arrDate , $item->TGL);
			}
			if(!in_array(["kd_obat"=>$item->KODE,"nm_obat"=>$item->OBAT,"nm_signa"=>$item->SIGNA], $arrObat)){
				array_push($arrObat , ["kd_obat"=>$item->KODE,"nm_obat"=>$item->OBAT,"nm_signa"=>$item->SIGNA]);
			}
			if(!in_array(["kd_obat"=>$item->KODE,"nm_obat"=>$item->OBAT,"satuan"=>$item->SATUAN], $listObatEso)){
				array_push($listObatEso ,["kd_obat"=>$item->KODE,"nm_obat"=>$item->OBAT,"satuan"=>$item->SATUAN]);
			}

		}
		foreach($listObatEso as $item){
			$textObatEso .= "<option value='".$item['kd_obat']."' satuan='".$item['satuan']."'>".$item['nm_obat']."</option>";
		}
		// var_dump($textObatEso);
		//draw table
		foreach($arrDate as $item){
			$thead .= "<th>".$item."</th>";
		}
		$datedinamyc = "";
		foreach($arrObat as $index => $item){
			$datedinamyc = "";
			foreach($arrDate as $date){
				$jumlah = "-";
				foreach($datas as $x =>  $exist){
					if($exist['kd_obat'] == $item['kd_obat'] && $exist['nm_signa'] == $item['nm_signa'] && $exist['tgl'] == $date ){
						$jumlah = $exist['jumlah'];
					}
				}
				$datedinamyc .= "<td style='text-align:center'>".$jumlah."</td>";
			}
			$tbody .= "<tr><td class='headcol'>".($index+1)."</td><td>".$item['kd_obat']."</td><td>".$item['nm_obat']."</td><td>".$item['nm_signa']."</td>".$datedinamyc."</tr>";
		}
		$result['detail'] = $data->data->detail;
		$result['thead'] = "<tr style='background: #2860a8; color:white; text-align:center'><th>NO</th><th>KODE</th><th style='padding-left:100px;padding-right:100px;'>BARANG</th><th style='padding-left:30px;padding-right:30px;'>SIGNA</th>".$thead."</tr>";
		$result['tbody'] = $tbody;
		$result['textObatEso'] = $textObatEso;

		echo json_encode($result);
	}
	public function dataDrp()
	{
		$authorization = 'Authorization:'.$_SESSION["if_ses_token_phc"];
		$url = $this->urlws."Farmasi/farmasiKlinis/dataDrp";
		$data = array(
            "noreg"         => $this->input->post('noreg'),
            "tgl"         => $this->input->post('tgl'),
		);
		// var_dump($data);die();
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array($authorization ));
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		$output = curl_exec($ch); 
		curl_close($ch);    
		$data = json_decode($output);
		
		$body = "";
		$res = [];
		foreach($data->data->drp as $index => $item){
			$disabled = $item->ACTIVE == 1 ? '' : 'disabled';
			$checked = $item->ACTIVE == 1 ? 'checked' : '';
			if($index == 0){
				$body .= "<tr class='is_head'><td colspan='3'>".$item->NAMA_GROUP."</td></tr>";
			}else{
				$sblmnya = $data->data->drp[$index-1];
				if($sblmnya->KODE_GROUP !== $item->KODE_GROUP){
					$body .= "<tr class='is_head'><td colspan='3'>".$item->NAMA_GROUP."</td></tr>";
				}
			}
			$body .= "<tr data-index='".$index."' class='is_child'>
						<td style='vertical-align:middle'>".$item->SUB_DRP."</td>
						<td width='5%' style='vertical-align:middle'>
							<label class='kt-checkbox' style='top:-4px' >
								<input class='checkbox_drug' type='checkbox' id='".$item->KODE_SUB."' value='".$item->ACTIVE."' ".$checked."> 
								<span></span>
							</label>
						</td>
						<td width='45%'><input type='text' class='form-control' id='catatan' value='".$item->CATATAN."' ".$disabled."></td>
					</tr>";
		}
		
		
		$res['body'] = $body;
		$res['id_farklin'] = $data->data->id_farklin;
		// var_dump($res);
		// die();
		echo json_encode($res);
	}
	public function action()
	{	
		$data =  array(
            "noreg" => $this->input->post('noreg'),
            "id_trans" => $this->input->post('id_trans'),
            "drp" => $this->input->post('drp'),
            "tgl" => $this->input->post('tgl'),
            "user" => $_SESSION["if_ses_username"],
            "norm" => $this->input->post('norm'),
            "hasil_asesmen" => $this->input->post('hasil_asesmen')
		);
		$data = http_build_query($data);
		// var_dump($data);die();
		$authorization = 'Authorization:'.$_SESSION["if_ses_token_phc"];
		$url = $this->urlws."Farmasi/farmasiKlinis/action";

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array($authorization ));
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		$output = curl_exec($ch); 
		curl_close($ch);    
		$datas = json_decode($output);

		// var_dump($output);die(); 
		echo json_encode($datas);
	}
	public function dataSoap()
	{
		$authorization = 'Authorization:'.$_SESSION["if_ses_token_phc"];
		$url = $this->urlws."Farmasi/farmasiKlinis/dataSoap";
		$data = array(
            "noreg"         => $this->input->post('noreg'),
            "norm"         => $this->input->post('norm'),
		);
		// var_dump($data);die();
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array($authorization ));
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		$output = curl_exec($ch); 
		curl_close($ch);    
		$data = json_decode($output);

		echo json_encode($data);
	}
	public function saveRekonsiliasi()
	{		
		$data =  array(
            "noreg" => $this->input->post('noreg'),
            "data_rekon" => $this->input->post('data_rekon'),
            "user" => $_SESSION["if_ses_username"],
            "norm" => $this->input->post('norm'),
		);
		$data = http_build_query($data);
		// var_dump($data);die();
		$authorization = 'Authorization:'.$_SESSION["if_ses_token_phc"];
		$url = $this->urlws."Farmasi/farmasiKlinis/saveRekonsiliasi";

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array($authorization ));
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		$output = curl_exec($ch); 
		curl_close($ch);    
		$datas = json_decode($output);

		// var_dump($output);die(); 
		echo json_encode($datas);
	}
	public function dataRekon()
	{
		$authorization = 'Authorization:'.$_SESSION["if_ses_token_phc"];
		$url = $this->urlws."Farmasi/farmasiKlinis/dataRekon";
		$data = array(
            "noreg"         => $this->input->post('noreg'),
            "norm"         => $this->input->post('norm'),
		);
		// var_dump($data);die();
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array($authorization ));
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		$output = curl_exec($ch); 
		curl_close($ch);    
		$data = json_decode($output);

		echo json_encode($data);
	}
	public function dataEso()
	{
		$authorization = 'Authorization:'.$_SESSION["if_ses_token_phc"];
		$url = $this->urlws."Farmasi/farmasiKlinis/dataEso";
		$data = array(
            "noreg"         => $this->input->post('noreg'),
            "norm"         => $this->input->post('norm'),
            "tgl"         => $this->input->post('tgl'),
		);
		// var_dump($data);die();
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array($authorization ));
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		$output = curl_exec($ch); 
		curl_close($ch);    
		$data = json_decode($output);
		// var_dump($data);die();
		echo json_encode($data);
	}
	public function saveEso()
	{		
		$data =  array(
            "noreg" => $this->input->post('noreg'),
            "user" => $_SESSION["if_ses_username"],
			"norm" => $this->input->post('norm'),
			
			//dataesoheader
			"tgl_cek" => $this->input->post('tgl_cek'),
			"penyakit_utama" => $this->input->post('penyakit_utama'),
			"penyakit_kondisi" => $this->input->post('penyakit_kondisi'),
			"manifestasi_eso" => $this->input->post('manifestasi_eso'),
			"tgl_mulai" => $this->input->post('tgl_mulai'),
			"tgl_selesai" => $this->input->post('tgl_selesai'),
			"kesudahan_eso" => $this->input->post('kesudahan_eso'),
			"riwayat_eso" => $this->input->post('riwayat_eso'),
			"keterangan" => $this->input->post('keterangan'),
			"tanggal_lab" => $this->input->post('tanggal_lab'),

			//dataesodetail
            "data_eso" => $this->input->post('data_eso'),
		);
		// var_dump($data);die();
		$data = http_build_query($data);
		
		$authorization = 'Authorization:'.$_SESSION["if_ses_token_phc"];
		$url = $this->urlws."Farmasi/farmasiKlinis/saveEso";

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array($authorization ));
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		$output = curl_exec($ch); 
		curl_close($ch);    
		$datas = json_decode($output);

		echo json_encode($datas);
	}
	public function dataSatuan(){
		
		$sql = $this->farmasi_klinis_model->dataSatuan();
		$res = "";
		foreach($sql as $item){
			$res .= "<option value='".$item->KDSAT."'>".$item->SATUAN."</option>";
		}


		echo json_encode($res);
	}
}
