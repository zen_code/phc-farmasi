<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<style>
    .is_head{
        background: #2860a8;
        color: white;
    }
    

    
</style>

<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
    <input type="hidden" value="<?php echo base_url(); ?>" id="url">
        <!-- begin:: Subheader -->
        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        Farmasi Klinis </h3>
                </div>
                <!-- kt-subheader__toolbar -->
                
            </div>
        </div>

        <!-- end:: Subheader -->

        <!-- begin:: Content -->
        <div class="kt-container  kt-grid__item kt-grid__item--fluid">

            <!--Begin::Dashboard 1-->

            <!--Begin::Row-->
            <div class="row">
                
                <div class="col-xl-12">
                    <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile ">
                        <!-- <div class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                Farmasi Klinis
                                </h3>
                            </div>
                        </div> -->
                        <div class="kt-portlet__body kt-portlet__body--fit">
                            <!--begin: Datatable -->
                            <div class="kt-portlet__body">
                                <button class="btn btn-info col-md-2" type="button" id="search_pasien"   data-toggle="kt-tooltip" data-placement="top"><i class="fa fa-search"></i> Search Px</button>
                                
                            </div>
                            <div class="row"> 
                                <div class="col-md-6">
                                    <div class="kt-portlet">
                                        <div class="kt-portlet__head">
                                            <div class="kt-portlet__head-label">
                                                <h3 class="kt-portlet__head-title">
                                                    Biodata Pasien
                                                </h3>
                                            </div>
                                        </div>
                                        <div class="kt-portlet__body">
                                            <div class="row"> 
                                                <div class="form-group col-md-5">
                                                    <label>Register</label>
                                                    <input type="text" class="form-control " value="" disabled id="noreg">
                                                    
                                                </div>
                                                <div class="form-group col-md-2">
                                                    <label>Sex</label>
                                                    <input type="text" class="form-control" value="" disabled id="sex">
                                                </div>
                                                <div class="form-group col-md-5">
                                                    <label>Age</label>
                                                    <input type="text" class="form-control" value="" disabled id="age">
                                                </div>
                                            </div>
                                            <div class="row"> 
                                                <div class="form-group col-md-12">
                                                    <label>Name</label>
                                                    <input type="text" class="form-control " value="" disabled id="name">
                                                </div>
                                            </div>
                                            <div class="row"> 
                                                <div class="form-group col-md-7">
                                                    <label>Address</label>
                                                    <input type="text" class="form-control " value="" disabled id="address">
                                                </div>
                                                <div class="form-group col-md-5">
                                                    <label>City</label>
                                                    <input type="text" class="form-control" value="" disabled id="city">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="kt-portlet">
                                        <div class="kt-portlet__head">
                                            <div class="kt-portlet__head-label">
                                                <h3 class="kt-portlet__head-title">
                                                    Debitur dan Kamar Pasien
                                                </h3>
                                            </div>
                                        </div>
                                        <div class="kt-portlet__body">
                                            <div class="row"> 
                                                <div class="form-group col-md-6">
                                                    <label>RM / Index</label>
                                                    <div class="row">
                                                        <div class="col-md-8">
                                                            <input type="text" class="form-control" value="" disabled id="rm"> 
                                                        </div>
                                                        <div class="col-md-4">
                                                            <input type="text" class="form-control" value="" disabled id="indexs"> 
                                                         </div>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label>Debitur</label>
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <input type="text" class="form-control" value="" disabled id="kddeb"> 
                                                        </div>
                                                        <div class="col-md-8">
                                                            <input type="text" class="form-control" value="" disabled id="nmdeb"> 
                                                         </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row"> 
                                                <div class="form-group col-md-6">
                                                    <label>Dinas</label>
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <input type="text" class="form-control" value="" disabled id="kddin"> 
                                                        </div>
                                                        <div class="col-md-8">
                                                            <input type="text" class="form-control" value="" disabled id="nmdin"> 
                                                         </div>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label>Kamar</label>
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <input type="text" class="form-control" value="" disabled id="kdkmr"> 
                                                        </div>
                                                        <div class="col-md-8">
                                                            <input type="text" class="form-control" value="" disabled id="nmkmr"> 
                                                         </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                <div class="kt-portlet">
                                        <div class="kt-portlet__head">
                                            <div class="kt-portlet__head-label">
                                                <h3 class="kt-portlet__head-title">
                                                    Info Rawat Inap
                                                </h3>
                                            </div>
                                        </div>
                                        <div class="kt-portlet__body">
                                            <div class="row"> 
                                                <div class="form-group col-md-3">
                                                    <label>Dokter</label>
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <input type="text" class="form-control" value="" disabled id="kddok"> 
                                                        </div>
                                                        <div class="col-md-8">
                                                            <input type="text" class="form-control" value="" disabled id="nmdok"> 
                                                         </div>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <label>TGL MRS/KRS</label>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <input type="text" class="form-control" value="" disabled id="tgl_masuk"> 
                                                        </div>
                                                        <div class="col-md-6">
                                                            <input type="text" class="form-control" value="" disabled id="tgl_keluar"> 
                                                         </div>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label>Diagnosa MRS</label>
                                                    <input type="text" class="form-control" value="" disabled id="diagnosa">
                                                    
                                                </div>
                                            </div>  
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="kt-portlet__body">
                                <ul class="nav nav-tabs  nav-tabs-line nav-tabs-line-2x nav-tabs-line-info" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" data-toggle="tab" href="#tab1" role="tab">Daftar Pemberian Obat</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#tab2" role="tab">Jenis Permasalahan terkait obat (DRP)</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#tab3" role="tab">Formulir Rekonsiliasi</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#tab4" role="tab">Monitoring Efek Samping</a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab1" role="tabpanel">
                                        <div style="overflow-x:auto;">
                                            <table id="table-list_obat"  class="table table-striped- table-hover table-checkable"  style="height:500px;overflow-y: scroll; display: block;">
                                                <thead>
                                                    <tr style="background: #2860a8; color:white; text-align:center">
                                                        <th>NO.</th>
                                                        <th>KODE</th>
                                                        <th>BARANG</th>
                                                        <th>KANDUNGAN OBAT</th>
                                                        <th>SIGNA</th>
                                                        <th>DINAMYC DATE</th>

                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td colspan="99" style="text-align:center">NO DATA</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tab2" role="tabpanel">
                                        <div class="row" style="margin-bottom:20px">
                                            
                                            <div class="col-md-4">
                                                <label>Tanggal</label>
                                                <div class="input-group date">
                                                    <input type="text" class="form-control form-control-sm kt-input datepicker" placeholder="From" id="tgl_cek_drp" />
                                                    <div class="input-group-append">
                                                        <span class="input-group-text"><i class="la la-calendar-o glyphicon-th"></i></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-2"  style="margin-top: 30px;">
                                                <label class='kt-checkbox' style='top:-4px' >
                                                    <input type='checkbox' id='is_check' value="0"> 
                                                    <span></span>
                                                </label>
                                                <label>Sudah di Cek</label>
                                            </div>
                                            <input type="hidden" id="id_trans">
                                        </div>
                                        <div class="row">
                                            <div class="col-md-8">
                                                
                                                <table class="table table-striped- table-checkable" id="table-drp"  style="height:500px;overflow-y: scroll;    display: block;">
                                                   <tbody>

                                                   </tbody>
                                                </table>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label> Subjective </label>
                                                    <textarea  class="form-control soap" rows="3" id="soap_s"></textarea>
                                                </div>
                                                <div class="form-group">
                                                    <label> Objective </label>
                                                    <textarea class="form-control soap" rows="3" id="soap_o"></textarea>
                                                </div>
                                                <div class="form-group">
                                                    <label> Assesment </label>
                                                    <textarea class="form-control soap" rows="3" id="soap_a"></textarea>
                                                </div>
                                                <div class="form-group">
                                                    <label> Plan </label>
                                                    <textarea class="form-control soap" rows="3" id="soap_p"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div style="text-align: center;">
                                            <button class="btn btn-success2" id="simpan" type="button"><i class="fa fa-check"></i> SIMPAN</button>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tab3" role="tabpanel">
                                        <button class="btn btn-info col-md-2" style="margin-bottom:10px" id="tambah-rekonsiliasi"> add </button>
                                        <div style="overflow-x:auto;">
                                            <table id="table-rekonsiliasi"  class="table table-striped- table-hover table-checkable">
                                                <thead>
                                                    <tr style="background: #2860a8; color:white;width:100%; text-align:center">
                                                        <th rowspan="2" style="vertical-align:middle">NO</th>
                                                        <th width="15%" rowspan="2" style="vertical-align:middle">Nama Obat</th>
                                                        <th width="6%" rowspan="2" style="vertical-align:middle">Jml</th>
                                                        <th rowspan="2" style="vertical-align:middle">Satuan</th>
                                                        <th rowspan="2" style="vertical-align:middle">Tanggal kadaluwarsa</th>
                                                        <th rowspan="2" style="vertical-align:middle">Rute</th>
                                                        <th rowspan="2" style="vertical-align:middle">Dosis</th>
                                                        <th rowspan="2" style="vertical-align:middle">Frekuensi</th>
                                                        <th colspan="2">Lanjut</th>
                                                        <th rowspan="2" width="10%">Obat dikembalikan ke Pasien</th>
                                                        <th rowspan="2" width="6%">Jml</th>
                                                        <th rowspan="2" style="vertical-align:middle">Satuan</th>
                                                        <th rowspan="2" ></th>
                                                    </tr>
                                                    <tr style="background: #2860a8; color:white;width:100%; text-align:center">
                                                        <th>Ya</th>
                                                        <th>Tidak</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr class="no-data">
                                                        <td colspan="99" style="text-align:center;padding:10px">NO DATA</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <div style="text-align: center;">
                                                <button class="btn btn-success2" id="simpan_rekonsiliasi" type="button"><i class="fa fa-check"></i> SIMPAN</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane " id="tab4" role="tabpanel">
                                        <div class="row"> 
                                            <div class="form-group col-md-4">
                                                <div class="col-md-12">
                                                    <label>Tanggal Cek</label>
                                                    <input type="text" class="form-control datepicker" value=""  id="tgl_cek_eso" style="width: 100%;"> 
                                                </div>
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label>Penyakit Utama</label>
                                                <textarea  class="form-control" rows="3" id="penyakit_utama"></textarea>
                                            </div>
                                            <div class="form-group col-md-4">
                                            <label>Penyakit/Kondisi yang menyertainya</label>
                                                <select id="penyakit_kondisi">
                                                    <option value="0">Pilih</option>
                                                    <option value="Gangguan Ginjal">Gangguan Ginjal</option>
                                                    <option value="Gangguan Hati">Gangguan Hati</option>
                                                    <option value="Alergi">Alergi</option>
                                                    <option value="Faktor Industri, Pertanian, Kimia<">Faktor Industri, Pertanian, Kimia</option>
                                                    <option value="Merokok">Merokok</option>
                                                    <option value="Minum Alkohol">Minum Alkohol</option>
                                                    <option value="Lain-lain    ">Lain-lain</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="kt-portlet">
                                                <div class="kt-portlet__head">
                                                    <div class="kt-portlet__head-label">
                                                        <h3 class="kt-portlet__head-title">
                                                            Efek Samping Obat atau Alergi
                                                        </h3>
                                                    </div>
                                                </div>
                                                <div class="kt-portlet__body">
                                                    <div class="row"> 
                                                        <div class="form-group col-md-4">
                                                            <label>Bentuk Manifestasi ESO yang terjadi</label>
                                                            <textarea  class="form-control" rows="3" id="manifestasi_eso"></textarea>
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <label>Tanggal/Saat Mulai Terjadi</label>
                                                                    <input type="text" class="form-control datepicker" value=""  id="tgl_mulai" style="width: 100%;"> 
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <label>Tanggal/Saat Kesudahan ESO</label>
                                                                    <input type="text" class="form-control datepicker" value=""  id="tgl_selesai" style="width: 100%;"> 
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <label>Kesudahan ESO</label>
                                                                    <input type="text" class="form-control" value="" id="kesudahan_eso"> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label>Riwayat ESO yang dialami</label>
                                                            <textarea  class="form-control" rows="3" id="riwayat_eso"></textarea>
                                                        </div>
                                                    </div>  
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="kt-portlet">
                                                <div class="kt-portlet__head">
                                                    <div class="kt-portlet__head-label">
                                                        <h3 class="kt-portlet__head-title">
                                                            Obat
                                                        </h3>
                                                    </div>
                                                </div>
                                                <div class="kt-portlet__body">
                                                    <div class="row"> 
                                                        <div class="form-group col-md-12">
                                                            <button class="btn btn-info col-md-2" style="margin-bottom:10px" id="tambah-eso"> add </button>
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <div style="overflow-x:auto;">
                                                                <table id="table-eso"  class="table table-striped- table-hover table-checkable">
                                                                    <thead>
                                                                        <tr style="background: #2860a8; color:white;width:100%; text-align:center">
                                                                            <th rowspan="2" style="vertical-align:middle">NO</th>
                                                                            <th rowspan="2" style="vertical-align:middle;witdh:25%">Nama Dagang</th>
                                                                            <th rowspan="2" style="vertical-align:middle">Bentuk Sediaan</th>
                                                                            <th rowspan="2" style="vertical-align:middle">Obat yang dicurigai </th>
                                                                            <th colspan="4">Pemberian</th>
                                                                            <th rowspan="2" style="vertical-align:middle">Indikasi Penggunaan</th>
                                                                            <th rowspan="2" style="vertical-align:middle"></th>
                                                                            
                                                                        </tr>
                                                                        <tr style="background: #2860a8; color:white;width:100%; text-align:center">
                                                                            <th>Cara</th>
                                                                            <th>Dosis</th>
                                                                            <th>Tanggal Mulai</th>
                                                                            <th>Tanggal Akhir</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <tr class="no-data">
                                                                            <td colspan="99" style="text-align:center;padding:10px">NO DATA</td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>  
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="kt-portlet">
                                                <div class="kt-portlet__head">
                                                    <div class="kt-portlet__head-label">
                                                        <h3 class="kt-portlet__head-title">
                                                            Keterangan
                                                        </h3>
                                                    </div>
                                                </div>
                                                <div class="kt-portlet__body">
                                                    <div class="row"> 
                                                        <div class="form-group col-md-4">
                                                            <label>Keterangan Tambahan</label>
                                                            <textarea  class="form-control" rows="3" id="keterangan"></textarea>
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label>Tanggal Pemeriksaan Laboratorium</label>
                                                            <input type="text" class="form-control datepicker" value=""  id="tanggal_lab" style="width: 100%;">
                                                        </div>
                                                    </div>  
                                                </div>
                                            </div>
                                        </div>
                                        <div style="text-align: center;">
                                            <button class="btn btn-success2" id="simpan_eso" type="button"><i class="fa fa-check"></i> SIMPAN</button>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>

            <!--End::Row-->

            

            <!--End::Dashboard 1-->
        </div>

        <!-- end:: Content -->
    </div>
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="modal-review">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content ">
            <div class="modal-header">
                <h5 class="modal-title"><span class="tag_type"></span> Pasien</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table id="table-pasien"  class="table table-striped- table-hover table-checkable">
                    <thead>
                        <tr style="background: #2860a8; color:white;text-align:center">
                            <th style=" color:white;">NOREG</th>  
                            <th style=" color:white;">RM</th>  
                            <th style=" color:white;">TGL_MRS</th>  
                            <th style=" color:white;">NAMA</th>  
                            <th style=" color:white;">KAMAR</th>  
                            <th style=" color:white;">RUANGAN</th>  
                            <th></th>  
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th style=" color:white;">NOREG</th>  
                            <th style=" color:white;">RM</th>  
                            <th style=" color:white;">TGL_MRS</th>  
                            <th style=" color:white;">NAMA</th>  
                            <th style=" color:white;">KAMAR</th>  
                            <th style=" color:white;">RUANGAN</th>  
                            <th></th>  
                        </tr>
                    </tfoot>
                </table>
            </div>  
        </div>
    </div>
</div>
<!-- javascript this page -->
<script src="<?php echo base_url()?>assets/assets_metronic/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/assets_metronic/vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>application/modules/Farmasi_klinis/views/farmasi_klinis_view.js" type="text/javascript"></script>
<!-- <script src="<?php echo base_url()?>assets/assets_metronic/js/demo9/pages/dashboard.js" type="text/javascript"></script> -->

<script>

</script>