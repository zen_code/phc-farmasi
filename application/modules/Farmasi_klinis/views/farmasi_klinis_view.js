"use strict";

// Class definition

var FarmasiKlinis = function() {
    return {
        // Init demos
        init: function() {
            getSatuan();
            // dataPasien();
            $('.datepicker ').datepicker({
                autoclose:true, 
                format: 'dd/mm/yyyy', 
                changeMonth: true,
                changeYear: true,
            });
            $.fn.dataTable.Api.register('column().title()', function() {
                return $(this.header()).text().trim();
            });
            $('#penyakit_kondisi').select2({
                placeholder: "Silahkan Pilih",  
                width: '100%',
            });
            $('#kt_datepicker_1').datepicker({
                rtl: KTUtil.isRTL(),
                todayHighlight: true,
                orientation: "bottom right",
            });


            $('#table-pasien tfoot th').each( function () {
                var title = $(this).text();
                switch (title) {
                    case 'NOREG':
                    case 'RM':
                    case 'NAMA':
                    case 'KAMAR':
                    case 'RUANGAN':
                        $(this).html( '<input  placeholder="Search '+title+'" type="text" class="form-control form-control-sm form-filter kt-input"/>' );
                        break;
                    case 'TGL_MRS':
                        $(this).html( `<div class="input-group date">
                                        <input type="text" class="form-control form-control-sm kt-input" placeholder="From" id="kt_datepicker_1" />
                                        <div class="input-group-append">
                                            <span class="input-group-text"><i class="la la-calendar-o glyphicon-th"></i></span>
                                        </div>
                                    </div>` );
                        break;
                }
                $('#kt_datepicker_1').datepicker({autoclose:true, format: 'dd/mm/yyyy'});
                // input = $(`<input type="text" class="form-control form-control-sm form-filter kt-input" data-col-index="` + column.index() + `"/>`);
                //                 
                // $(this).html( '<input type="text" class="form-control form-control-sm form-filter kt-input"/>' );
            } );

            // var $tfoot = $('#table-pasien tfoot tr').detach();
            // // $('#table-pasien thead ').append('<tr></tr>');
            // $('#table-pasien thead').append($tfoot);
         
            // DataTable
            var table = $('#table-pasien').DataTable({
                responsive: true,
                dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
                lengthMenu: [5, 10, 25, 50],
                pageLength: 5,
                language: {
                    'lengthMenu': 'Display _MENU_',
                },
                searchDelay: 500,
                ordering: true,
                processing: true,
                serverSide: false,
                ajax: {
                    url: 'farmasi_klinis/dataPasien',
                    type: 'POST',
                    data: {
                        columnsDef: ['NOREG', 'RM', 'TGL_MRS', 'NAMA', 'KAMAR','RUANGAN','btn'],
                    },
                },
                columns       : [
                    {data : "NOREG"},
                    {data : "RM"},
                    {data : "TGL_MRS"},
                    {data : "NAMA"},
                    {data : "KAMAR"},
                    {data : "RUANGAN"},
                    {data : "btn"}
                ],
                "drawCallback": function( settings ) {
                    $('[data-toggle="kt-tooltip"]').tooltip();
                },
               
            });
         
            // Apply the search
            table.columns().every( function () {
                var that = this;
                $( 'input', this.footer() ).on( 'keyup change clear', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                });
            });
        },
    };
}();

// var tablepasien = $('#table-pasien');


var clickedLine = 0;
var dataobat = 0;
function detailPasien(noreg){
    $('#modal-review').modal('hide');
    loading()
    $.ajax({
        type: "GET",
        url: "farmasi_klinis/detailPasien", 
        data:{noreg : noreg},
        dataType: 'json',
        success: function (data) {
            var detail = data.detail
            $('#noreg').val(detail[0].NOREG);
            $('#sex').val(detail[0].JK);
            $('#age').val(detail[0].USIA);
            $('#name').val(detail[0].NAMA);
            $('#address').val(detail[0].ALAMAT);
            $('#city').val(detail[0].KOTA);
            $('#rm').val(detail[0].RM);
            $('#indexs').val(detail[0].IDX);
            $('#kddeb').val(detail[0].KODE_DEBITUR);
            $('#nmdeb').val(detail[0].DEBITUR);
            $('#kddin').val(detail[0].KODE_DINAS);
            $('#nmdin').val(detail[0].DINAS);
            $('#kdkmr').val(detail[0].KODE_KAMAR);
            $('#nmkmr').val(detail[0].KAMAR);
            $('#kddok').val(detail[0].KODE_DOKTER);
            $('#nmdok').val(detail[0].DOKTER);
            $('#tgl_masuk').val(detail[0].TGL_MRS);
            $('#tgl_keluar').val(detail[0].TGL_PRS);
            $('#diagnosa').val(detail[0].DIAGNOSA);

            $('#table-list_obat thead').html("");
            $('#table-list_obat thead').append(data.thead);
            $('#table-list_obat tbody').html("");
            $('#table-list_obat tbody').append(data.tbody);
            dataobat = data.textObatEso;
            // $('.checkbox_drug').attr('value', 0);
            // $('.checkbox_drug').prop('checked', false);
            
            // if(data.exist == 1){
            //     $('#id_trans').val(data.id_trans) ;
            // }else{
            //     $('#id_trans').val("") ;
            // }
            var d = new Date();
            var day = d.getDate();
            var strDate =  (day < 10 ? "0"+day : day)  + "/" +(d.getMonth()+1)  + "/" + d.getFullYear()  ;
            
            $('#tgl_cek_drp').val(strDate);
            $('#tgl_cek_eso').val(strDate);
            dataDrp(detail[0].NOREG, strDate);
            dataSoap(detail[0].NOREG, detail[0].RM);
            dataRekon(detail[0].NOREG, detail[0].RM);
            dataEso(detail[0].NOREG, detail[0].RM, strDate);
        },
        error: function (xhr,status,error) {

        }
    });
}
// function dataDrp(){
//     $.ajax({
//         type: "GET",
//         url: "farmasi_klinis/dataDrp", 
//         dataType: 'json',
//         success: function (data) {
//             $('#table-drp tbody').append(data);
//         },
//         error: function (xhr,status,error) {

//         }
//     });
// }
$('body').on('change', '.checkbox_drug', function(){
    clickedLine = $(this).closest('tr').data('index');
    var $tr = $('#table-drp > tbody').find('tr[data-index="'+clickedLine+'"]');
    var catatan = $tr.find("#catatan");
    if($(this).is(':checked')){
        $(this).attr('value', 1);
        catatan.prop('disabled', false);
        $('#is_check').prop('checked',true);
    } else {
        $(this).attr('value', 0);
        catatan.prop('disabled', true);
        catatan.val('');
    }
});
$('#is_check').on('change', function(){
    if($(this).is(':checked')){
        $(this).attr('value', 1);
    } else {
        $(this).attr('value', 0);
    }
});


$('#search_pasien').click(function(){
   $('#modal-review').modal('show')
});
$('#tambah-rekonsiliasi').click(function(){
    addRow();
})
var i = 1;
var listSatuan="";
var listSatuan2="";
function addRow(){
    var tr_nodata = $('#table-rekonsiliasi tbody tr.no-data');

    if(tr_nodata.length > 0 ){
        tr_nodata.remove()
    }

    listSatuan = buildList('list_satuan', dataSatuan, i, "");
    listSatuan2 = buildList('list_satuan2', dataSatuan, i, "");

    var a ="";
    a = '<tr data-index = "'+i+'">'+
            '<td style="text-align: center;vertical-align: middle;"></td>' +
            '<td><input type="text" class="form-control" id="nm_obat"></td>' +
            '<td><input type="text" class="form-control numbers" id="jumlah"></td>' +
            '<td>'+listSatuan+'</td>' +
            '<td>'+input_type("date","tgl_kadaluwarsa")+'</td>' +
            '<td><input type="text" class="form-control" id="rute"></td>' +
            '<td><input type="text" class="form-control" id="dosis"></td>' +
            '<td><input type="text" class="form-control" id="frekuensi"></td>' +
            '<td style="vertical-align: middle;text-align: center;">'+input_type("checkbox","lanjut_y")+'</td>' +
            '<td style="vertical-align: middle;text-align: center;">'+input_type("checkbox","lanjut_t")+'</td>' +
            '<td style="vertical-align: middle;text-align: center;">'+input_type("checkbox","nm_obat_kembali")+'</td>' +
            // '<td><input type="text" class="form-control" id="nm_obat_kembali"></td>' +
            '<td><input type="text" class="form-control numbers" id="jml_obat_kembali"></td>' + 
            '<td>'+listSatuan2+'</td>' +
            '<td><button class="btn btn-danger" id="btn-del"><i class="fa fa-times"   data-toggle="kt-tooltip" data-placement="top" title="xxxxxxx"></i> </button></td>' +
        '</tr>';
    $('#table-rekonsiliasi tbody').append(a);
    
    i++;
    renumber();
    input_date();
    input_number();
    $('.select_list').select2();
}
function buildList(name, data, index, dsb){
    var list = "<select class='form-control "+name+" select_list' id='"+name+"-"+index+"' "+dsb+" style='width:100%'><option> Silahkan Pilih</option>"+data+"</select>";
    
    return list;
}
function renumber() {
    var index = 1;
    $('#table-rekonsiliasi tbody tr').each(function () {
        $(this).find('td').eq(0).html(index);//.find('.num_text')
        index++;
    });
}
$('#table-rekonsiliasi').on('click', 'tr #btn-del', function () {
    var baris = $(this).parents('tr')[0];
    baris.remove();
    renumber();

    var tr = $('#table-rekonsiliasi tbody tr');
    if(tr.length < 1){
        var a = "";
        a += '<tr class="no-data">'+
            '<td colspan="99" style="text-align:center;padding:10px">No Data</td>' +
            '</tr>';
        $('#table-rekonsiliasi tbody').append(a);
    }
});
$('#tambah-eso').click(function(){
    if($('#noreg').val() != ""){
        addRowEso();
    }else{
        swal.fire('warning','Pilih Pasien terlebih dahulu', 'warning');
    }
    
})
var i_eso = 1;
var listobat=""
function addRowEso(){
    var tr_nodata = $('#table-eso tbody tr.no-data');

    if(tr_nodata.length > 0 ){
        tr_nodata.remove()
    }
    listobat = buildList("list_obat", dataobat, i, "");
    var a ="";
    a = '<tr data-index = "'+i_eso+'">'+
            '<td style="text-align: center;vertical-align: middle;"></td>' +
            '<td>'+listobat+'</td>' +
            '<td><input type="text" class="form-control" id="satuan" disabled></td>' +
            '<td style="text-align: center;">'+input_type("checkbox","obat_dicurigai")+'</td>' +
            // '<td><input type="text" class="form-control" id="obat_dicurigai" ></td>' +
            '<td><input type="text" class="form-control" id="cara"></td>' +
            '<td><input type="text" class="form-control" id="dosis"></td>' +
            '<td>'+input_type("date","tgl_mulai")+'</td>' +
            '<td>'+input_type("date","tgl_selesai")+'</td>' +
            '<td><input type="text" class="form-control" id="txt_penggunaan"></td>' +
            '<td><button class="btn btn-danger" id="btn-del"><i class="fa fa-times"   data-toggle="kt-tooltip" data-placement="top" title="Delete"></i> </button></td>' +
        '</tr>';
    $('#table-eso tbody').append(a);
    i_eso++;
    renumberEso();
    input_date();
    $('.select_list').select2();
}
// function buildListObat(data, index){
//     var list = "<select class='form-control list_obat' id='list-obat-"+index+"'><option> Silahkan Pilih</option>"+data+"</select>";
    
//     return list;
// }
var x = "";
$('#table-eso').on('change', 'tr .list_obat', function () {
    x = $(this).closest('tr').data('index');
    var $tr = $('#table-eso > tbody').find('tr[data-index="'+x+'"]');
    var satuan = $tr.find(".list_obat option:selected").attr('satuan');
    $tr.find("#satuan").val(satuan);
})
function input_type(type, param){
    var res = "";
    if(type == "date"){
        res = '<div class="input-group date">'+
                '<input type="text" class="form-control datepickers '+param+'" readonly placeholder="Pilih Tanggal" />'+
            '</div>' ;  
    }else if(type == "checkbox"){
        res = "<label class='kt-checkbox' style='top:-4px' >"+
                "<input type='checkbox' id='"+param+"'> "+
                "<span></span>"+
            "</label>"
    }
    
    return res;
}
function renumberEso() {
    var index = 1;
    $('#table-eso tbody tr').each(function () {
        $(this).find('td').eq(0).html(index);//.find('.num_text')
        index++;
    });
}
$('#table-eso').on('click', 'tr #btn-del', function () {
    var baris = $(this).parents('tr')[0];
    baris.remove();
    renumberEso();

    var tr = $('#table-eso tbody tr');
    if(tr.length < 1){
        var a = "";
        a += '<tr class="no-data">'+
            '<td colspan="99" style="text-align:center;padding:10px">No Data</td>' +
            '</tr>';
        $('#table-eso tbody').append(a);
    }
});

function dataDrp(noreg, tgl){
    $('#table-drp tbody').html("");
    var data = {
        noreg : noreg,
        tgl : tgl,
    }
    $.ajax({
        type: "POST",
        url: "farmasi_klinis/dataDrp", 
        dataType: 'json',
        data: data,
        success: function (data) {
            $('#id_trans').val(data.id_farklin);
            $('#table-drp tbody').append(data.body);
            if(data.id_farklin != ""){
                $('#is_check').prop('checked',true);
            }else{
                $('#is_check').prop('checked',false);
            }
            KTApp.unblockPage();
        },
        error: function (xhr,status,error) {

        }
    });
}
$('#tgl_cek_drp').change(function(){
    var noreg = $('#noreg').val();
    if (noreg != ""){
        loading()
        var date = $('#tgl_cek_drp').val();
        dataDrp(noreg,date)
    }else{
        swal.fire('warning','Pilih Pasien terlebih dahulu', 'warning');
        $('#tgl_cek_drp').val("");
    }
})
function dataSoap(noreg, norm){
    $('.soap ').val("");
    var data = {
        noreg : noreg,
        norm : norm,
    }
    $.ajax({
        type: "POST",
        url: "farmasi_klinis/dataSoap", 
        dataType: 'json',
        data: data,
        success: function (data) {
            if(data != null){
                data = data.data;
                var arrSoap = data.split("<br>")
                var Text = arrSoap[1].split(" : ");
                $('#soap_s').val(Text[1])
    
                 Text = arrSoap[2].split(" : ");
                $('#soap_o').val(Text[1])
    
                 Text = arrSoap[3].split(" : ");
                $('#soap_a').val(Text[1])
    
                 Text = arrSoap[4].split(" : ");
                $('#soap_p').val(Text[1])
            }
        },
        error: function (xhr,status,error) {

        }
    });
}
function input_date(){
    $('.datepickers').datepicker({
        rtl: KTUtil.isRTL(),
        todayHighlight: true,
        orientation: "bottom left",
        format : 'dd/mm/yyyy',
        autoclose: true
    });
}
function input_number(){
    $(".numbers").on("keypress keyup blur",function (event) {    
        $(this).val($(this).val().replace(/[^\d].+/, ""));
         if ((event.which < 48 || event.which > 57)) {
             event.preventDefault();
         }
    });
}
function loading(){
    KTApp.blockPage({
        overlayColor: '#000000',
        type: 'v2',
        state: 'primary',
        message: 'Processing...'
    });
}
$('#simpan').click(function(){
    
    if($('#noreg').val() == "" || $('#noreg').val() ==null){
        swal.fire('Peringatan', 'Belum memilih pasien', 'warning');
    }else{
        loading()
        var drp = [];
        $('#table-drp .is_child').each(function(){
            drp.push({
                id: $(this).find('.checkbox_drug').attr('id'),
                value: $(this).find('.checkbox_drug').attr('value'),
                catatan: $(this).find('#catatan').val(),
            })  
        });

        //soap
        var textsoap = "<p><br>S : "+$('#soap_s').val()+"<br>O : "+$('#soap_o').val()+"<br>A : "+$('#soap_a').val()+"<br>P : "+$('#soap_p').val()+"<br></p>";
                
        var data = {
            noreg : $('#noreg').val(),
            drp : drp ,
            id_trans : $('#id_trans').val(),
            tgl : $('#tgl_cek_drp').val(),

            //erm
            norm : $('#rm').val(),
            hasil_asesmen : textsoap,
        }
        // console.log(data);
        $.ajax({
            type: "POST",
            url: "farmasi_klinis/action", 
            dataType: 'json',
            data: data,
            success: function (data) {
                // $('#table-drp tbody').html("");
                // $('input').val("");
                KTApp.unblockPage();
                swal.fire(data.data.status, data.data.message,data.data.status );
            },
            error: function (xhr,status,error) {
    
            }
        });
    }
    
})


$('#simpan_rekonsiliasi').click(function(){
    
    if($('#noreg').val() == "" || $('#noreg').val() ==null){
        swal.fire('Peringatan', 'Belum memilih pasien', 'warning');
    }else{
        var msg = "";
        loading();  
        // console.log("asdadas");
        var data_rekon = [];
        $('#table-rekonsiliasi tbody tr').each(function(i){
            var jumlah = $(this).find('#jumlah').val();
            var jml_obat_kembali = $(this).find('#jml_obat_kembali').val();

            if(parseInt(jml_obat_kembali) > parseInt(jumlah) ){
                msg += "<br>Baris " + (i+1) + " , Jumlah yang dikembalikan lebih besar dari jumlah asal";
            }
            data_rekon.push({
                no: (i+1),
                nm_obat: $(this).find('#nm_obat').val(),
                jumlah: $(this).find('#jumlah').val(),
                satuan: $(this).find('.list_satuan option:selected').val(),
                tgl_kadaluwarsa: $(this).find('.tgl_kadaluwarsa').val(),
                rute: $(this).find('#rute').val(),
                dosis: $(this).find('#dosis').val(),
                frekuensi: $(this).find('#frekuensi').val(),
                lanjut : $(this).find('#lanjut_y').is(":checked") == true ? 1 : 0,
                nm_obat_kembali: $(this).find('#nm_obat_kembali').val(),
                jml_obat_kembali: $(this).find('#jml_obat_kembali').val(),
                satuan_kembali: $(this).find('.list_satuan2 option:selected').val(),
            })  
        });
        var data = {
            noreg : $('#noreg').val(),
            norm : $('#rm').val(),
            data_rekon : data_rekon ,
        }
        console.log(data);
        return false;
        if(msg != ""){
            $.ajax({
                type: "POST",
                url: "farmasi_klinis/saveRekonsiliasi", 
                dataType: 'json',
                data: data,
                success: function (data) {
                    KTApp.unblockPage();
                    swal.fire(data.data.status, data.data.massage,data.data.status );
                },
                error: function (xhr,status,error) {
        
                }
            });
        }else{  
            KTApp.unblockPage();
            swal.fire('Error', msg,'error' ); 
        }
        // console.log(data_rekon);
        
    }
    
})

$('#table-rekonsiliasi').on('change', 'tr #lanjut_y ', function () {
    clickedLine = $(this).closest('tr').data('index');
    var $tr = $('#table-rekonsiliasi > tbody').find('tr[data-index="'+clickedLine+'"]');
    var lanjut_y = $tr.find('#lanjut_y').is(":checked")
    var lanjut_t = $tr.find('#lanjut_t').is(":checked")
    if(lanjut_y &&  lanjut_t){
        $tr.find('#lanjut_t').prop('checked', false)
    }
});
$('#table-rekonsiliasi').on('change', 'tr #lanjut_t ', function () {
    clickedLine = $(this).closest('tr').data('index');
    var $tr = $('#table-rekonsiliasi > tbody').find('tr[data-index="'+clickedLine+'"]');
    var lanjut_y = $tr.find('#lanjut_y').is(":checked")
    var lanjut_t = $tr.find('#lanjut_t').is(":checked")
    if(lanjut_y &&  lanjut_t){
        $tr.find('#lanjut_y').prop('checked', false)
    }
});
function dataRekon(noreg, norm){
    // $('.soap ').val("");
    $('#table-rekonsiliasi > tbody').html("");
    var data = {
        noreg : noreg,
        norm : norm,
    }
    $.ajax({
        type: "POST",
        url: "farmasi_klinis/dataRekon", 
        dataType: 'json',
        data: data,
        success: function (data) {
            console.log(data);
            if (data != null){
                var detail = data.data.detail;
                if(detail != null){
                    $.each( detail, function( i, val ){
                        addRow();
                        var $tr = $('#table-rekonsiliasi > tbody').find('tr').last();
                        $tr.find('#nm_obat').val(val.nm_obat);
                        $tr.find('#jumlah').val(val.jumlah);
                        $tr.find('.tgl_kadaluwarsa').val(val.tgl_kadaluwarsa);
                        $tr.find('#rute').val(val.rute);
                        $tr.find('#dosis').val(val.dosis);
                        $tr.find('#frekuensi').val(val.frekuensi);
                        if(val.lanjut == 1){
                            $tr.find('#lanjut_y').prop("checked", true);
                        }else{
                            $tr.find('#lanjut_t').prop("checked", true);
                        }
                        $tr.find('#nm_obat_kembali').val(val.nm_obat_kembali);
                        $tr.find('#jml_obat_kembali').val(val.jml_obat_kembali);
                        $tr.find('list_satuan').val(val.satuan);
                        $tr.find('list_satuan').trigger('change');
                        $tr.find('list_satuan2').val(val.satuan_kebali);
                        $tr.find('list_satuan2').trigger('change');
                    });
                }else{
                    var a = "";
                    a += '<tr class="no-data"><td colspan="99" style="text-align:center;padding:10px">NO DATA</td></tr>';
                    $('#table-rekonsiliasi  tbody').append(a);
                }
            }
            
        },
        error: function (xhr,status,error) {

        }
    });
}
$('#simpan_eso').click(function(){
    if($('#noreg').val() == "" || $('#noreg').val() ==null){
        swal.fire('Peringatan', 'Belum memilih pasien', 'warning');
    }else{
        var data_eso = [];
        $('#table-eso tbody tr').each(function(i){
            
            data_eso.push({
                no_urut: (i+1),
                kdbrg: $(this).find('.list_obat option:selected').val(),
                obat_dicurigai: $(this).find('#obat_dicurigai').val(),
                cara: $(this).find('#cara').val(),
                dosis: $(this).find('#dosis').val(),
                tgl_mulai: $(this).find('.tgl_mulai').val(),
                tgl_selesai: $(this).find('.tgl_selesai').val(),
                txt_penggunaan: $(this).find('#txt_penggunaan').val(),
            })  
        });
        var data = {
            noreg : $('#noreg').val(),
            norm : $('#rm').val(),
            tgl_cek : $('#tgl_cek_eso').val(),
            penyakit_utama : $('#penyakit_utama').val(),
            penyakit_kondisi : $('#penyakit_kondisi').val(),
            manifestasi_eso : $('#manifestasi_eso').val(),
            tgl_mulai : $('#tgl_mulai').val(),
            tgl_selesai : $('#tgl_selesai').val(),
            kesudahan_eso : $('#kesudahan_eso').val(),
            riwayat_eso : $('#riwayat_eso').val(),
            keterangan : $('#keterangan').val(),
            tanggal_lab : $('#tanggal_lab').val(),
            data_eso : data_eso ,
        }   
        $.ajax({
            type: "POST",
            url: "farmasi_klinis/saveEso", 
            dataType: 'json',
            data: data,
            success: function (data) {
                KTApp.unblockPage();
                swal.fire(data.data.status, data.data.massage,data.data.status );
            },
            error: function (xhr,status,error) {
    
            }
        });
    }
});
function dataEso(noreg, norm, tgl){
    // $('.soap ').val("");
    $('#table-rekonsiliasi > tbody').html("");
    var data = {
        noreg : noreg,
        norm : norm,
        tgl : tgl,
    }
    $.ajax({
        type: "POST",
        url: "farmasi_klinis/dataEso", 
        dataType: 'json',
        data: data,
        success: function (data) {
            if(data !=null){
                console.log(data);
                var header = data.data.header;
                var detail = data.data.detail;
                if(header !== null){
                    $('#penyakit_utama').val(header.asd);
                    $('#penyakit_kondisi').val(header.asd);
                    $('#manifestasi_eso').val(header.asd);
                    $('#tgl_mulai').val(header.asd);
                    $('#tgl_selesai').val(header.asd);
                    $('#kesudahan_eso').val(header.asd);
                    $('#riwayat_eso').val(header.asd);
                    $('#keterangan').val(header.asd);
                    $('#tanggal_lab').val(header.asd);
                    if(detail != null){
                        $.each( detail, function( i, val ){
                            addRow();
                            var $tr = $('#table-eso > tbody').find('tr').last();
                            $tr.find('.list_obat').val(val.nm_obat);
                            $tr.find('#obat_dicurigai').val(val.jumlah);
                            $tr.find('#cara').val(val.tgl_kadaluwarsa);
                            $tr.find('#dosis').val(val.rute);
                            $tr.find('.tgl_mulai').val(val.dosis);
                            $tr.find('.tgl_selesai').val(val.frekuensi);
                            $tr.find('#txt_penggunaan').val(val.nm_obat_kembali);
                        });
                    }else{
                        var a = "";
                        a += '<tr class="no-eso"><td colspan="99" style="text-align:center;padding:10px">NO DATA</td></tr>';
                        $('#table-rekonsiliasi  tbody').append(a);
                    }
                }
            }
            

            // if(detail != null){
            //     $.each( detail, function( i, val ){
            //         addRow();
            //         var $tr = $('#table-rekonsiliasi > tbody').find('tr').last();
            //         $tr.find('#nm_obat').val(val.nm_obat);
            //         $tr.find('#jumlah').val(val.jumlah);
            //         $tr.find('.tgl_kadaluwarsa').val(val.tgl_kadaluwarsa);
            //         $tr.find('#rute').val(val.rute);
            //         $tr.find('#dosis').val(val.dosis);
            //         $tr.find('#frekuensi').val(val.frekuensi);
            //         if(val.lanjut == 1){
            //             $tr.find('#lanjut_y').prop("checked", true);
            //         }else{
            //             $tr.find('#lanjut_t').prop("checked", true);
            //         }
            //         $tr.find('#nm_obat_kembali').val(val.nm_obat_kembali);
            //         $tr.find('#jml_obat_kembali').val(val.jml_obat_kembali);
            //     });
            // }else{
            //     var a = "";
            //     a += '<tr class="no-data"><td colspan="99" style="text-align:center;padding:10px">NO DATA</td></tr>';
            //     $('#table-rekonsiliasi  tbody').append(a);
            // }
        },
        error: function (xhr,status,error) {

        }
    });
}
var dataSatuan = [];
function getSatuan(){
    $.ajax({
        type: "GET",
        url: "farmasi_klinis/dataSatuan", 
        dataType: 'json',
        success: function (data) {
            dataSatuan = data;
            // addRow();
        },
        error: function (xhr,status,error) {

        }
    });
}
// Class initialization on page load
jQuery(document).ready(function() {
    FarmasiKlinis.init();
});