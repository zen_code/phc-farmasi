<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class laporan_rincianBarangKeluar extends CI_Controller {
    public $urlws = null;

	public function __construct() {
        parent::__construct();
		$this->load->model('laporan_rincianBarangKeluar_model');
		$this->load->library('session');
		$this->urlws = $this->globals->ws_phc();

    }

    private function _render($view,$data = array())
	{
		$this->load->view('header',$data);
	    // $this->load->view('body_header');
	    // $this->load->view('sidebar');
	    $this->load->view($view,$data);
	    $this->load->view('footer');
    }
    
    public function index()
	{
		//load library
		// var_dump($this->session->all_userdata());die();
		$uri =& load_class('URI', 'core');
	  	redirect(base_url().'index.php/laporan_rincianBarangKeluar/data',$data);
	}

	public function data()
	{
		// var_dump($_SESSION["ses_userid"]);die();
		if($_SESSION["if_ses_depo"] == null){
			redirect(base_url().'depo');
		}else{
			if(in_array(7,$_SESSION['if_ses_menu'])){
				$data['parent_active'] = 7 ;
				$data['child_active'] = 47 ;
				$this->_render('laporan_rincianBarangKeluar_view', $data);
			}else{
				redirect(base_url().'beranda/data');}
		}
	}
	
	public function get_kode()
	{
		$query = $this->laporan_rincianBarangKeluar_model->get_kode_model();
		echo json_encode($query->result());
	}

	public function get_layanan()
	{
		$query = $this->laporan_rincianBarangKeluar_model->get_layanan_model();
		echo json_encode($query->result());
	}

	// public function laporanOld()
	// {
	// 	$tgl = $this->input->get('date_from');
	// 	$query = $this->laporan_rincianBarangKeluar_model->laporan($tgl);
	// 	$data 			= array();
	// 	foreach ($query->result() as $item) {
	// 		// $btn 		= '<button name="btnproses" id="btnproses" onclick="update(' . "'" . $item->KDIDX . "'" . ',' . "'" . $item->KDBHN . "'" . ',' . "'" . $item->FUNGSI . "'" . ')" type="button" class="btn btn-success btn-sm" data-toggle="kt-tooltip" data-placement="top" title="Update"> <i class="fa fa-edit"></i> </button>
	// 		// 			<button name="btnbatal" id="btnbatal" onclick="remove(' . "'" . $item->KDIDX . "'" . ')" type="button" class="btn btn-danger btn-sm" data-toggle="kt-tooltip" data-placement="top" title="Delete"> <i class="fa fa-times"></i> </button>';
	// 		$data[] 	= array(
	// 			"tanggal"		=> $item->TGL,
	// 			"mutasi"		=> $item->MUTASI,
	// 			"kode"			=> $item->KDBRG,
	// 			"nama"			=> $item->NMBRG,
	// 			"harga"			=> $item->HJUAL,
	// 			"jumlah"		=> $item->JUMLAH,
	// 			"total"			=> $item->TOTAL,

	// 			// "btn"		=> $btn

	// 		);
	// 	}

	// 	$obj = array("data" => $data);

	// 	echo json_encode($obj);
	// }

	public function laporanOld()
	{
		$tglmulai = $this->input->get('tglmulai'); 
		$tglselesai = $this->input->get('tglselesai'); 
		$valMNAMA = $this->input->get('valMNAMA');
		$valLAYANAN = $this->input->get('valLAYANAN'); 
		// $data 			= array();
		// $a = $valMNAMA;
		// var_dump($a);die();
		$query = $this->laporan_rincianBarangKeluar_model->laporanOld($tglmulai,$tglselesai,$valMNAMA,$valLAYANAN);
		// echo $query ? json_encode($query->result()) : 'kosong';
		$data 			= array();
		// die();
		$no = 1;
		foreach ($query->result() as $index => $item) {
			// $btn 		= '<button name="btnproses" id="btnproses" onclick="update(' . "'" . $item->KDIDX . "'" . ',' . "'" . $item->KDBHN . "'" . ',' . "'" . $item->FUNGSI . "'" . ')" type="button" class="btn btn-success btn-sm" data-toggle="kt-tooltip" data-placement="top" title="Update"> <i class="fa fa-edit"></i> </button>
			// 			<button name="btnbatal" id="btnbatal" onclick="remove(' . "'" . $item->KDIDX . "'" . ')" type="button" class="btn btn-danger btn-sm" data-toggle="kt-tooltip" data-placement="top" title="Delete"> <i class="fa fa-times"></i> </button>';
			 
			$data[] 	= array(
				"no"			=> $no,
				"tanggal"		=> $item->TANGGAL,
				"mutasi"		=> $item->MUTASI,
				"kode"			=> $item->KODE,
				"nama"			=> $item->NAMA,
				"harga"			=> $item->HARGA,
				"jumlah"		=> $item->JUMLAH,
				"total"			=> $item->TOTAL

				// "btn"		=> $btn

			);
			$no++;
		}

		$obj = array("data" => $data);

		echo json_encode($obj);
	}

	public function export_excel(){
		// print_r("yey"); exit();
		// $this->load->model("laporan_mutasiBarangMasuk_model");
		$this->load->library('excel');
		$object = new PHPExcel();

		$date_f = $this->input->post('date_fr');
		$date_t = $this->input->post('date_t');
		$kddebt = $this->input->post('MNAM');
		$kdlayanan = $this->input->post('LAYAN');

		// var_dump($kddebt); die();

		$object->setActiveSheetIndex(0);
		$table_columns = array("No","Tanggal", "Mutasi", "Kode", "Nama", "Harga", "Jumlah", "Total");

		$column = 0;

		foreach ($table_columns as $field) {
			$object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
			$column++;
		}

		$excel_row = 2;

		
		$laporan_data = $this->laporan_rincianBarangKeluar_model->laporanOld($date_f, $date_t, $kddebt, $kdlayanan);
		$no = 1;
		foreach ($laporan_data->result() as $row) {
			$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $no);
			$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->TANGGAL);
			$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->MUTASI);
			$object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row->KODE);
			$object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row->NAMA);
			$object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $row->HARGA);
			$object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $row->JUMLAH);
			$object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, $row->TOTAL);
			
			$excel_row++;
			$no++;
		}
			
		

		$object_writer = new PHPExcel_Writer_Excel2007($object);
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="Laporan Rincian Barang Keluar.xls"');
		$object_writer->save('php://output');

	}

	function cetak_pdf(){
		$date_f = $this->input->post('date_fr');
		$date_t = $this->input->post('date_t');
		$kddebt = $this->input->post('MNAM');
		$kdlayanan = $this->input->post('LAYAN');

		// var_dump($date_f); die();

		$laporan_data = $this->laporan_rincianBarangKeluar_model->laporanOld($date_f, $date_t, $kddebt, $kdlayanan);
		$data['query'] = $laporan_data->result();

		$this->load->library('pdf2');
		$this->pdf2->setPaper('A4', 'potrait');
		$this->pdf2->load_view('../views/laporan_export_pdf/laporan_barangKeluar',  $data);
	}

	function export_pdf()
	{
		// $this->load->model("report_timely_model");
		$this->load->library('pdf');

		// var_dump($this->input->post());
		$date_f = $this->input->post('date_fr');
		$date_t = $this->input->post('date_t');
		$kddebt = $this->input->post('MNAM');
		$kdlayanan = $this->input->post('LAYAN');
		// var_dump($date_f); die();
		
		$laporan_data = $this->laporan_rincianBarangKeluar_model->laporanOld($date_f, $date_t, $kddebt, $kdlayanan)->result();

		
		$pdf = new FPDF('l', 'mm', 'A4');
		// membuat halaman baru
		$pdf->AddPage();
		// setting jenis font yang akan digunakan
		$pdf->SetFont('Arial', 'B', 16);
		// mencetak string
		$pdf->Cell(260, 12, 'Laporan Rincian Barang Keluar', 0, 1, 'C');
		$pdf->SetFont('Arial', 'B', 10);
		// Memberikan space kebawah agar tidak terlalu rapat
		$pdf->Cell(10, 7, '', 0, 1);
		$pdf->Cell(10, 7, 'Periode: ' . $date_f . ' - ' . $date_t, 0, 1);
		$pdf->Cell(10, 7, 'Debitur: ' . (count($laporan_data) > 0 && $kddebt != "" ? $laporan_data[0]->KDOT : ''), 0, 1);
		$pdf->Cell(10, 7, 'Layanan: ' . (count($laporan_data) > 0 && $kdlayanan != "" ? $laporan_data[0]->MNAMA : ''), 0, 1);
		$pdf->Cell(10, 4, '', 0, 1);
		$pdf->Cell(10, 6, 'NO', 1, 0);
		$pdf->Cell(45, 6, 'TANGGAL', 1, 0);
		$pdf->Cell(35, 6, 'KODE', 1, 0);
		$pdf->Cell(135, 6, 'NAMA', 1, 0);
		$pdf->Cell(55, 6, 'JUMLAH', 1, 0);
		$pdf->SetFont('Arial', '', 10);
		
		$pdf->Cell(10, 6, '', 0, 1);
		$no=1;
		foreach ($laporan_data as $row) {
			$pdf->Cell(10, 6, $no, 1, 0);
			$pdf->Cell(45, 6, $row->TANGGAL, 1, 0);
			$pdf->Cell(35, 6, $row->KODE, 1, 0);
			$pdf->Cell(135, 6, $row->NAMA, 1, 0);
			$pdf->Cell(55, 6, $row->JUMLAH, 1, 0);
			$pdf->Cell(10, 6, '', 0, 1);
			$no++;
			// $pdf->Cell(25, 6, $row->fit, 1, 0, 'R');
			// $pdf->Cell(30, 6, $row->unfit, 1, 1, 'R');
			// $summaryFit += $row->fit;
			// $summaryUnfit += $row->unfit;
		}
		
		$pdf->Output();
	}
}