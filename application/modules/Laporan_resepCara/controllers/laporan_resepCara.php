<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class laporan_resepCara extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('laporan_resepCara_model');
		$this->load->library('session');
		$this->urlws = "http://localhost/phc-ws/api/";
	}
	private function _render($view,$data = array())
	{
		$this->load->view('header',$data);
	    // $this->load->view('body_header');
	    // $this->load->view('sidebar');
	    $this->load->view($view,$data);
	    $this->load->view('footer');
	}

	public function index()
	{
		//load library
		$uri =& load_class('URI', 'core');
	  	redirect(base_url().'index.php/laporan_resepCara/data',$data);
	}

	public function data()
	{
		// var_dump($_SESSION["ses_depo"]);die();
		if($_SESSION["if_ses_depo"] == null){
			redirect(base_url().'depo');
		}else{
			if(in_array(7,$_SESSION['if_ses_menu'])){
				$data['parent_active'] = 7 ;
				$data['child_active'] = null ;
				$this->_render('laporan_resepCara_view', $data);
			}else{
				redirect(base_url().'beranda/data');}
		}

		// if($_SESSION["if_ses_depo"] == null){
		// 	redirect(base_url().'depo');
		// }else{
		// 	$data['parent_active'] = 7 ;
		// 	$data['child_active'] = null ;
		// 	$this->_render('laporan_resepCara_view', $data);
		// }
		
	}
}
