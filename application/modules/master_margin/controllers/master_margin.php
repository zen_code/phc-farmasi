<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class master_margin extends CI_Controller {
	public $urlws = null;
	public function __construct() {
        parent::__construct();
		// $this->load->model('master_interaksi_model');
		$this->load->library('session');
		$this->urlws = $this->globals->ws_phc();

    }
	private function _render($view,$data = array())
	{
		$this->load->view('header',$data);
	    // $this->load->view('body_header');
	    // $this->load->view('sidebar');
	    $this->load->view($view,$data);
	    $this->load->view('footer');
	}

	public function index()
	{
		//load library
		// var_dump($this->session->all_userdata());die();
		$uri =& load_class('URI', 'core');
	  	redirect(base_url().'index.php/master_interaksi/data',$data);
	}

	public function data()
	{
		if($_SESSION["if_ses_depo"] == null){
			redirect(base_url().'depo');
		}else{
			if(in_array(3,$_SESSION['if_ses_menu'])){
				$data['parent_active'] = 3 ;
				$data['child_active'] = 77 ;
				$this->_render('master_margin_view', $data);
			}else{
				redirect(base_url().'beranda/data');}
		}
	}
	public function dataMaster()
	{
		// var_dump('asd');die();
		$authorization = 'Authorization:'.$_SESSION["if_ses_token_phc"];
		$url = $this->urlws."Farmasi/Margin/dataMaster";
		// $data = array(
		// 	"app_id" => $this->app_id
		// );
		$ch = curl_init();
		// curl_setopt($ch, CURLOPT_POST, 1);
		// curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array($authorization ));
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		$output = curl_exec($ch); 
		curl_close($ch);    
		$datas = json_decode($output);
		// var_dump($datas);die();
		if($datas->status == true){
			$data 			= array();
			foreach($datas->data as $item){
				$btn 		='<button name="btnproses" id="btnproses" type="button" class="btn btn-success btn-sm" data-toggle="kt-tooltip" data-placement="top" title="Update"> <i class="fa fa-edit"></i> </button>
                            <button name="btnbatal" id="btnbatal" type="button" class="btn btn-danger btn-sm" data-toggle="kt-tooltip" data-placement="top" title="Delete"> <i class="fa fa-times"></i> </button>';
                
				$data[] 	= array(
					"IDMARGIN" 			=> $item->IDMARGIN,
					"KET" 	=> $item->KET,
					"VALID1" 		=> $item->VALID1 ,
					"VALID2"	=> $item->VALID2,
					"btn"			=> $btn
				);
			}
			$obj = array("data"=> $data);

			// var_dump($data);die();
			echo json_encode($obj);
		}else{
			$error = array(
				'data' => [],
				'error' => $cek_output->message
			);
			
			echo json_encode($error);
		}
		
	}
	public function dataJenis()
	{
		// var_dump('asd');die();
		$authorization = 'Authorization:'.$_SESSION["if_ses_token_phc"];
		$url = $this->urlws."Farmasi/Margin/dataJenis";
		// $data = array(
		// 	"app_id" => $this->app_id
		// );
		$ch = curl_init();
		// curl_setopt($ch, CURLOPT_POST, 1);
		// curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array($authorization ));
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		$output = curl_exec($ch); 
		curl_close($ch);    
		$datas = json_decode($output);
		// var_dump($datas);die();
		if($datas->status == true){
			$data 			= array();
			foreach($datas->data as $item){
				$btn 		='<button name="btnjenis" id="btnjenis" type="button" class="btn btn-success btn-sm" data-toggle="kt-tooltip" data-placement="top" title="Update"> <i class="fa fa-edit"></i> </button>
                            <button name="btnbataljenis" id="btnbataljenis" type="button" class="btn btn-danger btn-sm" data-toggle="kt-tooltip" data-placement="top" title="Delete"> <i class="fa fa-times"></i> </button>';
                
				$data[] 			= array(
					"KODE" 			=> $item->KODE,
					"NAMA" 			=> $item->NAMA,
					"NAMA2" 		=> $item->NAMA2 ,
					"KODE2"			=> $item->KODE2,
					"btn"			=> $btn
				);
			}
			$obj = array("data"=> $data);

			// var_dump($data);die();
			echo json_encode($obj);
		}else{
			$error = array(
				'data' => [],
				'error' => $cek_output->message
			);
			
			echo json_encode($error);
		}
		
	}
	public function getDetail()
	{
		// var_dump($this->input->post());die();
		$authorization = 'Authorization:'.$_SESSION["if_ses_token_phc"];
		$url = $this->urlws."Farmasi/Margin/getDetail";
		$data = array(
			"margin" => $this->input->post('margin'),
			"jenis" => $this->input->post('jenis'),
		);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array($authorization ));
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		$output = curl_exec($ch); 
		curl_close($ch);    
		$datas = json_decode($output);
		var_dump($datas);die();
		if($datas->status == true){
			$data 			= array();
			foreach($datas->data as $item){
				$btn 		='<button name="btnjenis" id="btnjenis" type="button" class="btn btn-success btn-sm" data-toggle="kt-tooltip" data-placement="top" title="Update"> <i class="fa fa-edit"></i> </button>
                            <button name="btnbataljenis" id="btnbataljenis" type="button" class="btn btn-danger btn-sm" data-toggle="kt-tooltip" data-placement="top" title="Delete"> <i class="fa fa-times"></i> </button>';
                
				$data[] 			= array(
					"KODE" 			=> $item->KODE,
					"NAMA" 			=> $item->NAMA,
					"NAMA2" 		=> $item->NAMA2 ,
					"KODE2"			=> $item->KODE2,
					"btn"			=> $btn
				);
			}
			$obj = array("data"=> $data);

			// var_dump($data);die();
			echo json_encode($obj);
		}else{
			$error = array(
				'data' => [],
				'error' => $cek_output->message
			);
			
			echo json_encode($error);
		}
	}
	public function action(){
		// var_dump($this->input->post());die();	
		$authorization = 'Authorization:'.$_SESSION["if_ses_token_phc"];
		$url = $this->urlws."Farmasi/Interaksi/action";
		$data = array(
			"kode" => $this->input->post('kode'),
			"interaksi" => $this->input->post('interaksi'),
			"jenis" => $this->input->post('jenis'),
			"rekomendasi" => $this->input->post('rekomendasi'),
			"detail" => $this->input->post('detail'),
			"user" => $_SESSION["if_ses_username"],
		);
		$data = http_build_query($data);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array($authorization ));
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		$output = curl_exec($ch); 
		curl_close($ch);    
		$datas = json_decode($output);
		var_dump($datas);die();
		echo json_encode($datas);
	}
}
