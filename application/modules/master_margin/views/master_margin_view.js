"use strict";

// Class definition

var MasterInteraksi = function() {
    
    
    
    return {
        // Init demos
        init: function() {
            getDataMaster();
            loading();
            getDataJenis();
            setSelect();
            marginlist();
            jenislist();
        },
    };
}();
$('#btn_pilih').click(function(){
    
    
});
function getDataMaster(){
    // console.log('assadad');
    $('#data_master').DataTable( {
        "destroy": true,
        "processing" : true,
        "type": "GET",
        "ajax": 'master_margin/dataMaster',
        "columns"       : [
            {"data" : "IDMARGIN"},
            {"data" : "KET"},
            {"data" : "VALID1"},
            {"data" : "VALID2"},
            {"data" : "btn"},
        ],
        
        "createdRow"    : function ( row, data, index ) {
            // $('td', row).eq(0).addClass('text-center');
           // $('td', row).eq(2).addClass('text-right');
        }, 
        "drawCallback": function( settings ) {
            $('[data-toggle="kt-tooltip"]').tooltip();
        },
        "ordering": false,
        "initComplete" : function(){
            KTApp.unblockPage();
        }
    });
}
function getDataJenis(){
    // console.log('assadad');
    $('#data_jenis').DataTable( {
        "destroy": true,
        "processing" : true,
        "type": "GET",
        "ajax": 'master_margin/dataJenis',
        "columns"       : [
            {"data" : "KODE"},
            {"data" : "NAMA"},
            {"data" : "NAMA2"},
            {"data" : "KODE2"},
            {"data" : "btn"},
        ],
        
        "createdRow"    : function ( row, data, index ) {
            // $('td', row).eq(0).addClass('text-center');
           // $('td', row).eq(2).addClass('text-right');
        }, 
        "drawCallback": function( settings ) {
            $('[data-toggle="kt-tooltip"]').tooltip();
        },
        "ordering": false,
        "initComplete" : function(){
            KTApp.unblockPage();
        }
    });
}
function loading(){
    KTApp.blockPage({
        overlayColor: '#000000',
        type: 'v2',
        state: 'primary',
        message: 'Processing...'
    });
}

var clickedLine = "";


var i = 1;
function addRowPicked(idx, nama){
    var tr_nodata = $('#picked_bahan tbody tr.no-data');

    if(tr_nodata.length > 0 ){
        tr_nodata.remove()
    }
    var a ="";
    a = '<tr data-index = "'+i+'">'+
            '<td style="vertical-align: middle;"></td>'+
            '<td style="vertical-align: middle;"><input type="hidden" id="idx" value="'+idx+'">'+idx+'</td>'+
            '<td style="vertical-align: middle;">'+nama+'</td>'+
            '<td><button class="btn btn-danger btn-sm" id="btn-del"><i class="fa fa-times"   data-toggle="kt-tooltip" data-placement="top" title="xxxxxxx"></i> </button></td>' +
        '</tr>';
    $('#picked_bahan tbody').append(a);

    i++;
    renumber();

}
function renumber() {
    var index = 1;
    $('#picked_bahan tbody tr').each(function () {
        $(this).find('td').eq(0).html(index);//.find('.num_text')
        index++;
    });
}
function setSelect() {
    $('.datepicker ').datepicker({
        autoclose:true, 
        format: 'dd/mm/yyyy', 
        changeMonth: true,
        changeYear: true,
    });
}
$('#add-margin').click(function(){
    console.log('asdad')
    $('.tag_type').text('Tambah')
    $('#modal-margin').modal('show');
})
$('#add-jenis').click(function(){
    console.log('asdad')
    $('.tag_type').text('Tambah')
    $('#modal-jenis').modal('show');
})
function marginlist(){
    $.ajax({
        type: "GET",
        url: "master_margin/dataMaster", 
        dataType: 'json',
        success: function (data) {
            var a = "<option value='0'>Silahkan Pilih</option>";
            $.each(data.data, function( index, value ) {
                a += '<option value="'+value.IDMARGIN+'">'+ value.KET +'</option>'; 
            });
            $('#margin_list').select2({
                width: '100%',
                placeholder: '',
            });
            $('#margin_list').append(a);
            // KTApp.unblockPage();
            // console.log(a);
        },
        error: function (xhr,status,error) {

        }
    });
}
function jenislist(){
    $.ajax({
        type: "GET",
        url: "master_margin/dataJenis", 
        dataType: 'json',
        success: function (data) {
            var a = "<option value='0'>Silahkan Pilih</option>";
            $.each(data.data, function( index, value ) {
                a += '<option value="'+value.KODE+'">'+ value.NAMA +'</option>'; 
            });
            $('#jenis_list').select2({
                width: '100%',
                placeholder: '',
            });
            $('#jenis_list').append(a);
            // KTApp.unblockPage();
            // console.log(a);
        },
        error: function (xhr,status,error) {

        }
    });
}
$('#pilih').click(function(){
    var margin = $('#margin_list');
    var jenis = $('#jenis_list');
    if(margin.val() != 0 && jenis.val() != 0){
        $.ajax({
            type: "POST",
            url: "master_margin/getDetail", 
            dataType: 'json',
            data : {
                margin : margin.val(),
                jenis : jenis.val(),
            },
            success: function (data) {
                console.log(data);
            },
            error: function (xhr,status,error) {
    
            }
        });
    }else{
        swal.fire("warning", "Margin dan jenis margin harus dipilih", "warning");
    }
    
});
// Class initialization on page load
jQuery(document).ready(function() {
    MasterInteraksi.init();
});