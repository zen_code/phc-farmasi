<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<style>
    .passrow{
        display:none
    }
</style>
<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
    <input type="hidden" value="<?php echo base_url(); ?>" id="url">
        <!-- begin:: Subheader -->
        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                    Master Margin </h3>
                </div>
                <!-- kt-subheader__toolbar -->
                
            </div>
        </div>

        <!-- end:: Subheader -->

        <!-- begin:: Content -->
        <div class="kt-container  kt-grid__item kt-grid__item--fluid">

            <!--Begin::Dashboard 1-->

            <!--Begin::Row-->
            <div class="row">
                
                <div class="col-xl-12">
                    <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile ">
                        <div class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Master Margin
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body kt-portlet__body--fit">
                            <div class="kt-portlet__body">
                                
                           </div>
                            <div class="kt-portlet__body">
                                <div class="row">
                                    <div class="col-2">
                                        <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                            <a class="nav-link active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">Master Margin</a>
                                            <a class="nav-link" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false">Master Jenis</a>
                                            <a class="nav-link" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab" aria-controls="v-pills-messages" aria-selected="false">Item Margin</a>
                                            
                                        </div>
                                    </div>
                                    <div class="col-10">
                                        <div class="tab-content" id="v-pills-tabContent">
                                            <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                                                <button class="btn btn-info col-md-2" type="button" id="add-margin"   data-toggle="kt-tooltip" data-placement="top"><i class="fa fa-plus"></i> Tambah Margin</button>
                                                <table class="table table-striped- table-bordered table-hover table-checkable" id="data_master">
                                                    <thead>
                                                        <tr style="background:#2860a8;text-align: center;">
                                                            <th style="color:white">ID MARGIN</th>
                                                            <th style="color:white">KETERANGAN</th>
                                                            <th style="color:white">TANGGAL MULAI</th>
                                                            <th style="color:white">TANGGAL AKHIR</th>
                                                            <th style="color:white; width:10%"></th>
                                                        </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                            <div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
                                                <button class="btn btn-info col-md-2" type="button" id="add-jenis"   data-toggle="kt-tooltip" data-placement="top"><i class="fa fa-plus"></i> Tambah Jenis</button>
                                                <table class="table table-striped- table-bordered table-hover table-checkable" id="data_jenis">
                                                    <thead>
                                                        <tr style="background:#2860a8;text-align: center;">
                                                            <th style="color:white">KODE</th>
                                                            <th style="color:white">NAME</th>
                                                            <th style="color:white">KETERANGAN</th>
                                                            <th style="color:white">KODE 2</th>
                                                            <th style="color:white; width:10%"></th>
                                                        </tr>
                                                    </thead>
                                                </table>    
                                            </div>
                                            <div class="tab-pane fade" id="v-pills-messages" role="tabpanel" aria-labelledby="v-pills-messages-tab">
                                                <div class="row">
                                                    <div class="form-group col-md-3">
                                                        <label> Margin</label>
                                                        <select id="margin_list"></select>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label> Jenis Margin</label>
                                                        <div class="row">
                                                            <div class="form-group col-md-6">
                                                                <select id="jenis_list"></select>
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <button class="btn btn-info btn-sm" type="button" id="pilih">PILIH</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row" id="list_item" style="display:none">adadasadasdsdasd</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                            
                            <!--end: Datatable -->
                        </div>
                    </div>
                   
                </div>
            </div>

            <!--End::Row-->

            

            <!--End::Dashboard 1-->
        </div>

        <!-- end:: Content -->
    </div>
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="modal-margin">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content ">
        <div class="modal-header">
            <h5 class="modal-title"><span class="tag_type"></span> Master Margin</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="form-group col-md-3">
                    <label>ID MARGIN</label>
                    <input type="text" class="form-control" aria-describedby="emailHelp" placeholder="ID MARGIN" id="kode" disabled>
                </div>
                <div class="form-group col-md-9">
                    <label> KETERANGAN</label>
                    <input type="text" class="form-control" aria-describedby="emailHelp" placeholder="KETERANGAN" id="ket">
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-6">
                    <label>Tanggal Mulai</label>
                    <div class="input-group date">
                        <input type="text" class="form-control form-control-sm kt-input datepicker" placeholder="Tanggal Mulai" id="date_1"  />
                        <div class="input-group-append">
                            <span class="input-group-text"><i class="la la-calendar-o glyphicon-th"></i></span>
                        </div>
                    </div>
                </div>
                <div class="form-group col-md-6">
                    <label>Tanggal Selesai</label>
                    <div class="input-group date">
                        <input type="text" class="form-control form-control-sm kt-input datepicker" placeholder="Tanggal Selesai" id="date_1"  />
                        <div class="input-group-append">
                            <span class="input-group-text"><i class="la la-calendar-o glyphicon-th"></i></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary" id="action_margin"><span class="tag_type"></span></button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="modal-jenis">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content ">
        <div class="modal-header">
            <h5 class="modal-title"><span class="tag_type"></span> Master Jenis Margin</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="form-group col-md-3">
                    <label>KODE</label>
                    <input type="text" class="form-control" aria-describedby="emailHelp" placeholder="Kode" id="kode" >
                </div>
                <div class="form-group col-md-9">
                    <label> KODE 2</label>
                    <input type="text" class="form-control" aria-describedby="emailHelp" placeholder="Kode 2" id="kode2">
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-3">
                    <label>NAMA</label>
                    <input type="text" class="form-control" aria-describedby="emailHelp" placeholder="Nama" id="nama" >
                </div>
                <div class="form-group col-md-9">
                    <label> NAMA 2</label>
                    <input type="text" class="form-control" aria-describedby="emailHelp" placeholder="Nama 2" id="nama2">
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary" id="action_margin"><span class="tag_type"></span></button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
</div>
<!-- javascript this page -->
<script src="<?php echo base_url()?>assets/assets_metronic/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>application/modules/master_margin/views/master_margin_view.js" type="text/javascript"></script>
<!-- <script src="<?php echo base_url()?>assets-custom/user_view.js" type="text/javascript"></script> -->

<script>

</script>