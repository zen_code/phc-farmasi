<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class signa extends CI_Controller {
	
	public $urlws = null;
	public $app_id = 1;
	public function __construct() {
        parent::__construct();
		$this->load->model('roles_model');
		$this->load->library('session');
		$this->urlws = $this->globals->ws_phc();
		// $this->urlwscentra = "http://centra-dev.pelindo.co.id/public/api/";
		$this->app_id = 1;
    }
	private function _render($view,$data = array())
	{
		$this->load->view('header',$data);
	    // $this->load->view('body_header');
	    // $this->load->view('sidebar');
	    $this->load->view($view,$data);
	    $this->load->view('footer');
	}

	public function index()
	{
		//load library
		$uri =& load_class('URI', 'core');
	  	redirect(base_url().'index.php/signa/data',$data);
	}

	public function data()
	{
		if($_SESSION["if_ses_depo"] == null){
			redirect(base_url().'depo');
		}else{
			if(in_array(3,$_SESSION['if_ses_menu'])){
				$data['parent_active'] = 3 ;
				$data['child_active'] = 32 ;
				$this->_render('signa_view',$data);
			}else{
				redirect(base_url().'beranda/data');
			}
		}
		
		
	}
	public function dataSigna()
	{
		// var_dump('asd');die();
		$authorization = 'Authorization:'.$_SESSION["if_ses_token_phc"];
		$url = $this->urlws."Farmasi/Signa/dataSigna";
		// $data = array(
		// 	"app_id" => $this->app_id
		// );
		$ch = curl_init();
		// curl_setopt($ch, CURLOPT_POST, 1);
		// curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array($authorization ));
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		$output = curl_exec($ch); 
		curl_close($ch);    
		$datas = json_decode($output);
		
		if($datas->status == true){
			$data 			= array();
			foreach($datas->data as $item){
				$btn 		='<button name="btnproses" id="btnproses" type="button" class="btn btn-success btn-sm" data-toggle="kt-tooltip" data-placement="top" title="Update"> <i class="fa fa-edit"></i> </button>
                            <button name="btnbatal" id="btnbatal" type="button" class="btn btn-danger btn-sm" data-toggle="kt-tooltip" data-placement="top" title="Delete"> <i class="fa fa-times"></i> </button>';
                
				$data[] 	= array(
					"KDSIGNA" 	=> $item->KDSIGNA,
					"SIGNACEPAT" 	=> $item->SIGNACEPAT,
					"SIGNA" 			=> $item->SIGNA ,
					"URAIAN"		=> $item->URAIAN,
					"PERHARI"		=> $item->PERHARI,
					"FREK1D"		=> $item->FREK1D,
					"JMLPAKAI"		=> $item->JMLPAKAI,
					"SATPAKAI"		=> $item->SATPAKAI,
					"FREK1D"		=> $item->FREK1D,
					"btn"			=> $btn
				);
			}
			$obj = array("data"=> $data);

			echo json_encode($obj);
		}else{
			$error = array(
				'data' => [],
				'error' => $cek_output->message
			);
			
			echo json_encode($error);
		}
		
	}
	public function actionSigna(){
		$data = array(
            "kode"     			=> $this->input->post('kode'),
            "signa_cepat"     	=> $this->input->post('signa_cepat'),
            "signa"     		=> $this->input->post('signa'),
            "uraian"     		=> $this->input->post('uraian'),
            "satuan"     		=> $this->input->post('satuan'),
            "jam_per_hari"     	=> $this->input->post('jam_per_hari'),
            "frek_per_hari"     => $this->input->post('frek_per_hari'),
            "jam_sekali_minum"  => $this->input->post('jam_sekali_minum'),
            "user"     		=> $_SESSION["if_ses_username"],
		);
		// var_dump( $data);die();
		$authorization = 'Authorization: '.$_SESSION["if_ses_token_phc"];
		$url = $this->urlws."Farmasi/Signa/action";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array($authorization ));
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		$output = curl_exec($ch); 
		curl_close($ch);    

		$cek_output = json_decode($output);

		// var_dump($output);die();
		if($cek_output->status == true){
			echo json_encode($cek_output);
		}else{
			$error = array(
				'data' => [],
				'error' => $cek_output->message
			);
			
		}
	}
	public function deleteSigna(){
		$data = array(
            "id_profile"     => $this->input->post('ses_username'),
            "user_id"     =>  $_SESSION["if_ses_userid"]
		);

		$authorization = 'Authorization: '.$_SESSION["if_ses_token_phc"];
		$url = $this->urlws."Farmasi/Signa/delete";
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array($authorization ));
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		$output = curl_exec($ch); 
		curl_close($ch);    
		// var_dump($output);die();
		$cek_output = json_decode($output);
		if($cek_output->status == true){
			echo json_encode($cek_output);
		}else{
			$error = array(
				'data' => [],
				'error' => $cek_output->message
			);
			
		}
	}
	
}
