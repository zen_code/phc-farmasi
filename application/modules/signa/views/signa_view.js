"use strict";

// Class definition

var Signa = function() {
    return {
        // Init demos
        init: function() {
            loading();
            getDataSigna();
        },
    };
}();

function getDataSigna() {
    // console.log('assadad');
    $('#data_signa').DataTable({
        "destroy": true,
        "processing": true,
        "type": "GET",
        "ajax": 'signa/dataSigna',
        "columns": [
            { "data": "KDSIGNA" },
            { "data": "SIGNACEPAT" },
            { "data": "SIGNA" },
            { "data": "URAIAN" },
            { "data": "SATPAKAI" },
            { "data": "PERHARI" },
            { "data": "JMLPAKAI" },
            { "data": "FREK1D" },
            { "data": "btn" },
        ],

        "createdRow": function(row, data, index) {
            // $('td', row).eq(0).addClass('text-center');
            // $('td', row).eq(2).addClass('text-right');
        },
        "drawCallback": function(settings) {
            $('[data-toggle="kt-tooltip"]').tooltip();
        },
        "initComplete": function() {
            KTApp.unblockPage();
        }
    });
}

function update(id, type) {
    swal.fire('success', "id=" + id + " type=" + type, 'success');
}

$('#add_signa').click(function() {
    $('input').val('');
    $('.tag_type').text('Tambah');
    $('#modal-review').modal({ backdrop: 'static', keyboard: false });
    $('#modal-review').modal('show');
});
$('#action').click(function() {
    $('#modal-review').modal('hide');
    // loading();
    $.ajax({
        type: "POST",
        url: "signa/actionSigna",
        data: {
            kode: $('#kode').val(),
            signa_cepat: $('#signa_cepat').val(),
            signa: $('#signa').val(),
            uraian: $('#uraian').val(),
            satuan: $('#satuan').val(),
            jam_per_hari: $('#jam_per_hari').val(),
            frek_per_hari: $('#frek_per_hari').val(),
            jam_sekali_minum: $('#jam_sekali_minum').val()
        },
        dataType: 'json',
        async: false,
        success: function(data) {
            console.log(data);
            KTApp.unblockPage();
            swal.fire(data.data.status, data.data.message, data.data.status);
            getDataSigna();
        },
        error: function(xhr, status, error) {
            console.log(status, error);
        }
    });

});
$('#data_signa').on('click', 'tr #btnproses', function() {
    var baris = $(this).parents('tr')[0];
    var table = $('#data_signa').DataTable();
    var datas = table.row(baris).data();

    $('.tag_type').text('Edit');
    $('#kode').val(datas['KDSIGNA']);
    $('#signa_cepat').val(datas['SIGNACEPAT']);
    $('#signa').val(datas['SIGNA']);
    $('#uraian').val(datas['URAIAN']);
    $('#satuan').val(datas['SATPAKAI']);
    $('#jam_per_hari').val(datas['PERHARI']);
    $('#frek_per_hari').val(datas['FREK1D']);
    $('#jam_sekali_minum').val(datas['JMLPAKAI']);
    $('#modal-review').modal('show');
});
$('#data_signa').on('click', 'tr #btnbatal', function() {
    var baris = $(this).parents('tr')[0];
    var table = $('#data_signa').DataTable();
    var datas = table.row(baris).data();

    swal.fire({
        title: '<span style="font-size: 19px;font-weight: 700;">Menghapus User</span>',
        text: 'Apakah anda yakin menghapus profil ini?',
        type: 'question',
        showCancelButton: true,
        confirmButtonText: 'Ya',
        cancelButtonText: 'Tidak',
    }).then(function(result) {
        if (result.value) {
            deleteSigna(datas['KDSIGNA']);
        }
    })
})

function deleteSigna(id_profil) {

    $.ajax({
        type: "POST",
        url: "signa/delete",
        data: {
            kode: id_profil
        },
        dataType: 'json',
        success: function(data) {
            KTApp.unblockPage();
            getDataSigna();
        },
        error: function(xhr, status, error) {}
    });
}

function loading() {
    KTApp.blockPage({
        overlayColor: '#000000',
        type: 'v2',
        state: 'primary',
        message: 'Processing...'
    });
}
// Class initialization on page load
jQuery(document).ready(function() {
    Signa.init();
});