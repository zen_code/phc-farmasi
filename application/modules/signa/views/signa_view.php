<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
    <input type="hidden" value="<?php echo base_url(); ?>" id="url">
        <!-- begin:: Subheader -->
        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        SIGNA </h3>
                </div>
                <!-- kt-subheader__toolbar -->
                
            </div>
        </div>

        <!-- end:: Subheader -->

        <!-- begin:: Content -->
        <div class="kt-container  kt-grid__item kt-grid__item--fluid">

            <!--Begin::Dashboard 1-->

            <!--Begin::Row-->
            <div class="row">
                
                <div class="col-xl-12">
                    <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile ">
                        <div class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    SIGNA
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body kt-portlet__body--fit">
                            <div class="kt-portlet__body">
                                <button class="btn btn-info col-md-2" type="button" id="add_signa"   data-toggle="kt-tooltip" data-placement="top"><i class="fa fa-plus"></i> Tambah Signa</button>
                                
                            </div>
                            <!--begin: Datatable -->
                            <div class="kt-portlet__body">
                                
                                <table class="table table-striped- table-bordered table-hover table-checkable" id="data_signa">
                                    <thead>
                                        <tr style="background:#2860a8;text-align: center;">
                                            <th style="color:white">KODE</th>
                                            <th style="color:white">SIGNA CEPAT</th>
                                            <th style="color:white">SIGNA</th>
                                            <th style="color:white">URAIAN</th>
                                            <th style="color:white">SATUAN</th>
                                            <th style="color:white">JUMLAH PER HARI</th>
                                            <th style="color:white">JUMLAH SEKALI MINUM</th>
                                            <th style="color:white">FREK. / HARI</th>
                                            <th style="color:white; width:10%"></th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                            
                            <!--end: Datatable -->
                        </div>
                    </div>
                </div>
            </div>

            <!--End::Row-->

            

            <!--End::Dashboard 1-->
        </div>

        <!-- end:: Content -->
    </div>
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="modal-review">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content ">
        <div class="modal-header">
            <h5 class="modal-title"><span class="tag_type"></span> Signa</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="form-group col-md-3">
                    <label>KODE</label>
                    <input type="text" class="form-control" aria-describedby="emailHelp" placeholder="kode" id="kode" disabled>
                </div>
                <div class="form-group col-md-3">
                    <label> Signa Cepat</label>
                    <input type="text" class="form-control" aria-describedby="emailHelp" placeholder="Signa Cepat" id="signa_cepat">
                </div>
                <div class="form-group col-md-6">
                    <label> Signa</label>
                    <input type="text" class="form-control" aria-describedby="emailHelp" placeholder="Signa" id="signa">
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-12">
                    <label>Uraian<span style="color:red">*</span></label>
                    <input type="text" class="form-control" aria-describedby="emailHelp" placeholder="Uraian" id="uraian">
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-3">
                    <label> Satuan</label>
                    <input type="text" class="form-control" aria-describedby="emailHelp" placeholder="Satuan" id="satuan">
                </div>
                <div class="form-group col-md-3">
                    <label>Jumlah Per Hari<span style="color:red">*</span></label>
                    <input type="number" class="form-control" aria-describedby="emailHelp" placeholder="Jam Per Hari" id="jam_per_hari">
                </div>
                <div class="form-group col-md-3">
                    <label> Frek./ Hari</label>
                    <input type="number" class="form-control" aria-describedby="emailHelp" placeholder="Frek./ Hari" id="frek_per_hari">
                </div>
                <div class="form-group col-md-3">
                    <label>Jam Sekali Minum</label>
                    <input type="number" class="form-control" aria-describedby="emailHelp" placeholder="Jam Sekali Minum" id="jam_sekali_minum">
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary" id="action"><span class="tag_type"></span></button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
</div>
<!-- javascript this page -->
<script src="<?php echo base_url()?>assets/assets_metronic/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>application/modules/signa/views/signa_view.js" type="text/javascript"></script>
<!-- <script src="<?php echo base_url()?>assets/assets_metronic/js/demo9/pages/dashboard.js" type="text/javascript"></script> -->

<script>

</script>