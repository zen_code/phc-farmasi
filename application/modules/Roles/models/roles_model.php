<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class roles_model extends CI_Model{
	public function __construct()
  {
      // Call the CI_Model constructor
      parent::__construct();
     $this->load->database();
  }
 
  /*public function getdatalogin($user,$pass)
  {
    $this->load->database();
    $sql = "exec inap_sp_login_bacapriv '$user','$pass'";
    return $this->db->query($sql);  
  }*/
  // public function get_datarole()
  // {
  //   $this->load->database();
  //   $sql = "select * from APP_PROFIL";
  //   return $this->db->query($sql);    
  // }
  public function getPrivelege($id){
    $data = [];
    $profile = $this->db->query("select * from APP_PROFIL where ID_PROFIL = ".$id);
    $data['profile'] = $profile->result();

    return $data;

  }
  public function allmenu($app_id){
    $data = [];

    $menu = $this->db->query("select * from APP_MASTER_MENU_APLIKASI WHERE PARENT_MENU is null and APLIKASI =".$app_id);
    foreach($menu->result() as $x => $item){
      $data[$x]['id'] = $item->ID_MENU;
      $data[$x]['name'] = $item->NAMA_MODUL;
      $data[$x]['parent'] = $item->PARENT_MENU;
      
      $child = $this->db->query("select * from APP_MASTER_MENU_APLIKASI WHERE PARENT_MENU=".$item->ID_MENU);
      foreach($child->result() as $y => $itemy){
        $data[$x]['child'][$y]['id'] = $itemy->ID_MENU;
        $data[$x]['child'][$y]['name'] = $itemy->NAMA_MODUL;
        $data[$x]['child'][$y]['parent'] = $itemy->PARENT_MENU;
      }
      
    }
    return $data;
  }
  public function getUser($id){
    $sql = $this->db->query("select * from APP_USER_PROFIL WHERE PROFIL = ".$id);
    return $sql->result();
  }
  public function profilemenu($app_id){
    $menu = $this->db->query("select * from APP_PRIVILEGES_PROFIL WHERE PROFIL = ".$app_id);
    return  $menu->result();
  }
}
