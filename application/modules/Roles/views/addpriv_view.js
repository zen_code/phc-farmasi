"use strict";

// Class definition

var asd = "<?php echo base_url()?> ";
var addprivView = function() {
    return {
        // Init demos
        init: function() {
            // getUser();
            getAllUser();
            getAllMenu();
            // console.log('asdadasasdasda');
            KTApp.blockPage({
                overlayColor: '#000000',
                type: 'v2',
                state: 'primary',
                message: 'Processing...'
            });
        },
    };
}();

function getUser(){
    $.ajax({
        type: "POST",
        url: "roles/getUser", 
        data : {
            ID_PROFIL:$('#ID_PROFIL').val()
        },
        dataType: 'json',
        success: function (data) {
            jQuery.each(data, function(i, val) {
                // console.log(val.USERID);
                addRow();
                var $tr = $('#user-table > tbody').find('tr').last();
                $tr.find('.list_user').val(val.USERID);
                $tr.find('.list_user').trigger('change')
            });
        },
        error: function (xhr,status,error) {
        }
    });
}
var datauser='';
var listuser = '';
function getAllUser(){
    $.ajax({
        type: "GET",
        // url: "user/dataUser", 
        url: "roles/getAllUser", 
        dataType: 'json',
        success: function (data) {
            datauser = data.data;
            getUser();
        },
        error: function (xhr,status,error) {
            console.log(xhr)
        }
    });
}
$('#tambah-user').click(function(){
    addRow();
})
var i = 1;
function addRow(){
    var tr_nodata = $('#user-table tbody tr.no-data');

    if(tr_nodata.length > 0 ){
        tr_nodata.remove()
    }
    
    listuser = buildListUser(datauser, i);
    
    var a ="";
    // listobat = '<select><option>123131331</option><option>xccxcc</option><option>asdadasda</option></select>';
    a = '<tr data-index = "'+i+'">'+
        '<td style="text-align: center;vertical-align: middle;"></td>' +
        '<td>'+listuser+'</td>' +
        '<td><button class="btn btn-danger" id="btn-del"><i class="fa fa-times"   data-toggle="kt-tooltip" data-placement="top" title="xxxxxxx"></i> </button></td>' +
        '</tr>';
    $('#user-table tbody').append(a);
    i++;
    renumber();
    $('.list_user').select2();
}
function buildListUser(data, index){
    var a = '';
    jQuery.each(data, function(i, val) {
        a += "<option value='"+val.id+"'>"+ val.username +"</option>";
    });
    var list = "<select class='form-control list_user' id='list-user-"+index+"'><option> Pilih User</option>"+a+"</select>";
    return list;
}
function renumber() {
    var index = 1;
    $('#user-table tbody tr').each(function () {
        $(this).find('td').eq(0).html(index);//.find('.num_text')
        index++;
    });
}
$('#user-table').on('click', 'tr #btn-del', function () {
    var baris = $(this).parents('tr')[0];
    baris.remove();
    renumber();

    var tr = $('#user-table tbody tr');
    if(tr.length < 1){
        var a = "";
        a += '<tr class="no-data">'+
            '<td colspan="99" style="text-align:center;padding:10px">No Data</td>' +
            '</tr>';
        $('#user-table tbody').append(a);
    }
});
function getAllMenu(){
    $.ajax({
        type: "POST",
        url: "roles/getallmenu", 
        dataType: 'json',
        success: function (data) {
            var a="";
            jQuery.each(data, function(i, val) {
                var lbldis = i == 0 ? 'kt-checkbox--disabled' : '';
                var inpdis = i == 0 ? 'disabled checked' : '';
                a +='<div class="row">'+
                        '<label class="kt-checkbox '+lbldis+'">'+
                            '<input type="checkbox" value="'+val.id+'" data-parent="'+val.parent+'" id="check_box" '+inpdis+'> <b>'+val.name+'</b>'+
                            '<span></span>'+
                        '</label>'+
                    '</div>';
                    if(val.child){
                        jQuery.each(val.child, function(x, valx) {
                            a +='<div class="row" style="padding-left: 20px;">'+
                                    '<label class="kt-checkbox">'+
                                        '<input type="checkbox" value="'+valx.id+'" data-parent="'+valx.parent+'" id="check_box"> '+valx.name+
                                        '<span></span>'+
                                    '</label>'+
                                '</div>';

                        })
                    }
                a+='<hr>';
                    
            });
            $('#all-menu').append(a);
            getProfileMenu();
        },
        error: function (xhr,status,error) {
        }
    });
}
$('body').on('change','input[type="checkbox"]', function(){
    var check_value = $(this).val();
    var check_parent = $(this).data('parent');

    if($(this).prop("checked") == true){
        if(check_parent != null){
            $('input[type="checkbox"]').each(function(){
                if($(this).val() == check_parent){
                    $(this).prop('checked', true );
                }
            });
        }else{
            $('input[type="checkbox"]').each(function(){
                if($(this).data('parent') != null && $(this).data('parent') == check_value){
                    $(this).prop('checked', true );
                }
            });
        }
    }else{
        if(check_parent == null){
            $('input[type="checkbox"]').each(function(){
                if($(this).data('parent') != null && $(this).data('parent') == check_value){
                    $(this).prop('checked', false );
                }
            });
        }
    }
});
function getProfileMenu(){
    $.ajax({
        type: "POST",
        url: "roles/getprofilemenu", 
        data : {
            ID_PROFIL:$('#ID_PROFIL').val()
        },
        dataType: 'json',
        success: function (data) {
            jQuery.each(data, function(i, val) {
                $('input[type="checkbox"]').each(function(){
                    if($(this).val() == val.MENU_APLIKASI){
                        $(this).prop('checked', true );
                    }
                });
            });
            KTApp.unblockPage();
        },
        error: function (xhr,status,error) {
        }
    });
}
$('#btn-back').click(function(){
    location.href = "roles"
});
$('#btn-save').click(function(){
    KTApp.blockPage({
        overlayColor: '#000000',
        type: 'v2',
        state: 'primary',
        message: 'Processing...'
    });
    var menu = [];
    $('input[type="checkbox"]').each(function(){
        if($(this).prop('checked') == true){
            var menu_id = $(this).val();
            menu.push(menu_id)
        }
    });
    var user = [];
    $('#user-table tbody tr').each(function(){
        var user_id = $(this).find('td:eq(1) .list_user option:selected').val();
        user.push(user_id)
    });
    $.ajax({
        type: "POST",
        url: "roles/addPrivAction", 
        data : {
            id_profile:$('#ID_PROFIL').val(),
            user:user,
            menu:menu,
        },
        dataType: 'json',
        success: function (data) {
            // console.log(data);
            KTApp.unblockPage();
            swal.fire(data.data.status, data.data.message,data.data.status);
            location.href = "roles"
        },
        error: function (xhr,status,error) {
        }
    });
    // console.log(menu, user)
});
// Class initialization on page load
jQuery(document).ready(function() {
    addprivView.init();
});