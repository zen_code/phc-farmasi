<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<style>
    .read1{
        background: #2860a8;
    color: white;
    }
    .dataTables_filter{
        float:right
    }
</style>
<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
    <input type="hidden" value="<?php echo base_url(); ?>" id="url">
        <!-- begin:: Subheader -->
        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                    Add Privelege Menu dan User </h3>
                </div>
                <!-- kt-subheader__toolbar -->
                
            </div>
        </div>

        <!-- end:: Subheader -->

        <!-- begin:: Content -->
        <div class="kt-container  kt-grid__item kt-grid__item--fluid">

            <!--Begin::Dashboard 1-->

            <!--Begin::Row-->
            <div class="row">
                
                <div class="col-xl-12">
                    <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile ">
                        
                        <div class="kt-portlet__body kt-portlet__body--fit">
                            
                            <!--begin: Datatable -->
                            <div class="kt-portlet__body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="kt-portlet">
                                            <div class="kt-portlet__head">
                                                <div class="kt-portlet__head-label">
                                                    <h3 class="kt-portlet__head-title">
                                                        Profile
                                                    </h3>
                                                </div>
                                            </div>
                                            <div class="kt-portlet__body">
                                                <div class="row"> 
                                                    <div class="form-group col-md-4">
                                                        <label>Nama Profile</label>
                                                        <input type="text" class="form-control " value="<?php echo $priv['profile'][0]->NAMA_PROFIL ?>" readonly>
                                                        <input type="hidden" value="<?php echo $priv['profile'][0]->ID_PROFIL ?>" id="ID_PROFIL">
                                                    </div>
                                                    <div class="form-group col-md-4">
                                                        <label>Info Profile</label>
                                                        <input type="email" class="form-control" value="<?php echo $priv['profile'][0]->INFO ?>" readonly>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                                <div class="row"> 
                                    <div class="col-md-6">
                                        <div class="kt-portlet">
                                            <div class="kt-portlet__head">
                                                <div class="kt-portlet__head-label">
                                                    <h3 class="kt-portlet__head-title">
                                                        User List
                                                    </h3>
                                                </div>
                                            </div>
                                            <div class="kt-portlet__body">
                                                <button class="btn btn-warning col-md-2" id="tambah-user" style="margin-bottom: 10px;"><i class="fa fa-plus"></i> User</button>
                                                <table class="table table-striped- table-bordered table-hover table-checkable" id="user-table">
                                                    <thead >
                                                        <tr style="background:#2860a8;text-align: center;">
                                                            <th style="color:white;width:10%">NO</th>
                                                            <th style="color:white">USER</th>
                                                            <th style="color:white;width:10%"></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr class="no-data">
                                                            <td colspan="99" style="text-align:center;padding:10px">NO DATA</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="kt-portlet">
                                            <div class="kt-portlet__head">
                                                <div class="kt-portlet__head-label">
                                                    <h3 class="kt-portlet__head-title">
                                                        Menu List
                                                    </h3>
                                                </div>
                                            </div>
                                            <div class="kt-portlet__body">
                                                <div id="all-menu"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row"> 
                                    <div class="col-md-6">
                                        <button class="btn btn-danger" style="float:right" id="btn-back"><i class="fa fa-arrow-left"></i> Back</button>
                                    </div>
                                    <div class="col-md-6">
                                        <button class="btn btn-success"  id="btn-save"><i class="fa fa-check" ></i> Save</button>
                                    </div>
                                </div>
                                
                            </div>
                            
                            <!--end: Datatable -->
                        </div>
                    </div>
                </div>
            </div>

            <!--End::Row-->

            

            <!--End::Dashboard 1-->
        </div>

        <!-- end:: Content -->
    </div>
</div>
<!-- javascript this page -->
<script src="<?php echo base_url()?>assets/assets_metronic/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>application/modules/Roles/views/addpriv_view.js" type="text/javascript"></script>

<script>
    // $('#user-table').DataTable();
</script>