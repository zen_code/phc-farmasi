<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<style>
    .passrow{
        display:none
    }
</style>
<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
    <input type="hidden" value="<?php echo base_url(); ?>" id="url">
        <!-- begin:: Subheader -->
        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        User </h3>
                </div>
                <!-- kt-subheader__toolbar -->
                
            </div>
        </div>

        <!-- end:: Subheader -->

        <!-- begin:: Content -->
        <div class="kt-container  kt-grid__item kt-grid__item--fluid">

            <!--Begin::Dashboard 1-->

            <!--Begin::Row-->
            <div class="row">
                
                <div class="col-xl-12">
                    <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile ">
                        <div class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    User
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body kt-portlet__body--fit">

                            <!--begin: Datatable -->
                            <div class="kt-portlet__body">
                                <button class="btn btn-success col-md-2" type="button" id="add_user"><i class="fa fa-plus"></i>Tambah User</button>
                                <table class="table table-striped- table-bordered table-hover table-checkable" id="data_user">
                                    <thead>
                                        <tr style="background:#2860a8;text-align: center;">
                                            <th style="color:white;">ID</th>
                                            <th style="color:white;">NIPP</th>
                                            <th style="color:white">Email</th>
                                            <th style="color:white">SIP</th>
                                            <th style="color:white">NAMA DOKTER </th>
                                            <th style="color:white">KLINIK</th>
                                            <th style="color:white">RUANG KEPERAWATAN   </th>
                                            <th style="color:white">ACTION</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                            
                            <!--end: Datatable -->
                        </div>
                    </div>
                </div>
            </div>

            <!--End::Row-->

            

            <!--End::Dashboard 1-->
        </div>

        <!-- end:: Content -->
    </div>
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="modal-review">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title"><span class="tag_type"></span> User</h5>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="form-group col-md-6">
                    <label>NIPP<span style="color:red">*</span></label>
                    <input type="text" class="form-control" aria-describedby="emailHelp" placeholder="NIPP" id="nipp">
                    <input type="hidden" id="id_user">
                </div>
                <div class="form-group col-md-6">
                    <label>Email</label>
                    <input type="email" class="form-control" aria-describedby="emailHelp" placeholder="Email" id="email">
                </div>
            </div>
            <div class="row passrow" id="passrow">
                <div class="form-group col-md-6">
                    <label>Password</label>
                    <input type="password" class="form-control" aria-describedby="emailHelp" placeholder="Password" id="pass">
                </div>
                <div class="form-group col-md-6">
                    <label>Confirm Password</label>
                    <input type="password" class="form-control" aria-describedby="emailHelp" placeholder="Confirm Password" id="pass2">
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-6">
                    <label>SIP</label>
                    <input type="text" class="form-control" aria-describedby="emailHelp" placeholder="SIP" id="sip">
                </div>
                <div class="form-group col-md-6">
                    <label>Dokter</label>
                    <select id="list_dokter"  class="form-control"></select>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-6">
                    <label>Klinik</label>
                    <select id="list_klinik"  class="form-control"></select>
                </div>
                <div class="form-group col-md-6">
                    <label>Ruang Perawatan</label>
                    <select id="list_perawatan"  class="form-control"></select>
                </div>
            </div>
            
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary" id="action"><span class="tag_type"></span></button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
</div>
<!-- javascript this page -->
<script src="<?php echo base_url()?>assets/assets_metronic/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>application/modules/User/views/user_view.js" type="text/javascript"></script>
<!-- <script src="<?php echo base_url()?>assets-custom/user_view.js" type="text/javascript"></script> -->

<script>

</script>