<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class user extends CI_Controller {
	public $urlws = null;
	public function __construct() {
        parent::__construct();
		$this->load->model('user_model');
		$this->load->library('session');
		$this->urlws = $this->globals->ws_phc();

    }
	private function _render($view,$data = array())
	{
		$this->load->view('header',$data);
	    // $this->load->view('body_header');
	    // $this->load->view('sidebar');
	    $this->load->view($view,$data);
	    $this->load->view('footer');
	}

	public function index()
	{
		//load library
		// var_dump($this->session->all_userdata());die();
		$uri =& load_class('URI', 'core');
	  	redirect(base_url().'index.php/user/data',$data);
	}

	public function data()
	{
		// var_dump($_SESSION["ses_userid"]);die();
		$this->_render('user_view');
	}
	public function dataUser(){
		$authorization = 'Authorization: '.$_SESSION["if_ses_token_phc"];
		$url = $this->urlws."Farmasi/listUser";
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_POST, 1);
		// curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array($authorization ));
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		$output = curl_exec($ch); 
		curl_close($ch);    

		$cek_output = json_decode($output);
		// var_dump($cek_output);die();

		if($cek_output->status == true){
			$data 			= array();
			foreach($cek_output->data as $item){
				$btn 		='<button name="btnproses" id="btnproses" type="button" class="btn btn-success btn-sm "> <i class="fa fa-edit"></i> </button>
                            <button onclick="update('.$item->USERID.',2)" name="btnbatal" id="btnbatal" type="button" class="btn btn-danger btn-sm"> <i class="fa fa-times"></i> </button>';
                $data[] 	= array(
                    "USERID" 	=> $item->USERID,
                    "NIPP" 		=> $item->NIPP,
                    "EMAIL" 	=> $item->EMAIL ,
                    "SIP"		=> $item->SIP,
                    "nmDok"		=> $item->nmDok,
                    "REKENING"	=> $item->REKENING,
                    "NM_RUANG"	=> $item->NM_RUANG,
                    "btn" 		=> $btn
                );
			}
			$obj = array("data"=> $data);
			echo json_encode($obj);
		}else{
			$error = array(
				'data' => [],
				'error' => $cek_output->message
			);
			
			echo json_encode($error);
		}
		
	}
	// public function dataUser()
	// {
	// 	// var_dump('asdad');die();
	// 	$query=$this->user_model->get_datauser();
	// 	$data 			= array();
	// 	foreach($query->result() as $item){
	// 		$btn 		='<button name="btnproses" id="btnproses" type="button" class="btn btn-success btn-sm "> <i class="fa fa-edit"></i> </button>
	// 					<button onclick="update('.$item->USERID.',2)" name="btnbatal" id="btnbatal" type="button" class="btn btn-danger btn-sm"> <i class="fa fa-times"></i> </button>';
	// 		$data[] 	= array(
	// 			"USERID" 	=> $item->USERID,
	// 			"NIPP" 		=> $item->NIPP,
	// 			"EMAIL" 	=> $item->EMAIL ,
	// 			"SIP"		=> $item->SIP,
	// 			"nmDok"		=> $item->nmDok,
	// 			"REKENING"	=> $item->REKENING,
	// 			"NM_RUANG"	=> $item->NM_RUANG,
	// 			"btn" 		=> $btn
	// 		);
	// 	}
	// 	// var_dump($data);die();

	// 	$obj = array("data"=> $data);

	// 	echo json_encode($obj);
	// }
	public function dataDokter(){
		$authorization = 'Authorization: '.$_SESSION["ses_token"];
		$url = $this->urlws."Global/listDokter";
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_POST, 1);
		// curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array($authorization ));
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		$output = curl_exec($ch); 
		curl_close($ch);    

		$cek_output = json_decode($output);
		// var_dump($output);die();
		// 

		if($cek_output->status == true){
			echo $output;
		}else{
			$error = array(
				'data' => [],
				'error' => $cek_output->message
			);
			
			echo json_encode($error);
		}
	}
	public function dataKlinik(){
		$authorization = 'Authorization: '.$_SESSION["ses_token"];
		$url = $this->urlws."Global/listKlinik";
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_POST, 1);
		// curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array($authorization ));
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		$output = curl_exec($ch); 
		curl_close($ch);    

		$cek_output = json_decode($output);
		// var_dump($output);die();
		// 

		if($cek_output->status == true){
			echo $output;
		}else{
			$error = array(
				'data' => [],
				'error' => $cek_output->message
			);
			
			echo json_encode($error);
		}
		// $query=$this->user_model->get_dataklinik();

		// echo json_encode($query->result());
	}
	public function dataPerawatan(){
		$authorization = 'Authorization: '.$_SESSION["ses_token"];
		$url = $this->urlws."Global/listPerawatan";
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_POST, 1);
		// curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array($authorization ));
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		$output = curl_exec($ch); 
		curl_close($ch);    

		$cek_output = json_decode($output);
		// var_dump($output);die();
		// 

		if($cek_output->status == true){
			echo $output;
		}else{
			$error = array(
				'data' => [],
				'error' => $cek_output->message
			);
			
			echo json_encode($error);
		}
		// $query=$this->user_model->get_dataperawatan();

		// echo json_encode($query->result());
	}
	public function actionUser(){
		$id_user = $this->input->post('id_user');	
		$pass = base64_encode($this->input->post('pass'));	
		$nipp = $this->input->post('nipp');	
		$email = $this->input->post('email');	
		$sip = $this->input->post('sip');	
		$list_dokter = $this->input->post('list_dokter');	
		$list_klinik = $this->input->post('list_klinik');	
		$list_perawatan = $this->input->post('list_perawatan');
		$create_user = $_SESSION["ses_userid"];
		if($id_user != null){
			//update		
			$query=$this->user_model->updateUser($id_user,$nipp ,$email ,$sip ,$list_dokter ,$list_klinik ,$list_perawatan,$create_user);
		}else{
			//insert
			$query=$this->user_model->insertUser($id_user,$pass,$nipp ,$email ,$sip ,$list_dokter ,$list_klinik ,$list_perawatan,$create_user);
		}
		// var_dump($query->row());
		echo json_encode($query->result());
	}
	public function deleteUser(){
		$id_user = $this->input->post('id_user');
		$create_user = $_SESSION["ses_userid"];
		// var_dump($id_user,$create_user);die();
		$query=$this->user_model->deleteUser($id_user,$create_user);
	}
}
