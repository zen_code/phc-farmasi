
<html lang="en">

<head>
    <meta charset="utf-8"/>
    <title>Login</title>
    <meta name="description" content="Latest updates and statistic charts">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">

    <link href="<?php echo base_url() ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/geni/geni.css" rel="stylesheet">
    <!-- <link href="{{ asset('assets/geni/geni.css') }}" rel="stylesheet" type="text/css"/> -->

    <style>
        html, body {
            font-family: 'Montserrat', "Segoe UI", "Helvetica Neue", Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
            height:100%;
        }

        body{
            display: flex;
            height: 100%;
        }
    </style>
</head>

<body>

    <div class="login grid grid-hor grid-root page">
        <div class="grid-item grid-item-fluid grid grid-ver-desktop grid-desktop grid-tablet-and-mobile grid-hor-tablet-and-mobile login login-1 login-singin login-signin">
            <div class="grid-item grid-item-order-tablet-and-mobile-2 login-aside">
                <div class="stack stack-hor stack-desktop">
                    <div class="stack-item stack-item-fluid">
                        <div class="login-wrapper">
                            <div class="login-logo">
                                <a class="text-center db"><img class="login-img" src="<?php echo base_url().'image/ptphc.png'; ?>"/><br/>
                                </a>
                            </div>
                            <?php if($msg != null){?>
                            <div class="col-md-12" style="padding: 10px;background: red;    color: white;">
                                <?php echo $msg ;?>
                            </div>
                            <?php } ?>
                            <div id="login" class="login-signin">
                                <div id="failedlogin" class="col-xs-12 col-sm-12 col-md-12">
                                </div>
                                <form action="<?php echo base_url();?>index.php/login/getloginws" method="POST" class="form-horizontal form-material" id="loginform">
                                    
                                    <div class="form-group">
                                        <input class="form-control" type="text" required placeholder="Username"  name="userid" id="userid" autofocus >
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" type="password" required placeholder="Password" name="passid" id='passid'>
                                    </div>
                                    <div class="form-group text-center m-t-20">
                                        <div class="col-xs-12">
                                            <button class="btn btn-md btn-block waves-effect waves-dark centra-bg-orange text-white" type="submit" style="    background: #2e3487;">Masuk</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="grid-item grid-item-fluid grid grid-center grid-hor grid-item-order-tablet-and-mobile-1	login-content" style="
                    background-image:url(<?php echo base_url().'image/bg-login.jpeg'; ?> );
                    background-size:     contain;
                    background-repeat:   no-repeat;
                    background-position: right bottom;
                    position:relative;">
                <div style="position:absolute;bottom:50px;right:50px">
                    <span class="text-white" style="font-size: 12px;font-weight: 600">&copy; <span id="year"></span> PT Pelindo Husada Citra</span>
                </div>
            </div>
        </div>
</div>
<!-- <link href="<?php echo base_url()?>assets/assets_metronic/css/demo9/style.bundle.css" rel="stylesheet" type="text/css" /> -->
<script src="<?php echo base_url()?>assets/assets_metronic/vendors/general/jquery/dist/jquery.js" type="text/javascript"></script>
<script>
    jQuery(document).ready(function() {
        $.ajax({
            type: "GET",
            url: "login/getdepo", 
            dataType: 'json',
            success: function (data) {
                var a = '';
                jQuery.each(data, function(i, val) {
                    a += "<option value='"+val.KODE_MUTASI  +"'>"+ val.LAYANAN +"</option>";
                });
                console.log(a);
            },
            error: function (xhr,status,error) {

            }
        });
    });
</script>

</body>
</html>