<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	public $urlws = null;
	public function __construct() {
        parent::__construct();
		$this->load->model('login_model');
		$this->load->library('session');
		$this->urlws =  $this->globals->ws_phc();
		$this->urlwscentra = $this->globals->ws_centra();
		$this->app_id = 1;
    }
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	private function _render($view,$data = array())
	{
		// var_dump($data);die();
		// $this->load->view('header_login',$data);
	    //$this->load->view('body_header');
	    //$this->load->view('sidebar');
	    $this->load->view($view,$data);
	    //$this->load->view('footer');
	}
	public function index($msg = null)
	{
		$data['msg'] = $msg;
		// var_dump($data['msg'] == null);die();
		$this->load->library('session');
		$this->session->unset_userdata('if_ses_kodepriv');
		$this->session->unset_userdata('if_ses_keterangan');
		$this->session->unset_userdata('if_ses_sidebar');
		$this->session->unset_userdata('if_ses_token_phc');
		$this->session->unset_userdata('if_ses_token_centra');
		$this->session->unset_userdata('if_ses_menu');
		$this->session->unset_userdata('if_ses_header');
		$this->session->unset_userdata('if_ses_depo');
		$this->session->unset_userdata('if_ses_depo_code');
		$this->session->unset_userdata('if_ses_depo_id');
		// $this->load->view('index');
		$this->_render('index', $data);
		// redirect(base_url().'login/datalogin');
	}
	public function getlogin(){
		// var_dump('asdasdasd');die();
		$uri =& load_class('URI', 'core');
		$this->load->library('session');
		$this->load->model('login_model');
		$user   = $this->input->post('userid');
		$user = $this->clean($user);
		$pass   = $this->input->post('passid');
		$pass = $this->clean($pass);
		$this->simpanlog((substr($user,0,49)),(substr($pass,0,49)));
		// var_dump($user, $pass);die();

		if($this->login_model->getdatalogin($user,$pass,base_url()) )
		{
			$query = $this->login_model->getdatalogin($user,$pass,base_url());
			$keterangan=$query->row()->KETERANGAN;
			if ($keterangan!="")
			{
				$datasesi = array(
						'ses_kodepriv'  => $query->row()->KD_PRIV,
						'ses_keterangan' => $query->row()->KETERANGAN,
						'ses_sidebar' => $query->row()->SIDEBAR
				);
				$this->session->set_userdata($datasesi);
				// var_dump($datasesi);die();
				redirect(base_url().'index.php/beranda/data');
			}
			else
			{

				$this->session->set_flashdata('hasil',$query->row()->ALERT);
				redirect(base_url().'index.php/login',$data);


			}
		}
	}
	function clean($string) 
	{
	   $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
	   $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.

	   return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
	}
	public function simpanlog($user,$pass)
	{
		$ip=$this->get_client_ip();
		$this->login_model->simpanlog($user,$pass,$ip);
	}
	function get_client_ip() 
	{
	$ipaddress = '';
		if (isset($_SERVER['HTTP_CLIENT_IP']))
			$ipaddress = $_SERVER['HTTP_CLIENT_IP'];
		else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
			$ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
		else if(isset($_SERVER['HTTP_X_FORWARDED']))
			$ipaddress = $_SERVER['HTTP_X_FORWARDED'];
		else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
			$ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
		else if(isset($_SERVER['HTTP_FORWARDED']))
			$ipaddress = $_SERVER['HTTP_FORWARDED'];
		else if(isset($_SERVER['REMOTE_ADDR']))
			$ipaddress = $_SERVER['REMOTE_ADDR'];
		else
			$ipaddress = 'UNKNOWN';
	return $ipaddress;
	}
	public function logout(){
		// $this->load->library('session');
		// $this->session->unset_userdata('ses_kodepriv');
		// $this->session->unset_userdata('ses_keterangan');
		// $this->session->unset_userdata('ses_sidebar');
		$this->index('');
		// redirect(base_url().'index.php/login');
	}
	public function getloginws(){ 
		$this->load->library('session');
		$data = array(
            "username"         => $this->input->post('userid'),
            "password"     => $this->input->post('passid'),
            "app_id"     => 1,
		);

		
		$url = $this->globals->ws_phc()."Global/login";
		// $url = "http://centra-dev.pelindo.co.id/public/api/login-mobile";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		$output = curl_exec($ch); 
		curl_close($ch);    
		$data = json_decode($output);
		// var_dump($data);die();
		if($data->status == 'S'){
			$header = [];
			$list_menu_id = [];
			foreach($data->result->menu as $item){
				array_push($list_menu_id,$item->ID_MENU);
				if($item->PARENT_MENU == null){
					$header[$item->ID_MENU]['id'] = $item->ID_MENU;
					$header[$item->ID_MENU]['name'] = $item->NAMA_MODUL;
					$header[$item->ID_MENU]['parent'] = $item->PARENT_MENU;
					$header[$item->ID_MENU]['icon'] = $item->ICONS;
					$header[$item->ID_MENU]['url'] = $item->URL;
				}else{
					$header[$item->PARENT_MENU]['child'][$item->ID_MENU]['id'] = $item->ID_MENU;
					$header[$item->PARENT_MENU]['child'][$item->ID_MENU]['name'] = $item->NAMA_MODUL;
					$header[$item->PARENT_MENU]['child'][$item->ID_MENU]['parent'] = $item->PARENT_MENU;
					$header[$item->PARENT_MENU]['child'][$item->ID_MENU]['icon'] = $item->ICONS;
					$header[$item->PARENT_MENU]['child'][$item->ID_MENU]['url'] = $item->URL;
				}
			}
			// var_dump($list_menu_id);
			// var_dump(!in_array(2,$list_menu_id));
			// die();


			$datasesi = array(
				'if_ses_userid'  => $data->result->userid,
				'if_ses_username' => $data->result->user_name,
				'if_ses_sidebar' => $data->result->userid,
				'if_ses_token_phc' => $data->result->token_phc,
				'if_ses_token_centra' => $data->result->token_centra,
				// 'ses_menu' => $data->result->menu,
				'if_ses_menu' => $list_menu_id,
				'if_ses_header' => $header,
				'if_ses_depo' => null,
				'if_ses_depo_code' => null,
				'if_ses_depo_id' => null,
			);
			// var_dump($data->result->token_centra);
			// die();
			$this->session->set_userdata($datasesi);
			redirect(base_url().'depo');

		}else{
			$this->index($data->result->message);
		}
		
	}
	public function getdepo(){
		// var_dump('lazi');die();
		$url = $this->urlws."Farmasi/Role/getDepoFarmasi";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		$output = curl_exec($ch);
		curl_close($ch);
		$data = json_decode($output);

		echo json_encode($data);
	}
}
