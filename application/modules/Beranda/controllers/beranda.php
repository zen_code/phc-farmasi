<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class beranda extends CI_Controller {
	public function __construct() {
        parent::__construct();
		$this->load->model('beranda_model');
		$this->load->library('session');

    }
	private function _render($view,$data = array())
	{
		$this->load->view('header',$data);
	    // $this->load->view('body_header');
	    // $this->load->view('sidebar');
	    $this->load->view($view,$data);
	    $this->load->view('footer');
	}

	public function index()
	{
		//load library
		$uri =& load_class('URI', 'core');
	  	redirect(base_url().'index.php/beranda/data',$data);
	}

	public function data()
	{
		// var_dump($_SESSION["ses_depo"]);die();
		if($_SESSION["if_ses_depo"] == null){
			redirect(base_url().'depo');
		}else{
			$data['parent_active'] = 2 ;
			$data['child_active'] = null ;
			$this->_render('beranda_view', $data);
		}
		
	}
	public function tabelVerifikasi()
	{
		// var_dump('asd');die();
		$query=$this->beranda_model->get_datajadwal();
		$data 			= array();
		foreach($query->result() as $item){
			$btn 		='<button onclick="proses('.$item->noid.',1)" name="btnproses" id="btnproses" type="button" class="btn btn-success btn-sm "> <i class="fa fa-check"></i> </button>
						<button onclick="proses('.$item->noid.',2)" name="btnbatal" id="btnbatal" type="button" class="btn btn-danger btn-sm"> <i class="fa fa-times"></i> </button>';
			$data[] 	= array(
				"tglRencana" 		=> $item->tgljadwal,
				"jamOp" 			=> $item->jam_mulai . ' - ' .$item->jam_akhir ,
				"noRm"				=> '',
				"namaPasien"		=> $item->nama_pasien,
				"tglLahirPasien"	=> $item->TGL_LHR,
				"AlamatPasien"		=> $item->ALAMAT,
				"kamarOk"			=> $item->ruang,
				"namaDokter"		=> $item->nmDok,
				"tindakan"			=> $item->tindakan,
				"ket"				=> $item->ket,
				"btn"				=> $btn
				
			);
		}

		$obj = array("data"=> $data);

		echo json_encode($obj);
	}
}
