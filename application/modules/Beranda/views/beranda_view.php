<?php
defined('BASEPATH') or exit('No direct script access allowed');

?>
<style>
    .amcharts-export-menu{
        display:none
    }
</style>
<link href="//www.amcharts.com/lib/3/plugins/export/export.css" rel="stylesheet" type="text/css" />

<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
        <input type="hidden" value="<?php echo base_url(); ?>" id="url">
        <!-- begin:: Subheader -->
        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        Dashboard </h3>
                </div>
                <!-- kt-subheader__toolbar -->

            </div>
        </div>

        <!-- end:: Subheader -->

        <!-- begin:: Content -->
        <div class="kt-container  kt-grid__item kt-grid__item--fluid">

            <!--Begin::Dashboard 1-->

            <!--Begin::Row-->
            <div class="row">

                <div class="col-md-6">
                    <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile ">
                        <div class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Total Resep Per Hari
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body kt-portlet__body--fit">

                            <!--begin: Datatable -->
                            <div class="kt-portlet__body">
                                <div id="kt_amcharts_12" style="height: 500px;"></div>
                            </div>

                            <!--end: Datatable -->
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile ">
                        <div class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Top 5 Debitur
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body kt-portlet__body--fit">

                            <!--begin: Datatable -->
                            <div class="kt-portlet__body">
                                <div id="chart_2" style="height: 500px;"></div>
                            </div>

                            <!--end: Datatable -->
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile ">
                        <div class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Total Reser per Depo <span></span>
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body kt-portlet__body--fit">

                            <!--begin: Datatable -->
                            <div class="kt-portlet__body">
                                <div id="kt_amcharts_12" style="height: 500px;"></div>
                            </div>

                            <!--end: Datatable -->
                        </div>
                    </div>
                </div>
            </div>

            <!--End::Row-->



            <!--End::Dashboard 1-->
        </div>

        <!-- end:: Content -->
    </div>
</div>
<!-- javascript this page -->
<script src="<?php echo base_url() ?>assets/assets_metronic/vendors/custom/amcharts/core.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/assets_metronic/vendors/custom/amcharts/charts.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/assets_metronic/vendors/custom/amcharts/animated.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/assets_metronic/vendors/custom/amcharts/theme_material.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/assets_metronic/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>application/modules/Beranda/views/beranda.js" type="text/javascript"></script>
<!-- <script src="<?php echo base_url() ?>assets/assets_metronic/js/demo9/pages/dashboard.js" type="text/javascript"></script> -->