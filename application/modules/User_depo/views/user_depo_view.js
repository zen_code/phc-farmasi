"use strict";

// Class definition

var RolesView = function() {
    return {
        // Init demos
        init: function() {
            getDataRole();


            // var loading = new KTDialog({'type': 'loader', 'placement': 'top center', 'message': 'Loading ...'});
            // loading.show();
            // setTimeout(function() {
            //     loading.hide();
            // }, 3000);
        },
    };
}();
function getDataRole(){
    $('#data_role').DataTable( {
        "destroy": true,
        "processing" : true,
        "type": "POST",
        "ajax": 'roles/dataRole',
        "columns"       : [
            {"data" : "ID"},
            {"data" : "NAMA_PROFIL"},
            {"data" : "INFO"},
            {"data" : "ACTIVE"},
            {"data" : "btn"},
        ],
        "columnDefs": [
            {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            },
        ],
        "scrollX": true,
        "createdRow"    : function ( row, data, index ) {
            // $('td', row).eq(0).addClass('text-center');
           // $('td', row).eq(2).addClass('text-right');
        }, 
        "drawCallback": function( settings ) {
            $('[data-toggle="kt-tooltip"]').tooltip();
        }
    });
}
function update(id, type) {
    swal.fire('success', "id="+ id + " type=" + type, 'success');
}

$('#add_profile').click(function(){
    $('.tag_type').text('Add');
    $('#modal-review').modal({backdrop: 'static', keyboard: false})  ;
    $('#modal-review').modal('show');
});
$('#action').click(function(){
    $('#modal-review').modal('hide');
    KTApp.blockPage({
        overlayColor: '#000000',
        type: 'v2',
        state: 'primary',
        message: 'Processing...'
    });

    var name = $('#name').val();
    var info = $('#info').val();
    var id_profile = $('#id_profile').val();
    $.ajax({
        type: "POST",
        url: "roles/actionRole", 
        data : {
            name:name,
            info:info,
            id_profile:id_profile
        },
        dataType: 'json',
        async : false,
        success: function (data) {
            console.log(data);
            KTApp.unblockPage();
            if(id_profile !=""){
                swal.fire('Success', 'Berhasil Update', 'success')
            }else{
                swal.fire('Success', 'Berhasil Input', 'success')
            }
            getDataRole();
        },
        error: function (xhr,status,error) {
            console.log(xhr);
        }
    });
    
});
$('#data_role').on( 'click', 'tr #btnproses', function () {
    var baris = $(this).parents('tr')[0];
    var table = $('#data_role').DataTable();
    var datas = table.row(baris).data();
    
    $('.tag_type').text('Edit');
    $('#id_profile').val(datas['ID']);
    $('#name').val(datas['NAMA_PROFIL']);
    $('#info').val(datas['INFO']);
    $('#modal-review').modal('show');
});
$('#data_role').on( 'click', 'tr #btnbatal', function () {
    var baris = $(this).parents('tr')[0];
    var table = $('#data_role').DataTable();
    var datas = table.row(baris).data();

    swal.fire({
        title: '<span style="font-size: 19px;font-weight: 700;">Menghapus User</span>' ,
        text:'Apakah anda yakin menghapus profil ini?',
        type:'question',
        showCancelButton: true,
        confirmButtonText: 'Ya',
        cancelButtonText: 'Tidak',  
    }).then(function(result) {
        if (result.value) {
            deleteRole(datas['ID']);
        }
    })
})
$('#data_role').on( 'click', 'tr #addPriv', function () {
    var baris = $(this).parents('tr')[0];
    var table = $('#data_role').DataTable();
    var datas = table.row(baris).data();
    console.log(datas['ID']);

    $.ajax({
        type: "POST",
        url: "roles/addPriv", 
        data : {
            id:datas['ID']
        },
        dataType: 'json',
        success: function (data) {
            // KTApp.unblockPage();
            // getDataRole();
        },
        error: function (xhr,status,error) {
        }
    });
})
function deleteRole(id_profil){
    KTApp.blockPage({
        overlayColor: '#000000',
        type: 'v2',
        state: 'primary',
        message: 'Processing...'
    });
    $.ajax({
        type: "POST",
        url: "roles/deleteRole", 
        data : {
            id_profil:id_profil
        },
        dataType: 'json',
        success: function (data) {
            KTApp.unblockPage();
            getDataRole();
        },
        error: function (xhr,status,error) {
        }
    });
}
// Class initialization on page load
jQuery(document).ready(function() {
    RolesView.init();
});