<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class user_depo extends CI_Controller {
	
	public $urlws = null;
	public $app_id = 1;
	public function __construct() {
        parent::__construct();
		$this->load->model('user_depo_model');
		$this->load->library('session');
		$this->urlws = "http://localhost/phc-ws/api/";
		$this->urlwscentra = "http://centra-dev.pelindo.co.id/public/api/";
		$this->app_id = 1;
    }
	private function _render($view,$data = array())
	{
		$this->load->view('header',$data);
	    // $this->load->view('body_header');
	    // $this->load->view('sidebar');
	    $this->load->view($view,$data);
	    $this->load->view('footer');
	}

	public function index()
	{
		//load library
		$uri =& load_class('URI', 'core');
	  	redirect(base_url().'index.php/user_depo/data',$data);
	}

	public function data()
	{
		if(in_array(3,$_SESSION['ses_menu'])){
			$data['parent_active'] = 3 ;
			$data['child_active'] = 5 ;
			$this->_render('user_depo_view',$data);
		}else{
			redirect(base_url().'beranda/data');
		}
		
	}
	public function addPriv()
	{
		$data['parent_active'] = 3 ;
		$data['child_active'] = 5 ;
		$id = base64_decode($this->input->get('id'));
		$data['priv'] = $this->user_depo_model->getPrivelege($id);
		$data['allmenu'] = $this->user_depo_model->allmenu($this->app_id);
		$this->_render('addpriv_view',$data);
	}
	public function dataRole()
	{
		$authorization = 'Authorization:	 '.$_SESSION["ses_token_phc"];
		$url = $this->urlws."Farmasi/Role/listData";
		$data = array(
			"app_id" => $this->app_id
		);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array($authorization ));
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		$output = curl_exec($ch); 
		curl_close($ch);    

		// var_dump($output);die();	
		$cek_output = json_decode($output);
		
		if($cek_output->status == true){
			$data 			= array();
			foreach($cek_output->data as $item){
				$btn 		='<button name="btnproses" id="btnproses" type="button" class="btn btn-success btn-sm" data-toggle="kt-tooltip" data-placement="top" title="Update"> <i class="fa fa-edit"></i> </button>
                             <button name="addDetail" id="addPriv" type="button" class="btn btn-info btn-sm "><a href="'.base_url().'user_depo/addPriv?id='.base64_encode($item->ID_PROFIL).'" style="color:white" data-toggle="kt-tooltip" data-placement="top" title="Add Privelege"> <i class="fa fa-user-plus"></i> </a></button>
                            <button name="btnbatal" id="btnbatal" type="button" class="btn btn-danger btn-sm" data-toggle="kt-tooltip" data-placement="top" title="Delete"> <i class="fa fa-times"></i> </button>';
                
				$data[] 	= array(
					"ID" 	=> $item->ID_PROFIL,
					"NAMA_PROFIL" 	=> $item->NAMA_PROFIL,
					"INFO" 			=> $item->INFO ,
					"ACTIVE"		=> $item->ACTIVE,
					"btn"			=> $btn
				);
			}
			$obj = array("data"=> $data);
			echo json_encode($obj);
		}else{
			$error = array(
				'data' => [],
				'error' => $cek_output->message
			);
			
			echo json_encode($error);
		}
		
	}
	public function actionRole(){
		$data = array(
            "name"     => $this->input->post('name'),
            "info"     => $this->input->post('info'),
            "id_profile"     => $this->input->post('id_profile'),
            "user_id"     =>  $_SESSION["ses_userid"],
            "app_id"     =>  $this->app_id,
		);

		$authorization = 'Authorization: '.$_SESSION["ses_token_phc"];
		$url = $this->urlws."Farmasi/Role/action";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array($authorization ));
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		$output = curl_exec($ch); 
		curl_close($ch);    
		// var_dump($output);die();
		$cek_output = json_decode($output);
		if($cek_output->status == true){
			echo json_encode($cek_output);
		}else{
			$error = array(
				'data' => [],
				'error' => $cek_output->message
			);
			
		}
	}
	public function deleteRole(){
		$data = array(
            "id_profile"     => $this->input->post('id_profile'),
            "user_id"     =>  $_SESSION["ses_userid"]
		);

		$authorization = 'Authorization: '.$_SESSION["ses_token_phc"];
		$url = $this->urlws."Farmasi/Role/delete";
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array($authorization ));
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		$output = curl_exec($ch); 
		curl_close($ch);    
		// var_dump($output);die();
		$cek_output = json_decode($output);
		if($cek_output->status == true){
			echo json_encode($cek_output);
		}else{
			$error = array(
				'data' => [],
				'error' => $cek_output->message
			);
			
		}
	}
	public function getAllUser(){
		$authorization = 'Authorization: Bearer'.$_SESSION["ses_token_centra"];
		$url = $this->urlwscentra."dp/base/setting/user/list-user";
		$ch = curl_init();
		// curl_setopt($ch, CURLOPT_GET, 1);
		// curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array($authorization ));
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		$output = curl_exec($ch); 
		curl_close($ch);    
		$cek_output = json_decode($output);
		// var_dump(json_encode($cek_output));die();
		echo json_encode($cek_output);
	}
	public function getUser(){
		$id_profile = $this->input->post('ID_PROFIL');
		$query = $this->user_depo_model->getUser($id_profile);
		// var_dump($query);die();
		echo json_encode($query);
	}
	public function getallmenu(){
		$query = $this->user_depo_model->allmenu($this->app_id);
		echo json_encode($query);
		// return $query;
	}
	public function getprofilemenu(){
		$id_profile = $this->input->post('ID_PROFIL');
		$query = $this->user_depo_model->profilemenu($id_profile);
		
		echo json_encode($query);
		// return $query;
	}
	public function addPrivAction()
	{
		
		// $id_profile = $this->input->post('id_profile');
		$data = array(
            "id_profile"     => $this->input->post('id_profile'),
            "user"     =>$this->input->post('user'),		
            "menu"     => $this->input->post('menu'),
            "user_id"     =>  $_SESSION["ses_userid"]
		);

		$data = http_build_query($data);
		// var_dump($data);die();
		$authorization = 'Authorization: '.$_SESSION["ses_token_phc"];
		$url = $this->urlws."Farmasi/Role/addPrivAction";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array($authorization ));
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		$output = curl_exec($ch); 
		curl_close($ch);    
		// var_dump($output);die();	
		$cek_output = json_decode($output);
		echo json_encode($cek_output);
		
	}
}
