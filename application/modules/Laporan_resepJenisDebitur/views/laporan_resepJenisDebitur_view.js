"use strict";

// Class definition

var LaporanResep = function () {
    function getdebt() {
        $.ajax({
          type: "GET",
          url: "laporan_resepJenisDebitur/get_debt",
          data: {},
          dataType: "json",
    
          success: function(data) {
            // console.log(data);
            var datakode = data;
            jQuery.each(datakode, function(index, val) {
              var a = "";
              a =
                "<option value='" +
                val.KOTA +
                "' data-kode='" +
                val.NMDEBT +
                "'><b>" +
                val.KDDEBT +
                " - " +
                val.NMDEBT +
                "</b>" +
                "</option>";
              $("#MNAMA").append(a);
            });
          },
          error: function(xhr, status, error) {
            console.log(xhr, status, error);
          }
        });
        $("#MNAMA").select2({
          placeholder: "Pilih Debitur",
          width: "100%"
        });
        
    }

    function getlayanan() {
        $.ajax({
          type: "GET",
          url: "laporan_resepJenisDebitur/get_layanan",
          data: {},
          dataType: "json",
    
          success: function(data) {
            // console.log(data);
            var datalayanan = data;
            jQuery.each(datalayanan, function(index, val) {
              var a = "";
              a =
                "<option value='" +
                val.IDLAYANAN +
                "' data-layanan='" +
                val.LAYANAN +
                "'><b>" +
                val.IDLAYANAN +
                " - " +
                val.LAYANAN +
                "</b>" +
                "</option>";
              $("#LAYANAN").append(a);
            });
          },
          error: function(xhr, status, error) {
            console.log(xhr, status, error);
          }
        });
        $("#LAYANAN").select2({
          placeholder: "Pilih Layanan",
          width: "100%"
        });
        
    }

    return {
        // Init demos
        init: function () {
            getdebt();
            getlayanan();
            input_number();
            $('.datepicker').datepicker({
                autoclose: true,
                todayHighlight: true,
                format: 'dd/mm/yyyy',
                changeMonth: true,
                changeYear: true,
            });
            

            

            
        },
    };
}();

$(document).ready(function () {
  $("#date_from").change(function () {
    $('input[name=date_fr]').val($(this).val());
    //  alert("tgl : " + tgl);
  });
  $("#date_to").change(function () {
      $('input[name=date_t]').val($(this).val());
  });
  $("#MNAMA").change(function () {
      $('input[name=MNAM]').val($(this).val());
  });
  $("#LAYANAN").change(function () {
    $('input[name=LAYAN]').val($(this).val());
  });
});

function filterLaporan(){
    // alert('yey');
    var tglmulai = $('#date_from').val();
    var tglselesai = $('#date_to').val();

    var valdebt = $('#MNAMA').val();
    var vallayanan = $('#LAYANAN').val();
    if(tglmulai=='' || tglselesai=='' || valdebt=='' || vallayanan==''){
      swal.fire('Peringatan','Mohon Lengkapi Filter Laporan','warning');
    }else{
      tabelLaporan();
    }
    
}

function tabelLaporan(){
    loading();
    var tglmulai = $('#date_from').val();
    var tglselesai = $('#date_to').val();

    var valdebt = $('#MNAMA').val();
    var vallayanan = $('#LAYANAN').val();

    // console.log(valdebt);

    $('#tabelReport').DataTable({
        destroy: true,
        processing: true,
        type: "GET",
        ajax:
          "laporan_resepJenisDebitur/laporanOld/?valdebt="
          +valdebt+"&tglmulai="+tglmulai+"&tglselesai="+tglselesai+"&vallayanan="+vallayanan,
        columns:[
          {
            data: "no"
          },
          {
            data: "tanggal"
          },
          {
            data: "resep"
          },
          {
            data: "kitir"
          },
          {
            data: "medik"
          },
          {
            data: "idx"
          },
          {
            data: "deb"
          },
          {
            data: "klinik"
          },
          {
            data: "nama"
          },
          {
            data: "keluarga"
          },
          {
            data: "total"
          }
        ],
        scrollX: true,
        createdRow: function(row, data, index){
          $("td", row)
            .eq(0)
            .addClass("text-center");
        },
        drawCallback: function(){
          KTApp.unblockPage();
        }
    });
}


$('#search_layanan').click(function(){
    $('#modal-layanan').modal('show')
});
$('#search_debitur').click(function(){
    $('#modal-layanan').modal('show')
});

function loading() {
    KTApp.blockPage({
        overlayColor: '#000000',
        type: 'v2',
        state: 'primary',
        message: 'Processing...'
    });
}

function input_number() {
    $(".numbers").on("keypress keyup blur", function (event) {
        $(this).val($(this).val().replace(/[^\d].+/, ""));
        if ((event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });
}

// Class initialization on page load
jQuery(document).ready(function () {
    LaporanResep.init();
});