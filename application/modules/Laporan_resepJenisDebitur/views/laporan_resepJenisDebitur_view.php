<?php
defined('BASEPATH') or exit('No direct script access allowed');

?>

<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
        <input type="hidden" value="<?php echo base_url(); ?>" id="url">
        <!-- begin:: Subheader -->
        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                    Laporan Resep Tunai/Kredit </h3>
                </div>
                <!-- kt-subheader__toolbar -->

            </div>
        </div>

        <!-- end:: Subheader -->

        <!-- begin:: Content -->
        <div class="kt-container  kt-grid__item kt-grid__item--fluid">
            <!--Begin::Row-->
            <div class="row">

                <div class="col-xl-12 order-lg-2 order-xl-1">
                    <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile ">
                        <div class="kt-portlet__body kt-portlet__body--fit">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="kt-portlet">
                                        <div class="kt-portlet__head">
                                            <div class="kt-portlet__head-label">
                                                <h3 class="kt-portlet__head-title">
                                                    Pilih Tanggal dan Debitur
                                                </h3>
                                            </div>
                                        </div>
                                        <!-- FormTanggal&NoMedik -->
                                        <div class="kt-portlet__body">
                                            <div class="row">
                                                <div class="form-group col-md-12">
                                                    <div class="row">
                                                        <div class="form-group col-md-2">
                                                            <label>Start Date</label>
                                                            <div class="input-group date">
                                                                <input type="text" autocomplete="off"
                                                                class="form-control kt-input datepicker"
                                                                name="date_from" placeholder="From" id="date_from" />
                                                                <div class="input-group-append">
                                                                    <span class="input-group-text"><i
                                                                        class="la la-calendar-o glyphicon-th"></i></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-2">
                                                            <label>End Date</label>
                                                            <div class="input-group date">
                                                                <input type="text" autocomplete="off"
                                                                class="form-control kt-input datepicker"
                                                                name="date_to" placeholder="To" id="date_to" />
                                                                <div class="input-group-append">
                                                                    <span class="input-group-text"><i
                                                                        class="la la-calendar-o glyphicon-th"></i></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-md-3">
                                                                <label>Debitur</label>
                                                                <!-- <input type="text" class="form-control " id="jumlah_anmaag"
                                                                    placeholder="Jumlah"> -->
                                                                <select class="form-control" id="MNAMA" name="MNAMA">
                                                                    <option></option>
                                                                </select>
                                                            </div>
                                                            <div class="form-group col-md-3">
                                                                <label>Layanan</label>
                                                                <!-- <input type="text" class="form-control " id="jumlah_anmaag"
                                                                    placeholder="Jumlah"> -->
                                                                <select class="form-control" id="LAYANAN" name="LAYANAN">
                                                                    <option></option>
                                                                </select>
                                                            </div>
                                                            <div class="input-group input-group-lg col-md-6">
                                                                <div style="text-align: center;margin-bottom:10px">
                                                                    <button class="btn btn-success2" onclick="filterLaporan()" id="btnFilter"
                                                                        type="button">
                                                                    TAMPILKAN</button>
                                                                </div>
                                                            </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xl-12 order-lg-2 order-xl-1">
                    <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile ">
                        <div class="kt-portlet__body kt-portlet__body--fit">
                                <!-- <div class="row"> -->
                            <div class="col-md-12">
                                <div class="kt-portlet">
                                            <div class="kt-portlet__head">
                                                <div class="kt-portlet__head-label">
                                                    <h3 class="kt-portlet__head-title">
                                                        Main Report
                                                    </h3>
                                                </div>
                                            </div>
                                            <!-- Tabel -->
                                            <br>

                                            <div class="row">
                                                <div class="col-md-1">
                                                    <form method="POST" target="_blank" action="<?php echo base_url(); ?>laporan_resepJenisDebitur/export_excel">
                                                        <input type="hidden" name="date_fr" />
                                                        <input type="hidden" name="date_t" />
                                                        <input type="hidden" name="MNAM" />
                                                        <input type="hidden" name="LAYAN" />
                                                        <input type="submit" id="export-excel" name="export" class="btn btn-outline-success" value="EXCEL" />
                                                    
                                                        <!-- <div style="text-align: center;margin-bottom:10px">
                                                            <button class="btn btn-success2" id="btnFilter"
                                                            type="button">
                                                        EXCEL</button>
                                                        </div> -->
                                                    </form>
                                                </div>
                                                <div class="col-md-1">
                                                    <form method="post" target="_blank" action="<?php echo base_url(); ?>laporan_resepJenisDebitur/cetak_pdf">
                                                        <input type="hidden" name="date_fr" />
                                                        <input type="hidden" name="date_t" />
                                                        <input type="hidden" name="MNAM" />
                                                        <input type="hidden" name="LAYAN" />
                                                        
                                                        <input type="submit" id="export-pdf" name="export" class="btn btn-outline-success" value="PDF" />
                                                            
                                                        <!-- <div style="text-align: center;margin-bottom:10px">
                                                            <button class="btn btn-success2" id="btnFilter"
                                                            type="button">
                                                        PDF</button>
                                                        </div> -->
                                                    </form>
                                                </div>
                                            </div>

                                            <br>

                                            <table class="table table-striped- table-bordered table-hover table-checkable"
                                            id="tabelReport">
                                            <thead>
                                                <tr style="background: #2860a8; color:white;text-align:center">
                                                    <th style="color:white">NO</th>
                                                    <th style="color:white" width="8%">TANGGAL</th>
                                                    <th style="color:white">RESEP</th>
                                                    <th style="color:white">KITIR</th>
                                                    <th style="color:white" width="8%">NO.MEDIK</th>
                                                    <th style="color:white">IDX</th>
                                                    <th style="color:white">DEB</th>
                                                    <th style="color:white">KLINIK</th>
                                                    <th style="color:white">NAMA</th>
                                                    <th style="color:white">KELUARGA</th>
                                                    <th style="color:white">TOTAL</th>
                                                </tr>
                                            </thead>
                                            <tbody style="text-align: center">
                                                <tbody>
                                                </table>
                                </div>
                            </div>
                                        <!-- </div> -->
                        </div>
                    </div>
                </div>
                            <!--End::Row-->
            </div>
                        <!-- end:: Content -->
        </div>
    </div>
</div>

                <div class="modal fade" tabindex="-1" role="dialog" id="modal-layanan">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content ">
                            <div class="modal-header">
                                <h5 class="modal-title"><span class="tag_type"></span> Layanan</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <table id="table-layanan" class="table table-striped- table-hover table-checkable">
                                    <thead>
                                        <tr style="background: #2860a8; color:white;text-align:center">
                                            <th style=" color:white;">NOREG</th>
                                            <th style=" color:white;">RM</th>
                                            <th style=" color:white;">TGL_MRS</th>
                                            <th style=" color:white;">NAMA</th>
                                            <th style=" color:white;">KAMAR</th>
                                            <th style=" color:white;">RUANGAN</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th style=" color:white;">NOREG</th>
                                            <th style=" color:white;">RM</th>
                                            <th style=" color:white;">TGL_MRS</th>
                                            <th style=" color:white;">NAMA</th>
                                            <th style=" color:white;">KAMAR</th>
                                            <th style=" color:white;">RUANGAN</th>
                                            <th></th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- javascript this page -->
                <script src="<?php echo base_url() ?>assets/assets_metronic/vendors/custom/datatables/datatables.bundle.js"
                    type="text/javascript"></script>
                    <script src="<?php echo base_url() ?>application/modules/Laporan_resepJenisDebitur/views/laporan_resepJenisDebitur_view.js"
                        type="text/javascript"></script>
                        <script
                        src="<?php echo base_url() ?>assets/assets_metronic/vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"
                        type="text/javascript"></script>
    <!-- <script src="<?php echo base_url() ?>assets/assets_metronic/js/demo9/pages/dashboard.js" type="text/javascript"></script> -->