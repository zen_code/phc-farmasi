<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class laporan_resepJenisDebitur_model extends CI_Model{
	public function __construct()
  {
      // Call the CI_Model constructor
      parent::__construct();
     $this->load->database();
  }
 
  /*public function getdatalogin($user,$pass)
  {
    $this->load->database();
    $sql = "exec inap_sp_login_bacapriv '$user','$pass'";
    return $this->db->query($sql);  
  }*/
  // public function get_datajadwal()
  // {
  //   $this->load->database();
  //   $sql = "select top 100 convert(varchar, a.tgljadwal, 103) tgljadwal, a.kddok, 
  //           b.nmDok,d.ruang, a.tindakan,a.ket,a.noid, e.NAMA nama_pasien,  
  //           convert(varchar, e.TGL_LHR, 103) TGL_LHR,e.ALAMAT
  //           ,convert(varchar, a.tglops, 8) jam_mulai, convert(varchar, a.tglpos, 8) jam_akhir
  //           from OK_JADWAL a
  //           join DR_MDOKTER b on a.kddok = b.kdDok 
  //           join OK_Mruang d on a.kdruang = d.kode
  //           join RI_MASTERPX c on a.noreg = c.noreg
  //           join Rirj_masterpx e on c.nopeserta = e.NO_PESERTA
  //           order by tgljadwal desc ";
  //   return $this->db->query($sql);    
  // }

  public function get_debitur_model()
  {
      $query  = $this->db->query("SELECT * FROM RIRJ_MDEBITUR");
      return $query;
  }

  public function get_layanan_model()
  {
      $query  = $this->db->query("SELECT * FROM IF_MLAYANAN");
      return $query;
  }

  public function laporanOld($valdebt, $tglmulai,$tglselesai,$vallayanan)
  {
    // $this->load->database();
    // var_dump($valdebt); die();
      ini_set('max_execution_time', 0);
      ini_set('memory_limit', '2048M');
      $mut = "";
      if($vallayanan == 0){
        $mut = "SELECT KODE_MUTASI FROM IF_MLAYANAN WHERE ACTIVE=1";
      }else{
        $mut = "SELECT KODE_MUTASI FROM IF_MLAYANAN WHERE ACTIVE=1 AND IDLAYANAN = ".$vallayanan;
      }
      // $sql = $this->db->query("exec if_sp_baru_laporan_pendapatan_jenis_debitur '".$valdebt."','".$tglmulai."','".$tglselesai."',".$vallayanan);
      $sql = $this->db->query(" SELECT 
        ROW_NUMBER() OVER (ORDER BY IF_HTRANS.TGL) AS 'NO', convert(varchar,TGL,103) 'TANGGAL', 
        CONVERT(VARCHAR,NOMOR) NOMOR,NOTA,NORM,ISNULL(IDXPX,'') IDX,KDDEB,KDKLIN, 
        NMPX,NMKK,SUM(IF_TRANS.JUMLAH * IF_TRANS.HJUAL + JASA) 'JUMLAH',0 TOTAL, 
        'KREDIT' AS 'KET',GETDATE() TGL_AWAL,GETDATE() TGL_AKHIR  
            FROM IF_HTRANS INNER JOIN IF_TRANS ON IF_TRANS.ID_TRANS = IF_HTRANS.ID_TRANS  
          INNER JOIN RIRJ_MDEBITUR D ON D.KDDEBT=IF_HTRANS.KDDEB
            WHERE D.KOTA='".$valdebt."' AND convert(varchar,TGLSHIFT,103) BETWEEN '".$tglmulai."' AND  '".$tglselesai."9' AND MUTASI = 'O' AND IF_HTRANS.KDOT <= '400'  
            AND IF_HTRANS.ACTIVE = 1 
            AND IF_HTRANS.KDMUT IN (".$mut.")
            GROUP BY TGL,NOMOR,NOTA,NORM,IDXPX,KDDEB,KDKLIN,NMPX,NMKK
            ORDER BY TGL,CONVERT(INT,NOMOR)");
      
      // var_dump("exec if_sp_baru_laporan_pendapatan_jenis_debitur '".$valdebt."','".$tglmulai."','".$tglselesai."',".$vallayanan);die();
      // var_dump($sql->result());die();
      return $sql;
  }
    
}
