<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class laporan_resepJenisDebitur extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('laporan_resepJenisDebitur_model');
		$this->load->library('session');
		$this->urlws = "http://localhost/phc-ws/api/";
		
	}
	private function _render($view,$data = array())
	{
		$this->load->view('header',$data);
	    // $this->load->view('body_header');
	    // $this->load->view('sidebar');
	    $this->load->view($view,$data);
	    $this->load->view('footer');
	}

	public function index()
	{
		//load library
		$uri =& load_class('URI', 'core');
	  	redirect(base_url().'index.php/laporan_resepJenisDebitur/data',$data);
	}

	public function data()
	{
		// var_dump($_SESSION["ses_depo"]);die();
		if($_SESSION["if_ses_depo"] == null){
			redirect(base_url().'depo');
		}else{
			if(in_array(7,$_SESSION['if_ses_menu'])){
				$data['parent_active'] = 7 ;
				$data['child_active'] = null ;
				$this->_render('laporan_resepJenisDebitur_view', $data);
			}else{
				redirect(base_url().'beranda/data');}
		}

		// if($_SESSION["if_ses_depo"] == null){
		// 	redirect(base_url().'depo');
		// }else{
		// 	$data['parent_active'] = 7 ;
		// 	$data['child_active'] = null ;
		// 	$this->_render('laporan_resepJenisDebitur_view', $data);
		// }
		
	}
	public function dataPasien()
	{
		$authorization = 'Authorization:'.$_SESSION["if_ses_token_phc"];
		$url = $this->urlws."Farmasi/FarmasiKlinis/dataPasien";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, array($authorization ));
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		$output = curl_exec($ch); 
		curl_close($ch);    
		$cek_output = json_decode($output);
		// var_dump($cek_output);die();
		if($cek_output->status == true){
			$data 			= array();
			foreach($cek_output->data as $item){
				$btn 		='<button name="btnproses" id="btnproses" onclick="detailPasien('.$item->NOREG.')" type="button" class="btn btn-success btn-sm" data-toggle="kt-tooltip" data-placement="top" title="Choose"> <i class="fa fa-check"></i> </button>';
                
				$data[] 	= array(
					"NOREG" 	=> $item->NOREG,
					"RM" => $item->RM,
					"TGL_MRS" 	=> $item->TGL_MRS ,
					"NAMA"		=> $item->NAMA,
					"KAMAR"		=> $item->KAMAR,
					"RUANGAN"		=> $item->RUANGAN,
					"btn"		=> $btn
				);
			}
			$obj = array("data"=> $data);
			echo json_encode($obj);
		}else{
			$error = array(
				'data' => [],
				'error' => $cek_output->message
			);
			
			echo json_encode($error);
		}
		
	}

	public function get_debt()
	{
		$query = $this->laporan_resepJenisDebitur_model->get_debitur_model();
		echo json_encode($query->result());
	}

	public function get_layanan()
	{
		$query = $this->laporan_resepJenisDebitur_model->get_layanan_model();
		echo json_encode($query->result());
	}

	public function laporanOld()
	{
		$tglmulai = $this->input->get('tglmulai'); 
		$tglselesai = $this->input->get('tglselesai'); 
		$valdebt = $this->input->get('valdebt');
		$vallayanan = $this->input->get('vallayanan'); 
		// $data 			= array();
		// $a = $valMNAMA;
		$data 			= array();
		
		$query = $this->laporan_resepJenisDebitur_model->laporanOld($valdebt, $tglmulai,$tglselesai,$vallayanan);
		// echo $query ? json_encode($query->result()) : 'kosong';
		
		$data 			= array();
		// die();
		$no = 1;
		foreach ($query->result() as $index => $item) {
			// $btn 		= '<button name="btnproses" id="btnproses" onclick="update(' . "'" . $item->KDIDX . "'" . ',' . "'" . $item->KDBHN . "'" . ',' . "'" . $item->FUNGSI . "'" . ')" type="button" class="btn btn-success btn-sm" data-toggle="kt-tooltip" data-placement="top" title="Update"> <i class="fa fa-edit"></i> </button>
			// 			<button name="btnbatal" id="btnbatal" onclick="remove(' . "'" . $item->KDIDX . "'" . ')" type="button" class="btn btn-danger btn-sm" data-toggle="kt-tooltip" data-placement="top" title="Delete"> <i class="fa fa-times"></i> </button>';
			 
			$data[] 	= array(
				"no"			=> $no,
				"tanggal"		=> $item->TANGGAL,
				"resep"			=> $item->NOMOR,
				"kitir"			=> $item->NOTA,
				"medik"			=> $item->NORM,
				"idx"			=> $item->IDX,
				"deb"			=> $item->KDDEB,
				"klinik"		=> $item->KDKLIN,
				"nama"			=> $item->NMPX,
				"keluarga"		=> $item->NMKK,
				"total"			=> $item->JUMLAH

				// "btn"		=> $btn

			);
			$no++;
		}

		$obj = array("data" => $data);

		echo json_encode($obj);
	}

	public function export_excel(){
		// print_r("yey"); exit();
		// $this->load->model("laporan_mutasiBarangMasuk_model");
		$this->load->library('excel');
		$object = new PHPExcel();

		$date_f = $this->input->post('date_fr');
		$date_t = $this->input->post('date_t');
		$kddebt = $this->input->post('MNAM');
		$kdlayanan = $this->input->post('LAYAN');

		// var_dump($date_f); die();

		$object->setActiveSheetIndex(0);
		$table_columns = array("No","Tanggal", "Resep", "Kitir", "No.Medik", "IDX", "Deb", "Klinik", "Nama", "Keluarga", "Total");

		$column = 0;

		foreach ($table_columns as $field) {
			$object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
			$column++;
		}

		$excel_row = 2;

		
		$laporan_data = $this->laporan_resepJenisDebitur_model->laporanOld($kddebt, $date_f,$date_t,$kdlayanan);
		$no = 1;
		foreach ($laporan_data->result() as $row) {
			$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $no);
			$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->TANGGAL);
			$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->NOMOR);
			$object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row->NOTA);
			$object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row->NORM);
			$object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $row->IDX);
			$object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $row->KDDEB);
			$object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, $row->KDKLIN);
			$object->getActiveSheet()->setCellValueByColumnAndRow(8, $excel_row, $row->NMPX);
			$object->getActiveSheet()->setCellValueByColumnAndRow(9, $excel_row, $row->NMKK);
			$object->getActiveSheet()->setCellValueByColumnAndRow(10, $excel_row, $row->JUMLAH);
			
			$excel_row++;
			$no++;
		}
			
		

		$object_writer = new PHPExcel_Writer_Excel2007($object);
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="Laporan Resep Jenis Debitur.xls"');
		$object_writer->save('php://output');

	}

	function cetak_pdf()
	{
		$date_f = $this->input->post('date_fr');
		$date_t = $this->input->post('date_t');
		$kddebt = $this->input->post('MNAM');
		$kdlayanan = $this->input->post('LAYAN');

		$laporan_data = $this->laporan_resepJenisDebitur_model->laporanOld($kddebt, $date_f,$date_t,$kdlayanan);
		$data['query'] = $laporan_data->result();

		$this->load->library('pdf2');
		// $customPaper = array(0,0,381.89,595.28);
		$this->pdf2->setPaper('A4', 'landscape');
		// $this->pdf2->load_view('laporan_pdf', $data);
		$this->pdf2->load_view('../views/laporan_export_pdf/laporan_resep_jenisDeb',  $data);
	}
}
