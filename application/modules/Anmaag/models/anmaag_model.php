<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class anmaag_model extends CI_Model{
	public function __construct()
	{
	      // Call the CI_Model constructor
	    parent::__construct();
	    $this->load->database();
	}
	public function list_obat($kdObat,$nmObat,$is_anmaag)
	{
		$where = "";
		if($is_anmaag == "1"){
			$where .= " and JENIS = 'G4' ";
		}
		if($kdObat != ""){
			$where .= " and a.KDBRG like '%".$kdObat."%' ";
		}
		if($nmObat != ""){
			$where .= " and a.NMBRG like '%".$nmObat."%' ";
		}

		$sql = "select b.HRATA, b.HBIJI,b.HNA, a.KDBRG, a.NMBRG, a.SATUAN from IF_MBRG_GD a 
			join IF_MBRG_GD_GUPER b on a.kdbrg = b.KDBRG
			where a.ACTIVE = 1 
			 ".$where."
			GROUP BY b.HRATA, b.HBIJI,b.HNA, a.KDBRG, a.NMBRG, a.SATUAN
			ORDER BY a.NMBRG
			";
		// var_dump($sql);die();
    	return $this->db->query($sql);    
	}
	public function action($header, $detail, $user)
	{
		$res= [];
		try{
			$id_trans = $this->db->query("exec SP_IF_GENKODETRANS_ANMAAG '".date('m/d/Y')."'");
			$id_trans_out = $id_trans->row()->KODE;
			
			$sql = $this->db->query("select * from if_mketot where MKODE = 403");
			$ketout = $sql->row();

	 		$val = "";
			$koma = "";
			// create out
			//header
			$this->db->query("insert into IF_HTRANS (ID_TRANS, KDMUT, TGL, NOMOR, MUTASI, KDOT, NMPX, LOKASI, CLOSING, KEU, TIPEIF, ACTIVE, INPUTBY, INPUTDATE)
					VALUES ('".$id_trans_out."', '".$header['kdmut']."', cast(convert(datetime, '".$header['tgl']."', 103) as datetime), '0', 'O', '".$ketout->MKODE."', 
					'".$ketout->MNAMA."','1','1','1','1','1','".$user."', GETDATE() )");
			// //detail
			// var_dump("insert into IF_HTRANS (ID_TRANS, KDMUT, TGL, NOMOR, MUTASI, KDOT, NMPX, LOKASI, CLOSING, KEU, TIPEIF, ACTIVE, INPUTBY, INPUTDATE)
			// 		VALUES ('".$id_trans_out."', '".$header['kdmut']."', cast(convert(datetime, '".$header['tgl']."', 103) as datetime), '0', 'O', '".$ketout->MKODE."', 
			// 		'".$ketout->MNAMA."','1','1','1','1','1','".$user."', GETDATE() )");die();
			foreach($detail as $index =>$item){
				if($index == 0){
	            	$koma = "";
	            }else{
	            	$koma = ",";
	            }

	            $val .= $koma . "('".$id_trans_out."','".$item['no_urut']."','0','".$item['kdbrg']."','".$item['hrata']."',,'".$item['hbiji']."','".$item['jumlah']."','".$user."', GETDATE())";
			}
			$this->db->query("insert into if_trans (ID_TRANS, NO, ID, KDBRG, HARGA, HBIJI, JUMLAH, ACTIVE, INPUTBY, INPUTDATE ) VALUES ". $val);

			// create in
			$id_trans = $this->db->query("exec SP_IF_GENKODETRANS_ANMAAG '".date('m/d/Y')."'");
			$id_trans_in = $id_trans->row()->KODE;
			
			$sql = $this->db->query("select * from if_mketin where MKODE = 917");
			$ketin = $sql->row();
			$val = "";
	        $koma = "";
	        //header
			$this->db->query("insert into IF_HTRANS (ID_TRANS, KDMUT, TGL, NOMOR, MUTASI, KDIN, NMPX, LOKASI, CLOSING, KEU, TIPEIF, ACTIVE, INPUTBY, INPUTDATE, ID_TRANS_ANMAAG)
					VALUES ('".$id_trans_in."', '".$header['kdmut']."', cast(convert(datetime, '".$header['tgl']."', 103) as datetime), '0', 'O', '".$ketin->MKODE."', 
					'".$ketin->MNAMA."','1','1','1','1','1','".$user."', GETDATE(), '".$id_trans_out."' )");

			//detail
			$this->db->query("insert into if_trans (ID_TRANS, NO, ID, KDBRG, HARGA, HBIJI, JUMLAH, ACTIVE, INPUTBY, INPUTDATE ) 
				VALUES ('".$id_trans_in."', 1, 0, '".$header['kdObat']."', '".$header['harga']."', '".$header['harga']."', '".$header['jumlah']."', 1,'".$user."', GETDATE() )");
			$this->db->trans_commit();
			$res['status'] = 'success'; 
			$res['message'] = 'Berhasil Tambah Anmaag'; 
		}
		catch(Exception $e){
			$this->db->trans_rollback();
			$res['status'] = 'error'; 
			$res['message'] = 'Gagal Tambah Anmaag';
		}
		// create out
		
		return $res;
	}
}