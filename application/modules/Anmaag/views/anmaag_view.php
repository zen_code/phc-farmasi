<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<style>
    .is_head{
        background: #2860a8;
        color: white;
    }
    .dataTables_wrapper{
        width: 100%;
    }
</style>
<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
    <input type="hidden" value="<?php echo base_url(); ?>" id="url">
        <!-- begin:: Subheader -->
        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        Anmaag </h3>
                </div>
                <!-- kt-subheader__toolbar -->
                
            </div>
        </div>

        <!-- end:: Subheader -->

        <!-- begin:: Content -->
        <div class="kt-container  kt-grid__item kt-grid__item--fluid">

            <!--Begin::Dashboard 1-->

            <!--Begin::Row-->
            <div class="row">
                
                <div class="col-xl-12">
                    <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile ">
                        <!-- <div class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                Farmasi Klinis
                                </h3>
                            </div>
                        </div> -->
                        <div class="kt-portlet__body kt-portlet__body--fit">
                            <!--begin: Datatable -->
                            <!-- <div class="kt-portlet__body">
                                <button class="btn btn-info col-md-2" type="button" id="search_pasien"   data-toggle="kt-tooltip" data-placement="top"><i class="fa fa-search"></i> Search Px</button>
                                
                            </div> -->
                            <div class="row"> 
                                
                                <div class="col-md-8">
                                    <div class="kt-portlet">
                                        <div class="kt-portlet__head">
                                            <div class="kt-portlet__head-label">
                                                <h3 class="kt-portlet__head-title">
                                                    Komposisi Anmaag
                                                </h3>
                                            </div>
                                        </div>
                                        <div class="kt-portlet__body">
                                            <div class="row" style="padding:10px"> 
                                                <table id="table-obat"  class="table table-striped- table-hover table-checkable">
                                                    <thead>
                                                        <tr style="background: #2860a8; color:white;text-align:center">
                                                            <th style=" color:white;">NO</th>  
                                                            <th style=" color:white;">KODE</th>  
                                                            <th style=" color:white;">NAMA BARANG</th>  
                                                            <th style=" color:white;">SATUAN</th>  
                                                            <th style=" color:white;">HARGA</th>  
                                                            <th style=" color:white;">JUMLAH</th>  
                                                            <th width="5%"></th>  
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr data-index="1">
                                                            <td style="text-align: center;vertical-align: middle;"> 1. </td>
                                                            <td style="width:10%">
                                                                <input type="hidden" id="hbiji">
                                                                <input type="hidden" id="hna">
                                                                <input type="text" class="form-control input-list" id="kdbarang_detail">
                                                            </td>
                                                            <td style="width:50%"><input type="text" class="form-control input-list" id="nmbarang_detail"></td>
                                                            <td><input type="text" class="form-control input-list" id="satuan" disabled></td>
                                                            <td><input type="text" class="form-control input-list" id="hrata" disabled></td>
                                                            <td><input type="text" class="form-control numbers input-list" id="jumlah"></td>
                                                            <td></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                
                                            </div>
                                            <div style="text-align: right;margin-bottom:10px">
                                                <button class="btn btn-success2" id="ok" type="button"><i class="fa fa-check"></i> OK</button>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                                <div class="col-md-4">
                                    <div class="kt-portlet">
                                        <div class="kt-portlet__head">
                                            <div class="kt-portlet__head-label">
                                                <h3 class="kt-portlet__head-title">
                                                    Pilih Obat Anmaag
                                                </h3>
                                            </div>
                                        </div>
                                        <div class="kt-portlet__body">
                                            <div class="row"> 
                                                <div class="form-group col-md-12">
                                                    <label>Obat Anmaag</label>
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <input type="text" class="form-control inputan" id="kdobat" placeholder="Kode Obat" disabled onfocus="this.placeholder = ''"
onblur="this.placeholder = 'enter your text'" > 
                                                        </div>
                                                        <div class="col-md-8">
                                                            <input type="text" class="form-control inputan" id="nmobat" placeholder="Nama Obat" disabled> 
                                                         </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row"> 
                                                <div class="form-group col-md-6">
                                                    <label>Jumlah AK</label>
                                                    <input type="text" class="form-control  inputan" id="jumlah_anmaag" placeholder="Jumlah" style="text-align:right" disabled>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label>Harga</label>
                                                    <div class="input-group date">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text">RP</span>
                                                        </div>
                                                        <input type="text" class="form-control  inputan" id="harga" placeholder="Harga" style="text-align:right" disabled>
                                                    </div>
                                                    <!--  -->
                                                </div>
                                            </div>
                                            <div class="row"> 
                                                <div class="form-group col-md-6">
                                                    <label>Tanggal</label>
                                                    <div class="input-group date">
                                                        <input type="text" class="form-control form-control-sm kt-input inputan" placeholder="From" id="date_1"  disabled/>
                                                        <div class="input-group-append">
                                                            <span class="input-group-text"><i class="la la-calendar-o glyphicon-th"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div style="text-align: right;margin-bottom:10px">
                                                <button class="btn btn-success2 inputan" id="simpan" type="button" disabled><i class="fa fa-check"></i> SIMPAN</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>

            <!--End::Row-->

            

            <!--End::Dashboard 1-->
            <div class="modal fade" tabindex="-1" role="dialog" id="modal-review">
                <div class="modal-dialog" role="document">
                    <div class="modal-content ">
                    <div class="modal-header">
                        <h5 class="modal-title">List Obat Anmaag</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <table id="table-obat_anmaag"  class="table table-striped- table-hover table-checkable">
                                <thead>
                                    <tr style="background: #2860a8; color:white;text-align:center">
                                        
                                        <th style=" color:white;">KODE</th>  
                                        <th style=" color:white;">NAMA BARANG</th>  
                                        <th style=" color:white;">SATUAN</th>   
                                        <th width="5%"></th>  
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- end:: Content -->
    </div>
</div>
<!-- javascript this page -->
<script src="<?php echo base_url()?>assets/assets_metronic/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/assets_metronic/vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>application/modules/Anmaag/views/anmaag_view.js" type="text/javascript"></script>
<!-- <script src="<?php echo base_url()?>assets/assets_metronic/js/demo9/pages/dashboard.js" type="text/javascript"></script> -->

<script>

</script>