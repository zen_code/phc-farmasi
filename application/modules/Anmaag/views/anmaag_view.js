"use strict";

// Class definition

var Anmaag = function() {
    return {
        // Init demos
        init: function() {
            input_number();
            $('#date_1').datepicker({autoclose:true, format: 'dd/mm/yyyy'});
        },
    };
}();
var clickedLine = 0;
$('#table-obat').on('keydown', 'tr #jumlah', function (evt) {
    // evt.preventDefault()
    if (evt.key === 'Enter') {
        clickedLine = $(this).closest('tr').data('index');
        var $tr = $('#table-obat > tbody').find('tr[data-index="'+clickedLine+'"]');
        var kdObat = $tr.find("#kdbarang_detail").val()
        if(kdObat != ""){
            addRow();
        }else{
            swal.fire('Peringatan', 'Belum memilih Obat', 'warning');
        }   
    }
});
var i = 2;
function addRow(){
    var a ="";
    a = '<tr data-index = "'+i+'">'+
          '<td style="text-align: center;vertical-align: middle;"></td>' + //<span class="num_text"></span>
          '<td><input type="hidden" id="hbiji"><input type="hidden" id="hna"><input type="text" id="kdbarang_detail" class="form-control input-list"></td>' +
          '<td><input type="text" id="nmbarang_detail" class="form-control input-list"></td>' +
          '<td><input type="text" id="satuan" class="form-control input-list" disabled></td>' +
          '<td><input type="text" id="hrata" class="form-control input-list" disabled></td>' +
          '<td><input type="text" id="jumlah" class="form-control numbers input-list"></td>' +
          '<td><button class="btn btn-danger input-list" id="btn-del"><i class="fa fa-times" data-toggle="kt-tooltip" data-placement="top" title="hapus"></i> </button></td></td>' +
        '</tr>';
    $('#table-obat tbody').append(a);
    i++;
    renumber();
    input_number();
}
function renumber() {
    var index = 1;
    $('#table-obat tbody tr').each(function () {
        $(this).find('td').eq(0).html(index + ".");//.find('.num_text')
        index++;
    });
}
$('#table-obat').on('click', 'tr #btn-del', function () {
    var baris = $(this).parents('tr')[0];
    baris.remove();
    renumber();
});
$('#kdobat').on('keydown', function(evt){
    if (evt.key === 'Enter') {
        var kdObat = $('#kdobat').val();
        listObatAnmaag(1,kdObat,"");
    }
});
$('#nmobat').on('keydown', function(evt){
    if (evt.key === 'Enter') {
        var nmobat = $('#nmobat').val();
        listObatAnmaag(1,"",nmobat);
    }
});
 function listObatAnmaag(is_anmaag,kdObat,nmObat){
    
    loading();
    $('#table-obat_anmaag').DataTable( {
        "destroy": true,
        "processing" : true,
        "type": "POST",
        "ajax": 'anmaag/dataObatAnmaag?kdObat='+kdObat+'&nmObat='+nmObat+'&is_anmaag='+is_anmaag,
        "columns"       : [
            {"data" : "KDBRG"},
            {"data" : "NMBRG"},
            {"data" : "SATUAN"},
            {"data" : "btn"},
        ],
        
        "createdRow"    : function ( row, data, index ) {
            // $('td', row).eq(0).addClass('text-center');
           // $('td', row).eq(2).addClass('text-right');
        }, 
        "drawCallback": function( settings ) {
            $('[data-toggle="kt-tooltip"]').tooltip();
        },
        "initComplete" : function(){
            KTApp.unblockPage();
            $('#modal-review').modal('show');
        }
    });

}
function pilih(kode, nama,type,satuan, hrata,hbiji,hna){
    if(type == 1){
        $('#nmobat').val(nama);
        $('#kdobat').val(kode);
    }else{
        var $tr = $('#table-obat > tbody').find('tr[data-index="'+clickedLine+'"]');
        $tr.find('#kdbarang_detail').val(kode);
        $tr.find('#nmbarang_detail').val(nama);
        $tr.find('#satuan').val(satuan);
        $tr.find('#hrata').val(hrata);
        $tr.find('#hbiji').val(hbiji);
        $tr.find('#hna').val(hna);
    }
    
    $('#modal-review').modal('hide');
}

function loading(){
    KTApp.blockPage({
        overlayColor: '#000000',
        type: 'v2',
        state: 'primary',
        message: 'Processing...'
    });
}
var clickedLine = 0;
$('#table-obat').on('keydown', 'tr #kdbarang_detail ', function (evt) {
    clickedLine = $(this).closest('tr').data('index');
    var $tr = $('#table-obat > tbody').find('tr[data-index="'+clickedLine+'"]');
    var kdObat = $tr.find('#kdbarang_detail').val();

    if (evt.key === 'Enter') {
        listObatAnmaag(0,kdObat,"");
    }
});
$('#table-obat').on('keydown', 'tr #nmbarang_detail ', function (evt) {
    clickedLine = $(this).closest('tr').data('index');
    var $tr = $('#table-obat > tbody').find('tr[data-index="'+clickedLine+'"]');
    var nmObat = $tr.find('#nmbarang_detail').val();

    if (evt.key === 'Enter') {
        listObatAnmaag(0,"",nmObat);
    }
});
function input_number(){
    $(".numbers").on("keypress keyup blur",function (event) {    
        $(this).val($(this).val().replace(/[^\d].+/, ""));
         if ((event.which < 48 || event.which > 57)) {
             event.preventDefault();
         }
    });
}
$('#simpan').click(function(){
    var header = {
        kdObat : $('#kdobat').val(),
        jumlah : $('#jumlah_anmaag').val(),
        harga : $('#harga').val(),
        tgl : $('#date_1').val(),
        kdmut : $('#depo_nm').val(),
    }
    var detail = [];
    $('#table-obat tbody tr').each(function(i){
        var kdbrg = $(this).find('#kdbarang_detail').val()
        if(kdbrg != ''){
            detail.push({
                no_urut: (i+1),
                kdbrg: kdbrg,
                hbiji: $(this).find('#hbiji').val(),
                hrata: $(this).find('#hrata').val(),
                jumlah: $(this).find('#jumlah').val(),
            }) 
        }
         
    });
    var data = {
        header : header,
        detail : detail
    }
    // console.log(data);
    $.ajax({
        type: "POST",
        url: "anmaag/action", 
        dataType: 'json',
        data:  data,
        success: function (data) {
            console.log(data);
        },
        error: function (xhr,status,error) {

        }
    });
})
$('#ok').click(function(){
    $('.inputan').attr('disabled', false)
    $('.input-list').attr('disabled', true)
    // console.log('asdads');
})
// Class initialization on page load
jQuery(document).ready(function() {
    Anmaag.init();
});