<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class anmaag extends CI_Controller {
	public $urlws = null;
	public function __construct() {
        parent::__construct();
		$this->load->model('anmaag_model');
		$this->load->library('session');
		$this->urlws = $this->globals->ws_phc();//"http://localhost/phc-ws/api/";

    }
	private function _render($view,$data = array())
	{
		$this->load->view('header',$data);
	    // $this->load->view('body_header');
	    // $this->load->view('sidebar');
	    $this->load->view($view,$data);
	    $this->load->view('footer');
	}

	public function index()
	{
		//load library
		// var_dump($this->session->all_userdata());die();
		$uri =& load_class('URI', 'core');
	  	redirect(base_url().'index.php/anmaag_view/data',$data);
	}

	public function data()
	{
		// var_dump($_SESSION["ses_userid"]);die();
		if($_SESSION["if_ses_depo"] == null){
			redirect(base_url().'depo');
		}else{
			if(in_array(6,$_SESSION['if_ses_menu'])){
				$data['parent_active'] = 6 ;
				$data['child_active'] = 28 ;
				$this->_render('anmaag_view', $data);
			}else{
				redirect(base_url().'beranda/data');}
		}
	}
	
	public function dataObatAnmaag()
	{
		// $authorization = 'Authorization:'.$_SESSION["ses_token_phc"];
		// $url = $this->urlws."Farmasi/FarmasiKlinis/dataPasien";
		// $ch = curl_init();
		// curl_setopt($ch, CURLOPT_HTTPHEADER, array($authorization ));
		// curl_setopt($ch, CURLOPT_URL, $url); 
		// curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		// $output = curl_exec($ch); 
		// curl_close($ch);    
		// $cek_output = json_decode($output);
		$kdObat = $this->input->get('kdObat');
		$nmObat = $this->input->get('nmObat');
		$is_anmaag = $this->input->get('is_anmaag');
		$query = $this->anmaag_model->list_obat($kdObat,$nmObat,$is_anmaag);

		// if($cek_output->status == true){
		$data 			= array();
		foreach($query->result() as $item){
			$btn 		='<button name="btnproses" id="btnproses"type="button" onClick="pilih('."'".$item->KDBRG."'".','."'".$item->NMBRG."'".','."'".$is_anmaag."'".','."'".$item->SATUAN."'".','."'".$item->HRATA."'".','."'".$item->HBIJI."'".','."'".$item->HNA."'".' )" class="btn btn-success btn-sm" data-toggle="kt-tooltip" data-placement="top" title="Choose"> <i class="fa fa-check"></i> </button>';
            
			$data[] 	= array(
				"KDBRG" 	=> $item->KDBRG,
				"NMBRG" 	=> $item->NMBRG,
				"SATUAN" 	=> $item->SATUAN ,
				"btn"		=> $btn
			);
		}
		$obj = array("data"=> $data);
		echo json_encode($obj);
		// }else{
		// 	$error = array(
		// 		'data' => [],
		// 		'error' => $cek_output->message
		// 	);
			
		// 	echo json_encode($error);
		// }
		
	}
	public function action()
	{	
		$header = $this->input->post('header');
		$detail = $this->input->post('detail');
		$user = $_SESSION["if_ses_username"];

		// var_dump($header, $detail, $user);die();
		$datas=$this->anmaag_model->action($header, $detail, $user);
		
		echo json_encode($datas);
	}
}
