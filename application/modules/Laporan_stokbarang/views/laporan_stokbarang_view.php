<?php
defined('BASEPATH') or exit('No direct script access allowed');

?>

<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
        <input type="hidden" value="<?php echo base_url(); ?>" id="url">
        <!-- begin:: Subheader -->
        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        Laporan Stok Barang </h3>
                </div>
                <!-- kt-subheader__toolbar -->

            </div>
        </div>

        <!-- end:: Subheader -->

        <!-- begin:: Content -->
        <div class="kt-container  kt-grid__item kt-grid__item--fluid">
            <!--Begin::Row-->
            <div class="row">

                <div class="col-xl-12 order-lg-2 order-xl-1">
                    <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile ">
                        <div class="kt-portlet__body kt-portlet__body--fit">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="kt-portlet">
                                        <div class="kt-portlet__head">
                                            <div class="kt-portlet__head-label">
                                                <h3 class="kt-portlet__head-title">
                                                    Pilih Layanan
                                                </h3>
                                            </div>
                                        </div>
                                        <!-- FormTanggal&NoMedik -->
                                        <div class="kt-portlet__body">
                                            <div class="row">
                                                <div class="form-group col-md-12">
                                                    <div class="row">
                                                        <div class="form-group col-md-2">
                                                            <label>Layanan</label>
                                                            <select class="form-control dis" id="layanan">
                                                                <option>Farmasi Jalan</option>
                                                                <option>Farmasi Tetap</option>
                                                            </select>
                                                        </div>
                                                        <div class="input-group input-group-lg col-md-12">
                                                            <div style="text-align: center;margin-bottom:10px">
                                                                <button class="btn btn-success2" id="simpan"
                                                                    type="button">
                                                                    TAMPILKAN</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xl-12 order-lg-2 order-xl-1">
                    <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile ">
                        <div class="kt-portlet__body kt-portlet__body--fit">
                            <!-- <div class="row"> -->
                            <div class="col-md-12">
                                <div class="kt-portlet">
                                    <div class="kt-portlet__head">
                                        <div class="kt-portlet__head-label">
                                            <h3 class="kt-portlet__head-title">
                                                Main Report
                                            </h3>
                                        </div>
                                    </div>
                                    <!-- Tabel -->
                                    <br>
                                    <table class="table table-striped- table-bordered table-hover table-checkable"
                                        id="tabelReport">
                                        <thead>
                                            <tr style="background: #2860a8; color:white;text-align:center">
                                                <th>NO</th>
                                                <th>KODE</th>
                                                <th>NAMA BARANG</th>
                                                <th>JENIS</th>
                                                <th>SATUAN</th>
                                                <th>AWAL</th>
                                                <th>MASUK</th>
                                                <th>KELUAR</th>
                                                <th>AKHIR</th>
                                                <th>HARGA</th>
                                                <th>TOTAL</th>
                                            </tr>
                                        </thead>
                                        <tbody style="text-align: center">
                                        <tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- </div> -->
                        </div>
                    </div>
                </div>
                <!--End::Row-->
            </div>
            <!-- end:: Content -->
        </div>
    </div>

    <!-- javascript this page -->
    <script src="<?php echo base_url() ?>assets/assets_metronic/vendors/custom/datatables/datatables.bundle.js"
        type="text/javascript"></script>
    <script src="<?php echo base_url() ?>application/modules/Laporan_stokbarang/views/laporan_stokbarang_view.js"
        type="text/javascript"></script>
    <script
        src="<?php echo base_url() ?>assets/assets_metronic/vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"
        type="text/javascript"></script>
    <!-- <script src="<?php echo base_url() ?>assets/assets_metronic/js/demo9/pages/dashboard.js" type="text/javascript"></script> -->