<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class setor_keuangan_model extends CI_Model{
    public function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
        $this->load->database();
    }
    
    public function get_debitur_model()
    {
        $query  = $this->db->query("SELECT * FROM RIRJ_MDEBITUR");
        return $query;
    }

    public function getLaporan_model($tglmulai,$tglselesai,$nama_debt,$shift)
    {
        $res = [];
        $sql1 = $this->db->query("exec if_sp_tampilkan_data_setor_keuangan '$tglmulai', '$tglselesai', '$nama_debt', 1, '$shift'");
        $res['jenis_1'] = $sql1->result();
        $sql2 = $this->db->query("exec if_sp_tampilkan_data_setor_keuangan '$tglmulai', '$tglselesai', '$nama_debt', 2, '$shift'");
        $res['jenis_2'] = $sql2->result();
        $sql3 = $this->db->query("exec if_sp_tampilkan_data_setor_keuangan '$tglmulai', '$tglselesai', '$nama_debt', 3, '$shift'");
        $res['jenis_3'] = $sql3->result();
        // var_dump($res['jenis_3']);die();

        return $res;

        
    }
    
}