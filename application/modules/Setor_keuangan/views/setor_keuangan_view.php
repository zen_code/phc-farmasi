<?php
defined('BASEPATH') or exit('No direct script access allowed');

?>
<style>
  .passrow {
    display: none
  }

  table.dataTable th.focus,
  table.dataTable td.focus {
    outline: none;
  }

  #events {
    margin-bottom: 1em;
    padding: 1em;
    background-color: #f6f6f6;
    border: 1px solid #999;
    border-radius: 3px;
    height: 100px;
    overflow: auto;
  }
</style>
<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
  <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
    <input type="hidden" value="<?php echo base_url(); ?>" id="url">
    <!-- begin:: Subheader -->
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
      <div class="kt-container ">
        <div class="kt-subheader__main">
          <h3 class="kt-subheader__title">
            Setor Keuangan </h3>
        </div>
        <div class="">
          <label class="col-form-label" style="color:royalblue">
            Date Sistem :&ensp;
          </label>
          <label id='date' class="col-form-label" style="color:#781400">
          </label>&ensp;
          <label id='jam' class="col-form-label" style="color:#781400">
          </label>&ensp;
          <label class="col-form-label" style="color:royalblue">
            Shift :&ensp;
          </label>
          <label id='shift' class="col-form-label" style="color:#781400">
          </label>
        </div>
        <!-- kt-subheader__toolbar -->

      </div>
    </div>

    <!-- end:: Subheader -->

    <!-- begin:: Content -->
    <div class="kt-container  kt-grid__item kt-grid__item--fluid">

      <!--Begin::Dashboard 1-->

      <!--Begin::Row-->
      <div class="row">
        <div class="col-xl-12 order-lg-2 order-xl-1">
          <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile">
            <!-- <div class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm bg-success">
              <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title" style="color: white;">
                  Biodata Pasien
                </h3>
              </div>
            </div> -->
            <form class="m-form" style="padding:20px;" method="POST" action="" id="form">
              <div class="form-group row">
                <div class="col-md-12" style="padding:20px 10px 20px 10px;">
                  <!--Biodata Pasien-->
                  <div class="m-portlet">
                    <div class="card">
                      <div class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                        <div class="kt-portlet__head-label">
                          <h3 class="kt-portlet__head-title">
                            Filter Laporan
                          </h3>
                        </div>
                      </div>

                      <div class="card-body">
                        <div class="m-portlet__body">
                            <div>
                                <!--  use to store every hidden input (start) -->
                                <input type="hidden" id="status_naik_kelas" value="0">
                                <input type="hidden" id="kode_trans" value="0">
                                <input type="hidden" id="antrian" value="0">
                            </div> <!--  use to store every hidden input (stop) -->
                            
                            <div class="m-form__group row">
                                <div class="col-md-5">
                                    <label class="col-md-12 col-form-label m-form__group">
                                    Tanggal
                                    </label>
                                </div>
                                <div class="col-md-6">
                                    <label class="col-md-12 col-form-label m-form__group">
                                    Debitur
                                    </label>
                                </div>
                                <div class="col-md-1">
                                    <label class="col-md-12 col-form-label m-form__group">
                                    Shift
                                    </label>
                                </div>
                                
                            </div>

                            <div class="m-form__group row">
                                <div class="col-md-5">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <div class="m-form__group input-group date">
                                                <input type="text" class="form-control kt-input datepicker" name="date_from" placeholder="From" id="date_from" />
                                                <div class="input-group-append">
                                                    <span class="input-group-text"><i class="la la-calendar-o glyphicon-th"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                        <label for="example-text-input" class="col-2 col-form-label" style="text-align:center; font-weight: bold;">-</label>
                                        <div class="col-md-5">
                                            <div class="m-form__group input-group date">
                                                <input type="text" class="form-control kt-input datepicker" name="date_to" placeholder="To" id="date_to" />
                                                <div class="input-group-append">
                                                    <span class="input-group-text"><i class="la la-calendar-o glyphicon-th"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <input type="text" class="form-control" value="" name="nama_debt" id="nama_debt" disabled>
                                        </div>
                                        <div class="col-md-9">
                                            <!-- <input name="" type="hidden" class="form-control m-input spcdis" id="KDDEBT" value="" placeholder=""> -->
                                            <select class="form-control" id="NMDEBT" name="NMDEBT">
                                                <option></option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-md-1">
                                    <div class="row">
                                        <select class="form-control" name="shift" id="shiftlaporan">
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                        </select>
                                    </div>
                                </div>

                                
                                
                                  
                            </div>
                            <br>
                            <div class="m-form__group row">
                              <div class="offset-md-11">
                                <button type="button" id="btnFilter" onclick="filterLaporan()" class="col-md-5 form-control m-input btn btn-success2" style=""><i class="fas fa-check">&nbsp;</i></button>&emsp;
                                <button type="reset" id="cancel" class="col-md-5 form-control m-input btn btn-warning2" style=""><i class="fas fa-times">&nbsp;</i></button>
                              </div>
                                
                            </div>

                            
                        </div>
                      </div>
                    </div>
                  </div>

                </div>

                
              </div>

              <div class="form-group row">
                <div class="col-md-4" style="padding:20px 10px 20px 10px;">
                  <!--Biodata Pasien-->
                  <div class="m-portlet">
                    <div class="card">
                      <div class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                        <div class="kt-portlet__head-label">
                          <h3 class="kt-portlet__head-title">
                            Rincian Pembayaran
                          </h3>
                        </div>
                      </div>

                      <div class="card-body">
                        <div class="m-portlet__body">
                            <div>
                                <!--  use to store every hidden input (start) -->
                                <input type="hidden" id="status_naik_kelas" value="0">
                                <input type="hidden" id="kode_trans" value="0">
                                <input type="hidden" id="antrian" value="0">
                            </div> <!--  use to store every hidden input (stop) -->
                            
                            <div class="m-form__group row">
                                <div class="col-md-6">
                                    <label class="col-md-12 col-form-label m-form__group">
                                    Bank
                                    </label>
                                </div>
                                <div class="col-md-6">
                                    <label class="col-md-12 col-form-label m-form__group">
                                    Total
                                    </label>
                                </div>
                                
                            </div>

                            <div class="m-form__group row">
                                
                                <div class="col-md-6">
                                    <input type="text" class="form-control" value="" id="bank">
                                </div>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" value="" id="total">
                                </div>
                            </div>

                            
                        </div>
                      </div>
                    </div>
                  </div>

                </div>

                <div class="col-md-8" style="padding:20px 10px 20px 10px;">
                  <!--Biodata Pasien-->
                  <div class="m-portlet">
                    <div class="card">
                      <div class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                        <div class="kt-portlet__head-label">
                          <h3 class="kt-portlet__head-title">
                            Rincian Resep
                          </h3>
                        </div>
                      </div>

                      <div class="card-body">
                        <div class="m-portlet__body">
                            <div>
                                <!--  use to store every hidden input (start) -->
                                <input type="hidden" id="status_naik_kelas" value="0">
                                <input type="hidden" id="kode_trans" value="0">
                                <input type="hidden" id="antrian" value="0">
                            </div> <!--  use to store every hidden input (stop) -->
                            
                            <div class="m-form__group row">
                                <table class="table table-striped- table-bordered table-hover table-checkable" id="laporan">
                                    <thead>
                                    <tr style="background:#2860a8;text-align: center;">
                                        <!-- <th style="color:white" width="2%">Cek</th> -->
                                        <th style="color:white" width="10%">Nota</th>
                                        <th style="color:white" width="8%">Nomor</th>
                                        <th style="color:white" width="11%">Tgl</th>
                                        <th style="color:white" width="9%">Jam</th>
                                        <th style="color:white" width="15%">Debitur</th>
                                        <th style="color:white" width="8%">RM</th>
                                        <th style="color:white" width="20%">Nama</th>
                                        <th style="color:white" width="14%">TOTAL</th>
                                        
                                    </tr>
                                    </thead>
                                    <tbody>
                                    

                                    </tbody>
                                </table>
                                
                            </div>

                            

                            
                        </div>
                      </div>

                        

                    </div>
                  </div>

                  <div class="m-portlet">
                    <div class="card">
                      <div class="card-body">
                        <div class="m-portlet__body">
                                                       
                            <div class="m-form__group row">
                                <div class="col-md-3">
                                </div>
                                <div class="col-md-9">
                                    <div class="row">
                                    <label class="col-md-5 col-form-label m-form__group">
                                    Total Pembayaran/Total Resep
                                    </label>
                                    <input type="text" class="col-md-3 form-control" name="total_bayar" value="" id="total_bayar">
                                    <label for="example-text-input" class="col-md-1 col-form-label" style="text-align:center; font-weight: bold;">/</label>
                                    <input type="text" class="col-md-3 form-control" name="total_resep" value="" id="total_resep">
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="m-form__group row">
                                <div class="col-md-7">
                                    <div class="m-checkbox-inline">
                                        <label class="m-checkbox col-md-12">
                                            <input type="checkbox" name="pilihan2" value="Tidak" required="">
                                                Tandai semua
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                
                                    <button type="button" id="save_all" class="col-md-7 form-control m-input btn btn-success2" style=""><i class="fas fa-check">&nbsp;</i>Kirim keuangan</button>&emsp;
                                    <button type="reset" id="cancel" class="col-md-4 form-control m-input btn btn-warning2" style=""><i class="fas fa-times">&nbsp;</i>Keluar</button>
                                </div>
                            </div>
                            <!-- <div class="row" style="float:right">
                                
                                    <button type="button" id="save_all" class="col-md-5 form-control m-input btn btn-success2" ><i class="fas fa-check">&nbsp;</i>Kirim</button>&emsp;
                                    <button type="reset" id="cancel" class="col-md-5 form-control m-input btn btn-warning2" style=""><i class="fas fa-times">&nbsp;</i>Keluar</button>
                                
                                
                            </div> -->

                            

                            
                        </div>
                      </div>

                        

                    </div>
                  </div>

                </div>
                
              </div>
              
            </form>
          </div>
        </div>
      </div>

      <!--End::Row-->



      <!--End::Dashboard 1-->
    </div>
    <!-- end:: Content -->
  </div>
</div>
<!-- javascript this page -->
<script src="<?php echo base_url() ?>assets/assets_metronic/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
<!-- <script src="<?php //echo base_url()
                  ?>application/modules/User/views/user_view.js" type="text/javascript"></script> -->
<script src="<?php echo base_url() ?>application/modules/Setor_keuangan/views/setor_keuangan_view.js" type="text/javascript"></script>
<script type="text/javascript">
  $(function() {
    $(".datepicker").datepicker({
      format: 'dd/mm/yyyy',
      autoclose: true,
      todayHighlight: true,
    });
  });
</script>