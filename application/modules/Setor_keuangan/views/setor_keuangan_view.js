"use strict";

var Setor_keuangan = (function () {
  function getkode() {
    $.ajax({
      type: "GET",
      url: "setor_keuangan/get_debitur",
      data: {},
      dataType: "json",

      success: function (data) {
        // console.log(data);
        var datakode = data;
        jQuery.each(datakode, function (index, val) {
          var a = "";
          a =
            "<option value='" +
            val.KDDEBT +
            "' data-kode='" +
            val.NMDEBT +
            "'><b>" +
            val.KDDEBT +
            " - " +
            val.NMDEBT +
            "</b>" +
            "</option>";
          $("#NMDEBT").append(a);
        });
      },
      error: function (xhr, status, error) {
        console.log(xhr, status, error);
      }
    });
    $("#NMDEBT").select2({
      placeholder: "Pilih Debitur",
      width: "100%"
    });

  }

  return {
    // Init demos
    init: function () {
      getkode();

      // tabelLaporan();
    }
  };
})();

function formatDate(date) {
  date = date.split("/");
  return date[2] + "/" + date[1] + "/" + date[0];
}

function filterLaporan() {
  // alert('yey');
  tabelLaporan();
}

function tabelLaporan() {
  loading();
  var tglmulai = $('#date_from').val();
  var tglmulai2 = formatDate(tglmulai);
  console.log(tglmulai2);
  
  var tglselesai = $('#date_to').val();
  var tglselesai2 = formatDate(tglselesai);
  console.log(tglselesai2);
  var nama_debt = $('#nama_debt').val();
  var shiftlaporan = $('#shiftlaporan').val();
  $.ajax({
    method: "GET",
    url: "setor_keuangan/getLaporan/",
    data: {
      tglmulai: tglmulai2,
      tglselesai: tglselesai2,
      nama_debt: nama_debt,
      shiftlaporan: shiftlaporan
    },
    dataType:'json',
    success: function (response) {
      // console.log(response.data.data.jenis3[0]);
      $('#bank').val(response.data.data.jenis3[0].bank);
      $('#total').val(response.data.data.jenis3[0].total);
      
      $('#total_resep').val(response.data.data.jenis1.length);
      // $('#total_bayar').val('');

      $('#laporan tbody').html('');
      $.each(response.data.data.jenis1, function(i, e){
          var $tbody = $('#laporan tbody');
          $tbody.append('<tr></tr>');
          var $tr = $tbody.find('tr:last');
          $tr.append('<td>'+e.nota+'</td>');
          $tr.append('<td>'+e.nomor+'</td>');
          $tr.append('<td>'+e.tgl+'</td>');
          $tr.append('<td>'+e.jam+'</td>');
          $tr.append('<td>'+e.debitur+'</td>');
          $tr.append('<td>'+e.rm+'</td>');
          $tr.append('<td>'+e.nama+'</td>');
          $tr.append('<td>'+e.total+'</td>');
      })

      $('#laporan').DataTable({
        destroy: true,
        processing: true,
        type: "GET",
        // ajax:
        //   "setor_keuangan/getLaporan/?tglmulai="
        //   +tglmulai2+"&tglselesai="+tglselesai2+"&nama_debt="+nama_debt+"&shiftlaporan="+shiftlaporan,
        columns:[
          {
            data: "nota"
          },
          {
            data: "nomor"
          },
          {
            data: "tgl"
          },
          {
            data: "jam"
          },
          {
            data: "debitur"
          },
          {
            data: "rm"
          },
          {
            data: "nama"
          },
          {
            data: "total"
          }
        ],
    
    
        // scrollX: true,
        createdRow: function(row, data, index){
          $("td", row)
            .eq(0)
            .addClass("text-center");
            // console.log(data);
        },
        "initComplete": function () {
          asd();
        },
        drawCallback: function(){
          KTApp.unblockPage();
        }
      });
    },
    error: function (response) { },
    complete: function () { }
  });
}

function asd() {
  var bayar = 0;
  
  $('#laporan tbody tr').each(function (i) {
      bayar = parseInt(bayar) + parseInt($(this).find('td:eq(7)').html());
      
  })
  $('#total_bayar').val(bayar);
  
  // console.log(x_fit, x_unfit);

}

$('#NMDEBT').change(function () {
  var getKD = $('#NMDEBT').val();
  // var tglselesai = $('#date_to').val();
  $('#nama_debt').val(getKD);
});

function loading(){
  KTApp.blockPage({
    overlayColor: "#000000",
    type: "v2",
    state: "primary",
    message: "Processing..."
  });
}

jQuery(document).ready(function () {
  Setor_keuangan.init();

  // $("#idlaporan").select2({
  //     placeholder: "Jenis Laporan",
  //     width: "100%"
  // });
});