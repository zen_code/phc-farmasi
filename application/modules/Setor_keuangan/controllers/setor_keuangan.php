<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class setor_keuangan extends CI_Controller {
    public $urlws = null;

	public function __construct() {
        parent::__construct();
		$this->load->model('setor_keuangan_model');
		$this->load->library('session');
		$this->urlws = $this->globals->ws_phc();

    }

    private function _render($view,$data = array())
	{
		$this->load->view('header',$data);
	    // $this->load->view('body_header');
	    // $this->load->view('sidebar');
	    $this->load->view($view,$data);
	    $this->load->view('footer');
    }
    
    public function index()
	{
		//load library
		// var_dump($this->session->all_userdata());die();
		$uri =& load_class('URI', 'core');
	  	redirect(base_url().'index.php/setor_keuangan/data',$data);
	}

	public function data()
	{
		// var_dump($_SESSION["ses_userid"]);die();
		if($_SESSION["if_ses_depo"] == null){
			redirect(base_url().'depo');
		}else{
			if(in_array(7,$_SESSION['if_ses_menu'])){
				$data['parent_active'] = 7 ;
				$data['child_active'] = 49 ;
				$this->_render('setor_keuangan_view', $data);
			}else{
				redirect(base_url().'beranda/data');}
		}
		
	}
	
	public function get_debitur()
	{
		$query = $this->setor_keuangan_model->get_debitur_model();
		echo json_encode($query->result());
	}

	public function getLaporan(){
		
		$tglmulai = $this->input->get('tglmulai'); 
		$tglselesai = $this->input->get('tglselesai'); 
		$nama_debt = $this->input->get('nama_debt');
		$shiftlaporan = $this->input->get('shiftlaporan');

		// var_dump($nama_debt); die();

		$query = $this->setor_keuangan_model->getLaporan_model($tglmulai,$tglselesai,$nama_debt,$shiftlaporan);

		$data 			= array(
			'jenis1' => array(),
			'jenis2' => array(),
			'jenis3' => array(),	
		);
		// var_dump($query['jenis_3']);die();

		foreach ($query['jenis_1'] as $index => $item) {
			// $btn 		= '<button name="btnproses" id="btnproses" onclick="update(' . "'" . $item->KDIDX . "'" . ',' . "'" . $item->KDBHN . "'" . ',' . "'" . $item->FUNGSI . "'" . ')" type="button" class="btn btn-success btn-sm" data-toggle="kt-tooltip" data-placement="top" title="Update"> <i class="fa fa-edit"></i> </button>
			// 			<button name="btnbatal" id="btnbatal" onclick="remove(' . "'" . $item->KDIDX . "'" . ')" type="button" class="btn btn-danger btn-sm" data-toggle="kt-tooltip" data-placement="top" title="Delete"> <i class="fa fa-times"></i> </button>';
			 
			$data['jenis1'][] 	= array(
				"nota"		=> $item->NOTA,
				"nomor"		=> $item->NOMOR,
				"tgl"		=> $item->TGL,
				"jam"		=> $item->JAM,
				"debitur"	=> $item->DEBITUR,
				"rm"		=> $item->RM,
				"nama"		=> $item->NAMA,
				"total"		=> $item->TOTAL

				// "bank"		=> $item->BANK

				// "btn"		['jenis_3']=> $btn

			);
		}

		// $obj = array("data" => $data);

		foreach ($query['jenis_3'] as $index => $item2) {
			// $btn 		= '<button name="btnproses" id="btnproses" onclick="update(' . "'" . $item->KDIDX . "'" . ',' . "'" . $item->KDBHN . "'" . ',' . "'" . $item->FUNGSI . "'" . ')" type="button" class="btn btn-success btn-sm" data-toggle="kt-tooltip" data-placement="top" title="Update"> <i class="fa fa-edit"></i> </button>
			// 			<button name="btnbatal" id="btnbatal" onclick="remove(' . "'" . $item->KDIDX . "'" . ')" type="button" class="btn btn-danger btn-sm" data-toggle="kt-tooltip" data-placement="top" title="Delete"> <i class="fa fa-times"></i> </button>';
			 
			$data['jenis3'][] 	= array(
				"bank"		=> $item2->BANK,
				"total"		=> $item2->TOTAL

				// "bank"		=> $item->BANK

				// "btn"		['jenis_3']=> $btn

			);
		}
		
		$hasil 			= array();
		$hasil['data'] = array("data" => $data);

		
		echo json_encode($hasil);
	}
}