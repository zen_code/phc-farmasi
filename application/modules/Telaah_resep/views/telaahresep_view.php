<?php
defined('BASEPATH') or exit('No direct script access allowed');

?>


<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
        <input type="hidden" value="<?php echo base_url(); ?>" id="url">
        <!-- begin:: Subheader -->
        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        Telaah Resep </h3>
                    <!-- <button>cek</button> -->
                </div>

            </div>
        </div>

        <div class="kt-container  kt-grid__item kt-grid__item--fluid">

            <!--Begin::Dashboard 1-->

            <!--Begin::Row-->
            <div class="row">

                <div class="col-xl-12 order-lg-2 order-xl-1">
                    <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile ">

                        <div class="kt-portlet__body kt-portlet__body--fit">
                            <div class="kt-datatable" id="kt_datatable_latest_orders"></div>
                            <div class="kt-portlet__body">
                                <div class="input-group date" style="float:right">
                                    <input type="text" class="col-sm-2 form-control form-control-sm kt-input datepicker" placeholder="TANGGAL" id="datepicker" />
                                    <div class="input-group-append">
                                        <span class="input-group-text"><i class="la la-calendar-o glyphicon-th"></i></span>
                                    </div>
                                </div>
                                <table class="table table-striped- table-bordered table-hover table-checkable" id="tabelTelaah">
                                    <thead>
                                        <tr style="background: #2860a8; color:white;text-align:center">
                                            <th style=" color:white;">CEK</th>
                                            <th style=" color:white;">CETAK</th>
                                            <th style=" color:white;">KARCIS</th>
                                            <th style=" color:white;">NOTA</th>
                                            <th style=" color:white;">TGL</th>
                                            <th style=" color:white;">JAM</th>
                                            <th style=" color:white;">ANTRIAN</th>
                                            <th style=" color:white;">NAMA</th>
                                            <th style=" color:white;">TGL LAHIR</th>
                                            <th style=" color:white;">L/P</th>
                                            <th style=" color:white;">DOKTER</th>
                                            <th style=" color:white;">SIP</th>
                                            <th style=" color:white;">KLINIK</th>
                                            <th style=" color:white;">DEBITUR</th>
                                            <th style=" color:white;">AKSI</th>
                                        </tr>
                                    </thead>
                                    <tbody style="text-align: center">
                                    <tbody>
                                </table>
                            </div>
                            <!-- <nav class="navbar navbar-expand-lg navbar-light bg-light">
                                <div class="form-inline my-2 my-lg-0">
                                    <input class="form-control mr-sm-2" type="search" placeholder="Telaah Resep" aria-label="Search">
                                    <button class="btn btn-outline-success my-2 my-sm-0" data-toggle="modal" data-target="#myModal">Search</button>
                                </div>
                            </nav> -->
                        </div>
                        <!--Informasi Pendaftaran-->

                        <div class="m-form" style="padding:20px;" method="POST" id="formtelaah">
                            <!-- <div class="form-group m-form__group row">
                                <div class="col-md-2">
                                    <input name="" type="text" class="form-control m-input" id="user" style="text-align: center" placeholder="USER" disabled>
                                </div>
                                <div class="col-md-2">
                                    <input name="" type="text" class="form-control m-input" id="simpan_tgl" style="text-align: center" placeholder="Tanggal Simpan" disabled>
                                </div>
                            </div> -->

                            <div class="form-group row">
                                <div class="col-6">
                                    <div class="m-portlet">
                                        <div class="card">
                                            <div class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                                                <div class="kt-portlet__head-label">
                                                    <h3 class="kt-portlet__head-title">
                                                        Informasi Pendaftaran
                                                        <input type="hidden" id="id_telaah">
                                                    </h3>
                                                </div>
                                            </div>

                                            <div class="card-body">
                                                <div class="m-portlet__body">
                                                    <div class="form-group m-form__group row">
                                                        <label class="col-md-3 col-form-label">
                                                            No. Antrian :
                                                        </label>
                                                        <div class="col-md-2">
                                                            <input name="" type="text" class="form-control m-input" id="noan" placeholder="" disabled>
                                                        </div>
                                                        <label class="col-md-1.5 col-form-label">
                                                            Nama :
                                                        </label>
                                                        <div class="col-md-6">
                                                            <input name="" type="text" class="form-control m-input" id="nama_pasien" placeholder="" disabled>
                                                        </div>
                                                    </div>
                                                    <div class="form-group m-form__group row">
                                                        <label class="col-md-3 col-form-label">
                                                            Umur Px. :
                                                        </label>
                                                        <div class="col-md-5">
                                                            <input name="" type="text" class="form-control m-input" id="umur" placeholder="" disabled>
                                                        </div>

                                                        <label class="col-md-2.5 col-form-label">
                                                            GENDER
                                                        </label>
                                                        <div class="col-md-2">
                                                            <input name="" type="text" class="form-control m-input" id="gender" placeholder="" disabled>
                                                        </div>

                                                    </div>
                                                    <div class="form-group m-form__group row">
                                                        <label class="col-md-3 col-form-label">
                                                            BB / TB :
                                                        </label>
                                                        <div class="col-md-8">
                                                            <input name="" type="text" class="form-control m-input" id="beban" disabled placeholder="">
                                                        </div>
                                                    </div>
                                                    <div class="form-group m-form__group row">
                                                        <label class="col-md-3 col-form-label">
                                                            Dokter :
                                                        </label>
                                                        <div class="col-md-8">
                                                            <input name="" type="text" class="form-control m-input" id="dokter" disabled placeholder="">
                                                        </div>
                                                    </div>
                                                    <div class="form-group m-form__group row">
                                                        <label class="col-md-3 col-form-label">
                                                            SIP:
                                                        </label>
                                                        <div class="col-md-8">
                                                            <input name="" type="text" class="form-control m-input" id="sip" disabled placeholder="">
                                                        </div>
                                                    </div>
                                                    <div class="form-group m-form__group row">
                                                        <label class="col-md-3 col-form-label">
                                                            Klinik :
                                                        </label>
                                                        <div class="col-md-8">
                                                            <input name="" type="text" class="form-control m-input" id="klinik" disabled placeholder="">
                                                        </div>
                                                    </div>
                                                    <div class="form-group m-form__group row">
                                                        <label class="col-md-3 col-form-label">
                                                            Id Trans :
                                                        </label>
                                                        <div class="col-md-8">
                                                            <input name="id_trans" type="text" class="form-control m-input" id="id_trans" disabled placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--Informasi Obat-->
                                    <!-- <div class="m-portlet">
                                        <div class="card" style="margin-top:20px">
                                            <div class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                                                <div class="kt-portlet__head-label">
                                                    <h3 class="kt-portlet__head-title">
                                                        Informasi Obat
                                                    </h3>
                                                </div>
                                            </div>
                                            <div class="card-body" style="height:370px; overflow-y: scroll;">
                                                <div class="m-portlet__body">
                                                    <div class="table-responsive">
                                                        <table class="table" id="detob">
                                                            <thead>
                                                                <tr style="background: #2860a8; color:white;text-align:center">
                                                                    <th></th>
                                                                    <th>KODE</th>
                                                                    <th>OBAT</th>
                                                                    <th>SIGNA</th>
                                                                    <th>KET</th>
                                                                    <th>JUMLAH</th>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td colspan="99" style="text-align:center;padding:10px">NO DATA</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div> -->

                                    <!-- Kajian -->
                                    <div class="m-portlet">
                                        <div class="card" style="margin-top:20px">
                                            <div class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                                                <div class="kt-portlet__head-label">
                                                    <h3 class="kt-portlet__head-title">
                                                        Kajian
                                                    </h3>
                                                </div>
                                            </div>

                                            <div class="card-body" style="padding-bottom: 0px">
                                                <div class="card" style="border:double; margin-bottom: 20px;">

                                                    <div class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm" style="background: #2860a8;">
                                                        <div class="kt-portlet__head-label">
                                                            <h3 class="kt-portlet__head-title" style="font-size: small; color: white;">
                                                                Kajian Persyaratan Administratif
                                                            </h3>
                                                        </div>
                                                        <i class="fa fa-arrow-circle-right" id="dev" style="float:right; padding-top: 15px; color:white;" onclick="myFunction()"></i>
                                                    </div>

                                                    <div class="card-body" id="mydiv" style="overflow-y: scroll; height: 200px; display: none;">
                                                        <div class="m-portlet__body">
                                                            <table class="table table-hover table-bordered" id="drp1">
                                                                <thead>
                                                                    <tr style="text-align: center; background:#158bb9; color: white;">
                                                                        <th> Name </th>
                                                                        <th> Catatan </th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody></tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="card" style="border:double; margin-bottom: 20px;">
                                                    <div class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm" style="background: #2860a8;">
                                                        <div class="kt-portlet__head-label">
                                                            <h3 class="kt-portlet__head-title" style="font-size: small; color:white">
                                                                Kajian Persyaratan Farmestik
                                                            </h3>
                                                        </div>
                                                        <i class="fa fa-arrow-circle-right" id="dev2" style="float:right; padding-top: 15px; color:white;" onclick="myFunction2()"></i>
                                                    </div>

                                                    <div class="card-body" id="mydiv2" style="overflow-y: scroll; height: 200px; display: none;">
                                                        <div class="m-portlet__body">
                                                            <table class="table table-hover table-bordered" id="drp2">
                                                                <thead>
                                                                    <tr style="text-align: center; background:#158bb9; color: white;">
                                                                        <th> Name </th>
                                                                        <th> Catatan </th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody></tbody>
                                                            </table>

                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="card" style="border:double; margin-bottom: 20px;">
                                                    <div class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm" style="background: #2860a8;">
                                                        <div class="kt-portlet__head-label">
                                                            <h3 class="kt-portlet__head-title" style="font-size: small; color:white">
                                                                Kajian Persyaratan Klinis
                                                            </h3>
                                                        </div>
                                                        <i class="fa fa-arrow-circle-right" id="dev3" style="float:right; padding-top: 15px; color:white;" onclick="myFunction3()"></i>
                                                    </div>

                                                    <div class="card-body" id="mydiv3" style="overflow-y: scroll; height: 200px; display: none;">
                                                        <div class="m-portlet__body">
                                                            <table class="table table-hover table-bordered" id="drp3">
                                                                <thead>
                                                                    <tr style="text-align: center; background:#158bb9; color: white;">
                                                                        <th> Name </th>
                                                                        <th> Catatan </th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody></tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Kajian Administratif -->
                                            <!-- <div class="card-body" style="padding-bottom: 0px"> -->
                                            <!-- <div class="kt-portlet__body" style="padding-bottom: 0px;">

                                                    <table class="table table-striped- table-bordered table-hover" id="user-table">
                                                        <thead>
                                                            <tr style="background:#2860a8;text-align: center;">
                                                                <th style="color:white">Kajian Persyaratan Farmestik <i class="fa fa-arrow-circle-down" id="dev2" style="float:right; padding-top: 4px;" onclick="myFunction2()">
                                                                    </i></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr class="no-data" id="mydiv2">
                                                                <td>
                                                                    <div class="form-group m-form__group row">
                                                                        <label class="col-xs-5 col-md-3 col-form-label">
                                                                            Nama Obat :
                                                                        </label>
                                                                       
                                                                        <label class="col-md-2 col-form-label">
                                                                            CATATAN :
                                                                        </label>
                                                                        <div class="col-md-5">
                                                                            <input name="" type="text" class="form-control m-input" id="" placeholder="">
                                                                        </div>

                                                                    </div>
                                                                    <div class="form-group m-form__group row">
                                                                        <label class="col-xs-5 col-md-3 col-form-label">
                                                                            Bentuk Sediaan :
                                                                        </label>
                                                                       
                                                                        <label class="col-md-2 col-form-label">
                                                                            CATATAN :
                                                                        </label>
                                                                        <div class="col-md-5">
                                                                            <input name="" type="text" class="form-control m-input" id="" placeholder="">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group m-form__group row">
                                                                        <label class="col-xs-5 col-md-3 col-form-label">
                                                                            Kekuatan Sediaan :
                                                                        </label>
                                                                        
                                                                        <label class="col-md-2 col-form-label">
                                                                            CATATAN :
                                                                        </label>
                                                                        <div class="col-md-5">
                                                                            <input name="" type="text" class="form-control m-input" id="" placeholder="">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group m-form__group row">
                                                                        <label class="col-xs-5 col-md-3 col-form-label">
                                                                            Dosis :
                                                                        </label>
                                                                        
                                                                        <label class="col-md-2 col-form-label">
                                                                            CATATAN :
                                                                        </label>
                                                                        <div class="col-md-5">
                                                                            <input name="" type="text" class="form-control m-input" id="" placeholder="">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group m-form__group row">
                                                                        <label class="col-xs-5 col-md-3 col-form-label">
                                                                            Jumlah Obat :
                                                                        </label>
                                                                     
                                                                        <label class="col-md-2 col-form-label">
                                                                            CATATAN :
                                                                        </label>
                                                                        <div class="col-md-5">
                                                                            <input name="" type="text" class="form-control m-input" id="" placeholder="">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group m-form__group row">
                                                                        <label class="col-xs-5 col-md-3 col-form-label">
                                                                            Aturan & Cara Penggunaan :
                                                                        </label>
                                                                      
                                                                        <label class="col-md-2 col-form-label">
                                                                            CATATAN :
                                                                        </label>
                                                                        <div class="col-md-5">
                                                                            <input name="" type="text" class="form-control m-input" id="" placeholder="">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group m-form__group row">
                                                                        <label class="col-xs-5 col-md-3 col-form-label">
                                                                            Stabilitas :
                                                                        </label>
                                                                        
                                                                        <label class="col-md-2 col-form-label">
                                                                            CATATAN :
                                                                        </label>
                                                                        <div class="col-md-5">
                                                                            <input name="" type="text" class="form-control m-input" id="" placeholder="">
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div> -->
                                            <!-- </div> -->

                                            <!-- Kajian Klinis -->


                                            <!-- Catatan Kajian-->
                                            <!-- <div class="m-portlet">
                                        <div class="card" style="margin-top:20px">
                                            <div class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                                                <div class="kt-portlet__head-label">
                                                    <h3 class="kt-portlet__head-title">
                                                        Catatan Kajian Resep
                                                    </h3>
                                                </div>
                                            </div>
                                            <div class="card-body">
                                                <div class="m-portlet__body">
                                                    <div class="form-group m-form__group row">
                                                        <label for="drp" class="col-md-2 col-form-label">
                                                            DRP :
                                                        </label>
                                                        <div class="col-md-9">
                                                            <textarea class="form-control" rows="5" id="drp" name="text"></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="form-group m-form__group row">
                                                        <label for="drp" class="col-md-2 col-form-label">
                                                            Tindak Lanjut :
                                                        </label>
                                                        <div class="col-md-9">
                                                            <textarea class="form-control" rows="5" id="drp" name="text"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div> -->

                                        </div>
                                    </div>
                                </div>


                                <!--Kajian-->
                                <div class="col-md-6">
                                    <div class="m-portlet">
                                        <div class="card">
                                            <div class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                                                <div class="kt-portlet__head-label">
                                                    <h3 class="kt-portlet__head-title">
                                                        Informasi Obat
                                                    </h3>
                                                </div>
                                            </div>
                                            <div class="card-body" style="height:392px; overflow-y: scroll;">
                                                <div class="m-portlet__body">
                                                    <div class="table-responsive">
                                                        <table class="table" id="detob">
                                                            <thead>
                                                                <tr style="background: #2860a8; color:white;text-align:center">
                                                                    <th></th>
                                                                    <th>KODE</th>
                                                                    <th>OBAT</th>
                                                                    <th>SIGNA</th>
                                                                    <th>KET</th>
                                                                    <th>JUMLAH</th>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td colspan="99" style="text-align:center;padding:10px">NO DATA</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="m-portlet">
                                        <div class="card" style="margin-top:20px">
                                            <div class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                                                <div class="kt-portlet__head-label">
                                                    <h3 class="kt-portlet__head-title">
                                                        Catatan Kajian Resep
                                                    </h3>
                                                </div>
                                            </div>
                                            <div class="card-body">
                                                <div class="m-portlet__body">
                                                    <div class="form-group m-form__group row">
                                                        <label for="drp" class="col-md-2 col-form-label">
                                                            DRP :
                                                        </label>
                                                        <div class="col-md-9">
                                                            <textarea class="form-control" style="height: 75px;" rows="5" id="drp" name="text"></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="form-group m-form__group row">
                                                        <label for="drp" class="col-md-2 col-form-label">
                                                            Tindak Lanjut :
                                                        </label>
                                                        <div class="col-md-9">
                                                            <textarea class="form-control" rows="5" style="height: 95px" id="tindak_lanjut" name="text"></textarea>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <!-- <label for="">
                                
                                        // var_dump($_SESSION);die();
                                        if (isset($_SESSION["ses_username"])) {
                                            echo str_replace(
                                                '\' ',
                                                '\'',
                                                ucwords(
                                                    str_replace(
                                                        '\'',
                                                        '\' ',
                                                        strtolower($_SESSION["ses_username"])
                                                    )
                                                )
                                            );
                                        } else {
                                            redirect(base_url() . 'index.php/login', $data);
                                        }
                                        
                                </label> -->
                                    </div>


                                    <!-- <div class="m-portlet">
                                        <div class="card">
                                            <div class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                                                <div class="kt-portlet__head-label">
                                                    <h3 class="kt-portlet__head-title">
                                                        Kajian
                                                    </h3>
                                                </div>
                                            </div>

                                            <div class="card-body" style="padding-bottom: 0px">
                                                <div class="card" style="border:double; margin-bottom: 20px;">

                                                    <div class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm" style="background: #2860a8;">
                                                        <div class="kt-portlet__head-label">
                                                            <h3 class="kt-portlet__head-title" style="font-size: small; color: white;">
                                                                Kajian Persyaratan Administratif
                                                            </h3>
                                                        </div>
                                                        <i class="fa fa-arrow-circle-right" id="dev" style="float:right; padding-top: 15px; color:white;" onclick="myFunction()"></i>
                                                    </div>

                                                    <div class="card-body" id="mydiv" style="overflow-y: scroll; height: 200px; display: none;">
                                                        <div class="m-portlet__body">
                                                            <table class="table table-hover table-bordered" id="drp1">
                                                                <thead>
                                                                    <tr style="text-align: center; background:#158bb9; color: white;">
                                                                        <th> Name </th>
                                                                        <th> Catatan </th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody></tbody>
                                                            </table> -->
                                    <!-- <div class="form-group m-form__group row">
                                                                <label class="col-xs-5 col-md-3 col-form-label">
                                                                    Nama Pasien :
                                                                </label>

                                                                <label class="col-md-2 col-form-label">
                                                                    CATATAN :
                                                                </label>
                                                                <div class="col-md-5">
                                                                    <input name="" type="text" class="form-control m-input" id="namapx" placeholder="">
                                                                </div>
                                                            </div>

                                                            <div class="form-group m-form__group row">
                                                                <label class="col-xs-5 col-md-3 col-form-label">
                                                                    Umur :
                                                                </label>

                                                                <label class="col-md-2 col-form-label">
                                                                    CATATAN :
                                                                </label>
                                                                <div class="col-md-5">
                                                                    <input name="" type="text" class="form-control m-input" id="umurpx" placeholder="">
                                                                </div>
                                                            </div>
                                                            <div class="form-group m-form__group row">
                                                                <label class="col-xs-5 col-md-3 col-form-label">
                                                                    Jenis Kelamin :
                                                                </label>

                                                                <label class="col-md-2 col-form-label">
                                                                    CATATAN :
                                                                </label>
                                                                <div class="col-md-5">
                                                                    <input name="" type="text" class="form-control m-input" id="genderpx" placeholder="">
                                                                </div>
                                                            </div>
                                                            <div class="form-group m-form__group row">
                                                                <label class="col-xs-5 col-md-3 col-form-label">
                                                                    BB / TB :
                                                                </label>

                                                                <label class="col-md-2 col-form-label">
                                                                    CATATAN :
                                                                </label>
                                                                <div class="col-md-5">
                                                                    <input name="" type="text" class="form-control m-input" id="beratpx" placeholder="">
                                                                </div>
                                                            </div>
                                                            <div class="form-group m-form__group row">
                                                                <label class="col-xs-5 col-md-3 col-form-label">
                                                                    Nama & Paraf Dokter :
                                                                </label>

                                                                <label class="col-md-2 col-form-label">
                                                                    CATATAN :
                                                                </label>
                                                                <div class="col-md-5">
                                                                    <input name="" type="text" class="form-control m-input" id="nama_paraf_dokter" placeholder="">
                                                                </div>
                                                            </div>
                                                            <div class="form-group m-form__group row">
                                                                <label class="col-xs-5 col-md-3 col-form-label">
                                                                    No. SIP :
                                                                </label>

                                                                <label class="col-md-2 col-form-label">
                                                                    CATATAN :
                                                                </label>
                                                                <div class="col-md-5">
                                                                    <input name="" type="text" class="form-control m-input" id="sippx" placeholder="">
                                                                </div>
                                                            </div>
                                                            <div class="form-group m-form__group row">
                                                                <label class="col-xs-5 col-md-3 col-form-label">
                                                                    Tanggal Resep :
                                                                </label>

                                                                <label class="col-md-2 col-form-label">
                                                                    CATATAN :
                                                                </label>
                                                                <div class="col-md-5">
                                                                    <input name="" type="text" class="form-control m-input" id="tanggal_reseppx" placeholder="">
                                                                </div>
                                                            </div> -->
                                    <!-- </div>
                                                    </div>
                                                </div>

                                                <div class="card" style="border:double; margin-bottom: 20px;">
                                                    <div class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm" style="background: #2860a8;">
                                                        <div class="kt-portlet__head-label">
                                                            <h3 class="kt-portlet__head-title" style="font-size: small; color:white">
                                                                Kajian Persyaratan Farmestik
                                                            </h3>
                                                        </div>
                                                        <i class="fa fa-arrow-circle-right" id="dev2" style="float:right; padding-top: 15px; color:white;" onclick="myFunction2()"></i>
                                                    </div>

                                                    <div class="card-body" id="mydiv2" style="overflow-y: scroll; height: 200px; display: none;">
                                                        <div class="m-portlet__body">
                                                            <table class="table table-hover table-bordered" id="drp2">
                                                                <thead>
                                                                    <tr style="text-align: center; background:#158bb9; color: white;">
                                                                        <th> Name </th>
                                                                        <th> Catatan </th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody></tbody>
                                                            </table>

                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="card" style="border:double; margin-bottom: 20px;">
                                                    <div class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm" style="background: #2860a8;">
                                                        <div class="kt-portlet__head-label">
                                                            <h3 class="kt-portlet__head-title" style="font-size: small; color:white">
                                                                Kajian Persyaratan Klinis
                                                            </h3>
                                                        </div>
                                                        <i class="fa fa-arrow-circle-right" id="dev3" style="float:right; padding-top: 15px; color:white;" onclick="myFunction3()"></i>
                                                    </div>

                                                    <div class="card-body" id="mydiv3" style="overflow-y: scroll; height: 200px; display: none;">
                                                        <div class="m-portlet__body">
                                                            <table class="table table-hover table-bordered" id="drp3">
                                                                <thead>
                                                                    <tr style="text-align: center; background:#158bb9; color: white;">
                                                                        <th> Name </th>
                                                                        <th> Catatan </th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody></tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> -->
                                    <!-- Kajian Administratif -->
                                    <!-- <div class="card-body" style="padding-bottom: 0px"> -->
                                    <!-- <div class="kt-portlet__body" style="padding-bottom: 0px;">

                                                    <table class="table table-striped- table-bordered table-hover" id="user-table">
                                                        <thead>
                                                            <tr style="background:#2860a8;text-align: center;">
                                                                <th style="color:white">Kajian Persyaratan Farmestik <i class="fa fa-arrow-circle-down" id="dev2" style="float:right; padding-top: 4px;" onclick="myFunction2()">
                                                                    </i></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr class="no-data" id="mydiv2">
                                                                <td>
                                                                    <div class="form-group m-form__group row">
                                                                        <label class="col-xs-5 col-md-3 col-form-label">
                                                                            Nama Obat :
                                                                        </label>
                                                                       
                                                                        <label class="col-md-2 col-form-label">
                                                                            CATATAN :
                                                                        </label>
                                                                        <div class="col-md-5">
                                                                            <input name="" type="text" class="form-control m-input" id="" placeholder="">
                                                                        </div>

                                                                    </div>
                                                                    <div class="form-group m-form__group row">
                                                                        <label class="col-xs-5 col-md-3 col-form-label">
                                                                            Bentuk Sediaan :
                                                                        </label>
                                                                       
                                                                        <label class="col-md-2 col-form-label">
                                                                            CATATAN :
                                                                        </label>
                                                                        <div class="col-md-5">
                                                                            <input name="" type="text" class="form-control m-input" id="" placeholder="">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group m-form__group row">
                                                                        <label class="col-xs-5 col-md-3 col-form-label">
                                                                            Kekuatan Sediaan :
                                                                        </label>
                                                                        
                                                                        <label class="col-md-2 col-form-label">
                                                                            CATATAN :
                                                                        </label>
                                                                        <div class="col-md-5">
                                                                            <input name="" type="text" class="form-control m-input" id="" placeholder="">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group m-form__group row">
                                                                        <label class="col-xs-5 col-md-3 col-form-label">
                                                                            Dosis :
                                                                        </label>
                                                                        
                                                                        <label class="col-md-2 col-form-label">
                                                                            CATATAN :
                                                                        </label>
                                                                        <div class="col-md-5">
                                                                            <input name="" type="text" class="form-control m-input" id="" placeholder="">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group m-form__group row">
                                                                        <label class="col-xs-5 col-md-3 col-form-label">
                                                                            Jumlah Obat :
                                                                        </label>
                                                                     
                                                                        <label class="col-md-2 col-form-label">
                                                                            CATATAN :
                                                                        </label>
                                                                        <div class="col-md-5">
                                                                            <input name="" type="text" class="form-control m-input" id="" placeholder="">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group m-form__group row">
                                                                        <label class="col-xs-5 col-md-3 col-form-label">
                                                                            Aturan & Cara Penggunaan :
                                                                        </label>
                                                                      
                                                                        <label class="col-md-2 col-form-label">
                                                                            CATATAN :
                                                                        </label>
                                                                        <div class="col-md-5">
                                                                            <input name="" type="text" class="form-control m-input" id="" placeholder="">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group m-form__group row">
                                                                        <label class="col-xs-5 col-md-3 col-form-label">
                                                                            Stabilitas :
                                                                        </label>
                                                                        
                                                                        <label class="col-md-2 col-form-label">
                                                                            CATATAN :
                                                                        </label>
                                                                        <div class="col-md-5">
                                                                            <input name="" type="text" class="form-control m-input" id="" placeholder="">
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div> -->
                                    <!-- </div> -->

                                    <!-- Kajian Klinis -->
                                    <!-- </div>
                                    </div> -->

                                </div>

                            </div>
                            <div class="row">
                                <button type="button" id="simpandata" class="col-md-3 form-control m-input btn btn-success2" value="save" style=""><i class="fas fa-check">&nbsp;</i>Simpan</button>&nbsp;

                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>

<!-- javascript this page -->
<script src="<?php echo base_url() ?>assets/assets_metronic/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
<!-- <script src="<?php //echo base_url()
                    ?>application/modules/User/views/user_view.js" type="text/javascript"></script> -->
<!-- <td colspan="99" style="text-align:center;padding:10px">NO DATA</td> -->
<script src="<?php echo base_url() ?>application/modules/Telaah_resep/views/telaahresep_view.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/assets_metronic/vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js" type="text/javascript"></script>