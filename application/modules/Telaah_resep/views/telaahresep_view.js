"use strict";

var TelaahResep = function() {

    return {

        init: function() {
            var d = new Date();
            var month = d.getMonth() + 1;
            month = (month < 10 ? 0 + month : month);
            var day = d.getDate();
            day = (day < 10 ? 0 + day : day);

            var dates = d.getFullYear() + "" + month + "" + day;
            $('#datepicker ').val(day + "/" + month + "/" + d.getFullYear());
            // console.log(dates);

            tabelVerifikasi(dates)
        }
    };

}();

function tabelVerifikasi(tgl) {
    // var tgl = '2018-08-18';
    $('#tabelTelaah').DataTable({
        "responsive": true,
        "lengthMenu": [5, 10, 25, 50],
        "pageLength": 5,
        "destroy": true,
        "processing": true,
        "type": "GET",
        "ajax": 'telaah_resep/tabelVerifikasi?tgl=' + tgl,
        "columns": [
            { "data": "cek" },
            { "data": "cetak" },
            { "data": "karcis" },
            { "data": "nota" },
            { "data": "tglResep" },
            { "data": "jam" },
            { "data": "antrianPasien" },
            { "data": "namaPasien" },
            { "data": "tglLahir" },
            { "data": "gender" },
            { "data": "namaDokter" },
            { "data": "sip" },
            { "data": "klinik" },
            { "data": "debitur" },
            { "data": "action" }
        ],
        "createdRow": function(row, data, index) {
            $('td', row).eq(0).addClass('text-center');
        },

    });
    $('#datepicker ').datepicker({
        autoclose: true,
        format: 'dd/mm/yyyy',
        changeMonth: true,
        changeYear: true,
    });

}
$('#datepicker').change(function() {
    var x = $('#datepicker').val();
    var xx = x.split("/");
    var changedate = xx[2] + "" + xx[1] + "" + xx[0];
    tabelVerifikasi(changedate);
})


function myFunction() {

    var x = document.getElementById("mydiv");
    var y = document.getElementById("dev");
    // console.log(x)
    if (x.style.display === "none") {
        x.style.display = "block";
        y.classList.add("fa-arrow-circle-down");
        y.classList.remove("fa-arrow-circle-right");
    } else {
        x.style.display = "none";
        y.classList.add("fa-arrow-circle-right");
        y.classList.remove("fa-arrow-circle-down");
    }

}

function myFunction2() {
    var x = document.getElementById("mydiv2");
    var y = document.getElementById("dev2");
    if (x.style.display === "none") {
        // console.log(x.style.display)
        x.style.display = "block";
        y.classList.add("fa-arrow-circle-down");
        y.classList.remove("fa-arrow-circle-right");

    } else {
        x.style.display = "none";
        y.classList.add("fa-arrow-circle-right");
        y.classList.remove("fa-arrow-circle-down");
    }

}

function myFunction3() {
    var x = document.getElementById("mydiv3");
    var y = document.getElementById("dev3");
    if (x.style.display === "none") {
        // console.log(x.style.display)
        x.style.display = "block";
        y.classList.add("fa-arrow-circle-down");
        y.classList.remove("fa-arrow-circle-right");

    } else {
        x.style.display = "none";
        y.classList.add("fa-arrow-circle-right");
        y.classList.remove("fa-arrow-circle-down");
    }

}

function loading() {
    KTApp.blockPage({
        overlayColor: '#000000',
        type: 'v2',
        state: 'primary',
        message: 'Processing...'
    });
}

$('#simpandata').click(function() {

    // if ($('#drp').val() == "" || $('#drp').val() == null) {
    //     swal.fire('Peringatan', 'Belum mengisi', 'warning');
    // }
    // if ($('#tindak_lanjut').val() == "" || $('#tindak_lanjut').val() == null) {
    //     swal.fire('Peringatan', 'Belum mengisi', 'warning');
    // } else {
    loading()
    var kajian = []
    $('.param').each(function() {
        kajian.push({
            id: $(this).attr('data-id'),
            catatan: $(this).val(),
            ada_telaah: $(this).val() == "" ? 0 : 1
        })


    });
    var data = {
        drp: $('#drp').val(),
        id_telaah: $('#id_telaah').val(),
        tindak_lanjut: $('#tindak_lanjut').val(),
        id_trans: $('#id_trans').val(),
        kajian: kajian,

    }

    $.ajax({
        type: "POST",
        url: "telaah_resep/simpanData",
        data: data,
        // drp: drp,
        // tindak_lanjut: tindak_lanjut,
        // id_trans: id_trans,
        // kajian: kajian,
        // id_telaah: id_telaah
        dataType: 'json',
        async: false,
        success: function(data) {
            console.log(data);
            var x = $('#datepicker').val();
            var xx = x.split("/");
            var changedate = xx[2] + "" + xx[1] + "" + xx[0];
            tabelVerifikasi(changedate);
            KTApp.unblockPage();
            swal.fire(data.status, data.message, data.status);
            clear();
        },
        error: function(xhr, status, error) {
            // console.log(xhr);
        }
    });

});



function informasi_pendaftaran(id_trans) {

    KTApp.blockPage({
        overlayColor: '#000000',
        type: 'v2',
        state: 'primary',
        message: 'Processing...'
    });
    $("#detob tbody").html('');
    // // console.log(id_trans);
    // // var data = {
    // //     id_trans: id_trans,
    // }
    var data_obat = "";
    $.ajax({
        type: "POST",
        url: "telaah_resep/tabelPendaftaran",
        // dataType: 'json',
        data: {
            id_trans: id_trans
        },
        success: function(data) {
            var datas = JSON.parse(data);
            data_obat = datas.list_obat;
            console.log(datas);
            //

            jQuery.each(data_obat, function(index, val) {
                var a = "";
                a =
                    '<tr style="text-align: left;">' +
                    "<td>" + val.INFO + "</td>" +
                    "<td>" + val.KODE + "</td>" +
                    "<td>" + val.OBAT + "</td>" +
                    "<td>" + val.SIGNA + "</td>" +
                    "<td>" + val.KET + "</td>" +
                    "<td>" + val.JUMLAH + "</td>" +
                    "</tr>";
                $("#detob tbody").append(a);
            });

            $('#noan').val(datas.detail_px[0].ANTRIAN);
            $('#nama_pasien').val(datas.detail_px[0].NAMA);
            $('#umur').val(datas.detail_px[0].UMUR);
            $('#gender').val(datas.detail_px[0].GENDER);
            $('#beban').val(datas.detail_px[0].BERAT_BADAN);
            $('#dokter').val(datas.detail_px[0].DOKTER);
            $('#sip').val(datas.detail_px[0].SIP);
            $('#klinik').val(datas.detail_px[0].KLINIK);
            $('#id_trans').val(datas.detail_px[0].ID_TRANS);

            // $('#drp').val
            // $('#tindak_lanjut')
            // datas.list_obat.each(function(val) {
            //     console.log(val);
            // })
            if (datas.exist != null) {
                $('#id_telaah').val(datas.exist[0].ID_TRANS_FARKLIN);
            } else {
                $('#id_telaah').val("");
            }

            if (datas.exist != null) {
                $('#drp').val(datas.exist[0].DRP);
                $('#tindak_lanjut').val(datas.exist[0].TINDAK_LANJUT);
            } else {
                $('#drp').val('');
                $('#tindak_lanjut').val('');
            }

            $('#drp1 tbody tr').remove();
            $('#drp2 tbody tr').remove();
            $('#drp3 tbody tr').remove();

            // $('#drp').val('');

            // $('#drp').val('fsfs');
            // $('#tindak_lanjut').val(datas.exist[0].TINDAK_LANJUT);

            $('#drp1 tbody').append(datas.drp1);
            $('#drp2 tbody').append(datas.drp2);
            $('#drp3 tbody').append(datas.drp3);
            KTApp.unblockPage();

        },
        error: function(xhr, status, error) {
            // alert("ERROR");
        }
    });
    // $('#nama_pasien').val(id_trans);

};
$('#asd').click(function() {
    console.log('asdads');
});

function clear() {
    $('#drp1 input').val('');
    $('#drp2 input').val('');
    $('#drp3 input').val('');

    $('#drp').val('');
    $('#tindak_lanjut').val('');

}
jQuery(document).ready(function() {
    TelaahResep.init();
});