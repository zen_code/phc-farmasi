<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class telaah_model extends CI_Model
{
  public function __construct()
  {
    // Call the CI_Model constructor
    parent::__construct();
    $this->load->database();
  }

  public function getPrivelege($id)
  {
    $data = [];
    $profile = $this->db->query("select * from APP_PROFIL where ID_PROFIL = " . $id);
    $data['profile'] = $profile->result();

    return $data;
  }
  public function allmenu($app_id)
  {
    $data = [];

    $menu = $this->db->query("select * from APP_MASTER_MENU_APLIKASI WHERE PARENT_MENU is null and APLIKASI =" . $app_id);
    foreach ($menu->result() as $x => $item) {
      $data[$x]['id'] = $item->ID_MENU;
      $data[$x]['name'] = $item->NAMA_MODUL;
      $data[$x]['parent'] = $item->PARENT_MENU;

      $child = $this->db->query("select * from APP_MASTER_MENU_APLIKASI WHERE PARENT_MENU=" . $item->ID_MENU);
      foreach ($child->result() as $y => $itemy) {
        $data[$x]['child'][$y]['id'] = $itemy->ID_MENU;
        $data[$x]['child'][$y]['name'] = $itemy->NAMA_MODUL;
        $data[$x]['child'][$y]['parent'] = $itemy->PARENT_MENU;
      }
    }
    return $data;
  }
  public function getUser($id)
  {
    $sql = $this->db->query("select * from APP_USER_PROFIL WHERE PROFIL = " . $id);
    return $sql->result();
  }
  public function profilemenu($app_id)
  {
    $menu = $this->db->query("select * from APP_PRIVILEGES_PROFIL WHERE PROFIL = " . $app_id);
    return  $menu->result();
  }

  public function get_datajadwal($tgl)
  {
    // $this->load->database();
    // $sql = "select top 100 * 
    // from IF_HTRANS_OL
    // order by TGL desc";
    // // $query = $this->db->get('IF_HTRANS_OL');
    // return $this->db->query($sql);  
    // // return $this->db->get('IF_HTRANS_OL');
    // $sql = "exec if_sp_baru_farklin_jalan_tampilkan_list_eresep 0, NULL";
    $sql = "exec if_sp_baru_farklin_jalan_tampilkan_list_eresep '','" . $tgl . "',''";
    // var_dump($sql);die();
    return $this->db->query($sql);
  }

  public function get_data_informasi_pendaftaran($id_trans)
  {
    $res = [];
    // $this->load->database();
    $sql = "exec if_sp_informasi_pendaftaran '" . $id_trans . "'";
    $hasil = $this->db->query($sql);
    $res['detail_px'] = $hasil->result();

    // $sql = "exec if_sp_baru_farklin_jalan_tampilkan_list_eresep 1, '".$id_trans."'";
    $sql = "exec if_sp_baru_farklin_jalan_tampilkan_list_eresep '" . $id_trans . "','',''";
    $hasil = $this->db->query($sql);
    $res['list_obat'] = $hasil->result();
    // var_dump($res);die();

    $exist = $this->db->query("select * from IF_FARKLIN_HTRANS where ID_TRANS_RJ_ONLINE = '" . $id_trans . "'");
    $drp1 = "";
    $drp2 = "";
    $drp3 = "";
    if ($exist->num_rows() > 0) {
      
      $res['exist'] = $exist->result();
      //drp
      $drp = $this->db->query("select b.ID_PARAMETER_SYARAT, b.ID_PERSYARATAN, a.CATATAN, b.KETERANGAN FROM IF_FARKLIN_TRANS a
                        join IF_FARKLIN_MPARAMETER b on a.PARAMETER = b.ID_PARAMETER_SYARAT
                        WHERE ID_TRANS_FARKLIN='".$exist->row()->ID_TRANS_FARKLIN."'");
       //var_dump($drp->result());die();
      foreach ($drp->result() as $item) {
        if ($item->ID_PERSYARATAN == 1) {
          $drp1 .= "<tr><td><label>" . $item->KETERANGAN . "</label></td><td><input type='text' class='form-control param' data-id='" . $item->ID_PARAMETER_SYARAT . "' value='". $item->CATATAN."'></td></tr>";
        }
        if ($item->ID_PERSYARATAN == 2) {
          $drp2 .= "<tr><td><label>" . $item->KETERANGAN . "</label></td><td><input type='text' class='form-control param' data-id='" . $item->ID_PARAMETER_SYARAT . "' value='". $item->CATATAN."'></td></tr>";
        } else if ($item->ID_PERSYARATAN == 3) {
          $drp3 .= "<tr><td><label>" . $item->KETERANGAN . "</label></td><td><input type='text' class='form-control param' data-id='" . $item->ID_PARAMETER_SYARAT . "' value='". $item->CATATAN."'></td></tr>";
        }
      }

    } else {
      $res['exist'] = null;
      $drp = $this->db->query("select * from IF_FARKLIN_MPARAMETER");
      foreach ($drp->result() as $item) {
        if ($item->ID_PERSYARATAN == 1) {
          $drp1 .= "<tr><td><label>" . $item->KETERANGAN . "</label></td><td><input type='text' class='form-control param' data-id='" . $item->ID_PARAMETER_SYARAT . "'></td></tr>";
        }
        if ($item->ID_PERSYARATAN == 2) {
          $drp2 .= "<tr><td><label>" . $item->KETERANGAN . "</label></td><td><input type='text' class='form-control param' data-id='" . $item->ID_PARAMETER_SYARAT . "'></td></tr>";
        } else if ($item->ID_PERSYARATAN == 3) {
          $drp3 .= "<tr><td><label>" . $item->KETERANGAN . "</label></td><td><input type='text' class='form-control param' data-id='" . $item->ID_PARAMETER_SYARAT . "'></td></tr>";
        }
      }
    }
    // var_dump($drp1);
    $res['drp1'] = $drp1;
    $res['drp2'] = $drp2;
    $res['drp3'] = $drp3;
    // var_dump($res);die();
    return $res;
  }

  public function get_penyimpanan_data($datax)
  {
    $res = [];
    $sql1 = $this->db->query("exec IF_SP_GETAUTOIDTRANS_FARKLIN_2");
    $id_telaah = $sql1->row()->ID_GENERATE;

    // print_r($hasil1);
    // exit();
    $sql3 = "UPDATE IF_HTRANS_OL SET TELAAH_RESEP=1 WHERE ID_TRANS='". $datax['id_trans'] ."'";
    $this->db->query($sql3);

    $res['status'] = 'sukses';
    $res['message'] = 'Sukses  ';

    if ($datax['telaah'] == "") {
      //CREATE
      $sql1 = $this->db->query("exec IF_SP_GETAUTOIDTRANS_FARKLIN_2");
      $id_telaah = $sql1->row()->ID_GENERATE;

      $sql = "INSERT INTO IF_FARKLIN_HTRANS(ID_TRANS_FARKLIN,ID_TRANS_RJ_ONLINE,DRP,TINDAK_LANJUT,INPUTBY,INPUTDATE) 
              VALUES ('" . $id_telaah . "','" . $datax['id_trans'] . "','" . $datax['drp'] . "','" . $datax['tindak_lanjut'] . "','" . $_SESSION['if_ses_username'] . "',GETDATE())";
      $hasil = $this->db->query($sql);

      foreach ($datax['kajian'] as $kajian) {
        $sql = $this->db->query("INSERT INTO IF_FARKLIN_TRANS(ID_TRANS_FARKLIN,PARAMETER,ADA_TELAAH,CATATAN,ACTIVE) 
            VALUES ('" . $id_telaah . "','" . $kajian['id'] . "','" . $kajian['ada_telaah'] . "','" . $kajian['catatan'] . "','1')");
      }
      $res['status'] = 'sukses';
      $res['message'] = 'Sukses Tambah Create';
    } else {
    
      //UPDATE
      $sql2 = "UPDATE IF_FARKLIN_HTRANS SET DRP= '" . $datax['drp'] . "', TINDAK_LANJUT= '" . $datax['tindak_lanjut'] . "', MODIBY='".$_SESSION['if_ses_username']."', MODIDATE=GETDATE()
      WHERE ID_TRANS_FARKLIN = '" .$datax['telaah']. "'";
      $hasil2 = $this->db->query($sql2);
      // var_dump($datax['kajian']);die();
      foreach ($datax['kajian'] as $kajian) {
        $this->db->query("update IF_FARKLIN_TRANS set catatan = '".$kajian['catatan']."' where ID_TRANS_FARKLIN='".$datax['telaah']."' and parameter = '".$kajian['id']."'");
      }

      $res['status'] = 'sukses';
      $res['message'] = 'Sukses UPDATE';


    }
    return $res;
  }
}
