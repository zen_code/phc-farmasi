<?php
defined('BASEPATH') or exit('No direct script access allowed');

class telaah_resep extends CI_Controller
{
	public $urlws = null;
	public function __construct()
	{
		parent::__construct();
		$this->load->model('telaah_model');
		$this->load->library('session');
		$this->urlws = $this->globals->ws_phc();
	}
	private function _render($view, $data = array())
	{
		$this->load->view('header', $data);
		// $this->load->view('body_header');
		// $this->load->view('sidebar');
		$this->load->view($view, $data);
		$this->load->view('footer');
	}

	public function index()
	{
		//load library
		$uri = &load_class('URI', 'core');
		redirect(base_url() . 'index.php/telaah_resep/data', $data);
	}

	public function data()
	{
		// var_dump($_SESSION["ses_userid"]);die();
		if($_SESSION["if_ses_depo"] == null){
			redirect(base_url().'depo');
		}else{
			if (in_array(6, $_SESSION['if_ses_menu'])) {
				$data['parent_active'] = 6;
				$data['child_active'] = 9;
				$this->_render('telaahresep_view', $data);
			} else {
				redirect(base_url() . 'beranda/data');
			}
		}
	}

	public function tabelVerifikasi()
	{

		$tgl = $this->input->get('tgl');
		// var_dump($this->input->get());die();
		$query = $this->telaah_model->get_datajadwal($tgl);
		$data 			= array();
		foreach ($query->result() as $item) {
			$silang		= '<i class="fa fa-times"></i>';
			$centang	= '<i class="fa fa-check"></i>';
			$btn		= '<button name="btnproses id="btnproses" type="button" class="btn btn-success btn-sm" data-toggle="kt-tooltip" data-placement="top" title="Choose" onclick="informasi_pendaftaran(' . "'" . $item->ID_TRANS . "'" . ')"> <i class="fa fa-check"></i></button>';
			$data[] 	= array(
				"cek" 			=> $item->CEK == 0 ? $silang : $centang,
				"cetak" 		=> $item->CETAK == 0 ? $silang : $centang,
				"karcis"		=> $item->KARCIS,
				"nota"			=> $item->NOTA,
				"tglResep"		=> $item->TGL,
				"jam"			=> $item->JAM,
				"antrianPasien"	=> $item->ANTRIAN,
				"namaPasien"	=> $item->NAMA,
				"tglLahir"		=> $item->TGL_LAHIR,
				"gender"		=> $item->GENDER,
				"namaDokter"	=> $item->DOKTER,
				"sip"			=> $item->SIP,
				"klinik"		=> $item->KLINIK,
				"debitur"		=> $item->DEBITUR,
				"action"		=> $btn

			);
		}

		$obj = array("data" => $data);

		echo json_encode($obj);
	}

	public function simpanData()
	{
		$data = array(
			"drp"     => $this->input->post('drp'),
			"tindak_lanjut" => $this->input->post('tindak_lanjut'),
			"id_trans" => $this->input->post('id_trans'),
			"kajian" => $this->input->post('kajian'),
			"telaah" => $this->input->post('id_telaah'),
		);
		$query = $this->telaah_model->get_penyimpanan_data($data);
		echo json_encode($query);
		// $drp = $this->input->post('drp');
		// $tindak_lanjut = $this->input->post('tindak_lanjut');
		// print_r($drp);
		// exit();
		// $query=$this->telaah_model->get_penyimpanan_data();
	}

	public function tabelPendaftaran()
	{
		// var_dump($this->input->post());die();
		$id_trans = $this->input->post('id_trans');
		$query = $this->telaah_model->get_data_informasi_pendaftaran($id_trans);
		// var_dump($query);die();
		// $data['NAMA'] = $query->row()->NAMA;
		echo json_encode($query);
	}

	public function getAllUser()
	{
		$authorization = 'Authorization: Bearer' . $_SESSION["if_ses_token_centra"];
		$url = $this->urlwscentra . "dp/base/setting/user/list-user";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, array($authorization));
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$output = curl_exec($ch);
		curl_close($ch);
		$cek_output = json_decode($output);
		// var_dump(json_encode($cek_output));die();
		echo json_encode($cek_output);
	}
	public function getUser()
	{
		$id_profile = $this->input->post('ID_PROFIL');
		$query = $this->telaah_model->getUser($id_profile);
		// var_dump($query);die();
		echo json_encode($query);
	}
	public function getallmenu()
	{
		$query = $this->telaah_model->allmenu($this->app_id);
		echo json_encode($query);
		// return $query;
	}
	public function getprofilemenu()
	{
		$id_profile = $this->input->post('ID_PROFIL');
		$query = $this->telaah_model->profilemenu($id_profile);

		echo json_encode($query);
		// return $query;
	}
	public function addPrivAction()
	{

		// $id_profile = $this->input->post('id_profile');
		$data = array(
			"id_profile"     => $this->input->post('id_profile'),
			"user"     => $this->input->post('user'),
			"menu"     => $this->input->post('menu'),
			"user_id"     =>  $_SESSION["if_ses_userid"]
		);

		$data = http_build_query($data);
		// var_dump($data);die();
		$authorization = 'Authorization: ' . $_SESSION["if_ses_token_phc"];
		$url = $this->urlws . "Farmasi/telaah_resep/addPrivAction";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array($authorization));
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$output = curl_exec($ch);
		curl_close($ch);
		// var_dump($output);die();	
		$cek_output = json_decode($output);
		echo json_encode($cek_output);
	}
}
