<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class master_interaksi extends CI_Controller {
	public $urlws = null;
	public function __construct() {
        parent::__construct();
		$this->load->model('master_interaksi_model');
		$this->load->library('session');
		$this->urlws = $this->globals->ws_phc();

    }
	private function _render($view,$data = array())
	{
		$this->load->view('header',$data);
	    // $this->load->view('body_header');
	    // $this->load->view('sidebar');
	    $this->load->view($view,$data);
	    $this->load->view('footer');
	}

	public function index()
	{
		//load library
		// var_dump($this->session->all_userdata());die();
		$uri =& load_class('URI', 'core');
	  	redirect(base_url().'index.php/master_interaksi/data',$data);
	}

	public function data()
	{
		if($_SESSION["if_ses_depo"] == null){
			redirect(base_url().'depo');
		}else{
			if(in_array(3,$_SESSION['if_ses_menu'])){
				$data['parent_active'] = 3 ;
				$data['child_active'] = 40 ;
				$this->_render('master_interaksi_view', $data);
			}else{
				redirect(base_url().'beranda/data');}
		}
	}
	public function dataMaster()
	{
		// var_dump('asd');die();
		$authorization = 'Authorization:'.$_SESSION["if_ses_token_phc"];
		$url = $this->urlws."Farmasi/Interaksi/dataMaster";
		// $data = array(
		// 	"app_id" => $this->app_id
		// );
		$ch = curl_init();
		// curl_setopt($ch, CURLOPT_POST, 1);
		// curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array($authorization ));
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		$output = curl_exec($ch); 
		curl_close($ch);    
		$datas = json_decode($output);
		if($datas->status == true){
			$data 			= array();
			foreach($datas->data as $item){
				$btn 		='<button name="btnproses" id="btnproses" type="button" class="btn btn-success btn-sm" data-toggle="kt-tooltip" data-placement="top" title="Update"> <i class="fa fa-edit"></i> </button>
                            <button name="btnbatal" id="btnbatal" type="button" class="btn btn-danger btn-sm" data-toggle="kt-tooltip" data-placement="top" title="Delete"> <i class="fa fa-times"></i> </button>';
                
				$data[] 	= array(
					"KDINTERAKSI" 			=> $item->KDINTERAKSI,
					"INTERAKSI" 	=> $item->INTERAKSI,
					"JENIS" 		=> $item->JENIS ,
					"REKOMENDASI"	=> $item->REKOMENDASI,
					"btn"			=> $btn
				);
			}
			$obj = array("data"=> $data);

			// var_dump($data);die();
			echo json_encode($obj);
		}else{
			$error = array(
				'data' => [],
				'error' => $cek_output->message
			);
			
			echo json_encode($error);
		}
		
	}
	public function jenis()
	{
		// var_dump('asd');die();
		$authorization = 'Authorization:'.$_SESSION["if_ses_token_phc"];
		$url = $this->urlws."Farmasi/Interaksi/jenis";
		// $data = array(
		// 	"app_id" => $this->app_id
		// );
		$ch = curl_init();
		// curl_setopt($ch, CURLOPT_POST, 1);
		// curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array($authorization ));
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		$output = curl_exec($ch); 
		curl_close($ch);    
		$datas = json_decode($output);
		
		if($datas->status == true){
			$data 			= "";
			foreach($datas->data as $item){
				$data 	.= "<option value='".$item->KDJENIS_INTERAKSI."'>".$item->JENIS."</option>";
			}

			echo json_encode($data);
		}else{
			$error = array(
				'data' => [],
				'error' => $cek_output->message
			);
			
			echo json_encode($error);
		}
		
	}
	public function bahan(){
		// var_dump($this->input->post());die();
		$authorization = 'Authorization:'.$_SESSION["if_ses_token_phc"];
		$url = $this->urlws."Farmasi/Interaksi/bahan";
		$data = array(
			"kdBahan" => $this->input->post('kdBahan')
		);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array($authorization ));
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		$output = curl_exec($ch); 
		curl_close($ch);    
		$datas = json_decode($output);

		if($datas->status == true){
			$data 			= "";
			foreach($datas->data as $index => $item){
				$data 	.= "<tr data-index='".$index."' class='is_child'>
								<td width='5%' style='vertical-align:middle'>
									<label class='kt-checkbox' style='top:-4px' >
										<input class='checkbox_bahan' type='checkbox' id='".$item->KDBHN."'> 
										<span></span>
									</label>
								</td>
								<td style='vertical-align:middle'>".$item->NMBHN."</td>
							</tr>";
			}

			echo json_encode($datas->data);
		}else{
			$error = array(
				'data' => [],
				'error' => $cek_output->message
			);
			
			echo json_encode($error);
		}	
	}
	public function action(){
		// var_dump($this->input->post());die();	
		$authorization = 'Authorization:'.$_SESSION["if_ses_token_phc"];
		$url = $this->urlws."Farmasi/Interaksi/action";
		$data = array(
			"kode" => $this->input->post('kode'),
			"interaksi" => $this->input->post('interaksi'),
			"jenis" => $this->input->post('jenis'),
			"rekomendasi" => $this->input->post('rekomendasi'),
			"detail" => $this->input->post('detail'),
			"user" => $_SESSION["if_ses_username"],
		);
		$data = http_build_query($data);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array($authorization ));
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		$output = curl_exec($ch); 
		curl_close($ch);    
		$datas = json_decode($output);
		var_dump($datas);die();
		echo json_encode($datas);
	}
	public function getDetail()
	{
		$authorization = 'Authorization:'.$_SESSION["if_ses_token_phc"];
		$url = $this->urlws."Farmasi/Interaksi/getDetail";
		$data = array(
			"kode" => $this->input->post('kode'),
		);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array($authorization ));
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		$output = curl_exec($ch); 
		curl_close($ch);    
		$datas = json_decode($output);

		echo json_encode($datas);
		// var_dump($data);die();
	}
}
