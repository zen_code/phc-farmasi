<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class master_interaksi_model extends CI_Model{
	public function __construct()
  {
      // Call the CI_Model constructor
      parent::__construct();
     $this->load->database();
  }
 
  /*public function getdatalogin($user,$pass)
  {
    $this->load->database();
    $sql = "exec inap_sp_login_bacapriv '$user','$pass'";
    return $this->db->query($sql);  
  }*/
  public function get_datauser()
  {
    // $sql = "select a.NIPP, a.EMAIL, a.SIP, b.nmDok, c.REKENING, d.NM_RUANG, a.USERID from APP_USER a
    //         left join dr_mdokter b on a.KDDOK = b.kdDok
    //         left join rj_mklinik c on a.KDKLINIK = c.KDKLINIK
    //         left join ri_mrperawatan d on a.KDRUANG_PERAWATAN = d.KDMT";
    $sql = "exec app_sp_baru_data_user";
    return $this->db->query($sql);    
  }
  public function get_datadokter()
  {
    $sql = "select kdDok, nmDok, Klinik from dr_mdokter";
    return $this->db->query($sql);    
  }
  public function get_dataklinik()
  {
    $sql = "select KDKLINIK, REKENING from rj_mklinik";
    return $this->db->query($sql);    
  }
  public function get_dataperawatan()
  {
    $sql = "select KDMT, NM_RUANG from ri_mrperawatan";
    return $this->db->query($sql);    
  }
  public function insertUser($id_user,$pass,$nipp ,$email ,$sip ,$list_dokter ,$list_klinik ,$list_perawatan,$create_user)
  {
    $sql = "exec app_sp_baru_insert_user '$id_user','$pass','$nipp' ,'$email' ,'$sip' ,'$list_dokter' ,'$list_klinik' ,'$list_perawatan', '$create_user'";
    // var_dump($sql);die();
    return $this->db->query($sql);    
  }
  public function updateUser($id_user,$nipp ,$email ,$sip ,$list_dokter ,$list_klinik ,$list_perawatan,$create_user)
  {
    $sql = "exec app_sp_baru_update_user '$id_user','$nipp' ,'$email' ,'$sip' ,'$list_dokter' ,'$list_klinik' ,'$list_perawatan', '$create_user'";
    // var_dump($sql);die();
    return $this->db->query($sql);    
  }
  public function deleteUser($id_user,$create_user)
  {
    $sql = "exec app_sp_baru_delete_user '$id_user', '$create_user'";
    // var_dump($sql);die();
    return $this->db->query($sql);    
  }
}
