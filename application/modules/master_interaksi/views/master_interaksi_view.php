<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<style>
    .passrow{
        display:none
    }
</style>
<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
    <input type="hidden" value="<?php echo base_url(); ?>" id="url">
        <!-- begin:: Subheader -->
        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                    Master Interaksi </h3>
                </div>
                <!-- kt-subheader__toolbar -->
                
            </div>
        </div>

        <!-- end:: Subheader -->

        <!-- begin:: Content -->
        <div class="kt-container  kt-grid__item kt-grid__item--fluid">

            <!--Begin::Dashboard 1-->

            <!--Begin::Row-->
            <div class="row">
                
                <div class="col-xl-12">
                    <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile ">
                        <div class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Master Interaksi
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body kt-portlet__body--fit">
                            <div class="kt-portlet__body">
                                <button class="btn btn-info col-md-2" type="button" id="add"   data-toggle="kt-tooltip" data-placement="top"><i class="fa fa-plus"></i> Tambah Interaksi</button>
                           </div>
                            <div class="kt-portlet__body">
                                <div class="row">
                                    <div class="col-md-12 ">
                                        <table class="table table-striped- table-bordered table-hover table-checkable" id="data_master">
                                            <thead>
                                                <tr style="background:#2860a8;text-align: center;">
                                                    <th style="color:white">KODE</th>
                                                    <th style="color:white">INTERAKSI</th>
                                                    <th style="color:white">JENIS</th>
                                                    <th style="color:white">REKOMENDASI</th>
                                                    <th style="color:white; width:10%"></th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                                
                            </div>
                            
                            <!--end: Datatable -->
                        </div>
                    </div>
                </div>
            </div>

            <!--End::Row-->

            

            <!--End::Dashboard 1-->
        </div>

        <!-- end:: Content -->
    </div>
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="modal-review">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content ">
        <div class="modal-header">
            <h5 class="modal-title"><span class="tag_type"></span> Master Interakasi</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="form-group col-md-3">
                    <label>KODE</label>
                    <input type="text" class="form-control" aria-describedby="emailHelp" placeholder="kode" id="kode" disabled>
                </div>
                <div class="form-group col-md-6">
                    <label> INTERAKSI</label>
                    <input type="text" class="form-control" aria-describedby="emailHelp" placeholder="Interaksi" id="interaksi">
                </div>
                <div class="form-group col-md-3">
                    <label> Jenis</label>
                    <select id="select_jenis"></select>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-12">
                    <label>Cari Bahan</label>
                    <input type="text" class="form-control" aria-describedby="emailHelp" placeholder="bahan" id="bahan"> 

                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-6">
                    <div style="padding: 10px;border: 1px solid aliceblue;background: aliceblue;">List Bahan</div>
                    <div style="height:300px;overflow-y: scroll;    display: block;border: 1px solid aliceblue;">
                        <table class="table table-striped- table-bordered table-hover table-checkable" id="list_bahan">
                        
                        </table>
                        
                    </div>
                </div>
                <div class="form-group col-md-6">
                <div style="padding: 10px;border: 1px solid aliceblue;background: aliceblue;">Yang Sudah Dipilih</div>
                    <div style="height:300px;overflow-y: scroll;    display: block;border: 1px solid aliceblue;padding: 10px;">
                        <table class="table table-striped- table-bordered table-hover table-checkable" id="picked_bahan">
                            <thead>
                                <tr style="background: #2860a8;color:white">
                                    <th width="20px">NO</th>
                                    <th width="25%">KODE</th>
                                    <th >NAMA</th>
                                    <th width="20px"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="no-data">
                                    <td colspan="99" style="text-align:center;padding:10px">NO DATA</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-12">
                    <label> REKOMENDASI</label>
                    <input type="text" class="form-control" aria-describedby="emailHelp" placeholder="rekomendasi" id="rekomendasi">
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary" id="action"><span class="tag_type"></span></button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
</div>
<!-- javascript this page -->
<script src="<?php echo base_url()?>assets/assets_metronic/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>application/modules/master_interaksi/views/master_interaksi_view.js" type="text/javascript"></script>
<!-- <script src="<?php echo base_url()?>assets-custom/user_view.js" type="text/javascript"></script> -->

<script>

</script>