"use strict";

// Class definition

var MasterInteraksi = function() {
    
    
    
    return {
        // Init demos
        init: function() {
            getDataMaster();
            loading();
            getJenis();
        },
    };
}();
$('#btn_pilih').click(function(){
    
    
});
function getDataMaster(){
    // console.log('assadad');
    $('#data_master').DataTable( {
        "destroy": true,
        "processing" : true,
        "type": "GET",
        "ajax": 'master_interaksi/dataMaster',
        "columns"       : [
            {"data" : "KDINTERAKSI"},
            {"data" : "INTERAKSI"},
            {"data" : "JENIS"},
            {"data" : "REKOMENDASI"},
            {"data" : "btn"},
        ],
        
        "createdRow"    : function ( row, data, index ) {
            // $('td', row).eq(0).addClass('text-center');
           // $('td', row).eq(2).addClass('text-right');
        }, 
        "drawCallback": function( settings ) {
            $('[data-toggle="kt-tooltip"]').tooltip();
        },
        "ordering": false,
        "initComplete" : function(){
            KTApp.unblockPage();
        }
    });
}
$('#add').click(function(){
    picked= [];

    getBahan("");
    $('#picked_bahan tbody').html("");

    $('input').val('');
    $('.tag_type').text('Tambah');
    $('#modal-review').modal({backdrop: 'static', keyboard: false})  ;
    $('#modal-review').modal('show');
    console.log(picked);
});
function getJenis(){
    $.ajax({
        type: "GET",
        url: "master_interaksi/jenis", 
        dataType: 'json',
        success: function (data) {
            
            $('#select_jenis').select2({
                width: '100%',
                placeholder: '',
            });
            $('#select_jenis').append(data);
            KTApp.unblockPage();
            // console.log(a);
        },
        error: function (xhr,status,error) {

        }
    });
}
function loading(){
    KTApp.blockPage({
        overlayColor: '#000000',
        type: 'v2',
        state: 'primary',
        message: 'Processing...'
    });
}

$('#bahan').on('keydown, keyup', function(){
    var bahan =  $('#bahan').val();
    getBahan(bahan);
})
var picked = [];
function getBahan(bahan){
    loading();
    $('#list_bahan').html("");
    $.ajax({
        type: "POST",
        url: "master_interaksi/bahan", 
        dataType: 'json',
        data : {
            kdBahan :bahan 
        },
        success: function (data) {
            var a ="";
            $.each(data, function( index, value ) {
                if(picked.indexOf(value.KDBHN) != 0){
                    a += "<tr data-index='"+index+"' class='is_child'>"+
                            "<td width='5%' style='vertical-align:middle'>"+
                                "<label class='kt-checkbox' style='top:-4px' >"+
                                    "<input class='checkbox_bahan' type='checkbox' id='"+value.KDBHN+"' nama='"+value.NMBHN+"'> "+
                                    "<span></span>"+
                                "</label>"+
                            "</td>"+
                            "<td style='vertical-align:middle'>"+value.KDBHN + " - " +value.NMBHN+"</td>"+
                        "</tr>";
                }
                
            });
            $('#list_bahan').append(a);      
            KTApp.unblockPage();
            // console.log(a);
        },
        error: function (xhr,status,error) {

        }
    });
}
var clickedLine = "";
$('body').on('change', '.checkbox_bahan', function(){
    clickedLine = $(this).closest('tr').data('index');
    var $tr = $('#list_bahan > tbody').find('tr[data-index="'+clickedLine+'"]');
    var idx = $(this).attr('id');
    var nama = $(this).attr('nama');
    picked.push(idx);
    addRowPicked(idx,nama);
    var baris = $(this).parents('tr')[0];
    baris.remove();
    console.log(picked);
});

var i = 1;
function addRowPicked(idx, nama){
    var tr_nodata = $('#picked_bahan tbody tr.no-data');

    if(tr_nodata.length > 0 ){
        tr_nodata.remove()
    }
    var a ="";
    a = '<tr data-index = "'+i+'">'+
            '<td style="vertical-align: middle;"></td>'+
            '<td style="vertical-align: middle;"><input type="hidden" id="idx" value="'+idx+'">'+idx+'</td>'+
            '<td style="vertical-align: middle;">'+nama+'</td>'+
            '<td><button class="btn btn-danger btn-sm" id="btn-del"><i class="fa fa-times"   data-toggle="kt-tooltip" data-placement="top" title="xxxxxxx"></i> </button></td>' +
        '</tr>';
    $('#picked_bahan tbody').append(a);

    i++;
    renumber();

}
function renumber() {
    var index = 1;
    $('#picked_bahan tbody tr').each(function () {
        $(this).find('td').eq(0).html(index);//.find('.num_text')
        index++;
    });
}
$('#picked_bahan').on('click', 'tr #btn-del', function () {
    clickedLine = $(this).closest('tr').data('index');
    var $tr = $('#picked_bahan > tbody').find('tr[data-index="'+clickedLine+'"]');
    var bahan = $tr.find("#idx").val();
    var delarr;
    $.each(picked, function(index, value) {
        if (bahan == value) {
            delarr = index;
        }
    });
    picked.splice(delarr, 1);

    var baris = $(this).parents('tr')[0];
    baris.remove();
    renumber();

    var tr = $('#picked_bahan tbody tr');
    if(tr.length < 1){
        var a = "";
        a += '<tr class="no-data">'+
            '<td colspan="99" style="text-align:center;padding:10px">No Data</td>' +
            '</tr>';
        $('#picked_bahan').append(a);
    }
    getBahan($('#bahan').val());
});
function update(kdinteraksi){
    console.log(kdinteraksi);
}

$('#action').click(function(){
    var detail = [];
    $('#picked_bahan tbody tr').each(function(i){
        detail.push({
            kdBahan : $(this).find("#idx").val()
        });
    });

    var data = {
        kode : $('#kode').val(),
        interaksi : $('#interaksi').val(),
        jenis : $('#select_jenis option:selected').val(),
        rekomendasi : $('#rekomendasi').val(),
        detail : detail
    }
    // console.log(data);
    $.ajax({
        type: "post",
        url: "master_interaksi/action", 
        dataType: 'json',
        data : data,
        success: function (data) {
            swal.fire(data.status, data.message,data.status)
            KTApp.unblockPage();
            $('#modal-review').modal('hide');
            // console.log(a);
            getDataMaster();
        },
        error: function (xhr,status,error) {

        }
    });
});
$('#data_master').on( 'click', 'tr #btnproses', function () {
    var baris = $(this).parents('tr')[0];
    var table = $('#data_master').DataTable();
    var datas = table.row(baris).data();
    $('#picked_bahan tbody').html("");
    $.ajax({
        type: "post",
        url: "master_interaksi/getdetail", 
        dataType: 'json',
        data : {
            kode : datas['KDINTERAKSI']
        },
        success: function (data) {
            var tr_nodata = $('#picked_bahan tbody tr.no-data');

            if(tr_nodata.length > 0 ){
                tr_nodata.remove()
            }
            
            var a = "";
            $.each( data.message, function( index, value ) {
                picked.push(value.KDBHN);
                a += '<tr data-index = "'+(index+1)+'">'+
                        '<td style="vertical-align: middle;">'+(index+1)+'</td>'+
                        '<td style="vertical-align: middle;"><input type="hidden" id="idx" value="'+value.KDBHN+'">'+value.KDBHN+'</td>'+
                        '<td style="vertical-align: middle;">'+value.NMBHN+'</td>'+
                        '<td><button class="btn btn-danger btn-sm" id="btn-del"><i class="fa fa-times"   data-toggle="kt-tooltip" data-placement="top" title="xxxxxxx"></i> </button></td>' +
                    '</tr>';
            });
            console.log(picked);
            $('#picked_bahan tbody').append(a);
            getBahan("");
            $('.tag_type').text('Edit');
            $('#interaksi').val(datas['INTERAKSI']);
            $('#select_jenis').val(datas['JENIS']);
            $('#kode').val(datas['KDINTERAKSI']);
            $('#rekomendasi').val(datas['REKOMENDASI']);
            $('#modal-review').modal('show');
            // console.log(a);
        },
        error: function (xhr,status,error) {

        }
    });
});
// Class initialization on page load
jQuery(document).ready(function() {
    MasterInteraksi.init();
});