<?php
defined('BASEPATH') or exit('No direct script access allowed');

?>
<style>
    .is_head {
        background: #2860a8;
        color: white;
    }
</style>

<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
        <input type="hidden" value="<?php echo base_url(); ?>" id="url">
        <!-- begin:: Subheader -->
        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        Retur Resep </h3>
                </div>
                <!-- kt-subheader__toolbar -->

            </div>
        </div>

        <!-- end:: Subheader -->

        <!-- begin:: Content -->
        <div class="kt-container  kt-grid__item kt-grid__item--fluid">

            <!--Begin::Dashboard 1-->

            <!--Begin::Row-->
            <div class="row">

                <div class="col-xl-12">
                    <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile ">
                        <!-- <div class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                Farmasi Klinis
                                </h3>
                            </div>
                        </div> -->
                        <div class="kt-portlet__body kt-portlet__body--fit">

                            <!-- <div class="kt-portlet__body">
                                <button class="btn btn-info col-md-2" type="button" id="search_pasien" data-toggle="kt-tooltip" data-placement="top" data-original-title="" title=""><i class="fa fa-search"></i> Search Px</button>
                            </div> -->

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="kt-portlet">
                                        <div class="kt-portlet__body">
                                            <div class="m-form__group row">
                                                <label class="col-md-1 col-form-label" style="text-align:center">
                                                    Search :
                                                </label>
                                                <div class="col-md-2">
                                                    <input name="" type="text" class="form-control m-input" id="search" placeholder="">
                                                </div>
                                            </div>
                                            <table id="table-pasien" class="table table-striped- table-hover table-checkable">
                                                <thead>
                                                    <tr style="background: #2860a8; color:white;text-align:center">
                                                        <th style=" color:white;">ID_TRANS</th>
                                                        <th style=" color:white;">NOTA</th>
                                                        <th style=" color:white;">RM</th>
                                                        <th style=" color:white;">TGL_MRS</th>
                                                        <th style=" color:white;">NAMA</th>
                                                        <th style=" color:white;">KLINIK</th>
                                                        <th style=" color:white;">DOKTER</th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="kt-portlet">
                                        <div class="kt-portlet__head">
                                            <div class="kt-portlet__head-label">
                                                <h3 class="kt-portlet__head-title">
                                                    Biodata Pasien
                                                </h3>
                                            </div>
                                        </div>
                                        <div class="kt-portlet__body">
                                            <div class="row">
                                                <div class="form-group col-md-6">
                                                    <label>Nomor</label>
                                                    <input type="text" class="form-control " value="" disabled id="nomorBio">
                                                    <input type="hidden" class="form-control " id="id_trans">

                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label>Nota / Reg</label>
                                                    <input type="text" class="form-control" value="" disabled id="notaBio">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-md-12">
                                                    <label>Nama</label>
                                                    <input type="text" class="form-control " value="" disabled id="namaBio">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-md-6">
                                                    <label>Alamat</label>
                                                    <textarea disabled class="form-control col-md-11 dis TabOnEnter" tabindex="3" id="alamatBio" cols="35" rows="5" style="left: 10px;"></textarea>
                                                    
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label>Kota</label>
                                                    <input type="text" class="form-control col-md-11 dis TabOnEnter" value="" disabled id="kotaBio">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="kt-portlet">
                                        <div class="kt-portlet__head">
                                            <div class="kt-portlet__head-label">
                                                <h3 class="kt-portlet__head-title">
                                                    Debitur Pasien
                                                </h3>
                                            </div>
                                        </div>
                                        <div class="kt-portlet__body">
                                            <div class="row">
                                                <div class="form-group col-md-6">
                                                    <label>Jenis Pasien</label>
                                                    <input type="text" class="form-control" value="" disabled id="jenisPasienDeb">
                                                    
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label>RM / Index</label>
                                                    <input type="text" class="form-control" value="" disabled id="rmDeb">
                                                    
                                                </div>
                                            </div>
                                            <div class="row">
                                                
                                                <div class="form-group col-md-6">
                                                    <label>Debitur</label>
                                                    <input type="text" class="form-control" value="" disabled id="noDebiturDeb">
                                                    
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label>Dinas</label>
                                                    <input type="text" class="form-control" value="" disabled id="noDinasDeb">
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="kt-portlet">
                                        <div class="kt-portlet__head">
                                            <div class="kt-portlet__head-label">
                                                <h3 class="kt-portlet__head-title">
                                                    Info Klinik / RP
                                                </h3>
                                            </div>
                                        </div>
                                        <div class="kt-portlet__body">
                                            <div class="row">
                                                <div class="form-group col-md-3">
                                                    <label>Klinik / RP</label>
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <input type="text" class="form-control" value="" disabled id="noKlinikKlin">
                                                        </div>
                                                        <div class="col-md-8">
                                                            <input type="text" class="form-control" value="" disabled id="namaKlinikKlin">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <label>Dokter</label>
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <input type="text" class="form-control" value="" disabled id="noDokterKlin">
                                                        </div>
                                                        <div class="col-md-8">
                                                            <input type="text" class="form-control" value="" disabled id="namaDokterKlin">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-2">
                                                    <label>User</label>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <input type="text" class="form-control" value="" disabled id="userKlin">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-2">
                                                    <label>Jenis Resep</label>
                                                    <input type="text" class="form-control" value="" disabled id="namaJenisResepKlin">  
                                                    <!-- <div class="row">
                                                        <div class="col-md-4">
                                                            <input type="text" class="form-control" value="" disabled id="noJenisResepKlin">
                                                        </div>
                                                        <div class="col-md-8">
                                                            
                                                        </div>
                                                    </div> -->
                                                </div>
                                                <div class="form-group col-md-2">
                                                    <label>Iter</label>
                                                    <input type="text" class="form-control" value="" disabled id="iterKlin">

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="kt-portlet__body">
                                
                                <div class="tab-content">
                                    
                                    <div class="tab-pane active" id="tab3" role="tabpanel">
                                        <div class="m-form__group row">
                                            <label class="col-md-2 col-form-label" style="text-align:center">
                                                Alasan Return Resep :
                                            </label>
                                            <div class="form-group col-md-2" style="">
                                                <select class="form-control" id="alasan_return">
                                                    <option></option>
                                                </select>
                                            </div>
                                        </div>

                                        <table id="table-rekonsiliasi" class="table-bordered table table-striped- table-hover table-checkable">
                                            <thead>
                                                <tr style="background: #2860a8; color:white;width:100%; text-align:center">
                                                    <th width="3%" style="vertical-align:middle">NO</th>
                                                    <th width="3%" style="vertical-align:middle">ID</th>
                                                    <th width="10%" style="vertical-align:middle">Kode</th>
                                                    <th width="19%" style="vertical-align:middle">Nama Barang</th>
                                                    <th width="10%" style="vertical-align:middle">Satuan</th>
                                                    <th width="10%" style="vertical-align:middle">Signa</th>
                                                    <th width="10%" style="vertical-align:middle">Harga</th>
                                                    <th width="5%" style="vertical-align:middle">Qty</th>
                                                    <th width="10%" style="vertical-align:middle">Total</th>
                                                    <th width="5%" style="vertical-align:middle">Cek</th>
                                                    <th width="5%" style="vertical-align:middle">Qty</th>
                                                    <th width="10%" style="vertical-align:middle">Harga</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr class="no-data">
                                                    <td colspan="99" style="text-align:center;padding:10px">NO DATA</td>
                                                </tr>
                                            </tbody>
                                        </table>

                                        <div class="form-group m-form__group row kt-space-between" style="margin-bottom: 0px;border-right-width: 10px;padding-right: 9px;padding-left: 10px;">
                                            &nbsp;
                                            <div style="float:right" class="col-md-3">
                                                <div class="form-group m-form__group row">
                                                    <label class="col-md-4 col-form-label">
                                                        Sub Total :
                                                    </label>
                                                    <div class="col-md-8">
                                                        <input name="" disabled style="text-align:right" type="text" class="form-control m-input numbers" id="subtotal" value="0">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row kt-space-between" style="margin-bottom: 0px;border-right-width: 10px;padding-right: 9px;padding-left: 10px;">
                                            &nbsp;
                                            <div style="float:right" class="col-md-3">
                                                <div class="form-group m-form__group row">
                                                    <label class="col-md-4 col-form-label">
                                                        Jasa Resep :
                                                    </label>
                                                    <div class="col-md-8">
                                                        <input name="" disabled style="text-align:right" type="text" class="form-control m-input numbers" id="jasa_r_tot" value="0">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row kt-space-between" style="margin-bottom: 0px;border-right-width: 10px;padding-right: 9px;padding-left: 10px;">
                                            &nbsp;
                                            <div style="float:right" class="col-md-3">
                                                <div class="form-group m-form__group row">
                                                    <label class="col-md-4 col-form-label">
                                                        Grand Total :
                                                    </label>
                                                    <div class="col-md-8">
                                                        <input name="" disabled style="text-align:right" type="text" class="form-control m-input numbers" id="total" value="0">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div style="text-align: center; padding-right: 10px;">
                                            <button class="btn btn-success2" id="simpan_all" type="button" style="float :right;"><i class="fa fa-check"></i> SIMPAN</button>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <!--End::Row-->



            <!--End::Dashboard 1-->
        </div>

        <!-- end:: Content -->
    </div>
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="modal-review">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content ">
            <div class="modal-header">
                <h5 class="modal-title"><span class="tag_type"></span> Pasien</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="m-form__group row">
                    <label class="col-md-2 col-form-label" style="text-align:center">
                        Search :
                    </label>
                    <div class="col-md-4">
                        <input name="" type="text" class="form-control m-input" id="search" placeholder="">
                    </div>
                </div>
                <table id="table-pasien" class="table table-striped- table-hover table-checkable">
                    <thead>
                        <tr style="background: #2860a8; color:white;text-align:center">
                            <th style=" color:white;">NOTA</th>
                            <th style=" color:white;">RM</th>
                            <th style=" color:white;">TGL_MRS</th>
                            <th style=" color:white;">NAMA</th>
                            <th style=" color:white;">KLINIK</th>
                            <th style=" color:white;">RUANGAN</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th style=" color:white;">NOTA</th>
                            <th style=" color:white;">RM</th>
                            <th style=" color:white;">TGL_MRS</th>
                            <th style=" color:white;">NAMA</th>
                            <th style=" color:white;">KLINIK</th>
                            <th style=" color:white;">RUANGAN</th>
                            <th></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- javascript this page -->
<script src="<?php echo base_url() ?>assets/assets_metronic/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/assets_metronic/vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>application/modules/Retur_resep/views/retur_resep_view.js" type="text/javascript"></script>
<!-- <script src="<?php echo base_url() ?>assets/assets_metronic/js/demo9/pages/dashboard.js" type="text/javascript"></script> -->

<script>

</script>