"use strict";

// Class definition

var ReturResep = (function() {
  return {
    // Init demos
    init: function() {
      loading();
      AlasanReturn();
      $(".datepicker ").datepicker({
        autoclose: true,
        format: "dd/mm/yyyy",
        changeMonth: true,
        changeYear: true
      });
      $.fn.dataTable.Api.register("column().title()", function() {
        return $(this.header())
          .text()
          .trim();
      });
      $("#penyakit_kondisi").select2({
        placeholder: "Silahkan Pilih",
        width: "100%"
      });
      $("#kt_datepicker_1").datepicker({
        rtl: KTUtil.isRTL(),
        todayHighlight: true,
        orientation: "bottom right"
      });
      getdata();
      // $("#modal-review").modal("show");
    }
  };
})();

$("#search").on("keypress", function(e) {
  if (e.keyCode == 13) {
    getdata();
  }
});

// $("#modal-review").on("hidden.bs.modal", function() {
//   $("#search").val("");
//   getdata();
// });
function AlasanReturn() {
  $.ajax({
    type: "GET",
    url: "retur_resep/alasan_return_ctrl",
    data: {},
    dataType: "json",

    success: function(data) {
      jQuery.each(data, function(index, val) {
        var a = "";
        a =
          "<option value='" +
          val.IDKETRETUR +
          "' data-alasan='" +
          val.KETRETUR +
          "'><b>" +
          val.IDKETRETUR +
          " - " +
          val.KETRETUR +
          "</b>" +
          "</option>";
        $("#alasan_return").append(a);
      });
    },
    error: function(xhr, status, error) {
      console.log(xhr, status, error);
    }
  });
  $("#alasan_return").select2({
    placeholder: "Pilih Alasan Return",
    width: "100%"
  });
}
if ($("#table-rekonsiliasi tbody tr").length == 0) {
  $("#table-rekonsiliasi tbody").append(
    '<tr class="no-data"><td colspan="99" style="text-align:center;padding:10px">NO DATA</td></tr>'
  );
}
var cek_cek;
var value;
var max;
var x;
$("#table-rekonsiliasi").on("click", "tr #cek", function() {
  x = $(this).closest('tr').data('index');
  cek_cek = $(this)
    .parents("tr")
    .find("#cek");
  if (cek_cek.prop("checked") == true) {
    max = parseInt(
      $(this)
        .parents("tr")
        .find("td")
        .eq(7)
        .html()
    );
    $(this)
      .parents("tr")
      .find("#qty2")
      .attr("disabled", false);
    $(this)
      .parents("tr")
      .find("#qty2")
      .val(max);
    // $(this).parents("tr").find("#qty2").attr({"max" : max, "min" : 0});
  } else if (cek_cek.prop("checked") == false) {
    $(this)
      .parents("tr")
      .find("#qty2")
      .attr("disabled", true);
    $(this)
      .parents("tr")
      .find("#qty2")
      .val("");
    $(this)
      .parents("tr")
      .find("#harga2")
      .val("");
    $(this)
      .parents("tr")
      .find("#total_new")
      .val(
        parseInt(
          $(this)
            .parents("tr")
            .find("td")
            .eq(8)
            .html()
        )
      );
    // count_subtotal();
  }
  hitungHarga(x);
});
function hitungHarga(x){
  var $tr = $('#table-rekonsiliasi > tbody').find('tr[data-index="'+x+'"]');
  var qty = $tr.find('#qty2').val();
  var hjual = $tr.find('#hjual').val();
  $tr.find('#harga2').val(qty * hjual);
  count_subtotal();
}

var new_total = 0;
var new_jasa_r = 0;
var dibalikin = 0;
$("#table-rekonsiliasi").on("input", "tr #qty2", function() {
  x = $(this).closest('tr').data('index');
  
  value = $(this)
    .parents("tr")
    .find("#qty2")
    .val();
  if (value !== "" && value.indexOf(".") === -1) {
    $(this)
      .parents("tr")
      .find("#qty2")
      .val(Math.max(Math.min(value, max), 0));
  }
  // in progress //
  if (
    $(this)
      .parents("tr")
      .find("#qty2")
      .val() == ""
  ) {
    dibalikin = 0;
  } else {
    dibalikin = parseInt(
      $(this)
        .parents("tr")
        .find("#qty2")
        .val()
    );
  }
  new_total =
    parseInt(
      $(this)
        .parents("tr")
        .find("td")
        .eq(6)
        .html()
    ) *
    (parseInt(
      $(this)
        .parents("tr")
        .find("td")
        .eq(7)
        .html()
    ) -
      dibalikin);
  $(this)
    .parents("tr")
    .find("#total_new")
    .val(new_total);
  //   new_jasa_r =  parseInt($(this).parents("tr").find("#jasa_r").val())*(parseInt($(this).parents("tr").find("td").eq(7).html()) - parseInt($(this).parents("tr").find("#qty2").val()));
  // count_subtotal();
  hitungHarga(x);
  // in progress //
});
function getdata() {
  var search = $("#search").val();
  $("#table-pasien").DataTable({
    destroy: true,
    responsive: true,
    dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
    lengthMenu: [5, 10, 25, 50],
    pageLength: 5,
    language: {
      lengthMenu: "Display _MENU_"
    },
    searchDelay: 500,
    ordering: true,
    processing: true,
    serverSide: false,
    ajax: {
      url: "retur_resep/dataPasien",
      type: "GET",
      data: {
        search: search
      }
    },
    columns: [
      { data: "ID_TRANS" },
      { data: "NOTA" },
      { data: "RM" },
      { data: "TGL_MRS" },
      { data: "NAMA" },
      { data: "KLINIK" },
      { data: "DOKTER" },
      { data: "btn" }
    ],
    drawCallback: function(settings) {
      $('[data-toggle="kt-tooltip"]').tooltip();
    },
    "initComplete" : function(data){
      // console.log(data)
    }
  });
  KTApp.unblockPage();
}

var clickedLine = 0;
var dataobat = 0;
var num = "";
var data2;
$("#table-pasien").on("click", "tr #btnproses", function() {
  num = $(this)
    .parents("tr")
    .find("td")
    .eq(0)
    .html();
  detailPasien(num);
  // console.log(num)
  // $("#search").val("");
  // getdata();
});
var subtotal = 0;
var disc = 0;
var jasa_r = 0;
var total = 0;
function detailPasien(nota) {
  var a = "";
  $("#table-rekonsiliasi tbody").html("");
  // console.log(nota);
  // $("#modal-review").modal("hide");
  KTApp.blockPage({
    overlayColor: "#000000",
    type: "v2",
    state: "primary",
    message: "Processing..."
  });
  $.ajax({
    type: "GET",
    url: "retur_resep/detailPasien",
    data: {
      kode: nota
    },
    dataType: "json",
    success: function(data) {
      // data_all = "[";
      data2 = data;
      console.log(data);
      //   $("#table-list_obat tbody").html("");
      jQuery.each(data["data"], function(index, val) {
        // data_all += JSON.stringify(data[index]) + ",";
        // console.log(val.nomor);
        // console.log(val.nama);
        $("#nomorBio").val(val.nomor);
        $("#notaBio").val(val.nota);
        $("#namaBio").val(val.nama);
        $("#alamatBio").val(val.ALMTPX);
        $("#kotaBio").val(val.KOTAPX);
        if (val.STPAS == 1) {
          $("#jenisPasienDeb").val("TUNAI");
        } else if (val.STPAS == 2) {
          $("#jenisPasienDeb").val("KREDIT");
        }
        $("#rmDeb").val(val.rm+ " / " +val.IDXPX);
        $("#indexDeb").val(val.IDXPX );
        $("#noDebiturDeb").val(val.KDDEB+ " - " +val.nmdeb);
        $("#namaDebiturDeb").val(val.nmdeb);
        $("#noDinasDeb").val(val.kddin+ " - " +val.nmdin);
        $("#namaDinasDeb").val(val.nmdin);
        $("#noKlinikKlin").val(val.KDKLIN);
        $("#namaKlinikKlin").val(val.NMKLIN);
        $("#userKlin").val(val.INPUTBY);
        $("#noDokterKlin").val(val.KDDOK);
        $("#namaDokterKlin").val(val.NMDOK);
        $("#noJenisResepKlin").val(val.KDOT);
        $("#namaJenisResepKlin").val(val.NMJNSRESEP);
        $("#iterKlin").val(val.ITER);
        $("#id_trans").val(val.ID_TRANS);
        a = a =
          "<tr data-index='" + index + "'>" +
            "<td>" + val.NO + "</td>" +
            "<td>" + val.ID + "</td>" +
            "<td>" + val.KDBRG + "</td>" +
            "<td>" + val.nmbrg + "</td>" +
            "<td>" + val.SATUAN + "</td>" +
            "<td>" + val.SIGNA + "</td>" +
            "<td><span class='numbers' id='hjual'>" + val.HJUAL + "</span></td>" +
            "<td>" + val.jumlah + "</td>" +
            "<td><span class='numbers'>" + val.sub_total + '</span><input type="hidden" id="jasa_r" value="' + val.JASA + '">' +
              '<input type="hidden" id="total_new" value="' + val.sub_total + '">' +
            "</td>" +
            "<td>" +'<label class="kt-checkbox"><input type="checkbox" class="form-check-input" value="" id="cek"><span></span></label>' +"</td>" +
            '<td><div id="qty2_cell"><input type="number" disabled id="qty2" class="form-control m-input"></div></td>' +
            '<td><input type="text" disabled id="harga2" class="form-control m-input numbers"></td>' +
          "</tr>";
        $("#table-rekonsiliasi tbody").append(a);
        // subtotal = subtotal + val.sub_total;
        // jasa_r = jasa_r + val.JASA;
        setInputMask();
      });
      $("#subtotal").val(subtotal);
      //   $("#disc").val(disc);
      $("#jasa_r_tot").val(jasa_r);
      total = subtotal + jasa_r;
      $("#total").val(total);
      KTApp.unblockPage();
    },
    error: function(xhr, status, error) {
      // console.log(xhr, status, error);
    }
  });
}

function setInputMask(){
  $(".numbers").inputmask(
      {   
          'alias': 'decimal', 
          'groupSeparator': ',', 
          'autoGroup': true,
          'removeMaskOnSubmit': true,
          'rightAlign': true,
          'autoUnmask': true,
          // groupSeparator: '.',
      }
  );
}

function count_subtotal() {
  var total = 0;
  var total_jasa_r = 0;
  var harga_total = 0;
  $('#table-rekonsiliasi > tbody  > tr').each(function(index, val) {
    var cek = $(this).find('#cek').is(":checked");
    var harga2 = $(this).find('#harga2').val();
    var jasa_r = $(this).find('#jasa_r').val();
    if(cek){
      total = parseInt(total) + parseInt(harga2);
      total_jasa_r = parseInt(total_jasa_r) + parseInt(jasa_r);
    }
  })
  harga_total = parseInt(total) + parseInt(total_jasa_r);
  $("#subtotal").val(total);
  $("#jasa_r_tot").val(total_jasa_r);
  $("#total").val(harga_total);
  // var total = 0;
  // var total_jasa_r = 0;
  // var harga_total = 0;
  // for (var i = 0; i < $("#table-rekonsiliasi > tbody > tr").length; i++) {
  //   total =
  //     parseFloat(total) +
  //     parseFloat(
  //       $("#table-rekonsiliasi tbody")
  //         .find("tr")
  //         .eq(i)
  //         .find("#harga2")
  //         .val()
  //     );
  //   total_jasa_r =
  //     parseFloat(total_jasa_r) +
  //     parseFloat(
  //       $("#table-rekonsiliasi tbody")
  //         .find("tr")
  //         .eq(i)
  //         .find("#jasa_r")
  //         .val()
  //     );
  // }
  // $("#subtotal").val(total);
  // $("#jasa_r_tot").val(total_jasa_r);
  // harga_total = total + total_jasa_r;
  // $("#total").val(harga_total);
}


$("body").on("change", ".checkbox_drug", function() {
  clickedLine = $(this)
    .closest("tr")
    .data("index");
  var $tr = $("#table-drp > tbody").find(
    'tr[data-index="' + clickedLine + '"]'
  );
  var catatan = $tr.find("#catatan");
  if ($(this).is(":checked")) {
    $(this).attr("value", 1);
    catatan.prop("disabled", false);
    $("#is_check").prop("checked", true);
  } else {
    $(this).attr("value", 0);
    catatan.prop("disabled", true);
    catatan.val("");
  }
});
$("#is_check").on("change", function() {
  if ($(this).is(":checked")) {
    $(this).attr("value", 1);
  } else {
    $(this).attr("value", 0);
  }
});

// $("#search_pasien").click(function() {
//   $("#modal-review").modal("show");
// });
$("#tambah-rekonsiliasi").click(function() {
  addRow();
  // alert('nyamm')
});

var i = 1;
function addRow() {
  var tr_nodata = $("#table-rekonsiliasi tbody tr.no-data");

  if (tr_nodata.length > 0) {
    tr_nodata.remove();
  }
  var a = "";
  a =
    '<tr data-index = "' +
    i +
    '">' +
    '<td style="text-align: center;vertical-align: middle;"></td>' +
    '<td><input type="text" class="form-control" id="id"></td>' +
    '<td><input type="text" class="form-control numbers" id="kode"></td>' +
    '<td><select class="form-control namaBarangS" id="namaBarang"></select></td>' +
    // '<td><input type="text" class="form-control namaBarangS" id="namaBarang"></td>' +
    '<td><input type="text" class="form-control" id="satuan"></td>' +
    '<td><input type="text" class="form-control" id="signa"></td>' +
    '<td><input type="text" class="form-control" id="harga"></td>' +
    '<td><input type="text" class="form-control numbers" id="qty"></td>' +
    '<td><input type="text" class="form-control numbers" id="total"></td>' +
    '<td><button class="btn btn-danger" id="btn-del"><i class="fa fa-times"   data-toggle="kt-tooltip" data-placement="top" title="xxxxxxx"></i> </button></td>' +
    "</tr>";
  $("#table-rekonsiliasi tbody").append(a);

  i++;
  renumber();
  input_date();
  input_number();
}
function renumber() {
  var index = 1;
  $("#table-rekonsiliasi tbody tr").each(function() {
    $(this)
      .find("td")
      .eq(0)
      .html(index); //.find('.num_text')
    index++;
  });
}
$("#table-rekonsiliasi").on("click", "tr #btn-del", function() {
  var baris = $(this).parents("tr")[0];
  baris.remove();
  renumber();

  var tr = $("#table-rekonsiliasi tbody tr");
  if (tr.length < 1) {
    var a = "";
    a +=
      '<tr class="no-data">' +
      '<td colspan="99" style="text-align:center;padding:10px">No Data</td>' +
      "</tr>";
    $("#table-rekonsiliasi tbody").append(a);
  }
});
$("#tambah-eso").click(function() {
  if ($("#noreg").val() != "") {
    addRowEso();
  } else {
    swal.fire("warning", "Pilih Pasien terlebih dahulu", "warning");
  }
});
var i_eso = 1;
var listobat = "";
function addRowEso() {
  // console.log("nyamm");

  var tr_nodata = $("#table-eso tbody tr.no-data");

  if (tr_nodata.length > 0) {
    tr_nodata.remove();
  }
  listobat = buildListObat(dataobat, i);
  var a = "";
  a =
    '<tr data-index = "' +
    i_eso +
    '">' +
    '<td style="text-align: center;vertical-align: middle;"></td>' +
    "<td>" +
    listobat +
    "</td>" +
    '<td><input type="text" class="form-control" id="satuan" disabled></td>' +
    '<td><input type="text" class="form-control" id="cara"></td>' +
    '<td><input type="text" class="form-control" id="dosis"></td>' +
    '<td><input type="text" class="form-control" id="txt_penggunaan"></td>' +
    '<td><button class="btn btn-danger" id="btn-del"><i class="fa fa-times"   data-toggle="kt-tooltip" data-placement="top" title="Delete"></i> </button></td>' +
    "</tr>";
  $("#table-eso tbody").append(a);
  i_eso++;
  renumberEso();
  input_date();
  $(".list_obat").select2();
}
function buildListObat(data, index) {
  var list =
    "<select class='form-control list_obat' id='list-obat-" +
    index +
    "'><option> Silahkan Pilih</option>" +
    data +
    "</select>";

  return list;
}
var x = "";
$("#table-eso").on("change", "tr .list_obat", function() {
  x = $(this)
    .closest("tr")
    .data("index");
  var $tr = $("#table-eso > tbody").find('tr[data-index="' + x + '"]');
  var satuan = $tr.find(".list_obat option:selected").attr("satuan");
  $tr.find("#satuan").val(satuan);
});
function input_type(type, param) {
  var res = "";
  if (type == "date") {
    res =
      '<div class="input-group date">' +
      '<input type="text" class="form-control datepickers ' +
      param +
      '" readonly placeholder="Pilih Tanggal" />' +
      "</div>";
  } else if (type == "checkbox") {
    res =
      "<label class='kt-checkbox' style='top:-4px' >" +
      "<input type='checkbox' id='" +
      param +
      "'> " +
      "<span></span>" +
      "</label>";
  }

  return res;
}

function input_date() {
  $(".datepickers").datepicker({
    rtl: KTUtil.isRTL(),
    todayHighlight: true,
    orientation: "bottom left",
    format: "dd/mm/yyyy",
    autoclose: true
  });
}
function input_number() {
  $(".numbers").on("keypress keyup blur", function(event) {
    $(this).val(
      $(this)
        .val()
        .replace(/[^\d].+/, "")
    );
    if (event.which < 48 || event.which > 57) {
      event.preventDefault();
    }
  });
}
function loading() {
  KTApp.blockPage({
    overlayColor: "#000000",
    type: "v2",
    state: "primary",
    message: "Processing..."
  });
}
var retur = [];
$("#simpan_all").click(function() {
  if ($("#notaBio").val() == "" || $("#notaBio").val() == null) {
    swal.fire("Peringatan", "Belum memilih pasien", "warning");
  }else{
    retur = [];
    $.each($("#table-rekonsiliasi tbody tr"), function(i, val){
      if($("#table-rekonsiliasi tbody tr").eq(i).find("#cek").prop('checked')){
        retur.push({
          no : $("#table-rekonsiliasi tbody tr").eq(i).find("td").eq(0).html(),
          qty : $("#table-rekonsiliasi tbody tr").eq(i).find("#qty2").val(),
        });
      }
    });
    if(retur.length > 0 && $('#alasan_return').val() != ''){
      loading();
      var data = {
        id_trans : $('#id_trans').val(),
        alasan : $('#alasan_return').val(),
        total : $('#total').val(),
        detail : retur
      }
  
      $.ajax({
          type: "POST",
          url: "retur_resep/action",  
          data: data,
          dataType: 'json',
          success: function (data) {
            swal.fire(data.status, data.message, data.status);
          },
          error: function (xhr,status,error) {
            console.log(status);
          }
      });
    }else{
      // console.log($('#alasan_return').val());
      swal.fire("warning", "Tidak ada barang yang dikembalikan", "warning");
    }
    
  }
});

// Class initialization on page load
jQuery(document).ready(function() {
  ReturResep.init();
});
