<?php
defined('BASEPATH') or exit('No direct script access allowed');

// class retur_resep extends CI_Controller {
class retur_resep extends CI_Controller
{
	public $urlws = null;
	public function __construct()
	{
		parent::__construct();
		$this->load->model('retur_resep_model');
		// $this->load->model('retur_resep_model');
		$this->load->library('session');
		$this->urlws = $this->globals->ws_phc();
	}
	private function _render($view, $data = array())
	{
		$this->load->view('header', $data);
		// $this->load->view('body_header');
		// $this->load->view('sidebar');
		$this->load->view($view, $data);
		$this->load->view('footer');
	}

	public function index()
	{
		//load library
		// var_dump($this->session->all_userdata());die();
		$uri = &load_class('URI', 'core');
		redirect(base_url() . 'index.php/retur_resep/data', $data);
	}

	public function data()
	{
		// var_dump($_SESSION["ses_userid"]);die();
		if($_SESSION["if_ses_depo"] == null){
			redirect(base_url().'depo');
		}else{
			if (in_array(6, $_SESSION['if_ses_menu'])) {
				$data['parent_active'] = 6;
				$data['child_active'] = 50;
				$this->_render('retur_resep_view', $data);
			} else {
				redirect(base_url() . 'beranda/data');
			}
		}
	}

	public function dataPasien()
	{
		$authorization = 'Authorization:' . $_SESSION["if_ses_token_phc"];
		$url = $this->urlws . "Farmasi/returresep/dataPasien";
		$data = array(
			"search"         => $this->input->get('search'),
		);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array($authorization));
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$output = curl_exec($ch);
		curl_close($ch);
		$cek_output = json_decode($output);
		// $cek_output = $this->retur_resep_model->getPasien($data);
		
		if ($cek_output->status == true) {
			$data 			= array();
			foreach ($cek_output->data as $item) {
				// foreach ($cek_output as $item) {
				// var_dump($item->NOTA);
				$btn 		= '<button name="btnproses" id="btnproses" type="button" class="btn btn-success btn-sm" data-toggle="kt-tooltip" data-placement="top" title="Choose"> <i class="fa fa-check"></i> </button>';

				$data[] 	= array(
					"ID_TRANS" 	=> $item->ID_TRANS,
					"NOTA" 	=> $item->NOTA,
					"RM" => $item->NORM,
					"TGL_MRS" 	=> $item->TGL,
					"NAMA"		=> $item->NMPX,
					"KLINIK"		=> $item->NMKLIN,
					"DOKTER"		=> $item->NMDOK,
					"btn"		=> $btn
				);
			}
			// die();
			$obj = array("data" => $data);
			// var_dump($obj);
			echo json_encode($obj);
		} else {
			$error = array(
				'data' => [],
				'error' => $cek_output
			);

			echo json_encode($error);
		}
	}
	public function detailPasien()
	{
		$authorization = 'Authorization:' . $_SESSION["if_ses_token_phc"];
		$url = $this->urlws . "Farmasi/returresep/detailPasien";
		$data = array(
			"kode"         => $this->input->get('kode'),
		);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array($authorization));
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$output = curl_exec($ch);
		curl_close($ch);
		$cek_output = json_decode($output);
		// var_dump($cek_output);die();
		if ($cek_output->status == true) {
			echo json_encode($cek_output);
		} else {
			$error = array(
				'data' => [],
				'error' => $cek_output->message
			);
		}
	}
	public function alasan_return_ctrl()
	{
		$query = $this->retur_resep_model->alasan_return_model();
		echo json_encode($query->result());
	}
	public function action()
	{
		// var_dump($this->input->post());die();
		$cek_output = $this->retur_resep_model->action($this->input->post());
		echo json_encode($cek_output);
	}
}
