<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// class farmasi_klinis_model extends CI_Model{
class retur_resep_model extends CI_Model{
	public function __construct()
  {
      // Call the CI_Model constructor
      parent::__construct();
     $this->load->database();
  }
 
  /*public function getdatalogin($user,$pass)
  {
    $this->load->database();
    $sql = "exec inap_sp_login_bacapriv '$user','$pass'";
    return $this->db->query($sql);  
  }*/
  public function getPasien($data)
    {
        // var_dump($data['search']);die();
        $query = $this->db->query("select top 100 * from IF_HTRANS where NOTA != '' and NOTA like '%".$data['search']."%' ");
        // var_dump( $query->result());die();
        return $query->result();
    }
  public function detailPasien($noreg)
  {
    $sql = "SELECT
          M.noreg NOREG,MP.KBUKU RM,MP.NAMA, MP.SEX,convert(varchar(10), MP.TGL_LHR, 103) TGL_LHR, MP.ALAMAT, MP.KOTA,M.[index],
          M.kdebi,M.kdini,
          convert(varchar(10), m.tgmas, 103) TGL_MRS,convert(varchar(10), m.tgl_plng, 103) TGL_PRS,
          K.NM_KMR KAMAR,K.KD_KMR, 
          RP.NM_RUANG2 RUANGAN,RP.RP, M.kddok, M.nm_diag_msk
            FROM RI_MASTERPX M 
            INNER JOIN RIRJ_MASTERPX MP ON MP.NO_PESERTA=M.nopeserta
            INNER JOIN RI_MKAMAR K ON K.KD_KMR=M.kmr_skrg AND K.[STATUS]=1
            INNER JOIN RI_MRPERAWATAN RP ON RP.RP=K.RP AND RP.STT_ACT=1
            where M.noreg = ".$noreg."
            ORDER BY 1";
    $detail_result = $this->db->query($sql);

    $result['detail'] = $detail_result->result();
    return $result;    
  }

  public function alasan_return_model()
  {
    $query  = $this->db->query("select * from IF_MKETRETUR");
    return $query;
  }
  public function action($data)
  { 
    try{
      $res = [];
      $this->db->trans_start();
      // generate id_trans
      $sql =  $this->db->query("exec if_sp_generate_kode_transaksi_resep_manual");
      $id_trans = $sql->row()->KDTRANS;//[0]->id_trans
      
      // generate nomor
      $sql =  $this->db->query("exec if_sp_baru_generate_nomor_resep_manual '".date('Y-m-d')."'");
      $nomor = $sql->row()->NO_RESEP;//[0]->id_trans
     
      // create if_htrans retur
      $sql = "insert into if_htrans (ID_TRANS, KDMUT, TGL, TGLSHIFT, NOMOR, MUTASI, BAYAR, KDOT, KDIN, STPAS, KDDEB, NMDEB, KDDIN, NMDIN, KDKLIN, NMKLIN,NORM, IDXPX, NMPX,NMKK, KDDOK, 
                  NMDOK, STS, JAM, JAGA, RESEPAW, LOKASI, CLOSING, KEU, TIPEIF, ACTIVE, INPUTBY, INPUTDATE, RMINDUK, ALMTPX, KOTAPX, TGL_RESEP, KETRETUR, NOPESERTA)
              select '".$id_trans."', KDMUT, convert(date, getdate()), convert(date, getdate()), '".$nomor."', MUTASI, BAYAR, KDOT, KDIN, STPAS, KDDEB, NMDEB, KDDIN, NMDIN, KDKLIN, NMKLIN,NORM, IDXPX, NMPX,NMKK, KDDOK, 
                  NMDOK, STS, convert(time, getdate()), JAGA, NOMOR, LOKASI, CLOSING, KEU, TIPEIF, ACTIVE, '".$_SESSION["if_ses_username"]."', getdate(), RMINDUK, ALMTPX, KOTAPX, TGL, '".$data["alasan"]."', NOPESERTA
              from if_htrans where ID_TRANS = '".$data["id_trans"]."'";
      $this->db->query($sql);
      
      // create if_trans retur
      foreach($data['detail'] as $index => $a){
        $sql = "insert into if_trans (ID_TRANS, [NO], ID, KDBRG, KDBRGDR, HARGA, HBIJI, HJUAL, DISC, JUMLAH, PUYER, ITER, SIGNA, SIGNA2, HARI, JASA, QTYDR, KETQTY, KDDEBI, SIGNACPT, ACTIVE, INPUTBY, INPUTDATE)
                select '".$id_trans."', ".($index+1).", ID, KDBRG, KDBRGDR, HARGA, HBIJI, HJUAL, DISC, ". ($a["qty"] * (-1)) .", PUYER, ITER, SIGNA, SIGNA2, HARI, JASA, QTYDR, KETQTY, KDDEBI, SIGNACPT, ACTIVE, '".$_SESSION["if_ses_username"]."', getdate() 
                from if_trans  where id_trans = '".$data["id_trans"]."' and no = ".$a["no"];
        $this->db->query($sql);
      }
      // die(); 
      
      // create tbayar
      $sql = "insert into IF_TBAYAR (ID_TRANS, BANK, NO_REK, RUPIAH, DIBAYAR, ACTIVE, INPUTBY, INPUTDATE) 
              values('".$id_trans."', '1', '', ".$data["total"].", ".$data["total"].", 1,'".$_SESSION["if_ses_username"]."', getdate()  ) ";

      $this->db->trans_complete();
      $res['status'] = 'success';
      $res['message'] = 'Berhasil Retur Resep';
    }catch(Exception $e){
      $res['status'] = 'error';
      $res['message'] = $e;
    }
    return $res;
  }
}
