<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class cek_resep_inap extends CI_Controller {
	
	public $urlws = null;
	public $app_id = 1;
	public function __construct() {
        parent::__construct();
		$this->load->model('cek_resep_inap_model');
		$this->load->library('session');
		$this->urlws = $this->globals->ws_phc();
		// $this->urlwscentra = "http://centra-dev.pelindo.co.id/public/api/";
		$this->app_id = 1;
    }
	private function _render($view,$data = array())
	{
		$this->load->view('header',$data);
	    // $this->load->view('body_header');
	    // $this->load->view('sidebar');
	    $this->load->view($view,$data);
	    $this->load->view('footer');
	}

	public function index()
	{
		//load library
		$uri =& load_class('URI', 'core');
	  	redirect(base_url().'index.php/cek_resep_inap/data',$data);
	}

	public function data()
	{
		if($_SESSION["if_ses_depo"] == null){
			redirect(base_url().'depo');
		}else{
			if(in_array(6,$_SESSION['if_ses_menu'])){
				$data['parent_active'] = 6 ;
				$data['child_active'] = 59 ;
				$this->_render('cek_resep_inap_view',$data);
			}else{
				redirect(base_url().'beranda/data');
			}
		}
		
		
	}
	public function getData(){
		$param = $this->input->post('param');
		$res = $this->cek_resep_inap_model->getData($param);
		// var_dump($res);die();
		// var_dump($res->total, int($res->HARGA)	);die();
		$status 			= "";
		$message 			= "";
		$det 			= "";
		if(count($res) > 0){
			$detail = $this->cek_resep_inap_model->getdetail($res->ID_TRANS);
			if($res->total == $res->HARGA){
				// $status = 'E';
				// $message = "Total Farmasi dan Inap Sama";
				$status = 'S';
				$message = $res;
				$det = $detail;
			}else{
				$status = 'S';
				$message = $res;
				$det = $detail;
			}
			
		}else{
			$status = 'E';
			$message = "Data Tidak ditemukan";
		}

		
		// foreach($res as $item){
		// 	$data[] 	= array(
		// 		"TGL" 	=> $item->TGL,
		// 		"NOTA" 	=> $item->NOTA,
		// 		"NOMOR" => $item->NOMOR,
		// 		"CARA_BAYAR" 	=> $item->KDDEB ,
		// 		"JENIS" 	=> '' ,
		// 		"TOTAL_FARMASI"		=> $item->total,
		// 		"TOTAL_INAP"		=> $item->HARGA,
		// 	);
		// }
		$obj = array("status"=> $status, "message"=> $message, "detail"=> $det);
		// var_dump($dates);die();
		echo json_encode($obj);
	}
	
}
