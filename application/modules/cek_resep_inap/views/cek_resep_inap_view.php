<?php
defined('BASEPATH') or exit('No direct script access allowed');

?>
<style>
    .is_head {
        background: #2860a8;
        color: white;
    }
</style>

<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
        <input type="hidden" value="<?php echo base_url(); ?>" id="url">
        <!-- begin:: Subheader -->
        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        Cek Selisih Resep rawat inap </h3>
                </div>
                <!-- kt-subheader__toolbar -->

            </div>
        </div>

        <!-- end:: Subheader -->

        <!-- begin:: Content -->
        <div class="kt-container  kt-grid__item kt-grid__item--fluid">

            <!--Begin::Dashboard 1-->

            <!--Begin::Row-->
            <div class="row">

                <div class="col-xl-12">
                    <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile ">
                        <!-- <div class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                Farmasi Klinis
                                </h3>
                            </div>
                        </div> -->
                        <div class="kt-portlet__body kt-portlet__body--fit">
                            <!--begin: Datatable -->
                            <!-- <div class="kt-portlet__body">
                                <button class="btn btn-info col-md-2" type="button" id="search_pasien" data-toggle="kt-tooltip" data-placement="top"><i class="fa fa-search"></i> Search Px</button>
                            </div> -->

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="kt-portlet">
                                        <div class="kt-portlet__head">
                                            <div class="kt-portlet__head-label">
                                                <h3 class="kt-portlet__head-title">
                                                    Pencarian
                                                </h3>
                                            </div>
                                        </div>
                                        <div class="kt-portlet__body">
                                            <div class="row">
                                                <div class="form-group col-md-5">
                                                    <label>No. Register</label>
                                                    <input autofocus placeholder="tekan ''ENTER'' untuk mencari data" type="text" class="form-control numbers" value="" id="search" labelx="No. Kwitansi">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-md-12">
                                                    <div style="overflow-x:auto;">
                                                    <table id="table-list_obat" class="table table-striped- table-hover table-checkable">
                                                        <thead>
                                                            <tr style="background: #2860a8; color:white; text-align:center">
                                                                
                                                                <th style="color:white">TGL</th>
                                                                <th style="color:white">NOTA</th>
                                                                <th style="color:white">NOMOR</th>
                                                                <th style="color:white">CARA BAYAR</th>
                                                                <th style="color:white">JENIS </th>
                                                                <th style="color:white">TOTAL FARMASI</th>
                                                                <th style="color:white">TOTAL INAP</th>
                                                                <th style="color:white">Aksi</th>

                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr class="nodata">
                                                                <td colspan="99" style="text-align:center">NO DATA</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                </div>
                                                
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>

            <!--End::Row-->



            <!--End::Dashboard 1-->
        </div>

        <!-- end:: Content -->
    </div>
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="modal-review">
    <div class="modal-dialog" role="document">
        <div class="modal-content modal-lg">
        <div class="modal-header">
            <h5 class="modal-title">List Detail</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="row">
                <table id="table-obat_anmaag"  class="table table-striped- table-hover table-checkable">
                    <thead>
                        <tr style="background: #2860a8; color:white;text-align:center">
                            
                            <th style=" color:white;">KODE</th>  
                            <th style=" color:white;">OBAT</th>  
                            <th style=" color:white;">JENIS</th>   
                            <th style=" color:white;">OBAT / ALKES</th>   
                            <th style=" color:white;">JUMLAH</th>   
                            <th width="5%"></th>  
                        </tr>
                    </thead>
                </table>
            </div>
            
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
</div>

<!-- javascript this page -->
<script src="<?php echo base_url() ?>assets/assets_metronic/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/assets_metronic/vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>application/modules/cek_resep_inap/views/cek_resep_inap_view.js" type="text/javascript"></script>
<!-- <script src="<?php echo base_url() ?>assets/cetak_resep_inap/js/demo9/pages/dashboard.js" type="text/javascript"></script> -->

<script>

</script>