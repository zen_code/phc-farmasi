"use strict";

// Class definition

var CekResepInap = function() {
    return {
        // Init demos
        init: function() {
            input_number();
            // loading();
        },
    };
}();

$('#search').on('keydown',function(evt){
    if (evt.key === 'Enter') {
        evt.preventDefault();
        var param = $('#search').val();
        if(param.length > 2){
            getData(param);
        }else{
            swal.fire('warning', 'Minimal 3 Huruf untuk Pencarian', 'warning')
        }
        
    }
})
function getData(param){
    loading()
    $.ajax({
        type: "POST",
        url: "cek_resep_inap/getData", 
        dataType: 'json',
        data:  {
            param    : param,
            },
        success: function (data) {
            KTApp.unblockPage();
            $('#table-list_obat tbody').html('');
            var a="";
            var b="";
            if(data.status == 'S'){
                var tr_nodata = $('#table-list_obat tbody tr.nodata');
                if(tr_nodata.length > 0 ){
                    tr_nodata.remove()
                }
                a+='<tr>'+
                    '<td>'+data.message.tgl+'</td>'+
                    '<td>'+data.message.nota+'</td>'+
                    '<td>'+data.message.nomor+'</td>'+
                    '<td>'+data.message.KDDEB + " - " + data.message.NMDEB +'</td>'+
                    '<td></td>'+
                    '<td style="text-align: right;"><span class="duit">'+data.message.total+'</span></td>'+
                    '<td style="text-align: right;"><span class="duit">'+parseInt(data.message.HARGA)+'</span></td>'+
                    '<td style="text-align: center;"><button class="btn btn-info" id="btn-view"><i class="fa fa-eye" ></i> </button></td>'+
                    '</tr>';
                $.each( data.detail, function( i, val ){
                    b+='<tr>'+
                        '<td>'+val.KDBRG+'</td>'+
                        '<td>'+val.NMBRG+'</td>'+
                        '<td>'+val.jenis+'</td>'+
                        '<td></td>'+
                        '<td>'+val.JUMLAH+'</td>'+
                    '</tr>';
                });
                

            }else{
                swal.fire('error', data.message, 'error');
                a += '<tr class="nodata">'+
                    '<td colspan="99" style="text-align:center;padding:10px">No Data</td>' +
                    '</tr>';
                b += '<tr class="nodata">'+
                    '<td colspan="99" style="text-align:center;padding:10px">No Data</td>' +
                    '</tr>';
            }
            $('#table-list_obat tbody').append(a);
            $('#table-obat_anmaag tbody').append(b);
            duit_number();
            btnclick();
            console.log(data);
        },
        error: function (xhr,status,error) {

        }
    });
}
function btnclick(){
    $('#btn-view').click(function(){
        console.log('asdadad')
        $('#modal-review').modal('show');
    });
}

function input_number(){
    $(".numbers").on("keypress keyup blur",function (event) {    
        $(this).val($(this).val().replace(/[^\d].+/, ""));
         if ((event.which < 48 || event.which > 57)) {
             event.preventDefault();
         }
    });
}
function duit_number(){
    $(".duit").inputmask(
        {   
            'alias': 'decimal', 
            'groupSeparator': ',', 
            'autoGroup': true,
            'removeMaskOnSubmit': true,
            'rightAlign': true,
            'autoUnmask': true,
            // groupSeparator: '.',
        }
    );
}
function loading() {
    KTApp.blockPage({
        overlayColor: '#000000',
        type: 'v2',
        state: 'primary',
        message: 'Processing...'
    });
}
// Class initialization on page load
jQuery(document).ready(function() {
    CekResepInap.init();
});