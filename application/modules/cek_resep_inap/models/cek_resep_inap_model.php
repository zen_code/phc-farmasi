<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class cek_resep_inap_model extends CI_Model{
	public function __construct()
  {
      // Call the CI_Model constructor
      parent::__construct();
     $this->load->database();
  }
 
  public function getData($param){
      $sql = $this->db->query("select top 1000 h.nota,h.nomor,convert(varchar, h.tgl, 103)tgl,sum(hjual*jumlah+jasa) total, h.KDDEB,h.NMDEB, h.ID_TRANS, b.HARGA from IF_HTRANS h 
      inner join if_trans t on h.id_Trans=t.id_trans
      left join RI_TINAP b on h.NOTA = b.NOREG
      where TAGIHINAP=1 and h.active=1 and (h.nota ='".$param."') and b.active=1 and b.resep is not null
      group by h.nota,h.nomor,h.tgl,h.KDDEB,h.ID_TRANS,b.HARGA,h.NMDEB
      order by nota desc
      ");
      // var_dump(count($sql->result()));die();
      return $sql->row();
  }
  public function getDetail($id_trans){
    $sql = $this->db->query("select a.KDBRG, a.JUMLAH, b.NMBRG, b.JENIS, b.VERSTS from if_trans a
    join IF_MBRG_GD b on a.KDBRG = b.KDBRG
    where id_trans = '".$id_trans."'");
    return $sql->result();
  }
}
