<?php
defined('BASEPATH') or exit('No direct script access allowed');

class master_fungsi extends CI_Controller
{
	public $urlws = null;
	public function __construct()
	{
		parent::__construct();
		$this->load->model('master_fungsi_model');
		$this->load->library('session');
		$this->urlws = "http://localhost/phc-ws/api/";
	}
	private function _render($view, $data = array())
	{
		$this->load->view('header', $data);
		// $this->load->view('body_header');
		// $this->load->view('sidebar');
		$this->load->view($view, $data);
		$this->load->view('footer');
	}

	public function index()
	{
		//load library
		$uri = &load_class('URI', 'core');
		redirect(base_url() . 'index.php/master_fungsi/data', $data);
	}

	public function data()
	{
		// var_dump($_SESSION["ses_userid"]);die();
		if ($_SESSION["if_ses_depo"] == null) {
			redirect(base_url() . 'depo');
		} else {
			if (in_array(6, $_SESSION['if_ses_menu'])) {
				$data['parent_active'] = 3;
				$data['child_active'] = 38;
				$this->_render('master_fungsi_view', $data);
			} else {
				redirect(base_url() . 'beranda/data');
			}
		}
	}

	public function get_bahan()
	{
		$query = $this->master_fungsi_model->get_bahan_model();
		echo json_encode($query->result());
		// echo (json_encode($query->result()));
	}
	public function simpanData()
	{
		$data = array(
			"kdbhn" 	=> $this->input->post('kdbhn'),
			"nmbhn"     => $this->input->post('nmbhn'),
			"fungsi"     => $this->input->post('fungsi'),

		);
		$query = $this->master_fungsi_model->get_penyimpanan_data($data);
		echo json_encode($query);
	}

	public function editData()
	{
		$data = array(
			"kdidx"		=> $this->input->post('kdidx'),
			"kdbhn" 	=> $this->input->post('kdbhn'),
			// "nmbhn"     => $this->input->post('nmbhn'),
			"fungsi"     => $this->input->post('fungsi'),

		);
		$query = $this->master_fungsi_model->edit_data($data);
		echo json_encode($query);
	}

	public function detail()
	{
		$query = $this->master_fungsi_model->get_detail();
		$data 			= array();
		foreach ($query->result() as $item) {
			$btn 		= '<button name="btnproses" id="btnproses" onclick="update(' . "'" . $item->KDIDX . "'" . ',' . "'" . $item->KDBHN . "'" . ',' . "'" . $item->FUNGSI . "'" . ')" type="button" class="btn btn-success btn-sm" data-toggle="kt-tooltip" data-placement="top" title="Update"> <i class="fa fa-edit"></i> </button>
						<button name="btnbatal" id="btnbatal" onclick="remove(' . "'" . $item->KDIDX . "'" . ')" type="button" class="btn btn-danger btn-sm" data-toggle="kt-tooltip" data-placement="top" title="Delete"> <i class="fa fa-times"></i> </button>';
			$data[] 	= array(
				"kode"		=> $item->KDBHN,
				"bahan"		=> $item->NMBHN,
				"fungsi"	=> $item->FUNGSI,
				"btn"		=> $btn

			);
		}

		$obj = array("data" => $data);

		echo json_encode($obj);
		//table biasa
		// $query = $this->master_fungsi_model->get_detail();
		// echo json_encode($query->result());
	}

	public function deleteData()
	{
		$data = array(
			"id" 	=> $this->input->post('id'),
		);
		$query = $this->master_fungsi_model->hapus($data);
		echo json_encode($query);
	}
}
