<?php
defined('BASEPATH') or exit('No direct script access allowed');

?>
<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
        <input type="hidden" value="<?php echo base_url(); ?>" id="url">
        <!-- begin:: Subheader -->
        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        Master Fungsi Bahan </h3>
                </div>

            </div>
        </div>
        <div class="kt-container  kt-grid__item kt-grid__item--fluid">
            <div class="row">

                <div class="col-xl-12 order-lg-2 order-xl-1">
                    <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile ">

                        <div class="kt-portlet__body kt-portlet__body--fit">
                            <div class="kt-datatable" id="kt_datatable_latest_orders"></div>

                            <div class="form-group row">
                                <div class="col-12">
                                    <div class="m-portlet">
                                        <div class="card">
                                            <div class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                                                <div class="kt-portlet__head-label">
                                                    <h3 class="kt-portlet__head-title">
                                                        Master Fungsi
                                                    </h3>
                                                </div>
                                            </div>

                                            <div class="card-body">
                                                <div class="m-portlet__body">
                                                    <div class="kt-portlet__body">
                                                        <button class="btn btn-info col-md-2" type="button" id="add_signa" data-toggle="kt-tooltip" data-placement="top"><i class="fa fa-plus"></i> Tambah Data Master Fungsi</button>
                                                    </div>
                                                    <!-- <div class="form-group m-form__group row">
                                                                <label class="col-md-1.5 col-form-label">
                                                                    NAMA BAHAN
                                                                </label>
                                                                <div class="col-md-5">
                                                                    <select class="form-control spcdis selectpicker" id="kdbhn" name="name">
                                                                        <option></option>
                                                                    </select>
                                                                </div>
                                                                <label class="col-md-1.5 col-form-label">
                                                                    Fungsi :
                                                                </label>
                                                                <div class="col-md-4">
                                                                    <input name="" type="text" class="form-control m-input" id="fungsi" placeholder="">
                                                                </div>
                                                            </div> -->

                                                    <!-- <div style="text-align: center; margin-bottom:10px; padding-top: 10px">
                                                                <button class="btn btn-success2" id="simpandata" type="button"><i class="fa fa-check"></i> SIMPAN</button>

                                                            </div> -->

                                                </div>

                                                <div class="card-body" id="">
                                                    <div class="m-portlet__body">
                                                        <table class="table table-striped- table-bordered table-hover table-checkable" id="bahanFungsi">
                                                            <thead>
                                                                <tr style="background:#2860a8;text-align: center;">
                                                                    <th style=" color:white;"> KODE </th>
                                                                    <th style=" color:white;"> BAHAN </th>
                                                                    <th style=" color:white;"> FUNGSI </th>
                                                                    <th style=" color:white;"> Action </th>
                                                                </tr>
                                                            </thead>
                                                            <tbody style="text-align: center"></tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="modal fade" tabindex="-1" role="dialog" id="modal-review">
                                                    <div class="modal-dialog modal-lg" role="document">
                                                        <div class="modal-content ">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title"><span class="tag_type"></span> Master Data Fungsi</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <div class="form-group m-form__group row">
                                                                    <label class="col-md-1.5 col-form-label">
                                                                        NAMA BAHAN
                                                                    </label>
                                                                    <div class="col-md-5">
                                                                        <select class="form-control spcdis selectpicker" id="kdbhn" name="name">
                                                                            <option></option>
                                                                        </select>
                                                                    </div>
                                                                    <label class="col-md-1.5 col-form-label">
                                                                        Fungsi :
                                                                    </label>
                                                                    <div class="col-md-4">
                                                                        <input name="" type="text" class="form-control m-input" id="fungsi" placeholder="">
                                                                    </div>
                                                                </div>

                                                            </div>
                                                            <div class="modal-footer">
                                                                <button class="btn btn-primary" id="simpandata" type="button"><i class="fa fa-check"></i> Tambah</button>
                                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="modal fade" tabindex="-1" role="dialog" id="modal-review2">
                                                <div class="modal-dialog modal-lg" role="document">
                                                    <div class="modal-content ">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title"><span class="tag_type"></span> Edit Master Data Fungsi</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="form-group m-form__group row">
                                                                <label class="col-md-1.5 col-form-label">
                                                                    NAMA BAHAN
                                                                </label>
                                                                <div class="col-md-5">
                                                                    <select class="form-control spcdis selectpicker" id="kdbhn1" name="name">
                                                                        <option></option>
                                                                    </select>
                                                                </div>
                                                                <label class="col-md-1.5 col-form-label">
                                                                    Fungsi :
                                                                </label>
                                                                <div class="col-md-4">
                                                                    <input name="" type="text" class="form-control m-input" id="fungsi1" placeholder="">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <input name="" type="text" class="form-control m-input" id="kdidx" placeholder="">
                                                            </div>

                                                        </div>
                                                        <div class="modal-footer">
                                                            <button class="btn btn-primary" id="editdata" type="button"><i class="fa fa-check"></i> Edit</button>
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- javascript this page -->
<script src="<?php echo base_url() ?>assets/assets_metronic/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
<!-- <script src="<?php //echo base_url()
                    ?>application/modules/User/views/user_view.js" type="text/javascript"></script> -->
<!-- <td colspan="99" style="text-align:center;padding:10px">NO DATA</td> -->
<script src="<?php echo base_url() ?>application/modules/Master_fungsi/views/master_fungsi.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/assets_metronic/vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js" type="text/javascript"></script>