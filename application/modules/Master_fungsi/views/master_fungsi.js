"use strict"

var master_fungsi = (function() {

    var namabahan = "";
    var data_detil = "";

    $('#add_signa').click(function() {
        $('input').val('');
        $('.tag_type').text('Tambah');
        $('#modal-review').modal({ backdrop: 'static', keyboard: false });
        $('#modal-review').modal('show');
    });


    $('#simpandata').click(function() {
        $('#modal-review').modal('hide');

        console.log('sff');
        var data = {
            kdbhn: $('#kdbhn').val(), //nnmbhn
            fungsi: $('#fungsi').val(),
        }
        $.ajax({
            type: "POST",
            url: "master_fungsi/simpanData",
            data: data,
            dataType: 'json',
            async: false,
            success: function(data) {
                $("#bahanFungsi tbody").empty();
                getDetail();
                swal.fire(data.status, data.message, data.status);
                $('#kdbhn').val('').trigger('change');
                $('#fungsi').val('');

            },
            error: function(xhr, status, error) {
                // console.log(xhr);
            }
        });
    });



    function getObat() {
        $.ajax({
            type: "GET",
            url: "master_fungsi/get_bahan",
            data: {},
            dataType: "json",

            success: function(data) {
                namabahan = data;
                jQuery.each(namabahan, function(index, val) {
                    var a = "";
                    a =
                        "<option value='" +
                        val.KDBHN +
                        "' data-obat='" +
                        val.NMBHN +
                        "'><b>" +
                        val.KDBHN +
                        " - " +
                        val.NMBHN +
                        "</b>" +
                        "</option>";
                    $("#kdbhn").append(a);
                });

            },
            error: function(xhr, status, error) {
                console.log(xhr, status, error);
            }
        });
        $("#kdbhn").select2({
            placeholder: "Pilih Bahan",
            width: "100%"
        });
    }

    function getObat2() {
        $.ajax({
            type: "GET",
            url: "master_fungsi/get_bahan",
            data: {},
            dataType: "json",

            success: function(data) {
                namabahan = data;
                jQuery.each(namabahan, function(index, val) {
                    var a = "";
                    a =
                        "<option value='" +
                        val.KDBHN +
                        "' data-obat='" +
                        val.NMBHN +
                        "'><b>" +
                        val.KDBHN +
                        " - " +
                        val.NMBHN +
                        "</b>" +
                        "</option>";
                    $("#kdbhn1").append(a);
                });

            },
            error: function(xhr, status, error) {
                console.log(xhr, status, error);
            }
        });
        $("#kdbhn1").select2({
            placeholder: "Pilih Bahan",
            width: "100%"
        });
    }

    function loading() {
        KTApp.blockPage({
            overlayColor: '#000000',
            type: 'v2',
            state: 'primary',
            message: 'Processing...'
        });
    }
    return {
        init: function() {
            getObat();
            getObat2();
            getDetail();
            loading();
        }
    };

})();

function getDetail() {

    $('#bahanFungsi').DataTable({
        "destroy": true,
        "processing": true,
        "type": "GET",
        "ajax": 'master_fungsi/detail',
        "columns": [
            { "data": "kode" },
            { "data": "bahan" },
            { "data": "fungsi" },
            { "data": "btn" },
        ],

        "createdRow": function(row, data, index) {
            $('td', row).eq(0).addClass('text-center');
            // $('td', row).eq(2).addClass('text-right');
        },
        "drawCallback": function(settings) {
            $('[data-toggle="kt-tooltip"]').tooltip();
        },
        "initComplete": function() {
            KTApp.unblockPage();
        }
    });
    // $.ajax({
    //     type: "POST",
    //     url: "master_fungsi/detail",
    //     // dataType: 'json',
    //     data: {},
    //     success: function(data) {
    //         var datas = jQuery.parseJSON(data);
    //         // data_obat = datas.list_obat;
    //         console.log(datas);
    //         //
    //         // data_detil = data;
    //         jQuery.each(datas, function(index, val) {
    //             var button = '<button name="btnproses id="btnbatal" type="button" class="btn btn-danger btn-sm" data-toggle="kt-tooltip" data-placement="top" title="Choose" onclick="hapus(' +
    //                 "'" + val.KDBHN +
    //                 "'" +
    //                 ')"> <i class="fa fa-times"></i></button>';
    //             var button1 = '<button name="btnproses id="btnproses" type="button" class="btn btn-success btn-sm" data-toggle="kt-tooltip" data-placement="top" title="Choose" onclick="edit(' +
    //                 "'" + val.KDBHN +
    //                 "'" +
    //                 ')"> <i class="far fa-edit" ></i></button>';
    //             var a = "";
    //             a =
    //                 '<tr style="text-align: center;">' +
    //                 '<input type="hidden" id="kdidx_' + index + '" value="' + val.KDIDX + '">' +
    //                 "<td>" + val.KDBHN + "</td>" +
    //                 "<td>" + val.NMBHN + "</td>" +
    //                 "<td>" + val.FUNGSI + "</td>" +
    //                 "<td>" + button1 + button + "</td>" +
    //                 "</tr>";
    //             $("#bahanFungsi tbody").append(a);
    //         });
    //     },
    //     error: function(xhr, status, error) {
    //         // alert("ERROR");
    //     }
    // });

}

function remove(id) {
    // alert(id);

    swal.fire({
        title: '<span style="font-size: 19px;font-weight: 700;">Menghapus Data</span>',
        text: 'Apakah anda yakin menghapus data ini?',
        type: 'question',
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: 'Ya',
        cancelButtonText: 'Tidak',
    }).then(function(result) {
        if (result.value) {
            $.ajax({
                type: "POST",
                url: "master_fungsi/deleteData",
                data: {
                    id: id
                },
                dataType: 'json',
                success: function(data) {
                    KTApp.unblockPage();
                    getDetail();
                },
                error: function(xhr, status, error) {}
            });
        }
    })

    // $.ajax({
    //     type: "POST",
    //     url: "master_fungsi/deleteData",
    //     data: {
    //         id: id
    //     },
    //     dataType: 'json',
    //     success: function(data) {
    //         KTApp.unblockPage();
    //         getDetail();
    //     },
    //     error: function(xhr, status, error) {}
    // });

}

function update(id, bahan, fungsi) {

    $('#kdidx').val(id);
    $('#kdbhn1').val(bahan);
    $('#kdbhn1').trigger('change');
    $('#fungsi1').val(fungsi);
    $('#modal-review2').modal('show');

    $('#editdata').click(function() {


        console.log('sff');
        var data = {
            kdidx: id,
            kdbhn: $('#kdbhn1').val(),
            fungsi: $('#fungsi1').val(),
        }
        $.ajax({
            type: "POST",
            url: "master_fungsi/editData",
            data: data,
            dataType: 'json',
            async: false,
            success: function(data) {
                // $("#bahanFungsi tbody").empty();
                getDetail();
                swal.fire(data.status, data.message, data.status);
                // $('#kdbhn1').val('').trigger('change');
                // $('#fungsi1').val('');
                $('#modal-review2').modal('hide');

            },
            error: function(xhr, status, error) {
                // console.log(xhr);

            }
        });
    });
}


jQuery(document).ready(function() {
    master_fungsi.init();
});