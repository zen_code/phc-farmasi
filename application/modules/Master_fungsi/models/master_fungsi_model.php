<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class master_fungsi_model extends CI_Model
{
  public function __construct()
  {
    // Call the CI_Model constructor
    parent::__construct();
    $this->load->database();
  }

  public function getPrivelege($id)
  {
    $data = [];
    $profile = $this->db->query("select * from APP_PROFIL where ID_PROFIL = " . $id);
    $data['profile'] = $profile->result();

    return $data;
  }
  public function allmenu($app_id)
  {
    $data = [];

    $menu = $this->db->query("select * from APP_MASTER_MENU_APLIKASI WHERE PARENT_MENU is null and APLIKASI =" . $app_id);
    foreach ($menu->result() as $x => $item) {
      $data[$x]['id'] = $item->ID_MENU;
      $data[$x]['name'] = $item->NAMA_MODUL;
      $data[$x]['parent'] = $item->PARENT_MENU;

      $child = $this->db->query("select * from APP_MASTER_MENU_APLIKASI WHERE PARENT_MENU=" . $item->ID_MENU);
      foreach ($child->result() as $y => $itemy) {
        $data[$x]['child'][$y]['id'] = $itemy->ID_MENU;
        $data[$x]['child'][$y]['name'] = $itemy->NAMA_MODUL;
        $data[$x]['child'][$y]['parent'] = $itemy->PARENT_MENU;
      }
    }
    return $data;
  }
  public function getUser($id)
  {
    $sql = $this->db->query("select * from APP_USER_PROFIL WHERE PROFIL = " . $id);
    return $sql->result();
  }
  public function profilemenu($app_id)
  {
    $menu = $this->db->query("select * from APP_PRIVILEGES_PROFIL WHERE PROFIL = " . $app_id);
    return  $menu->result();
  }

  public function get_bahan_model()
  {
    $sql = "select KDBHN,NMBHN from IF_MBAHAN_OBAT";
    return $this->db->query($sql);
    // $query  = $this->db->query("SELECT KDBHN,NMBHN FROM IF_MBAHAN_OBAT");
    // return $query;
  }

  public function get_detail(){
    $sql1 = "select a.KDIDX,b.NMBHN, a.KDBHN, a.FUNGSI from IF_MBAHAN_FUNGSI a join IF_MBAHAN_OBAT b ON a.KDBHN= b.KDBHN";
    return $this->db->query($sql1);
  }
 
  public function get_penyimpanan_data($datax)
  {
    $res = [];
    $sql = "INSERT INTO IF_MBAHAN_FUNGSI(KDBHN,FUNGSI,INPUTBY,INPUTDATE) VALUES ('".$datax['kdbhn']."','".$datax['fungsi']."','" . $_SESSION['ses_username'] . "',GETDATE())";
    $this->db->query($sql);

    $res['status'] = 'sukses';
    $res['message'] = 'Sukses  ';

    return $res;
  }

  public function edit_data($datax)
  {

    $res1 = [];
    $sql2= "UPDATE IF_MBAHAN_FUNGSI SET KDBHN='".$datax['kdbhn']."', FUNGSI='".$datax['fungsi']."' WHERE KDIDX=".$datax['kdidx'];
    // var_dump($sql2);die();
    $this->db->query($sql2);

    $res1['status'] = 'sukses';
    $res1['message'] = 'Update  ';

    return $res1;
  }

  public function hapus($datax)
  { 
    $res2 = [];
    $sql3 ="DELETE FROM IF_MBAHAN_FUNGSI WHERE KDIDX=".$datax['id'];
    $this->db->query($sql3);
    return $res2;
  }
}
