<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
    <input type="hidden" value="<?php echo base_url(); ?>" id="url">
        <!-- begin:: Subheader -->
        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        Master Data Standard </h3>
                </div>
                <!-- kt-subheader__toolbar -->
                
            </div>
        </div>

        <!-- end:: Subheader -->

        <!-- begin:: Content -->
        <div class="kt-container  kt-grid__item kt-grid__item--fluid">

            <!--Begin::Dashboard 1-->

            <!--Begin::Row-->
            <div class="row">
                
                <div class="col-xl-12">
                    <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile ">
                        <div class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Standard
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body kt-portlet__body--fit">
                            <div class="kt-portlet__body">
                                <button class="btn btn-info col-md-2" type="button" id="add_bahan"   data-toggle="kt-tooltip" data-placement="top"><i class="fa fa-plus"></i> Tambah Data</button>
                                
                            </div>
                            <!--begin: Datatable -->
                            <div class="kt-portlet__body">
                                
                                <table class="table table-striped- table-bordered table-hover table-checkable" id="data_standard">
                                    <thead>
                                        <tr style="background:#2860a8;text-align: center;">
                                            <th style="color:white; width:5%">KODE</th>
                                            <th style="color:white">KET. STANDARD</th>
                                            <th style="color:white">KET. STANDARD DOKTER</th>
                                            <th style="color:white; width:15%">VIEW DOKTER</th>
                                            <th style="color:white; width:15%">AKTIF</th>
                                            <th style="color:white; width:10%"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                            
                            <!--end: Datatable -->
                        </div>
                    </div>
                </div>
            </div>

            <!--End::Row-->

            

            <!--End::Dashboard 1-->
        </div>

        <!-- end:: Content -->
    </div>
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="modal-tambah">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content ">
        <div class="modal-header">
            <h5 class="modal-title"><span class="tag_type"></span> Standard</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="form-group col-md-3">
                    <label>KODE</label>
                    <input type="text" class="form-control" aria-describedby="emailHelp" placeholder="Kode" id="kode" disabled>
                </div>
                <div class="form-group col-md-9">
                    <label> KET. STANDARD</label>
                    <input type="text" class="form-control" aria-describedby="emailHelp" placeholder="Standard" id="standard">
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-3">
                    
                </div>
                <div class="form-group col-md-9">
                    <label> KET. STANDARD DOKTER</label>
                    <input type="text" class="form-control" aria-describedby="emailHelp" placeholder="Standard Dokter" id="standard_dokter">
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-3">
                    
                </div>
                <div class="form-group col-md-4">
                    <label class="m-checkbox">
                        <input type="checkbox" aria-describedby="emailHelp" name="view"  id="view">
                        View Dokter
                        <span></span>
                    </label>
                </div>
                <div class="form-group col-md-4">
                    <label class="m-checkbox">
                        <input type="checkbox" aria-describedby="emailHelp" name="aktif" id="aktif">
                        Aktif Standard
                        <span></span>
                    </label>
                    
                </div>
            </div>
            
            
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary" id="action"><span class="tag_type"></span></button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
</div>
<!-- javascript this page -->
<script src="<?php echo base_url()?>assets/assets_metronic/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>application/modules/Master_standard/views/master_standard_view.js" type="text/javascript"></script>
<!-- <script src="<?php echo base_url()?>assets/assets_metronic/js/demo9/pages/dashboard.js" type="text/javascript"></script> -->

<script>

</script>