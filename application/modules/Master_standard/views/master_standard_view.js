"use strict";

// Class definition

var MasterStandard = function() {
    return {
        // Init demos
        init: function() {
            // loading();
            getDataStandard();
        },
    };
}();

function getDataStandard() {
    // console.log('assadad');
    $('#data_standard').DataTable({
        "destroy": true,
        "processing": true,
        "type": "GET",
        "ajax": 'master_standard/dataStandard',
        "columns": [
            { "data": "KODE" },
            { "data": "KETER_STD" },
            { "data": "KETER_STD_DOKTER" },
            { "data": "VIEW_DOKTER" },
            { "data": "AKTIF_STD" },
            { "data": "btn" },
        ],

        "createdRow": function(row, data, index) {
            // $('td', row).eq(0).addClass('text-center');
            // $('td', row).eq(2).addClass('text-right');
        },
        "drawCallback": function(settings) {
            $('[data-toggle="kt-tooltip"]').tooltip();
        },
        "initComplete": function() {
            KTApp.unblockPage();
        }
    });
}

function update(id, type) {
    swal.fire('success', "id=" + id + " type=" + type, 'success');
}

$('#add_bahan').click(function() {
    $('input').val('');
    $('.tag_type').text('Tambah');
    $('#modal-tambah').modal({ backdrop: 'static', keyboard: false });
    $('#modal-tambah').modal('show');
});

$('#action').click(function() {
    $('#modal-tambah').modal('hide');
    loading();
    $.ajax({
        type: "POST",
        url: "master_standard/actionStandard",
        data: {
            kode: $('#kode').val(),
            standard: $('#standard').val(),
            standard_dokter: $('#standard_dokter').val(),
            // view: $("input[name=view]:checked").val(),
            view: $("#view").is(':checked')?1:0,
            // aktif: $("input[name=aktif]:checked").val()
            aktif: $("#aktif").is(':checked')?1:0
            
        },
        dataType: 'json',
        async: false,
        success: function(data) {
            console.log(data);
            KTApp.unblockPage();
            swal.fire(data.data.status, data.data.message, data.data.status);
            location.reload();
            getDataStandard();
        },
        error: function(xhr, status, error) {
            console.log(status, error);
        }
    });

});

$('#data_standard').on('click', 'tr #btnproses', function() {
    var baris = $(this).parents('tr')[0];
    var table = $('#data_standard').DataTable();
    var datas = table.row(baris).data();

    var checked = '<i class="fa fa-check"></i>';
    // var unchecked = '<i class="fa fa-times"></i>';
    // console.log(datas);

    $('.tag_type').text('Edit');
    $('#kode').val(datas['KODE']);
    $('#standard').val(datas['KETER_STD']);
    $('#standard_dokter').val(datas['KETER_STD_DOKTER']);

    if(datas['VIEW_DOKTER'] == checked){
        $('#view').prop('checked', true);
    }else{
        $('#view').prop('checked', false);
    }

    if(datas['AKTIF_STD'] == checked){
        $('#aktif').prop('checked', true);
    }else{
        $('#aktif').prop('checked', false);
    }

    // $('#aktif').val(datas['AKTIF_STD']?1:0);
    $('#modal-tambah').modal('show');
});

$('#data_standard').on('click', 'tr #btnbatal', function() {
    var baris = $(this).parents('tr')[0];
    var table = $('#data_standard').DataTable();
    var datas = table.row(baris).data();
    swal.fire({
        title: '<span style="font-size: 19px;font-weight: 700;">Menghapus Standard</span>',
        text: 'Apakah anda yakin menghapus standard ini?',
        type: 'question',
        showCancelButton: true,
        confirmButtonText: 'Ya',
        cancelButtonText: 'Tidak',
    }).then(function(result) {
        if (result.value) {
            deleteStandard(datas['KODE']);
            // console.log(datas['KDBHN']);
        }
    })
})

function deleteStandard(kode) {

    $.ajax({
        type: "POST",
        url: "master_standard/deleteStandard",
        data: {
            kode: kode
        },
        dataType: 'json',
        success: function(data) {
            KTApp.unblockPage();
            location.reload();
            getDataBahan();
        },
        error: function(xhr, status, error) {}
    });
}

function loading() {
    KTApp.blockPage({
        overlayColor: '#000000',
        type: 'v2',
        state: 'primary',
        message: 'Processing...'
    });
}

// Class initialization on page load
jQuery(document).ready(function() {
    MasterStandard.init();
});