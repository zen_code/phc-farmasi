<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class cetak_etiket_model extends CI_Model
{
  public function __construct()
  {
    // Call the CI_Model constructor
    parent::__construct();
    $this->load->database();
  }

  /*public function getdatalogin($user,$pass)
  {
    $this->load->database();
    $sql = "exec inap_sp_login_bacapriv '$user','$pass'";
    return $this->db->query($sql);  
  }*/
  public function get_bio_model($kode)
  {
    $query  = $this->db->query("select CONVERT(VARCHAR,M.TGL_LHR,103) TGL_LAHIR,t.NO,t.ID,h.nota,h.nomor,h.norm rm,h.IDXPX,h.NMPX nama,h.ALMTPX,h.KOTAPX,CONVERT(VARCHAR ,h.INPUTDATE,103) INPUTDATE,h.KDDEB,h.nmdeb,h.kddin,h.nmdin, h.KDKLIN, h.NMKLIN, h.INPUTBY, h.TGL , h.JAM, h.KDDOK, h.NMDOK, t.ITER, h.STPAS, h.KDOT, r.NMJNSRESEP,
    t.KDBRG,b.nmbrg, b.SATUAN, t.jumlah,t.HJUAL,t.jumlah*t.hjual as 'sub_total',s.URAIAN, t.DISC, t.JASA from if_htrans h inner join if_trans t on h.id_Trans=t.ID_TRANS
    inner join IF_MBRG_GD b on b.kdbrg=t.kdbrg INNER JOIN RIRJ_MASTERPX M ON M.NO_PESERTA=h.NOPESERTA
    left join IF_MSIGNA s on s.kdsigna=t.SIGNA
    join MJENISRESEP r on h.KDOT=r.KDJNSRESEP
    where NOTA = '$kode'");
    return $query;
  }
}
