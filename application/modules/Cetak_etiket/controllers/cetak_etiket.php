<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Cetak_etiket extends CI_Controller
{
	public $urlws = null;
	public function __construct()
	{
		parent::__construct();
		$this->load->model('cetak_etiket_model');
		$this->load->library('session');
		$this->urlws = $this->globals->ws_phc();
	}
	private function _render($view, $data = array())
	{
		$this->load->view('header', $data);
		$this->load->view($view, $data);
		$this->load->view('footer');
	}

	public function index()
	{
		//load library
		// var_dump($this->session->all_userdata());die();
		$uri = &load_class('URI', 'core');
		redirect(base_url() . 'index.php/cetak_etiket/data', $data);
	}

	public function data()
	{
		// var_dump($_SESSION["ses_userid"]);die();
		if ($_SESSION["if_ses_depo"] == null) {
			redirect(base_url() . 'depo');
		} else {
			if (in_array(6, $_SESSION['if_ses_menu'])) {
				$data['parent_active'] = 6;
				$data['child_active'] = 39;
				$this->_render('cetak_etiket_view', $data);
			} else {
				redirect(base_url() . 'beranda/data');
			}
		}
	}

	// public function get_data(){
	// 	$data['users']=array(
	// 		array('firstname'=>'Agung','lastname'=>'Setiawan','email'=>'ag@setiawan.com'),
	// 		array('firstname'=>'Hauril','lastname'=>'Maulida Nisfar','email'=>'hm@setiawan.com'),
	// 		array('firstname'=>'Akhtar','lastname'=>'Setiawan','email'=>'akh@setiawan.com'),
	// 		array('firstname'=>'Gitarja','lastname'=>'Setiawan','email'=>'git@setiawan.com')
	// 	);
	// 	echo json_encode($data);
	// }

	public function cetakPdf()
	{
		$kode = $this->input->get('kode');
		$query = $this->cetak_etiket_model->get_bio_model($kode);
		$data['query'] = $query->result();
		// $data = array(
		// 	"kode" 	=> $this->input->post('kode'),

		// );
		// $query = $this->cetak_etiket_model->get_bio_model($data);
		// var_dump($query->result());die();
		// echo json_encode($query->result());

		// var_dump($query->result());die();
		// $kode['aaa'] = $this->input->get('kode');
		// $kode['abc']=$this->input->get('tgl');

		// $this->load->view('../views/cetak/cetak_etiket_pdf', $kode);
		// $query = $this->cetak_etiket_model->get_bio_model($kode);
		// echo json_encode($query->result());

		// $data['users']=array(
		// 	array('firstname'=>'Agung','lastname'=>'Setiawan','email'=>'ag@setiawan.com'),
		// 	array('firstname'=>'Hauril','lastname'=>'Maulida Nisfar','email'=>'hm@setiawan.com'),
		// 	array('firstname'=>'Akhtar','lastname'=>'Setiawan','email'=>'akh@setiawan.com'),
		// 	array('firstname'=>'Gitarja','lastname'=>'Setiawan','email'=>'git@setiawan.com')
		// );

		$this->load->library('pdf2');
		$customPaper = array(0, 0, 381.89, 595.28);
		$this->pdf2->setPaper($customPaper, 'landscape');
		// $customPaper = array(0,0,297.6378,150.236);

		// $this->pdf2->setPaper('A4', 'portrait');
		// $customPaper = array(0, 0, 381.89, 595.28);
		// $this->pdf2->setPaper($customPaper, 'landscape');
		// $this->pdf2->load_view('laporan_pdf', $data);
		$this->pdf2->load_view('../views/cetak/cetak_etiket_pdf',  $data);
		// echo json_encode($data);
	}
	public function get_bio_ctrl()
	{
		$kode = $this->input->get('kode');
		$query = $this->cetak_etiket_model->get_bio_model($kode);
		echo json_encode($query->result());
	}
}
