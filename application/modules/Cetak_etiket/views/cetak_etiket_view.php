<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<style>
    .is_head {
        background: #2860a8;
        color: white;
    }
</style>

<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
        <input type="hidden" value="<?php echo base_url(); ?>" id="url">
        <!-- begin:: Subheader -->
        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title"> Cetak Ulang E-tiket </h3>
                </div>
                <!-- kt-subheader__toolbar -->

            </div>
        </div>

        <!-- end:: Subheader -->

        <!-- begin:: Content -->
        <div class="kt-container  kt-grid__item kt-grid__item--fluid">

            <!--Begin::Dashboard 1-->

            <!--Begin::Row-->
            <div class="row">

                <div class="col-xl-12">
                    <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile ">
                        <!-- <div class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                Farmasi Klinis
                                </h3>
                            </div>
                        </div> -->
                        <div class="kt-portlet__body kt-portlet__body--fit">
                            <!--begin: Datatable -->
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="kt-portlet">
                                        <div class="kt-portlet__body">
                                            <div class="row">
                                                <div class="form-group col-md-4">
                                                    <label>No. Resep</label>
                                                    <input type="number" class="form-control dis " value="" id="no_reseph">
                                                </div>
                                                <div class="form-group col-md-8">
                                                    <label>Nota / Registrasi</label>
                                                    <input type="number" class="form-control" value="" id="registrasih">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="kt-portlet">
                                        <div class="kt-portlet__head">
                                            <div class="kt-portlet__head-label">
                                                <h3 class="kt-portlet__head-title">
                                                    Biodata Pasien
                                                </h3>
                                            </div>
                                        </div>
                                        <div class="kt-portlet__body">
                                            <div class="row">
                                                <div class="form-group col-md-4">
                                                    <label>No. Resep</label>
                                                    <input type="number" class="form-control dis " value="" id="no_resep">
                                                </div>
                                                <div class="form-group col-md-8">
                                                    <label>Nota / Registrasi</label>
                                                    <input type="number" class="form-control dis" value="" id="registrasi">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-md-12">
                                                    <label>Nama</label>
                                                    <input type="text" class="form-control dis" value="" id="name">
                                                    <input type="hidden" id="tgl_lahir">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-md-12">
                                                    <label>Alamat</label>
                                                    <input type="text" class="form-control dis" value="" id="alamat">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-md-12">
                                                    <label>Kota</label>
                                                    <input type="text" class="form-control dis" value="" id="kota">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="kt-portlet">
                                        <div class="kt-portlet__head">
                                            <div class="kt-portlet__head-label">
                                                <h3 class="kt-portlet__head-title">
                                                    Debitur dan Kamar Pasien
                                                </h3>
                                            </div>
                                        </div>
                                        <div class="kt-portlet__body">
                                            <div class="row">
                                                <div class="form-group col-md-6">
                                                    <label>Jenis PX.</label>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <input type="text" class="form-control dis" value="" id="jenispx">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-md-6">
                                                    <label>RM / Index</label>
                                                    <div class="row">
                                                        <div class="col-md-8">
                                                            <input type="text" class="form-control dis" value="" id="rm">
                                                        </div>
                                                        <div class="col-md-4">
                                                            <input type="text" class="form-control dis" value=""
                                                                id="indexs">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group col-md-6">
                                                    <label>Tgl. Resep</label>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <input type="text" class="form-control dis" value="" id="tgl_resep">
                                                        </div>
                                                    </div>
                                                </div>
                                              
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-md-12">
                                                    <label>Debitur</label>
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <input type="text" class="form-control dis" value=""
                                                                id="kddeb">
                                                        </div>
                                                        <div class="col-md-9">
                                                            <input type="text" class="form-control dis" value=""
                                                                id="nmdeb">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-md-12">
                                                    <label>Dinas</label>
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <input type="text" class="form-control dis" value="" id="kddin">
                                                        </div>
                                                        <div class="col-md-9">
                                                            <input type="text" class="form-control dis" value="" id="nmdin">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="kt-portlet">
                                        <div class="kt-portlet__head">
                                            <div class="kt-portlet__head-label">
                                                <h3 class="kt-portlet__head-title">
                                                    Info Klinik / RP
                                                </h3>
                                            </div>
                                        </div>
                                        <div class="kt-portlet__body">
                                            <div class="row">
                                                <div class="form-group col-md-6">
                                                    <label>Klinik / RP</label>
                                                    <div class="row">
                                                        <div class="col-md-2">
                                                            <input type="text" class="form-control dis" value="" id="kdklin">
                                                        </div>
                                                        <div class="col-md-10">
                                                            <input type="text" class="form-control dis" value="" id="nmklin">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label>User / Tgl. Entri</label>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <input type="text" class="form-control dis" value="" id="user">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-md-6">
                                                    <label>Dokter</label>
                                                    <div class="row">
                                                        <div class="col-md-2">
                                                            <input type="text" class="form-control dis" value=""
                                                                id="kddok">
                                                        </div>
                                                        <div class="col-md-10">
                                                            <input type="text" class="form-control dis" value=""
                                                                id="nmdok">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label>Jenis Resep</label>
                                                    <div class="row">
                                                        <div class="col-md-2">
                                                            <input type="text" class="form-control dis" value=""
                                                                id="kdjenis">
                                                        </div>
                                                        <div class="col-md-8">
                                                            <input type="text" class="form-control dis" value=""
                                                                id="nmjenis">
                                                        </div>
                                                        <div class="col-md-2">
                                                            <input type="text" class="form-control dis" value=""
                                                                id="iter">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="kt-portlet__body">
                            <div style="overflow-x:scroll;">
                                    <table id="table-list_obat" class="table table-striped- table-hover table-checkable" style="display: block;">
                                        <thead>
                                            <tr style="background: #2860a8; color:white; text-align:center">
                                                <th>NO.</th>
                                                <th>ID</th>
                                                <th>KODE</th>
                                                <th>NAMA BARANG</th>
                                                <th>SATUAN</th>
                                                <th>SIGNA</th>
                                                <th>HARGA</th>
                                                <th>QTY</th>
                                                <th>TOTAL</th>
                                            </tr>
                                        </thead>
                                        <tbody style="text-align:center">
                                            <tr>
                                                <td colspan="99" style="text-align:center">NO DATA</td>
                                            </tr>
                                        </tbody>
                                    </table>

                                    <script>
                                        $('#table-list_obat thead th').css('min-width', '160px');
                                        $('#table-list_obat thead th').eq(0).css('min-width', '50px');
                                    </script>
                                </div>
                                <br>
                                <div class="kt-form kt-form">
                                    <div class="form-group row">
                                        <label for="example-text-input" class="col-1 col-form-label">Sub Total</label>
                                        <div class="col-2">
                                            <input class="form-control dis" type="number" value="" id="sub_total">
                                        </div>

                                        <label for="example-text-input" class="col-1 col-form-label">Disc.</label>
                                        <div class="col-2">
                                            <input class="form-control dis" type="number" value="" id="disc">
                                        </div>

                                        <label for="example-text-input" class="col-1 col-form-label">Jasa Resep</label>
                                        <div class="col-2">
                                            <input class="form-control dis" type="number" value="" id="jasa_r">
                                        </div>

                                        <label for="example-text-input" class="col-1 col-form-label">Total</label>
                                        <div class="col-2">
                                            <input class="form-control dis" type="number" value="" id="total">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <button class="btn btn-info col-md-1" type="button" id="print" data-toggle="kt-tooltip" data-placement="top" data-original-title="" title="" style="float:right">
                                            <i class="fa fa-print"></i> Etiket
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!--End::Row-->



            <!--End::Dashboard 1-->
        </div>

        <!-- end:: Content -->
    </div>
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="modal-review">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content ">
            <div class="modal-header">
                <h5 class="modal-title"><span class="tag_type"></span> Pasien</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table id="table-pasien" class="table table-striped- table-hover table-checkable">
                    <thead>
                        <tr style="background: #2860a8; color:white;text-align:center">
                            <th style=" color:white;">NOREG</th>
                            <th style=" color:white;">RM</th>
                            <th style=" color:white;">TGL_MRS</th>
                            <th style=" color:white;">NAMA</th>
                            <th style=" color:white;">KAMAR</th>
                            <th style=" color:white;">RUANGAN</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th style=" color:white;">NOREG</th>
                            <th style=" color:white;">RM</th>
                            <th style=" color:white;">TGL_MRS</th>
                            <th style=" color:white;">NAMA</th>
                            <th style=" color:white;">KAMAR</th>
                            <th style=" color:white;">RUANGAN</th>
                            <th></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- javascript this page -->
<script src="<?php echo base_url()?>assets/assets_metronic/vendors/custom/datatables/datatables.bundle.js"
    type="text/javascript"></script>
<script
    src="<?php echo base_url()?>assets/assets_metronic/vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"
    type="text/javascript"></script>
<script src="<?php echo base_url()?>application/modules/Cetak_etiket/views/cetak_etiket_view.js"
    type="text/javascript"></script>
<!-- <script src="<?php echo base_url()?>assets/assets_metronic/js/demo9/pages/dashboard.js" type="text/javascript"></script> -->

<script>

</script>