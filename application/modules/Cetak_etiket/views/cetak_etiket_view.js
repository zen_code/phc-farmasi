"use strict";

// Class definition

var CetakEtiket = (function() {
    return {
        // Init demos
        init: function() {
            console.log("asdasdad");
        }
    };
})();

//Bismillah


$("#registrasih").on("keypress", function(e) {
    if (e.keyCode == 13) {
        var kode = $("#registrasih").val();
        var sub_total = 0;
        var disc = 0;
        var jasa_resep = 0;
        var total = 0;
        KTApp.blockPage({
            overlayColor: "#000000",
            type: "v2",
            state: "primary",
            message: "Processing..."
        });
        $.ajax({
            type: "GET",
            url: "cetak_etiket/get_bio_ctrl",
            data: {
                kode: kode
            },
            dataType: "json",
            success: function(data) {

                $("#table-list_obat tbody").html("");
                // var sub_total = 0;
                jQuery.each(data, function(index, val) {
                    var a = "";
                    $("#no_resep").val(val.nomor);
                    $("#registrasi").val(val.nota);
                    $(".dis").attr("disabled", true);
                    $("#no_reseph").val(val.nomor);
                    $("#name").val(val.nama);
                    $("#alamat").val(val.ALMTPX);
                    $("#kota").val(val.KOTAPX);
                    if (val.STPAS == 1) {
                        $("#jenispx").val("TUNAI");
                    } else if (val.STPAS == 2) {
                        $("#jenispx").val("KREDIT");
                    }
                    $("#rm").val(val.rm);
                    $("#indexs").val(val.IDXPX);
                    $("#tgl_resep").val(val.INPUTDATE);
                    $("#kddeb").val(val.KDDEB);
                    $("#nmdeb").val(val.nmdeb);
                    $("#kddin").val(val.kddin);
                    $("#nmdin").val(val.nmdin);
                    $("#kdklin").val(val.KDKLIN);
                    $("#nmklin").val(val.NMKLIN);
                    $("#user").val(val.INPUTBY);
                    $("#tgl_entri").val(val.TGL);
                    $("#jam_entri").val(val.TGL);
                    $("#kddok").val(val.KDDOK);
                    $("#nmdok").val(val.NMDOK);
                    $("#kdjenis").val(val.KDOT);
                    $("#nmjenis").val(val.NMJNSRESEP);
                    $("#iter").val(val.ITER);
                    $("#tgl_lahir").val(val.TGL_LAHIR);
                    a =
                        "<tr>" +
                        "<td>" + val.NO + "</td>" +
                        "<td>" + val.ID + "</td>" +
                        "<td>" + val.KDBRG + "</td>" +
                        "<td>" + val.nmbrg + "</td>" +
                        "<td>" + val.SATUAN + "</td>" +
                        "<td>" + val.URAIAN + "</td>" +
                        "<td>" + val.HJUAL + "</td>" +
                        "<td>" + val.jumlah + "</td>" +
                        "<td>" + val.sub_total + "</td>" +
                        "</tr>";
                    $("#table-list_obat tbody").append(a);
                    // sub_total = (sub_total + val.sub_total);
                    sub_total = sub_total + val.sub_total;
                    disc = disc + val.DISC;
                    jasa_resep = jasa_resep + val.JASA;
                    console.log(sub_total);
                });
                $("#sub_total").val(sub_total);
                $("#disc").val(disc);
                $("#jasa_r").val(jasa_resep);
                total = sub_total - disc + jasa_resep;
                $('#total').val(total);
                KTApp.unblockPage();
            },
            error: function(xhr, status, error) {
                console.log(xhr, status, error);
            }
        });
    }
});
//

function loading() {
    KTApp.blockPage({
        overlayColor: "#000000",
        type: "v2",
        state: "primary",
        message: "Processing..."
    });
}
$("#no_reseph").on("keyup, keydown", function(evt) {
    if (evt.key === "Enter") {
        console.log($("#no_reseph").val());
    }
});


$('#print').click(function() {
    // var kode = $("#registrasih").val();
    // // var newWindow = window.open("", "_blank");
    // $.ajax({
    //     type: "POST",
    //     url: "cetak_etiket/cetakPdf",
    //     data: {
    //         kode: kode,
    //     },
    //     success: function(data) {
    //         // alert(JSON.stringify(data));
    //         // window.open("../views/cetak/cetak_etiket_pdf", 'kode');
    //         swal.fire("printing", 'check');
    //         // window.open("cetak_etiket/cetakPdf", '_blank');
    //         // window.open("cetak_etiket/cetakPdf", '_blank');
    //     },
    //     error: function(xhr, status, error) {
    //         // console.log(xhr, status, error); 
    //     }
    // });

    // if ($('#total').val() == '') {
    //     swal.fire('Perhatian', 'Lengkapi', 'warning');
    // } else {
    //     window.open("cetak_etiket/cetakPdf?kode=" + $("#registrasih").val() + "", '_blank');
    // }
    // var tgl = "";
    // $("#table-list_obat tbody tr:eq(0) td:eq(2)").text();
    // $("#table-list_obat tbody tr").each(function(i, e) {
    //     var nmbrg = $(this).find('td').eq(3).text();
    // });
    var kode = "";
    if ($('#total').val() == '') {
        swal.fire('Perhatian', 'Lengkapi', 'warning');
    } else {
        window.open("cetak_etiket/cetakPdf?kode=" + $("#registrasih").val() + "", '_blank');
        swal.fire('printing', 'check', 'success');
    }

});
// Class initialization on page load
jQuery(document).ready(function() {
    CetakEtiket.init();
});